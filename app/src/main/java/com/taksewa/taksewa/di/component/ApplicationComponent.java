package com.taksewa.taksewa.di.component;

import android.app.Application;
import android.content.Context;

import com.taksewa.taksewa.MyApp;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.di.ApplicationContext;
import com.taksewa.taksewa.di.module.ApplicationModule;
import com.taksewa.taksewa.service.LiveMessagingService;
import com.taksewa.taksewa.service.SyncService;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.disposables.CompositeDisposable;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyApp app);

    void inject(SyncService service);

    void inject(LiveMessagingService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}