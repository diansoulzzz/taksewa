package com.taksewa.taksewa.di.component;


import com.taksewa.taksewa.di.PerService;
import com.taksewa.taksewa.di.module.ServiceModule;
import com.taksewa.taksewa.service.LiveMessagingService;
import com.taksewa.taksewa.service.SyncService;

import dagger.Component;

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

    void inject(LiveMessagingService service);

}
