package com.taksewa.taksewa.di.module;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taksewa.taksewa.di.ActivityContext;
import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpView;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangPresenter;
import com.taksewa.taksewa.ui.a_entry_barang_harga.EntryBarangHargaMvpPresenter;
import com.taksewa.taksewa.ui.a_entry_barang_harga.EntryBarangHargaMvpView;
import com.taksewa.taksewa.ui.a_entry_barang_harga.EntryBarangHargaPresenter;
import com.taksewa.taksewa.ui.a_image_picker.ImagePickerMvpPresenter;
import com.taksewa.taksewa.ui.a_image_picker.ImagePickerMvpView;
import com.taksewa.taksewa.ui.a_image_picker.ImagePickerPresenter;
import com.taksewa.taksewa.ui.a_login.LoginMvpPresenter;
import com.taksewa.taksewa.ui.a_login.LoginMvpView;
import com.taksewa.taksewa.ui.a_login.LoginPresenter;
import com.taksewa.taksewa.ui.a_main.MainMvpPresenter;
import com.taksewa.taksewa.ui.a_main.MainMvpView;
import com.taksewa.taksewa.ui.a_main.MainPresenter;
import com.taksewa.taksewa.ui.a_maps_result_barang.MapsResultBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_maps_result_barang.MapsResultBarangMvpView;
import com.taksewa.taksewa.ui.a_maps_result_barang.MapsResultBarangPresenter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpView;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewPresenter;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailMvpPresenter;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailMvpView;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailPresenter;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistMvpPresenter;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistMvpView;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistPresenter;
import com.taksewa.taksewa.ui.a_message.MessageMvpPresenter;
import com.taksewa.taksewa.ui.a_message.MessageMvpView;
import com.taksewa.taksewa.ui.a_message.MessagePresenter;
import com.taksewa.taksewa.ui.a_message_list.MessageListMvpPresenter;
import com.taksewa.taksewa.ui.a_message_list.MessageListMvpView;
import com.taksewa.taksewa.ui.a_message_list.MessageListPresenter;
import com.taksewa.taksewa.ui.a_register.RegisterMvpPresenter;
import com.taksewa.taksewa.ui.a_register.RegisterMvpView;
import com.taksewa.taksewa.ui.a_register.RegisterPresenter;
import com.taksewa.taksewa.ui.a_report_barang.ReportBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_report_barang.ReportBarangMvpView;
import com.taksewa.taksewa.ui.a_report_barang.ReportBarangPresenter;
import com.taksewa.taksewa.ui.a_saldo_history.SaldoHistoryMvpPresenter;
import com.taksewa.taksewa.ui.a_saldo_history.SaldoHistoryMvpView;
import com.taksewa.taksewa.ui.a_saldo_history.SaldoHistoryPresenter;
import com.taksewa.taksewa.ui.a_saldo_withdraw.SaldoWithdrawMvpPresenter;
import com.taksewa.taksewa.ui.a_saldo_withdraw.SaldoWithdrawPresenter;
import com.taksewa.taksewa.ui.a_saldo_withdraw.SaldoWithdrawMvpView;
import com.taksewa.taksewa.ui.a_search.SearchMvpPresenter;
import com.taksewa.taksewa.ui.a_search.SearchMvpView;
import com.taksewa.taksewa.ui.a_search.SearchPresenter;
import com.taksewa.taksewa.ui.a_search_result.SearchResultMvpPresenter;
import com.taksewa.taksewa.ui.a_search_result.SearchResultMvpView;
import com.taksewa.taksewa.ui.a_search_result.SearchResultPresenter;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangMvpView;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangPresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpPresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpView;
import com.taksewa.taksewa.ui.a_splash.SplashPresenter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpPresenter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpView;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailPresenter;
import com.taksewa.taksewa.ui.a_transaksi_vendor_detail.TransaksiVendorDetailMvpPresenter;
import com.taksewa.taksewa.ui.a_transaksi_vendor_detail.TransaksiVendorDetailMvpView;
import com.taksewa.taksewa.ui.a_transaksi_vendor_detail.TransaksiVendorDetailPresenter;
import com.taksewa.taksewa.ui.a_vendor_barang_list.VendorBarangListMvpPresenter;
import com.taksewa.taksewa.ui.a_vendor_barang_list.VendorBarangListMvpView;
import com.taksewa.taksewa.ui.a_vendor_barang_list.VendorBarangListPresenter;
import com.taksewa.taksewa.ui.a_verify.VerifyMvpPresenter;
import com.taksewa.taksewa.ui.a_verify.VerifyMvpView;
import com.taksewa.taksewa.ui.a_verify.VerifyPresenter;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankMvpPresenter;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankMvpView;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankPresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpView;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangPresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpView;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriPresenter;
import com.taksewa.taksewa.ui.a_view_barang_review.ViewBarangReviewMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_review.ViewBarangReviewMvpView;
import com.taksewa.taksewa.ui.a_view_barang_review.ViewBarangReviewPresenter;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriMvpView;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriPresenter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpPresenter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpView;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanPresenter;
import com.taksewa.taksewa.ui.f_akun.AkunMvpPresenter;
import com.taksewa.taksewa.ui.f_akun.AkunMvpView;
import com.taksewa.taksewa.ui.f_akun.AkunPresenter;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberMvpPresenter;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberMvpView;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberPresenter;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorMvpPresenter;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorMvpView;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorPresenter;
import com.taksewa.taksewa.ui.f_home.HomeMvpPresenter;
import com.taksewa.taksewa.ui.f_home.HomeMvpView;
import com.taksewa.taksewa.ui.f_home.HomePresenter;
import com.taksewa.taksewa.ui.f_kategori.KategoriMvpPresenter;
import com.taksewa.taksewa.ui.f_kategori.KategoriMvpView;
import com.taksewa.taksewa.ui.f_kategori.KategoriPresenter;
import com.taksewa.taksewa.ui.f_report_barang_selesai.ReportBarangSelesaiMvpPresenter;
import com.taksewa.taksewa.ui.f_report_barang_selesai.ReportBarangSelesaiMvpView;
import com.taksewa.taksewa.ui.f_report_barang_selesai.ReportBarangSelesaiPresenter;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangMvpPresenter;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangMvpView;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangPresenter;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorMvpPresenter;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorMvpView;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorPresenter;
import com.taksewa.taksewa.utils.rx.AppSchedulerProvider;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager(AppCompatActivity activity) {
        return new GridLayoutManager(activity, 1);
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegisterMvpPresenter<RegisterMvpView> provideRegisterPresenter(
            RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SearchMvpPresenter<SearchMvpView> provideSearchPresenter(
            SearchPresenter<SearchMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SearchResultMvpPresenter<SearchResultMvpView> provideSearchResultPresenter(
            SearchResultPresenter<SearchResultMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SewaBarangMvpPresenter<SewaBarangMvpView> provideSewaBarangPresenter(
            SewaBarangPresenter<SewaBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ViewBarangMvpPresenter<ViewBarangMvpView> provideViewBarangPresenter(
            ViewBarangPresenter<ViewBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ViewKecamatanMvpPresenter<ViewKecamatanMvpView> provideViewKecamatanPresenter(
            ViewKecamatanPresenter<ViewKecamatanMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ViewBarangKategoriMvpPresenter<ViewBarangKategoriMvpView> provideViewBarangKategoriPresenter(
            ViewBarangKategoriPresenter<ViewBarangKategoriMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ViewBarangSubKategoriMvpPresenter<ViewBarangSubKategoriMvpView> provideViewBarangSubKategoriPresenter(
            ViewBarangSubKategoriPresenter<ViewBarangSubKategoriMvpView> presenter) {
        return presenter;
    }

    @Provides
    AkunMvpPresenter<AkunMvpView> provideAkunPresenter(
            AkunPresenter<AkunMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AkunMemberMvpPresenter<AkunMemberMvpView> provideAkunMemberPresenter(
            AkunMemberPresenter<AkunMemberMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AkunVendorMvpPresenter<AkunVendorMvpView> provideAkunVendorPresenter(
            AkunVendorPresenter<AkunVendorMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    HomeMvpPresenter<HomeMvpView> provideHomePresenter(
            HomePresenter<HomeMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    KategoriMvpPresenter<KategoriMvpView> provideKategoriPresenter(
            KategoriPresenter<KategoriMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    EntryBarangMvpPresenter<EntryBarangMvpView> provideEntryBarangPresenter(
            EntryBarangPresenter<EntryBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    EntryBarangHargaMvpPresenter<EntryBarangHargaMvpView> provideEntryBarangHargaPresenter(
            EntryBarangHargaPresenter<EntryBarangHargaMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MessageMvpPresenter<MessageMvpView> provideMessagePresenter(
            MessagePresenter<MessageMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MemberWishlistMvpPresenter<MemberWishlistMvpView> provideMemberWishlistPresenter(
            MemberWishlistPresenter<MemberWishlistMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MessageListMvpPresenter<MessageListMvpView> provideMessageListPresenter(
            MessageListPresenter<MessageListMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ImagePickerMvpPresenter<ImagePickerMvpView> provideImagePickerPresenter(
            ImagePickerPresenter<ImagePickerMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SearchResultVendorMvpPresenter<SearchResultVendorMvpView> provideSearchResultVendorPresenter(
            SearchResultVendorPresenter<SearchResultVendorMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SearchResultBarangMvpPresenter<SearchResultBarangMvpView> provideSearchResultBarangPresenter(
            SearchResultBarangPresenter<SearchResultBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    TransaksiMemberDetailMvpPresenter<TransaksiMemberDetailMvpView> provideTransaksiMemberDetailPresenter(
            TransaksiMemberDetailPresenter<TransaksiMemberDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    TransaksiVendorDetailMvpPresenter<TransaksiVendorDetailMvpView> provideTransaksiVendorDetailPresenter(
            TransaksiVendorDetailPresenter<TransaksiVendorDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    VerifyMvpPresenter<VerifyMvpView> provideVerifyPresenter(
            VerifyPresenter<VerifyMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MapsResultBarangMvpPresenter<MapsResultBarangMvpView> provideMapsResultBarangPresenter(
            MapsResultBarangPresenter<MapsResultBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MemberLastSeenBarangMvpPresenter<MemberLastSeenBarangMvpView> provideMemberLastSeenBarangPresenter(
            MemberLastSeenBarangPresenter<MemberLastSeenBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    VendorBarangListMvpPresenter<VendorBarangListMvpView> provideVendorBarangListPresenter(
            VendorBarangListPresenter<VendorBarangListMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MemberReviewMvpPresenter<MemberReviewMvpView> provideMemberReviewPresenter(
            MemberReviewPresenter<MemberReviewMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MemberReviewDetailMvpPresenter<MemberReviewDetailMvpView> provideMemberReviewDetailPresenter(
            MemberReviewDetailPresenter<MemberReviewDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ViewBarangReviewMvpPresenter<ViewBarangReviewMvpView> provideViewBarangReviewPresenter(
            ViewBarangReviewPresenter<ViewBarangReviewMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SaldoWithdrawMvpPresenter<SaldoWithdrawMvpView> provideSaldoWithdrawPresenter(
            SaldoWithdrawPresenter<SaldoWithdrawMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SaldoHistoryMvpPresenter<SaldoHistoryMvpView> provideSaldoHistoryPresenter(
            SaldoHistoryPresenter<SaldoHistoryMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ViewBankMvpPresenter<ViewBankMvpView> provideViewBankPresenter(
            ViewBankPresenter<ViewBankMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ReportBarangMvpPresenter<ReportBarangMvpView> provideReportBarangPresenter(
            ReportBarangPresenter<ReportBarangMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ReportBarangSelesaiMvpPresenter<ReportBarangSelesaiMvpView> provideReportBarangSelesaiPresenter(
            ReportBarangSelesaiPresenter<ReportBarangSelesaiMvpView> presenter) {
        return presenter;
    }
}
