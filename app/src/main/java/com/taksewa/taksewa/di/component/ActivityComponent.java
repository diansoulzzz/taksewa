package com.taksewa.taksewa.di.component;

import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.di.module.ActivityModule;
import com.taksewa.taksewa.ui.a_date_range_picker.DateRangePickerActivity;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangActivity;
import com.taksewa.taksewa.ui.a_entry_barang_harga.EntryBarangHargaActivity;
import com.taksewa.taksewa.ui.a_image_picker.ImagePickerActivity;
import com.taksewa.taksewa.ui.a_image_preview.ImagePreviewActivity;
import com.taksewa.taksewa.ui.a_login.LoginActivity;
import com.taksewa.taksewa.ui.a_main.MainActivity;
import com.taksewa.taksewa.ui.a_maps_result_barang.MapsResultBarangActivity;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangActivity;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewActivity;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailActivity;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistActivity;
import com.taksewa.taksewa.ui.a_message.MessageActivity;
import com.taksewa.taksewa.ui.a_message_list.MessageListActivity;
import com.taksewa.taksewa.ui.a_register.RegisterActivity;
import com.taksewa.taksewa.ui.a_report_barang.ReportBarangActivity;
import com.taksewa.taksewa.ui.a_saldo_history.SaldoHistoryActivity;
import com.taksewa.taksewa.ui.a_saldo_withdraw.SaldoWithdrawActivity;
import com.taksewa.taksewa.ui.a_search.SearchActivity;
import com.taksewa.taksewa.ui.a_search_result.SearchResultActivity;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangActivity;
import com.taksewa.taksewa.ui.a_splash.SplashActivity;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailActivity;
import com.taksewa.taksewa.ui.a_transaksi_vendor_detail.TransaksiVendorDetailActivity;
import com.taksewa.taksewa.ui.a_vendor_barang_list.VendorBarangListActivity;
import com.taksewa.taksewa.ui.a_verify.camera.VerifyCardTakeCameraActivity;
import com.taksewa.taksewa.ui.a_verify.VerifyActivity;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriActivity;
import com.taksewa.taksewa.ui.a_view_barang_review.ViewBarangReviewActivity;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriActivity;
import com.taksewa.taksewa.ui.a_view_harga_sewa.ViewHargaSewaActivity;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanActivity;
import com.taksewa.taksewa.ui.f_akun.AkunFragment;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberFragment;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorFragment;
import com.taksewa.taksewa.ui.f_home.HomeFragment;
import com.taksewa.taksewa.ui.f_inbox.InboxFragment;
import com.taksewa.taksewa.ui.f_kategori.KategoriFragment;
import com.taksewa.taksewa.ui.f_report_barang_selesai.ReportBarangSelesaiFragment;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangFragment;
import com.taksewa.taksewa.ui.f_search_result_barang.options.FilterBarangActivity;
import com.taksewa.taksewa.ui.f_search_result_barang.options.SortBarangActivity;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorFragment;
import com.taksewa.taksewa.ui.f_transaksi.TransaksiFragment;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberFragment;
import com.taksewa.taksewa.ui.f_transaksi_vendor.TransaksiVendorFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(SplashActivity activity);

    void inject(ImagePreviewActivity activity);

    void inject(ImagePickerActivity activity);

    void inject(MainActivity activity);

    void inject(LoginActivity activity);

    void inject(RegisterActivity activity);

    void inject(SearchActivity activity);

    void inject(SearchResultActivity activity);

    void inject(SewaBarangActivity activity);

    void inject(ViewBarangActivity activity);

    void inject(ViewBarangKategoriActivity activity);

    void inject(ViewBarangSubKategoriActivity activity);

    void inject(ViewKecamatanActivity activity);

    void inject(ViewHargaSewaActivity activity);

    void inject(EntryBarangActivity activity);

    void inject(EntryBarangHargaActivity activity);

    void inject(DateRangePickerActivity activity);

    void inject(SortBarangActivity activity);

    void inject(MessageActivity activity);

    void inject(MessageListActivity activity);

    void inject(FilterBarangActivity activity);

    void inject(TransaksiMemberDetailActivity activity);

    void inject(TransaksiVendorDetailActivity activity);

    void inject(MemberWishlistActivity activity);

    void inject(MemberLastSeenBarangActivity activity);

    void inject(MapsResultBarangActivity activity);

    void inject(VerifyCardTakeCameraActivity activity);

    void inject(VendorBarangListActivity activity);

    void inject(VerifyActivity activity);

    void inject(MemberReviewActivity activity);

    void inject(MemberReviewDetailActivity activity);

    void inject(ViewBarangReviewActivity activity);

    void inject(ViewBankActivity activity);

    void inject(SaldoWithdrawActivity activity);

    void inject(SaldoHistoryActivity activity);

    void inject(ReportBarangActivity activity);

    void inject(AkunFragment fragment);

    void inject(AkunMemberFragment fragment);

    void inject(AkunVendorFragment fragment);

    void inject(HomeFragment fragment);

    void inject(InboxFragment fragment);

    void inject(KategoriFragment fragment);

    void inject(TransaksiFragment fragment);

    void inject(TransaksiMemberFragment fragment);

    void inject(TransaksiVendorFragment fragment);

    void inject(SearchResultVendorFragment fragment);

    void inject(SearchResultBarangFragment fragment);

    void inject(ReportBarangSelesaiFragment fragment);
}
