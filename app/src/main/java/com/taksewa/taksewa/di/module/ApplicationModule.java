package com.taksewa.taksewa.di.module;

import android.app.Application;
import android.content.Context;


import com.taksewa.taksewa.BuildConfig;
import com.taksewa.taksewa.data.AppDataManager;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.ApiHeader;
import com.taksewa.taksewa.data.network.ApiHelper;
import com.taksewa.taksewa.data.network.AppApiHelper;
import com.taksewa.taksewa.data.prefs.AppPreferencesHelper;
import com.taksewa.taksewa.data.prefs.PreferencesHelper;
import com.taksewa.taksewa.di.AcceptHeader;
import com.taksewa.taksewa.di.ApiInfo;
import com.taksewa.taksewa.di.ApplicationContext;
import com.taksewa.taksewa.di.DatabaseInfo;
import com.taksewa.taksewa.di.PreferenceInfo;
import com.taksewa.taksewa.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @AcceptHeader
    String provideAcceptHeader() {
        return BuildConfig.ACCEPT_HEADER;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@AcceptHeader String accept,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(accept, preferencesHelper.getAccessToken());
    }
}
