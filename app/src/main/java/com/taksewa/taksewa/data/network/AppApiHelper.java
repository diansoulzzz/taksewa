package com.taksewa.taksewa.data.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.taksewa.taksewa.data.network.request.AuthRequest;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.request.HSewaRequest;
import com.taksewa.taksewa.data.network.request.HomeRequest;
import com.taksewa.taksewa.data.network.request.MessageRequest;
import com.taksewa.taksewa.data.network.request.ReviewBarangRequest;
import com.taksewa.taksewa.data.network.request.SearchRequest;
import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.AuthResponse;
import com.taksewa.taksewa.data.network.response.BankResponse;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.BarangSubKategoriResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppApiHelper implements ApiHelper {

    //    private final String TAG = getClass().getSimpleName();
    private final String TAG = "OkHttp";

    private ApiHeader mApiHeader;
    private Gson gson = new GsonBuilder().create();

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    private JSONObject toJSONObject(Object request) {
        Log.d(TAG, "GSON -> " + gson.toJson(request));
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(gson.toJson(request));
            Log.d(TAG, "JSONOBJECT -> " + jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public Single<AuthResponse.AccessToken> postServerRegister(AuthRequest.ServerRegister request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_REGISTER)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(AuthResponse.AccessToken.class);
    }

    @Override
    public Single<AuthResponse.RegisterOTP> postServerRegisterOTP(AuthRequest.ServerRegisterOTP request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_REGISTER_OTP)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(AuthResponse.RegisterOTP.class);
    }

    @Override
    public Single<AuthResponse.AccessToken> postAuthToken(AuthRequest.FirebaseAuth request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_FIREBASE_AUTH)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(AuthResponse.AccessToken.class);
    }

    @Override
    public Single<UserResponse.IData> postFirebaseFcmToken(AuthRequest.FirebaseFcmToken request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_FIREBASE_FCM_TOKEN)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(UserResponse.IData.class);
    }

    @Override
    public Single<UserResponse.IData> postDaftarVendor() {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_DAFTAR_VENDOR)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(UserResponse.IData.class);
    }

    @Override
    public Single<UserResponse.IData> getProfileInfo() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_PROFILE_INFO)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(UserResponse.IData.class);
    }

    @Override
    public Single<HomeResponse.IData> getHomeData(HomeRequest.NotFromID request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_HOME_DATA)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(HomeResponse.IData.class);
    }

    @Override
    public Single<BarangKategoriResponse.IList> getBarangKategoriList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BARANG_KATEGORI_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(BarangKategoriResponse.IList.class);
    }

    @Override
    public Single<BarangSubKategoriResponse.IList> getBarangSubKategoriList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BARANG_SUB_KATEGORI_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(BarangSubKategoriResponse.IList.class);
    }

    @Override
    public Single<BarangResponse.IData> getBarangDataFromId(BarangRequest.FromID request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BARANG_DATA_FROMID)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(BarangResponse.IData.class);
    }

    @Override
    public Single<BarangResponse.IWishListed> getBarangIsWishlistFromId(BarangRequest.FromID request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BARANG_IS_WISHLIST_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(BarangResponse.IWishListed.class);
    }

    @Override
    public Single<BarangResponse.IWishListed> postToogleBarangWishlistFromId(BarangRequest.FromID request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_TOOGLE_BARANG_WISHLIST_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(BarangResponse.IWishListed.class);
    }

    @Override
    public Single<HSewaResponse.IList> getVendorTransactionList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_VENDOR_TRANSACTION_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(HSewaResponse.IList.class);
    }

    @Override
    public Single<HSewaResponse.IList> getMemberTransactionList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_MEMBER_TRANSACTION_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(HSewaResponse.IList.class);
    }

    @Override
    public Single<BarangResponse.IList> getBarangWishlistList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BARANG_WISHLIST_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(BarangResponse.IList.class);
    }

    @Override
    public Single<BarangResponse.IList> getVendorBarangList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_VENDOR_BARANG_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(BarangResponse.IList.class);
    }

    @Override
    public Single<BarangResponse.ILastSeenList> postToogleLastSeenBarangFromId(BarangRequest.FromID request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_TOOGLE_BARANG_LAST_SEEN_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(BarangResponse.ILastSeenList.class);
    }

    @Override
    public Single<BarangResponse.IList> getBarangLastSeendBarangList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BARANG_LAST_SEEN_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(BarangResponse.IList.class);
    }

    @Override
    public Single<HSewaResponse.IData> getMemberTransactionDataFromId(HSewaRequest.FromID request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_MEMBER_TRANSACTION_DATA_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(HSewaResponse.IData.class);
    }

    @Override
    public Single<HSewaResponse.IData> getVendorTransactionDataFromId(HSewaRequest.FromID request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_VENDOR_TRANSACTION_DATA_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(HSewaResponse.IData.class);
    }

    @Override
    public Single<HSewaResponse.IData> postVendorTransactionDataChange(HSewaRequest.ChangeData request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_VENDOR_TRANSACTION_CHANGE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(HSewaResponse.IData.class);
    }

    @Override
    public Single<HSewaResponse.IData> postMemberTransactionDataChange(HSewaRequest.ChangeData request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_MEMBER_TRANSACTION_CHANGE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(HSewaResponse.IData.class);
    }


    @Override
    public Single<MessageResponse.IRoom> getMessageRoom() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_MESSAGE_ROOM)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(MessageResponse.IRoom.class);
    }

    @Override
    public Single<MessageResponse.IData> postNewMessage(MessageRequest.EntryData request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_NEW_MESSAGE_ENTRY)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(MessageResponse.IData.class);
    }

    @Override
    public Single<MessageResponse.IList> get10Message(MessageRequest.Take10 request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_10_MESSAGE_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(MessageResponse.IList.class);
    }

    @Override
    public Single<SearchResponse.IBarangData> getSearchBarang(SearchRequest.ISendBarang request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_SEARCH_BARANG)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addQueryParameter("query", toJSONObject(request).toString())
                .build()
                .getObjectSingle(SearchResponse.IBarangData.class);
    }

    @Override
    public Single<SearchResponse.IVendorData> getSearchVendor(SearchRequest.ISendVendor request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_SEARCH_VENDOR)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addQueryParameter("query", toJSONObject(request).toString())
                .build()
                .getObjectSingle(SearchResponse.IVendorData.class);
    }

    @Override
    public Single<UserResponse.IData> postUserVerification(UserRequest.EntryData request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_PROFILE_VERIFICATION)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(UserResponse.IData.class);
    }

    @Override
    public Single<AreaResponse.IKecamatanList> getKecamatanList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_AREA_KECAMATAN_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(AreaResponse.IKecamatanList.class);
    }

    @Override
    public Single<BarangResponse.IList> postVendorBarangDelete(BarangRequest.FromID request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_VENDOR_BARANG_DELETE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(BarangResponse.IList.class);
    }

    @Override
    public Single<HSewaResponse.IList> getMemberReviewList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_MEMBER_REVIEW_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(HSewaResponse.IList.class);
    }

    @Override
    public Single<HSewaResponse.IData> getMemberReviewDataFromId(HSewaRequest.FromID request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_MEMBER_REVIEW_DATA_FROMID)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(request)
                .build()
                .getObjectSingle(HSewaResponse.IData.class);
    }

    @Override
    public Single<HSewaResponse.IList> postMemberReviewEntry(ReviewBarangRequest.EntryData request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_MEMBER_REVIEW_ENTRY)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(HSewaResponse.IList.class);
    }

    @Override
    public Single<BankResponse.IList> getBankList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_BANK_LIST)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectSingle(BankResponse.IList.class);
    }

    @Override
    public Single<HSewaResponse.IList> getVendorReportTransactionListFromFilter(HSewaRequest.FromFilter request) {
        return Rx2AndroidNetworking.get(ApiEndPoint.GET_VENDOR_REPORT_TRANSACTION_LIST_FROMFILTER)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter("filter", toJSONObject(request).toString())
                .build()
                .getObjectSingle(HSewaResponse.IList.class);
    }

    @Override
    public Single<HomeResponse.GData> postSaldoWithdraw(UserRequest.SaldoWithdraw request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_SALDO_WITHDRAW)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(HomeResponse.GData.class);
    }

    @Override
    public Single<BarangResponse.IData> postVendorBarangEntry(BarangRequest.EntryData request) {
//        Log.d(TAG, "GSON -> " + gson.toJson(request));
////        JsonObject jsonObject = new Gson().fromJson(gson.toJson(request), JsonObject.class);
//        JSONObject jsonObject = null;
//        try {
//            jsonObject = new JSONObject(gson.toJson(request));
//            Log.d(TAG, "JSONOBJECT -> " + jsonObject.toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JSONObject jsonObject = new Gson().fromJson(gson.toJson(request), JSONObject.class);
//        JSONObject jsonObject1 = jsonObject.getAsJsonObject();
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_VENDOR_BARANG_ENTRY)
                .addHeaders(mApiHeader.getProtectedApiHeader())
//                .addBodyParameter("form",gson.toJson(request))
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(BarangResponse.IData.class);
    }

    @Override
    public Single<BarangResponse.IData> postVendorBarangEntryObject(BarangRequest.EntryDataObject request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.POST_VENDOR_BARANG_ENTRY)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(toJSONObject(request))
                .build()
                .getObjectSingle(BarangResponse.IData.class);
    }

//    @Override
//    public Single<AuthResponse.AccessToken> postSocialLogin(AuthRequest.SocialLogin request) {
//        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SOCIAL_LOGIN)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .addBodyParameter(request)
//                .build()
//                .getObjectSingle(AuthResponse.AccessToken.class);
//    }
//
//    @Override
//    public Single<AuthResponse.AccessToken> postServerLogin(AuthRequest.ServerLogin request) {
//        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .addBodyParameter(request)
//                .build()
//                .getObjectSingle(AuthResponse.AccessToken.class);
//    }
//
//    @Override
//    public Single<AuthResponse.ServerRegisterOTP> postRegisterServer(AuthRequest.ServerRegister request) {
//        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .addBodyParameter(request)
//                .build()
//                .getObjectSingle(AuthResponse.ServerRegisterOTP.class);
//    }
//
//    @Override
//    public Single<AuthResponse.TokenResponse> postAuthSocial(AuthRequest.SocialLogin request) {
//        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .addBodyParameter(request)
//                .build()
//                .getObjectSingle(AuthResponse.TokenResponse.class);
//    }
//
//    @Override
//    public Single<AuthResponse.TokenResponse> postLoginServer(AuthRequest.ServerLogin request) {
//        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .addBodyParameter(request)
//                .build()
//                .getObjectSingle(AuthResponse.TokenResponse.class);
//    }
//
//    @Override
//    public Single<AuthResponse.ServerRegisterOTP> postRegisterOTP(AuthRequest.ServerRegisterOTP request) {
//        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
//                .addHeaders(mApiHeader.getPublicApiHeader())
//                .addBodyParameter(request)
//                .build()
//                .getObjectSingle(AuthResponse.ServerRegisterOTP.class);
//    }

}

