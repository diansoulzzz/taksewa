package com.taksewa.taksewa.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Message;

public class MessageRequest {

    private MessageRequest() {
        // This class is not publicly instantiable
    }

    public static class Take10 {
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("users_id_to")
        private int userIdTo;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserIdTo() {
            return userIdTo;
        }

        public void setUserIdTo(int userIdTo) {
            this.userIdTo = userIdTo;
        }
    }

    public static class EntryData {
        @Expose
        @SerializedName("message")
        private Message data = new Message();

        public Message getMessage() {
            return data;
        }

        public void setMessage(Message message) {
            this.data = message;
        }
    }

}
