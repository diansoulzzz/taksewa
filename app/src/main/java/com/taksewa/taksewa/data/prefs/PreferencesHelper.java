package com.taksewa.taksewa.data.prefs;

import com.taksewa.taksewa.data.DataManager;

public interface PreferencesHelper {

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    Long getCurrentUserId();

    void setCurrentUserId(Long userId);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    String getAccessToken();

    void setAccessToken(String accessToken);

    Boolean getCurrentUserisVendor();

    void setCurrentUserIsVendor(Boolean isVendor);

    Boolean getCurrentUserisVerified();

    void setCurrentUserIsVerified(Boolean isVerified);


//    String getFirebaseToken();
//
//    void setFirebaseToken(String firebaseToken);

}
