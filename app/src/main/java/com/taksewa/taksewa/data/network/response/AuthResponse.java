package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Status;
import com.taksewa.taksewa.model.User;

public class AuthResponse {

    private AuthResponse() {

    }

    public static class RegisterOTP {
        @Expose
        @SerializedName("email")
        private String email;

        public RegisterOTP(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public class AccessToken {
        @SerializedName("data")
        @Expose
        private User data;
        @SerializedName("status")
        @Expose
        private Status status;

        public User getData() {
            return data;
        }

        public void setData(User data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public class AccessToken2 {
        @SerializedName("data")
        @Expose
        private User user;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("access_token")
        @Expose
        private String accessToken;

        public AccessToken2(User user, String message, String status, String accessToken) {
            this.user = user;
            this.message = message;
            this.status = status;
            this.accessToken = accessToken;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }
    }


}
