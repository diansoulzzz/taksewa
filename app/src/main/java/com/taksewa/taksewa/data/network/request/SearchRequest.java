package com.taksewa.taksewa.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.model.Message;

public class SearchRequest {

    private SearchRequest() {
        // This class is not publicly instantiable
    }

    public static class ISendBarang {
        public static int SORT_BY_RELEVANCE = 0;
        public static int SORT_BY_REVIEW = 1;
        public static int SORT_BY_NEW_ITEM = 2;
        public static int SORT_BY_LOW_PRICE = 3;
        public static int SORT_BY_HIGH_PRICE = 4;

        @SerializedName("query")
        @Expose
        private String query;
        @SerializedName("filter")
        @Expose
        private Filter filter;
        @SerializedName("sort")
        @Expose
        private int sort;
        @SerializedName("id")
        @Expose
        private int id;

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        public Filter getFilter() {
            return filter;
        }

        public void setFilter(Filter filter) {
            this.filter = filter;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class ISendVendor {

        @SerializedName("query")
        @Expose
        private String query;

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }
    }

}
