package com.taksewa.taksewa.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.Message;

public class HSewaRequest {

    private HSewaRequest() {

    }

    public static class FromFilter {
        @SerializedName("tipe")
        @Expose
        private String tipe;

        public FromFilter(String tipe) {
            this.tipe = tipe;
        }

        public String getTipe() {
            return tipe;
        }

        public void setTipe(String tipe) {
            this.tipe = tipe;
        }
    }


    public static class FromID {
        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

    public static class ChangeData {
        @Expose
        @SerializedName("h_sewa")
        private HSewa data = new HSewa();

        public HSewa getData() {
            return data;
        }

        public void setData(HSewa data) {
            this.data = data;
        }
    }

}
