package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.Kecamatan;
import com.taksewa.taksewa.model.Status;

import java.util.ArrayList;
import java.util.List;

public class AreaResponse {
    public AreaResponse() {
    }

    public static class IKecamatanList {
        @SerializedName("data")
        @Expose
        private List<Kecamatan> data = new ArrayList<Kecamatan>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<Kecamatan> getData() {
            return data;
        }

        public void setData(List<Kecamatan> data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
