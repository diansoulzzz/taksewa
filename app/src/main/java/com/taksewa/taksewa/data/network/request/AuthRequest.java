package com.taksewa.taksewa.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthRequest {

    private AuthRequest() {

    }
    public static class FirebaseFcmToken {
        @Expose
        @SerializedName("firebase_fcm_token")
        private String firebaseFcmToken;

        public FirebaseFcmToken(String firebaseFcmToken) {
            this.firebaseFcmToken = firebaseFcmToken;
        }

        public String getFirebaseFcmToken() {
            return firebaseFcmToken;
        }

        public void setFirebaseFcmToken(String firebaseFcmToken) {
            this.firebaseFcmToken = firebaseFcmToken;
        }
    }

    public static class FirebaseAuth {
        @Expose
        @SerializedName("firebase_token")
        private String firebaseToken;

        public FirebaseAuth(String firebaseToken) {
            this.firebaseToken = firebaseToken;
        }

        public String getFirebaseToken() {
            return firebaseToken;
        }

        public void setFirebaseToken(String firebaseToken) {
            this.firebaseToken = firebaseToken;
        }
    }

    public static class ServerRegisterOTP {
        @Expose
        @SerializedName("email")
        private String email;

        public ServerRegisterOTP(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class ServerRegister {
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("nama")
        private String nama;
        @Expose
        @SerializedName("no_telp")
        private String noTelp;
        @Expose
        @SerializedName("password")
        private String password;

        public ServerRegister(String email, String nama, String noTelp, String password) {
            this.email = email;
            this.nama = nama;
            this.noTelp = noTelp;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getNoTelp() {
            return noTelp;
        }

        public void setNoTelp(String noTelp) {
            this.noTelp = noTelp;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

}
