package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.Status;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class HSewaResponse {

    private HSewaResponse() {

    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private HSewa data = new HSewa();

        @SerializedName("status")
        @Expose
        private Status status;

        public HSewa getData() {
            return data;
        }

        public void setData(HSewa data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private ArrayList<HSewa> data = new ArrayList<>();
        @SerializedName("status")
        @Expose
        private Status status;

        public ArrayList<HSewa> getData() {
            return data;
        }

        public void setData(ArrayList<HSewa> data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
