package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.MessageRoom;
import com.taksewa.taksewa.model.Status;

import java.util.ArrayList;
import java.util.List;

public class MessageResponse {

    private MessageResponse() {

    }

    public static class IRoom {
        @SerializedName("data")
        @Expose
        private List<MessageRoom> data = new ArrayList<>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<MessageRoom> getData() {
            return data;
        }

        public void setData(List<MessageRoom> data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private List<Message> data = new ArrayList<>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<Message> getData() {
            return data;
        }

        public void setData(List<Message> data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private Message data = new Message();

        @SerializedName("status")
        @Expose
        private Status status;

        public Message getData() {
            return data;
        }

        public void setData(Message data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }
}
