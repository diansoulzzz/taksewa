package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Carousel;
import com.taksewa.taksewa.model.Status;
import com.taksewa.taksewa.model.User;

import java.util.ArrayList;
import java.util.List;

public class HomeResponse {

    private HomeResponse() {

    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private GData data = new GData();
        @SerializedName("status")
        @Expose
        private Status status;

        public GData getData() {
            return data;
        }

        public void setData(GData data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class GData {
        @SerializedName("barang")
        @Expose
        private List<Barang> barang = new ArrayList<Barang>();
        @SerializedName("carousel")
        @Expose
        private List<Carousel> carousel = new ArrayList<Carousel>();
        @SerializedName("kategori")
        @Expose
        private List<BarangKategori> kategori = new ArrayList<BarangKategori>();
        @SerializedName("subkategori")
        @Expose
        private List<BarangSubKategori> subkategori = new ArrayList<BarangSubKategori>();
        @SerializedName("user")
        @Expose
        private User user = new User();

        public List<Barang> getBarangList() {
            return barang;
        }

        public void setBarangList(List<Barang> barang) {
            this.barang = barang;
        }

        public List<BarangKategori> getBarangKategoriList() {
            return kategori;
        }

        public void setBarangKategoriList(List<BarangKategori> kategori) {
            this.kategori = kategori;
        }

        public List<BarangSubKategori> getBarangSubKategoriList() {
            return subkategori;
        }

        public void setBarangSubKategoriList(List<BarangSubKategori> subkategori) {
            this.subkategori = subkategori;
        }

        public List<Carousel> getCarouselList() {
            return carousel;
        }

        public void setCarouselList(List<Carousel> carousel) {
            this.carousel = carousel;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }
    }
}
