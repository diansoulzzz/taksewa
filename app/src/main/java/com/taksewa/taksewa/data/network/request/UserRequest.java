package com.taksewa.taksewa.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.User;

public class UserRequest {
    public UserRequest() {
    }

    public static class EntryData {
        @Expose
        @SerializedName("user")
        private User data = new User();

        public User getUser() {
            return data;
        }

        public void setUser(User message) {
            this.data = message;
        }
    }

    public static class SaldoWithdraw {
        @Expose
        @SerializedName("bank_id")
        private int bankId;
        @Expose
        @SerializedName("no_rekening")
        private String noRekening;
        @Expose
        @SerializedName("nominal")
        private int nominal;

        public SaldoWithdraw(int bankId, String noRekening, int nominal) {
            this.bankId = bankId;
            this.noRekening = noRekening;
            this.nominal = nominal;
        }

        public int getBankId() {
            return bankId;
        }

        public void setBankId(int bankId) {
            this.bankId = bankId;
        }

        public String getNoRekening() {
            return noRekening;
        }

        public void setNoRekening(String noRekening) {
            this.noRekening = noRekening;
        }

        public int getNominal() {
            return nominal;
        }

        public void setNominal(int nominal) {
            this.nominal = nominal;
        }
    }
}
