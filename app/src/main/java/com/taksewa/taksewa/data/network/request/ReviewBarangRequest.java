package com.taksewa.taksewa.data.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.ReviewBarang;

public class ReviewBarangRequest {

    public static class EntryData {
        @Expose
        @SerializedName("review_barang")
        private ReviewBarang data = new ReviewBarang();

        public ReviewBarang getReviewBarang() {
            return data;
        }

        public void setReviewBarang(ReviewBarang reviewBarang) {
            this.data = reviewBarang;
        }
    }

}
