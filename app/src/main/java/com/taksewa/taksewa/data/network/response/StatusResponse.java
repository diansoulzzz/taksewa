package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.Status;

import java.util.ArrayList;
import java.util.List;

public class StatusResponse {
    public StatusResponse() {
    }

    public static class IData {
        @SerializedName("status")
        @Expose
        private Status status;

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

//        @SerializedName("data")
//        @Expose
//        private List<Barang> data = new ArrayList<Barang>();

//        public List<Barang> getData() {
//            return data;
//        }

        //        public void setData(List<Barang> data) {
//            this.data = data;
//        }

    }
}
