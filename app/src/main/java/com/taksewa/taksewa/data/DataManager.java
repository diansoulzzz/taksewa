package com.taksewa.taksewa.data;

import com.taksewa.taksewa.data.network.ApiHelper;
import com.taksewa.taksewa.data.prefs.PreferencesHelper;
import com.taksewa.taksewa.model.User;

import io.reactivex.Observable;

public interface DataManager extends PreferencesHelper, ApiHelper {

    void updateApiHeader(Long userId, String accessToken);

    void setUserAsLoggedOut();

    void updateUserInfo(
            User userModel,
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath,
            Boolean isVendor,
            Boolean isVerified
    );

    enum LoggedInMode {
        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

}
