package com.taksewa.taksewa.data.network;


import com.taksewa.taksewa.data.network.request.AuthRequest;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.request.HSewaRequest;
import com.taksewa.taksewa.data.network.request.HomeRequest;
import com.taksewa.taksewa.data.network.request.MessageRequest;
import com.taksewa.taksewa.data.network.request.ReviewBarangRequest;
import com.taksewa.taksewa.data.network.request.SearchRequest;
import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.AuthResponse;
import com.taksewa.taksewa.data.network.response.BankResponse;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.BarangSubKategoriResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;

import io.reactivex.Single;

public interface ApiHelper {

    ApiHeader getApiHeader();

    Single<AuthResponse.AccessToken> postServerRegister(AuthRequest.ServerRegister request);

    Single<AuthResponse.RegisterOTP> postServerRegisterOTP(AuthRequest.ServerRegisterOTP request);

    Single<AuthResponse.AccessToken> postAuthToken(AuthRequest.FirebaseAuth request);

    Single<UserResponse.IData> postFirebaseFcmToken(AuthRequest.FirebaseFcmToken request);

    Single<BarangResponse.IData> postVendorBarangEntry(BarangRequest.EntryData request);

    Single<BarangResponse.IData> postVendorBarangEntryObject(BarangRequest.EntryDataObject request);

    Single<UserResponse.IData> postDaftarVendor();

    Single<UserResponse.IData> getProfileInfo();

    Single<HomeResponse.IData> getHomeData(HomeRequest.NotFromID request);

    Single<BarangKategoriResponse.IList> getBarangKategoriList();

    Single<BarangSubKategoriResponse.IList> getBarangSubKategoriList();

    Single<BarangResponse.IData> getBarangDataFromId(BarangRequest.FromID request);

    Single<BarangResponse.IWishListed> getBarangIsWishlistFromId(BarangRequest.FromID request);

    Single<BarangResponse.IWishListed> postToogleBarangWishlistFromId(BarangRequest.FromID request);

    Single<BarangResponse.IList> getBarangWishlistList();

    Single<BarangResponse.IList> getVendorBarangList();

    Single<BarangResponse.ILastSeenList> postToogleLastSeenBarangFromId(BarangRequest.FromID request);

    Single<BarangResponse.IList> getBarangLastSeendBarangList();

    Single<HSewaResponse.IList> getVendorTransactionList();

    Single<HSewaResponse.IList> getMemberTransactionList();

    Single<HSewaResponse.IData> getMemberTransactionDataFromId(HSewaRequest.FromID request);

    Single<HSewaResponse.IData> getVendorTransactionDataFromId(HSewaRequest.FromID request);

    Single<HSewaResponse.IData> postVendorTransactionDataChange(HSewaRequest.ChangeData request);

    Single<HSewaResponse.IData> postMemberTransactionDataChange(HSewaRequest.ChangeData request);

    Single<MessageResponse.IRoom> getMessageRoom();

    Single<MessageResponse.IData> postNewMessage(MessageRequest.EntryData request);

    Single<MessageResponse.IList> get10Message(MessageRequest.Take10 request);

    Single<SearchResponse.IBarangData> getSearchBarang(SearchRequest.ISendBarang request);

    Single<SearchResponse.IVendorData> getSearchVendor(SearchRequest.ISendVendor request);

    Single<UserResponse.IData> postUserVerification(UserRequest.EntryData request);

    Single<AreaResponse.IKecamatanList> getKecamatanList();

    Single<BarangResponse.IList> postVendorBarangDelete(BarangRequest.FromID request);

    Single<HSewaResponse.IList> getMemberReviewList();

    Single<HSewaResponse.IData> getMemberReviewDataFromId(HSewaRequest.FromID request);

    Single<HSewaResponse.IList> postMemberReviewEntry(ReviewBarangRequest.EntryData request);

    Single<BankResponse.IList> getBankList();

    Single<HSewaResponse.IList> getVendorReportTransactionListFromFilter(HSewaRequest.FromFilter request);

    Single<HomeResponse.GData> postSaldoWithdraw(UserRequest.SaldoWithdraw request);
}
