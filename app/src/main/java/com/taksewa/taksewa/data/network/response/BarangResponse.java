package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.LastSeenBarang;
import com.taksewa.taksewa.model.Status;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.model.Wishlist;

import java.util.ArrayList;
import java.util.List;

public class BarangResponse {

    private BarangResponse() {

    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private Barang data = new Barang();

        @SerializedName("status")
        @Expose
        private Status status;

        public Barang getData() {
            return data;
        }

        public void setData(Barang data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class IWishListed {
        @SerializedName("data")
        @Expose
        private Wishlist data = new Wishlist();

        @SerializedName("status")
        @Expose
        private Status status;

        public Wishlist getData() {
            return data;
        }

        public void setData(Wishlist data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }

    public static class ILastSeenList {
        @SerializedName("data")
        @Expose
        private LastSeenBarang data = new LastSeenBarang();

        @SerializedName("status")
        @Expose
        private Status status;

        public LastSeenBarang getData() {
            return data;
        }

        public void setData(LastSeenBarang data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private List<Barang> data = new ArrayList<Barang>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<Barang> getData() {
            return data;
        }

        public void setData(List<Barang> data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
