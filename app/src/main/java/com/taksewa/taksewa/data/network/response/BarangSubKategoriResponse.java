package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Status;

import java.util.ArrayList;
import java.util.List;

public class BarangSubKategoriResponse {

    private BarangSubKategoriResponse() {

    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private BarangSubKategori data;
        @SerializedName("status")
        @Expose
        private Status status;

        public BarangSubKategori getData() {
            return data;
        }

        public void setData(BarangSubKategori data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private List<BarangSubKategori> data = new ArrayList<>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<BarangSubKategori> getData() {
            return data;
        }

        public void setData(List<BarangSubKategori> data) {
            this.data = data;
        }


        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
