package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Bank;
import com.taksewa.taksewa.model.Kecamatan;
import com.taksewa.taksewa.model.Status;

import java.util.ArrayList;
import java.util.List;

public class BankResponse {
    public BankResponse() {
    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private List<Bank> data = new ArrayList<Bank>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<Bank> getData() {
            return data;
        }

        public void setData(List<Bank> data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
