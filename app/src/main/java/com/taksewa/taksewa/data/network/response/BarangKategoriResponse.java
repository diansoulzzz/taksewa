package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Carousel;
import com.taksewa.taksewa.model.Status;

import java.util.ArrayList;
import java.util.List;

public class BarangKategoriResponse {

    private BarangKategoriResponse() {

    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private BarangKategori data;
        @SerializedName("status")
        @Expose
        private Status status;

        public BarangKategori getData() {
            return data;
        }

        public void setData(BarangKategori data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private List<BarangKategori> data = new ArrayList<>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<BarangKategori> getData() {
            return data;
        }

        public void setData(List<BarangKategori> data) {
            this.data = data;
        }


        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
