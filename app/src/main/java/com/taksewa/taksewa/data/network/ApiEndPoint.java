package com.taksewa.taksewa.data.network;


import com.taksewa.taksewa.BuildConfig;

public final class ApiEndPoint {

    public static final String POST_FIREBASE_AUTH = BuildConfig.BASE_URL
            + "login/firebase";

    public static final String GET_PROFILE_INFO = BuildConfig.BASE_URL
            + "profile/info";

    public static final String POST_DAFTAR_VENDOR = BuildConfig.BASE_URL
            + "vendor/open-toko";

    public static final String POST_REGISTER = BuildConfig.BASE_URL
            + "register";

    public static final String POST_REGISTER_OTP = BuildConfig.BASE_URL
            + "register/otp";

    public static final String POST_FIREBASE_FCM_TOKEN = BuildConfig.BASE_URL
            + "firebase/fcm-token";

    public static final String POST_LOGOUT = BuildConfig.BASE_URL
            + "logout";

    public static final String GET_HOME_DATA = BuildConfig.BASE_URL
            + "home/data";

    public static final String GET_BARANG_KATEGORI_LIST = BuildConfig.BASE_URL
            + "barang-kategori/list";

    public static final String GET_BARANG_SUB_KATEGORI_LIST = BuildConfig.BASE_URL
            + "barang-sub-kategori/list";

    public static final String POST_VENDOR_BARANG_ENTRY = BuildConfig.BASE_URL
            + "vendor/barang/entry";

    public static final String GET_BARANG_DATA_FROMID = BuildConfig.BASE_URL
            + "barang/data/fromid";

    public static final String GET_BARANG_IS_WISHLIST_FROMID = BuildConfig.BASE_URL
            + "barang/is-wishlist/fromid";

    public static final String POST_TOOGLE_BARANG_WISHLIST_FROMID = BuildConfig.BASE_URL
            + "barang/toogle-wishlist/fromid";

    public static final String GET_BARANG_WISHLIST_LIST = BuildConfig.BASE_URL
            + "barang/wishlist/list";

    public static final String GET_VENDOR_BARANG_LIST = BuildConfig.BASE_URL
            + "vendor/barang/list";

    public static final String POST_TOOGLE_BARANG_LAST_SEEN_FROMID = BuildConfig.BASE_URL
            + "barang/toogle-lastseen/fromid";

    public static final String GET_BARANG_LAST_SEEN_LIST = BuildConfig.BASE_URL
            + "barang/lastseen/list";

    public static final String GET_VENDOR_TRANSACTION_LIST = BuildConfig.BASE_URL
            + "vendor/transaction/list";

    public static final String GET_MEMBER_TRANSACTION_LIST = BuildConfig.BASE_URL
            + "member/transaction/list";

    public static final String GET_MEMBER_TRANSACTION_DATA_FROMID = BuildConfig.BASE_URL
            + "member/transaction/data/fromid";

    public static final String GET_VENDOR_TRANSACTION_DATA_FROMID = BuildConfig.BASE_URL
            + "vendor/transaction/data/fromid";

    public static final String POST_FCM_TOKEN = BuildConfig.BASE_URL
            + "firebase/fcmtoken";

    public static final String GET_MESSAGE_ROOM = BuildConfig.BASE_URL
            + "message/room";

    public static final String POST_NEW_MESSAGE_ENTRY = BuildConfig.BASE_URL
            + "message/entry";

    public static final String GET_10_MESSAGE_FROMID = BuildConfig.BASE_URL
            + "message/take10";

    public static final String GET_SEARCH_BARANG = BuildConfig.BASE_URL
            + "search/barang";

    public static final String GET_SEARCH_VENDOR = BuildConfig.BASE_URL
            + "search/vendor";

    public static final String POST_PROFILE_VERIFICATION = BuildConfig.BASE_URL
            + "profile/verification";

    public static final String GET_AREA_KECAMATAN_LIST = BuildConfig.BASE_URL
            + "area/kecamatan/list";

    public static final String GET_BANK_LIST = BuildConfig.BASE_URL
            + "bank/list";

    public static final String GET_AREA_KOTA_LIST = BuildConfig.BASE_URL
            + "area/kota/list";

    public static final String POST_VENDOR_TRANSACTION_CHANGE = BuildConfig.BASE_URL
            + "vendor/transaction/change";

    public static final String POST_MEMBER_TRANSACTION_CHANGE = BuildConfig.BASE_URL
            + "member/transaction/change";

    public static final String POST_VENDOR_BARANG_DELETE = BuildConfig.BASE_URL
            + "vendor/barang/delete";

    public static final String GET_MEMBER_REVIEW_LIST = BuildConfig.BASE_URL
            + "member/review/list";

    public static final String GET_MEMBER_REVIEW_DATA_FROMID = BuildConfig.BASE_URL
            + "member/review/data/fromid";

    public static final String POST_MEMBER_REVIEW_ENTRY = BuildConfig.BASE_URL
            + "member/review/entry";

    public static final String GET_VENDOR_REPORT_TRANSACTION_LIST_FROMFILTER = BuildConfig.BASE_URL
            + "vendor/report/transaksi/list/fromfilter";

    public static final String POST_SALDO_WITHDRAW = BuildConfig.BASE_URL
            + "profile/saldo/withdraw";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
