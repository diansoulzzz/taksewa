package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.model.Status;

public class ReviewBarangResponse {

    public static class IData {
        @SerializedName("data")
        @Expose
        private ReviewBarang data = new ReviewBarang();

        @SerializedName("status")
        @Expose
        private Status status;

        public ReviewBarang getData() {
            return data;
        }

        public void setData(ReviewBarang data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }
}
