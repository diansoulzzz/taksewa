package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Status;
import com.taksewa.taksewa.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserResponse {

    private UserResponse() {
        // This class is not publicly instantiable
    }

    public static class IData {
        @SerializedName("data")
        @Expose
        private User data;
        @SerializedName("status")
        @Expose
        private Status status;

        public User getData() {
            return data;
        }

        public void setData(User data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class IList {
        @SerializedName("data")
        @Expose
        private List<User> data = new ArrayList<User>();
        @SerializedName("status")
        @Expose
        private Status status;

        public List<User> getData() {
            return data;
        }

        public void setData(List<User> data) {
            this.data = data;
        }


        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }
}
