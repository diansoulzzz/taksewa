package com.taksewa.taksewa.data;


import android.content.Context;
import android.text.TextUtils;

import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.taksewa.taksewa.data.network.ApiHeader;
import com.taksewa.taksewa.data.network.ApiHelper;
import com.taksewa.taksewa.data.network.request.AuthRequest;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.request.HSewaRequest;
import com.taksewa.taksewa.data.network.request.HomeRequest;
import com.taksewa.taksewa.data.network.request.MessageRequest;
import com.taksewa.taksewa.data.network.request.ReviewBarangRequest;
import com.taksewa.taksewa.data.network.request.SearchRequest;
import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.AuthResponse;
import com.taksewa.taksewa.data.network.response.BankResponse;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.BarangSubKategoriResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.data.prefs.PreferencesHelper;
import com.taksewa.taksewa.di.ApplicationContext;
import com.taksewa.taksewa.model.User;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public Single<AuthResponse.AccessToken> postServerRegister(AuthRequest.ServerRegister request) {
        return mApiHelper.postServerRegister(request);
    }

    @Override
    public Single<AuthResponse.RegisterOTP> postServerRegisterOTP(AuthRequest.ServerRegisterOTP request) {
        return mApiHelper.postServerRegisterOTP(request);
    }

    @Override
    public Single<AuthResponse.AccessToken> postAuthToken(AuthRequest.FirebaseAuth request) {
        return mApiHelper.postAuthToken(request);
    }

    @Override
    public Single<UserResponse.IData> postFirebaseFcmToken(AuthRequest.FirebaseFcmToken request) {
        return mApiHelper.postFirebaseFcmToken(request);
    }

    @Override
    public Single<BarangResponse.IData> postVendorBarangEntry(BarangRequest.EntryData request) {
        return mApiHelper.postVendorBarangEntry(request);
    }

    @Override
    public Single<BarangResponse.IData> postVendorBarangEntryObject(BarangRequest.EntryDataObject request) {
        return mApiHelper.postVendorBarangEntryObject(request);
    }

    @Override
    public Single<UserResponse.IData> postDaftarVendor() {
        return mApiHelper.postDaftarVendor();
    }

    @Override
    public Single<UserResponse.IData> getProfileInfo() {
        return mApiHelper.getProfileInfo();
    }

    @Override
    public Single<HomeResponse.IData> getHomeData(HomeRequest.NotFromID request) {
        return mApiHelper.getHomeData(request);
    }

    @Override
    public Single<BarangKategoriResponse.IList> getBarangKategoriList() {
        return mApiHelper.getBarangKategoriList();
    }

    @Override
    public Single<BarangSubKategoriResponse.IList> getBarangSubKategoriList() {
        return mApiHelper.getBarangSubKategoriList();
    }

    @Override
    public Single<BarangResponse.IData> getBarangDataFromId(BarangRequest.FromID request) {
        return mApiHelper.getBarangDataFromId(request);
    }

    @Override
    public Single<BarangResponse.IWishListed> getBarangIsWishlistFromId(BarangRequest.FromID request) {
        return mApiHelper.getBarangIsWishlistFromId(request);

    }

    @Override
    public Single<BarangResponse.IWishListed> postToogleBarangWishlistFromId(BarangRequest.FromID request) {
        return mApiHelper.postToogleBarangWishlistFromId(request);
    }

    @Override
    public Single<HSewaResponse.IList> getVendorTransactionList() {
        return mApiHelper.getVendorTransactionList();
    }

    @Override
    public Single<HSewaResponse.IList> getMemberTransactionList() {
        return mApiHelper.getMemberTransactionList();
    }

    @Override
    public Single<BarangResponse.IList> getBarangWishlistList() {
        return mApiHelper.getBarangWishlistList();
    }

    @Override
    public Single<BarangResponse.IList> getVendorBarangList() {
        return mApiHelper.getVendorBarangList();
    }

    @Override
    public Single<BarangResponse.ILastSeenList> postToogleLastSeenBarangFromId(BarangRequest.FromID request) {
        return mApiHelper.postToogleLastSeenBarangFromId(request);
    }

    @Override
    public Single<BarangResponse.IList> getBarangLastSeendBarangList() {
        return mApiHelper.getBarangLastSeendBarangList();
    }

    @Override
    public Single<HSewaResponse.IData> getMemberTransactionDataFromId(HSewaRequest.FromID request) {
        return mApiHelper.getMemberTransactionDataFromId(request);
    }

    @Override
    public Single<HSewaResponse.IData> getVendorTransactionDataFromId(HSewaRequest.FromID request) {
        return mApiHelper.getVendorTransactionDataFromId(request);
    }

    @Override
    public Single<HSewaResponse.IData> postVendorTransactionDataChange(HSewaRequest.ChangeData request) {
        return mApiHelper.postVendorTransactionDataChange(request);
    }

    @Override
    public Single<HSewaResponse.IData> postMemberTransactionDataChange(HSewaRequest.ChangeData request) {
        return mApiHelper.postMemberTransactionDataChange(request);
    }

    @Override
    public Single<MessageResponse.IRoom> getMessageRoom() {
        return mApiHelper.getMessageRoom();
    }

    @Override
    public Single<MessageResponse.IData> postNewMessage(MessageRequest.EntryData request) {
        return mApiHelper.postNewMessage(request);
    }

    @Override
    public Single<MessageResponse.IList> get10Message(MessageRequest.Take10 request) {
        return mApiHelper.get10Message(request);
    }

    @Override
    public Single<SearchResponse.IBarangData> getSearchBarang(SearchRequest.ISendBarang request) {
        return mApiHelper.getSearchBarang(request);
    }

    @Override
    public Single<SearchResponse.IVendorData> getSearchVendor(SearchRequest.ISendVendor request) {
        return mApiHelper.getSearchVendor(request);
    }

    @Override
    public Single<UserResponse.IData> postUserVerification(UserRequest.EntryData request) {
        return mApiHelper.postUserVerification(request);
    }

    @Override
    public Single<AreaResponse.IKecamatanList> getKecamatanList() {
        return mApiHelper.getKecamatanList();
    }

    @Override
    public Single<BarangResponse.IList> postVendorBarangDelete(BarangRequest.FromID request) {
        return mApiHelper.postVendorBarangDelete(request);
    }

    @Override
    public Single<HSewaResponse.IList> getMemberReviewList() {
        return mApiHelper.getMemberReviewList();
    }

    @Override
    public Single<HSewaResponse.IData> getMemberReviewDataFromId(HSewaRequest.FromID request) {
        return mApiHelper.getMemberReviewDataFromId(request);
    }

    @Override
    public Single<HSewaResponse.IList> postMemberReviewEntry(ReviewBarangRequest.EntryData request) {
        return mApiHelper.postMemberReviewEntry(request);
    }

    @Override
    public Single<BankResponse.IList> getBankList() {
        return mApiHelper.getBankList();
    }

    @Override
    public Single<HSewaResponse.IList> getVendorReportTransactionListFromFilter(HSewaRequest.FromFilter request) {
        return mApiHelper.getVendorReportTransactionListFromFilter(request);
    }

    @Override
    public Single<HomeResponse.GData> postSaldoWithdraw(UserRequest.SaldoWithdraw request) {
        return mApiHelper.postSaldoWithdraw(request);
    }

//
//    @Override
//    public Single<AuthResponse.AccessToken> postSocialLogin(AuthRequest.SocialLogin request) {
//        return mApiHelper.postSocialLogin(request);
//    }
//
//    @Override
//    public Single<AuthResponse.AccessToken> postServerLogin(AuthRequest.ServerLogin request) {
//        return mApiHelper.postServerLogin(request);
//    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setmAuthorization(accessToken);
    }

    @Override
    public Boolean getCurrentUserisVendor() {
        return mPreferencesHelper.getCurrentUserisVendor();
    }

    @Override
    public void setCurrentUserIsVendor(Boolean isVendor) {
        mPreferencesHelper.setCurrentUserIsVendor(isVendor);
    }

    @Override
    public Boolean getCurrentUserisVerified() {
        return mPreferencesHelper.getCurrentUserisVerified();
    }

    @Override
    public void setCurrentUserIsVerified(Boolean isVerified) {
        mPreferencesHelper.setCurrentUserIsVerified(isVerified);
    }

//    @Override
//    public String getFirebaseToken() {
//        return mPreferencesHelper.getFirebaseToken();
//    }
//
//    @Override
//    public void setFirebaseToken(String firebaseToken) {
//        mPreferencesHelper.setFirebaseToken(firebaseToken);
//    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public void updateApiHeader(Long userId, String accessToken) {
//        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        mApiHelper.getApiHeader().getProtectedApiHeader().setmAuthorization(accessToken);
    }

    @Override
    public void updateUserInfo(
            User userModel,
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath,
            Boolean isVendor,
            Boolean isVerified
    ) {
        if (!TextUtils.isEmpty(accessToken)) {
            UserDetail userDetail = LocalDataHandler.readObject("user_details", UserDetail.class);
            if (userDetail == null) {
                userDetail = new UserDetail();
                userDetail.setUserFullName(userModel.getNama());
                userDetail.setEmail(userModel.getEmail());
                userDetail.setPhoneNumber(userModel.getNoTelp());

                ArrayList<UserAddress> userAddresses = new ArrayList<>();
                UserAddress userAddress = new UserAddress();
                userAddress.setAddress("Jl Slipi No 15");
                userAddress.setCity("Jakarta");
                userAddress.setAddressType(com.midtrans.sdk.corekit.core.Constants.ADDRESS_TYPE_BOTH);
                userAddress.setZipcode("40184");
                userAddress.setCountry("IDN");
                userAddresses.add(userAddress);
                userDetail.setUserAddresses(userAddresses);
                LocalDataHandler.saveObject("user_details", userDetail);
            }
        }
        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserName(userName);
        setCurrentUserEmail(email);
        setCurrentUserProfilePicUrl(profilePicPath);
        setCurrentUserIsVendor(isVendor);
        setCurrentUserIsVerified(isVerified);
        updateApiHeader(userId, accessToken);

    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null,
                false,
                false
        );
    }

}
