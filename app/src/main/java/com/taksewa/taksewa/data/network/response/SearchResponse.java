package com.taksewa.taksewa.data.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Kotum;
import com.taksewa.taksewa.model.Provinsi;
import com.taksewa.taksewa.model.Status;
import com.taksewa.taksewa.model.User;

import java.util.ArrayList;
import java.util.List;

public class SearchResponse {

    private SearchResponse() {
        // This class is not publicly instantiable
    }

    public static class IBarangData {
        @SerializedName("data")
        @Expose
        private Data data = new Data();
        @SerializedName("status")
        @Expose
        private Status status;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class Data {
        @SerializedName("barang")
        @Expose
        private ArrayList<Barang> barang = new ArrayList<Barang>();
        @SerializedName("barang_kategori")
        @Expose
        private ArrayList<BarangKategori> barangKategori = new ArrayList<BarangKategori>();
        @SerializedName("barang_sub_kategori")
        @Expose
        private ArrayList<BarangSubKategori> barangSubKategori = new ArrayList<BarangSubKategori>();
        @SerializedName("kota")
        @Expose
        private ArrayList<Kotum> kota = new ArrayList<Kotum>();
        @SerializedName("provinsi")
        @Expose
        private ArrayList<Provinsi> provinsi = new ArrayList<Provinsi>();

        public ArrayList<Barang> getBarangList() {
            return barang;
        }

        public void setBarangList(ArrayList<Barang> barang) {
            this.barang = barang;
        }

        public ArrayList<BarangKategori> getBarangKategori() {
            return barangKategori;
        }

        public void setBarangKategori(ArrayList<BarangKategori> barangKategori) {
            this.barangKategori = barangKategori;
        }

        public ArrayList<BarangSubKategori> getBarangSubKategori() {
            return barangSubKategori;
        }

        public void setBarangSubKategori(ArrayList<BarangSubKategori> barangSubKategori) {
            this.barangSubKategori = barangSubKategori;
        }

        public ArrayList<Kotum> getKota() {
            return kota;
        }

        public void setKota(ArrayList<Kotum> kota) {
            this.kota = kota;
        }

        public ArrayList<Provinsi> getProvinsi() {
            return provinsi;
        }

        public void setProvinsi(ArrayList<Provinsi> provinsi) {
            this.provinsi = provinsi;
        }
    }

    public static class IVendorData {
        @SerializedName("data")
        @Expose
        private DVendor data = new DVendor();
        @SerializedName("status")
        @Expose
        private Status status;

        public DVendor getData() {
            return data;
        }

        public void setData(DVendor data) {
            this.data = data;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

    }

    public static class DVendor {
        @SerializedName("vendor")
        @Expose
        private ArrayList<User> users = new ArrayList<>();

        public ArrayList<User> getUsers() {
            return users;
        }

        public void setUsers(ArrayList<User> users) {
            this.users = users;
        }
    }
}
