package com.taksewa.taksewa.data.network.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.User;

import java.util.List;

public class BarangRequest {
    private BarangRequest() {
        // This class is not publicly instantiable
    }

    public static class FromID {
        @SerializedName("id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

    public static class EntryDataObject {
        @Expose
        @SerializedName("barang")
        private Barang data = new Barang();

        public Barang getBarang() {
            return data;
        }

        public void setBarang(Barang barang) {
            this.data = barang;
        }
    }

    public static class EntryData {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("barang_sub_kategori_id")
        @Expose
        private Integer barangSubKategoriId;
        @SerializedName("users_id")
        @Expose
        private Integer usersId;
        @SerializedName("stok")
        @Expose
        private Integer stok;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("deleted_at")
        @Expose
        private String deletedAt;
        @SerializedName("deposit_min")
        @Expose
        private Integer depositMin;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("barang_foto_utama")
        @Expose
        private BarangFoto barangFotoUtama;
        @SerializedName("barang_fotos")
        @Expose
        private List<BarangFoto> barangFotos = null;
        @SerializedName("barang_sub_kategori")
        @Expose
        private BarangSubKategori barangSubKategori;
        @SerializedName("barang_harga_sewas")
        @Expose
        private List<BarangHargaSewa> barangHargaSewas = null;
        @SerializedName("barang_harga_sewa_utama")
        @Expose
        private BarangHargaSewa barangHargaSewaUtama;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public Integer getBarangSubKategoriId() {
            return barangSubKategoriId;
        }

        public void setBarangSubKategoriId(Integer barangSubKategoriId) {
            this.barangSubKategoriId = barangSubKategoriId;
        }

        public Integer getUsersId() {
            return usersId;
        }

        public void setUsersId(Integer usersId) {
            this.usersId = usersId;
        }

        public Integer getStok() {
            return stok;
        }

        public void setStok(Integer stok) {
            this.stok = stok;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(String deletedAt) {
            this.deletedAt = deletedAt;
        }

        public Integer getDepositMin() {
            return depositMin;
        }

        public void setDepositMin(Integer depositMin) {
            this.depositMin = depositMin;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public BarangFoto getBarangFotoUtama() {
            return barangFotoUtama;
        }

        public void setBarangFotoUtama(BarangFoto barangFotoUtama) {
            this.barangFotoUtama = barangFotoUtama;
        }

        public List<BarangFoto> getBarangFotos() {
            return barangFotos;
        }

        public void setBarangFotos(List<BarangFoto> barangFotos) {
            this.barangFotos = barangFotos;
        }

        public BarangSubKategori getBarangSubKategori() {
            return barangSubKategori;
        }

        public void setBarangSubKategori(BarangSubKategori barangSubKategori) {
            this.barangSubKategori = barangSubKategori;
        }

        public List<BarangHargaSewa> getBarangHargaSewas() {
            return barangHargaSewas;
        }

        public void setBarangHargaSewas(List<BarangHargaSewa> barangHargaSewas) {
            this.barangHargaSewas = barangHargaSewas;
        }

        public BarangHargaSewa getBarangHargaSewaUtama() {
            return barangHargaSewaUtama;
        }

        public void setBarangHargaSewaUtama(BarangHargaSewa barangHargaSewaUtama) {
            this.barangHargaSewaUtama = barangHargaSewaUtama;
        }
    }

}
