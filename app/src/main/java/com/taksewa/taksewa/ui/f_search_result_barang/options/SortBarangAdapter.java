package com.taksewa.taksewa.ui.f_search_result_barang.options;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.model.MString;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SortBarangAdapter {

    public interface Callback {
        void onItemClick(MString data);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {
        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<MString> datas;
        private Callback mCallback;

        public AdapterList(Context context, List datas, Callback mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sort_barang, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.btnSortBy)
            Button btnSortBy;
            MString data;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.btnSortBy)
            void onBtnSortByClick() {
                mCallback.onItemClick(data);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                btnSortBy.setText(data.getNama());
                if (data.isOn())
                    btnSortBy.setTextColor(itemView.getResources().getColor(R.color.colorPrimary));
                else btnSortBy.setTextColor(itemView.getResources().getColor(R.color.black));
            }
        }
    }
}
