package com.taksewa.taksewa.ui.f_kategori;

import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_inbox.InboxMvpPresenter;
import com.taksewa.taksewa.ui.f_inbox.InboxMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class KategoriPresenter<V extends KategoriMvpView> extends BasePresenter<V>
        implements KategoriMvpPresenter<V> {
    @Inject
    public KategoriPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {

    }
}

