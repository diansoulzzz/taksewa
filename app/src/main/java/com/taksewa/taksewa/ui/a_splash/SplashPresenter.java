package com.taksewa.taksewa.ui.a_splash;

import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {
    @Inject
    public SplashPresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onSplashReady() {
        if (isUserLogin()){
            isAuthServCheck();
        }
    }
}

