package com.taksewa.taksewa.ui.a_login;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.AuthRequest;
import com.taksewa.taksewa.data.network.response.AuthResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V>
        implements LoginMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public LoginPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onServerAuth(String email, String password) {
        getMvpView().showOverlayLoading();
        getFirebaseAuth().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@androidx.annotation.NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "signInWithCredential:success");
                    onFirebaseAuthApiCall();
                } else {
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                    getMvpView().onError("Authentication Failed.");
                }
            }
        });
    }

    @Override
    public void onFacebookAuth(final AccessToken token) {
        getMvpView().showOverlayLoading();
        Log.d(TAG, "first:" + token.getToken());
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        getMvpView().showOverlayLoading();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        getFirebaseAuth().signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            onFirebaseAuthApiCall();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            getMvpView().onError("Authentication Failed.");
                        }
                    }
                });
    }

    @Override
    public void onGoogleAuth(final GoogleSignInAccount googleSignInAccount) {
        getMvpView().showOverlayLoading();
        Log.d(TAG, googleSignInAccount.getIdToken());
        final AuthCredential credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
        getFirebaseAuth().signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            onFirebaseAuthApiCall();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            getMvpView().onError("Authentication Failed.");
                        }
                    }
                });
    }

    public void onFirebaseAuthApiCall() {
        getFirebaseAuth().getCurrentUser().getIdToken(true)
                .addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if (task.isSuccessful()) {
                            String firebaseToken = task.getResult().getToken();
                            onAuthServer(firebaseToken);
                        } else {
                            getMvpView().hideOverlayLoading();
                        }
                    }
                });
    }

    public void onAuthServer(String firebaseToken) {
        getCompositeDisposable().add(getDataManager()
                .postAuthToken(new AuthRequest.FirebaseAuth(firebaseToken))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<AuthResponse.AccessToken>() {
                    @Override
                    public void accept(AuthResponse.AccessToken response) throws Exception {
                        sendFirebaseInstaceIdToken();
                        getDataManager().updateUserInfo(
                                response.getData(),
                                "Bearer " + response.getData().getApiToken(),
                                Long.valueOf(response.getData().getId()),
                                DataManager.LoggedInMode.LOGGED_IN_MODE_GOOGLE,
                                response.getData().getNama(),
                                response.getData().getEmail(),
                                response.getData().getFotoUrl(),
                                (response.getData().getIsVendor() == 1),
                                (response.getData().getVerified() != null)
                        );
                        getMvpView().hideOverlayLoading();
                        getMvpView().restartApp();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                        getMvpView().hideOverlayLoading();
                    }
                }));
    }
}

