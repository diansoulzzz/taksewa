package com.taksewa.taksewa.ui.f_search_result_vendor;

import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangMvpView;

public interface SearchResultVendorMvpPresenter<V extends SearchResultVendorMvpView> extends MvpPresenter<V> {

    void onViewPrepared(String queryString, int sortBy, Filter filter);
}

