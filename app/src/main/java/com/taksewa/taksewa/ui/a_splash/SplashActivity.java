package com.taksewa.taksewa.ui.a_splash;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.taksewa.taksewa.model.RemoteMessageWrapper;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_main.MainActivity;

import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        mPresenter.onSplashReady();
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//        if (getIntent().hasExtra("notification_data")) {
//            RemoteMessageWrapper remoteMessageWrapper = getIntent().getParcelableExtra("notification_data");
//            Map<String, String> data = remoteMessageWrapper.getRemoteMessage().getData();
//            intent = new Intent(data.get("click_action"));
//            intent.putExtra("notification_data", remoteMessageWrapper);
//        }
        startActivity(intent);
        finish();
    }
}
