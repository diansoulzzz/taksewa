package com.taksewa.taksewa.ui.a_view_barang_kategori;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpView;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriActivity;
import com.taksewa.taksewa.utils.AppBarStateChangeListener;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewBarangKategoriActivity extends BaseActivity implements ViewBarangKategoriMvpView {

    private final String TAG = getClass().getSimpleName();
    private final int VIEW_BARANG_SUB_KATEGORI_RQ = 2;

    @Inject
    ViewBarangKategoriMvpPresenter<ViewBarangKategoriMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvBarangKategori)
    RecyclerView rvBarangKategori;
    @BindView(R.id.etFilter)
    EditText etFilter;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    ViewBarangKategoriAdapter.AdapterEntry viewBarangKategoriAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<BarangKategori> barangKategoris = new ArrayList<>();
    BarangSubKategori barangSubKategori = new BarangSubKategori();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_barang_kategori);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBarangKategoriAdapter = new ViewBarangKategoriAdapter.AdapterEntry(this, barangKategoris, new ViewBarangKategoriAdapter.CallbackImage() {
            @Override
            public void onClickView(BarangKategori barangKategori, View itemView) {
                if (barangKategori.getBarangSubKategoris().size() > 1) {
                    Intent intent = new Intent(getBaseContext(), ViewBarangSubKategoriActivity.class);
                    intent.putParcelableArrayListExtra("barangSubKategori",barangKategori.getBarangSubKategoris());
                    startActivityForResult(intent, VIEW_BARANG_SUB_KATEGORI_RQ);
                }
//                Intent intent = new Intent();
//                intent.putExtra("barangKategoriId", barangKategori.getId().toString());
//                intent.putExtra("barangKategoriNama", barangKategori.getNama());
//                setResult(RESULT_OK, intent);
//                finish();
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarangKategori.setLayoutManager(gridLayoutManager);
        rvBarangKategori.setAdapter(viewBarangKategoriAdapter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewBarangKategoriAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPresenter.onViewPrepared();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIEW_BARANG_SUB_KATEGORI_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                barangSubKategori = data.getParcelableExtra("barangSubKategori");
                Intent intent = new Intent();
                intent.putExtra("barangSubKategori",barangSubKategori);
//                intent.putExtra("barangSubKategoriId", barangSubKategori.getId().toString());
//                intent.putExtra("barangSubKategoriNama", barangSubKategori.getNama());
                setResult(RESULT_OK, intent);
                finish();
            }
        }
//        Intent intent = new Intent();
//        intent.putExtra("barangKategoriId", barangKategori.getId().toString());
//        intent.putExtra("barangKategoriNama", barangKategori.getNama());
//        setResult(RESULT_OK, intent);
//        finish();
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvBarangKategori.setVisibility(View.VISIBLE);
//        clOptions.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBarangKategori.setVisibility(View.INVISIBLE);
//        clOptions.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onPresenterReady(BarangKategoriResponse.IList barangKategoriIListResponse) {
        barangKategoris.clear();
        barangKategoris.addAll(barangKategoriIListResponse.getData());
        viewBarangKategoriAdapter.notifyDataSetChanged();
        onRemoveWait();
    }

}

