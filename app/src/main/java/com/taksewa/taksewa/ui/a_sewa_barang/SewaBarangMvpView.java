package com.taksewa.taksewa.ui.a_sewa_barang;

import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.MvpView;

public interface SewaBarangMvpView extends MvpView {

    void onPresenterReady(BarangResponse.IData barangResponse);

    void onPresenterUserInfoReady(User user);
}
