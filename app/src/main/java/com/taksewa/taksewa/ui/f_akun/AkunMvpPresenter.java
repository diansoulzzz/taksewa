package com.taksewa.taksewa.ui.f_akun;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpView;

public interface AkunMvpPresenter<V extends AkunMvpView> extends MvpPresenter<V> {

    void attemptLogout();

    void onViewPrepared();
}

