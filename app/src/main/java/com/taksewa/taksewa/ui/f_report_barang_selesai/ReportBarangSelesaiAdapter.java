package com.taksewa.taksewa.ui.f_report_barang_selesai;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.CommonUtils.getDateParsedToFormat;

public class ReportBarangSelesaiAdapter {

    public interface Callback {
        void onClickView(HSewa data);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        static final int VIEW_TYPE_EMPTY = 0;
        static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<HSewa> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NotNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report_transaksi, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.title_list_empty), Html.fromHtml(parent.getResources().getString(R.string.empty_subtitle_belum_ada_transaksi)).toString());
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (this.datas != null && this.datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            }
            return VIEW_TYPE_EMPTY;
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvTglTransaksi)
            TextView tvTglTransaksi;
            @BindView(R.id.tvKodenota)
            TextView tvKodenota;
            @BindView(R.id.tvNamaBarang)
            TextView tvNamaBarang;
            @BindView(R.id.ivGambarBarang)
            ImageView ivGambarBarang;
            @BindView(R.id.tvTotalBayarValue)
            TextView tvTotalBayarValue;
            @BindView(R.id.tvStatusSewa)
            TextView tvStatusSewa;


            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final HSewa data = datas.get(i);
                assert data != null;
                Barang barang = data.getdSewaUtama().getBarangDetail().getBarang();
                assert barang != null;
                tvNamaBarang.setText(barang.getNama());
                if (barang.getBarangFotoUtama().getFotoUrl() != null) {
                    Glide.with(itemView).load(barang.getBarangFotoUtama().getFotoUrl()).into(ivGambarBarang);
                }
                if (data.getTglDiterimaPenyewaKembali() != null) {
                    tvStatusSewa.setBackgroundColor(context.getResources().getColor(R.color.colorGreen_50));
                    tvStatusSewa.setText(context.getResources().getString(R.string.barang_selesai_disewa));
                } else {
                    tvStatusSewa.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary_50));
                    tvStatusSewa.setText(context.getResources().getString(R.string.barang_sedang_dipinjam));
                }
                tvKodenota.setText(data.getKodenota());
                tvTglTransaksi.setText(getDateParsedToFormat(data.getTgl(), "dd MMM yyyy"));
                tvTotalBayarValue.setText(Html.fromHtml(context.getResources().getString(R.string.rp_, NumberTextWatcher.getDecimalFormattedString(data.getGrandtotal().toString()))));
            }
        }
    }
}
