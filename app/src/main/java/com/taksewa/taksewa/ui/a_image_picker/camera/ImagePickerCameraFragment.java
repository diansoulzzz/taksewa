package com.taksewa.taksewa.ui.a_image_picker.camera;

import android.Manifest;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.otaliastudios.cameraview.BitmapCallback;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.ui._base.BaseFragment;

import java.io.File;
import java.io.FileOutputStream;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.utils.CommonUtils.saveBitmapGetPath;

public class ImagePickerCameraFragment extends BaseFragment {

    private final String TAG = getClass().getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;

    String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

    @BindView(R.id.cameraView)
    CameraView cameraView;
    @BindView(R.id.ivAmbilPhoto)
    ImageView ivAmbilPhoto;
    @BindView(R.id.ivCameraPreview)
    ImageView ivCameraPreview;
    @BindView(R.id.ivUlangiPhoto)
    ImageView ivUlangiPhoto;
    @BindView(R.id.ivPakaiPhoto)
    ImageView ivPakaiPhoto;

    private Bitmap imgBitmap;

    private OnImagePickerCameraListener mCallback;

    public interface OnImagePickerCameraListener {
        void onCameraPickerImagePath(String filePath);
    }

    public void setOnImagePickerCameraListener(OnImagePickerCameraListener mCallback) {
        this.mCallback = mCallback;
    }

    public static ImagePickerCameraFragment newInstance() {
        ImagePickerCameraFragment fragmentFirst = new ImagePickerCameraFragment();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_picker_camera, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            setUnBinder(ButterKnife.bind(this, view));
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        if (!hasPermission(permissions)) {
            requestPermissionsSafely(permissions, REQUEST_PERMISSIONS_REQUEST_CODE);
            return;
        }
        cameraView.setLifecycleOwner(getViewLifecycleOwner());
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                result.toBitmap(new BitmapCallback() {
                    @Override
                    public void onBitmapReady(@Nullable Bitmap bitmap) {
                        onPhotoTaken(bitmap);
                    }
                });
                super.onPictureTaken(result);
            }
        });
//        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cameraView.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        cameraView.open();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.close();
    }

    @OnClick(R.id.ivAmbilPhoto)
    void onIvAmbilPhotoClick() {
        cameraView.takePictureSnapshot();
    }

    @OnClick(R.id.ivUlangiPhoto)
    void onIvUlangiPhotoClick() {
        onPhotoRestart();
    }

    @OnClick(R.id.ivPakaiPhoto)
    void onIvPakaiPhotoClick() {
        String filePath = saveBitmapGetPath(imgBitmap);
        if (filePath != null) {
            mCallback.onCameraPickerImagePath(filePath);
        }
    }

    public void onPhotoRestart() {
        ivPakaiPhoto.setVisibility(View.INVISIBLE);
        ivUlangiPhoto.setVisibility(View.INVISIBLE);
        ivCameraPreview.setVisibility(View.INVISIBLE);
        ivAmbilPhoto.setVisibility(View.VISIBLE);
        cameraView.setVisibility(View.VISIBLE);
    }

    public void onPhotoTaken(Bitmap bitmap) {
        imgBitmap = bitmap;
        Glide.with(getBaseActivity()).load(bitmap).into(ivCameraPreview);
        ivPakaiPhoto.setVisibility(View.VISIBLE);
        ivUlangiPhoto.setVisibility(View.VISIBLE);
        ivCameraPreview.setVisibility(View.VISIBLE);
        ivAmbilPhoto.setVisibility(View.INVISIBLE);
        cameraView.setVisibility(View.INVISIBLE);
    }
}
