package com.taksewa.taksewa.ui.a_view_barang_sub_kategori;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpView;

public interface ViewBarangSubKategoriMvpPresenter<V extends ViewBarangSubKategoriMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

