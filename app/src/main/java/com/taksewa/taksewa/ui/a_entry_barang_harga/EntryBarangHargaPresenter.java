package com.taksewa.taksewa.ui.a_entry_barang_harga;

import android.net.Uri;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import io.reactivex.disposables.CompositeDisposable;

public class EntryBarangHargaPresenter<V extends EntryBarangHargaMvpView> extends BasePresenter<V>
        implements EntryBarangHargaMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public EntryBarangHargaPresenter(DataManager dataManager,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void onViewPrepared() {

    }
}

