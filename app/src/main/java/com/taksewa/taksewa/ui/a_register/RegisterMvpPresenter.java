package com.taksewa.taksewa.ui.a_register;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_login.LoginMvpView;

public interface RegisterMvpPresenter<V extends RegisterMvpView> extends MvpPresenter<V> {

    void onServerAuth(String email, String password);

    void onFacebookAuth(AccessToken accessToken);

    void onGoogleAuth(GoogleSignInAccount googleSignInAccount);

    void onFirebaseAuthApiCall();
}

