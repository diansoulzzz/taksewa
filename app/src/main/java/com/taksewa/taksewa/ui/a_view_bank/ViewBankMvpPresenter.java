package com.taksewa.taksewa.ui.a_view_bank;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpView;

public interface ViewBankMvpPresenter<V extends ViewBankMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

