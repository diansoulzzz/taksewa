package com.taksewa.taksewa.ui.f_report_barang_selesai;

import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.ui._base.MvpPresenter;

public interface ReportBarangSelesaiMvpPresenter<V extends ReportBarangSelesaiMvpView> extends MvpPresenter<V> {

    void onViewPrepared(String tipe);
}

