package com.taksewa.taksewa.ui.f_search_result_barang.options;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.MString;
import com.taksewa.taksewa.ui._base.BaseActivity;

import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_HIGH_PRICE;
import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_LOW_PRICE;
import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_NEW_ITEM;
import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_RELEVANCE;
import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_REVIEW;

public class SortBarangActivity extends BaseActivity {
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvSortBarang)
    RecyclerView rvSortBarang;
    ArrayList<MString> sortList = new ArrayList<>();
    int sortBy = SORT_BY_RELEVANCE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("sortBy")) {
            sortBy = getIntent().getIntExtra("sortBy", SORT_BY_RELEVANCE);
        }
        sortList.add(new MString(SORT_BY_RELEVANCE, "Paling Sesuai", (sortBy == SORT_BY_RELEVANCE)));
        sortList.add(new MString(SORT_BY_HIGH_PRICE, "Harga Tertinggi", (sortBy == SORT_BY_HIGH_PRICE)));
        sortList.add(new MString(SORT_BY_LOW_PRICE, "Harga Terendah", (sortBy == SORT_BY_LOW_PRICE)));
        sortList.add(new MString(SORT_BY_NEW_ITEM, "Terbaru", (sortBy == SORT_BY_NEW_ITEM)));
        sortList.add(new MString(SORT_BY_REVIEW, "Ulasan", (sortBy == SORT_BY_REVIEW)));
//        review remark..
//        sortList.add(new MString(SORT_BY_REVIEW, "Ulasan", (sortBy == SORT_BY_REVIEW)));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        SortBarangAdapter.AdapterList adapterList = new SortBarangAdapter.AdapterList(this, sortList, new SortBarangAdapter.Callback() {
            @Override
            public void onItemClick(MString data) {
                Intent intent = new Intent();
                intent.putExtra("sortBy", data.getId());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        rvSortBarang.setAdapter(adapterList);
        rvSortBarang.setLayoutManager(gridLayoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    boolean isValid() {
//        boolean isvalid = true;
//        if (filter.getHargaMin() > filter.getHargaMax()) {
//            isvalid = false;
//        }
//        return isvalid;
//    }

}
