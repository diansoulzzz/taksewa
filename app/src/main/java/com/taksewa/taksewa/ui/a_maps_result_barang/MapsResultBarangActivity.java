package com.taksewa.taksewa.ui.a_maps_result_barang;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.transition.Slide;
import androidx.transition.TransitionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;

import java.util.ArrayList;

import javax.inject.Inject;

public class MapsResultBarangActivity extends BaseActivity implements MapsResultBarangMvpView, OnMapReadyCallback, MapsResultBarangBottomSheet.CallbackList {

    public final String TAG = getClass().getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    @Inject
    MapsResultBarangMvpPresenter<MapsResultBarangMvpView> mPresenter;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.ivPrev)
    ImageView ivPrev;
    @BindView(R.id.ivOpenBottomSheet)
    ImageView ivOpenBottomSheet;
    @BindView(R.id.tvCurrentItems)
    TextView tvCurrentItems;
    @BindView(R.id.ivInformation)
    ImageView ivInformation;
    @BindView(R.id.clGroupItems)
    ConstraintLayout clGroupItems;
    @BindView(R.id.clParent)
    ConstraintLayout clParent;

    String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    private static final float maxZoomLevel = 16.0f;
    private ArrayList<Barang> barangs;
    private MapsResultBarangAdapter.AdapterMaps adapter;
    private int currentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_result_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void setUp() {
        if (!hasPermission(permissions)) {
            requestPermissionsSafely(permissions, REQUEST_PERMISSIONS_REQUEST_CODE);
            return;
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (getIntent().hasExtra("barangs")) {
            barangs = getIntent().getParcelableArrayListExtra("barangs");
            mapFragment.getMapAsync(this);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        adapter = new MapsResultBarangAdapter.AdapterMaps(this, barangs, new MapsResultBarangAdapter.onMapAdapterEventListener() {
            @Override
            public void onMapInfoWindowClick(Barang barang) {
                Intent intent = new Intent(getBaseContext(), ViewBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivity(intent);
            }

            @Override
            public void onMarkerClicked(Marker marker, Barang barang, View mWindow, ImageView imageView, Integer position) {
//                int position = Integer.valueOf(marker.getTag().toString());
                Glide.with(mWindow).asBitmap().load(barang.getBarangFotoUtama().getFotoUrl()).into(new SimpleTarget<Bitmap>(200, 200) {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        Drawable drawable = new BitmapDrawable(getResources(), resource);
                        drawable.setBounds(0, 0, 200, 200);
                        imageView.setImageDrawable(drawable);
                    }
                });
                changeFocusToIndex(position);
//                showActivityInformation(barang);
            }
        });
        mMap.setInfoWindowAdapter(adapter);
        mMap.setOnMarkerClickListener(adapter);
        mMap.setOnInfoWindowClickListener(adapter);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setMaxZoomPreference(maxZoomLevel);
        mMap.setContentDescription("Location Tak Sewa");
        mMap.setMyLocationEnabled(true);
        setUpMarker();
        changeFocusToIndex(0);
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation().addOnCompleteListener(this, new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    mLastLocation = task.getResult();
                    LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, maxZoomLevel));
                } else {
                    Log.w("MAP", "getLastLocation:exception", task.getException());
                    showMessage("error");
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int granted : grantResults) {
                        if (granted != PackageManager.PERMISSION_GRANTED) {
                            showMessage("Permission denied");
                            finish();
                        }
                    }
                    setUp();
                }
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!hasPermission(permissions)) {
            requestPermissionsSafely(permissions, REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            getLastLocation();
        }
    }

    @OnClick(R.id.ivPrev)
    void onClickIvPrev() {
        changeFocusToIndex(currentIndex - 1);
    }

    @OnClick(R.id.ivOpenBottomSheet)
    void onClickIvOpenBottomSheet() {
        MapsResultBarangBottomSheet.MapList dialog = MapsResultBarangBottomSheet.MapList.newInstance(barangs);
        dialog.show(getSupportFragmentManager(), "add_photo_dialog_fragment");
    }

    @OnClick(R.id.ivNext)
    void onClickIvNext() {
        changeFocusToIndex(currentIndex + 1);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void setUpMarker() {
        int i = 0;
        for (Barang barang : barangs) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(barang.getUser().getPinLatitude(), barang.getUser().getPinLongitude()))
                    .title(barang.getNama())
                    .snippet(barang.getNama())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))

            );
            marker.setTag(i);
            i++;
        }
    }

    @Override
    public void onClickView(Barang selectedBarang) {
        LatLng latLng = new LatLng(selectedBarang.getUser().getPinLatitude(), selectedBarang.getUser().getPinLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, maxZoomLevel));
        tvCurrentItems.setText(selectedBarang.getNama());
        changeFocusToIndex(barangs.indexOf(selectedBarang));
    }

    public void changeFocusToIndex(int newIndex) {
        if ((barangs.size() - 1 >= newIndex) && (newIndex > 0)) {
            for (Barang brg : barangs) {
                barangs.get(barangs.indexOf(brg)).setIsSelected(0);
            }
            barangs.get(newIndex).setIsSelected(1);
            LatLng latLng = new LatLng(barangs.get(newIndex).getUser().getPinLatitude(), barangs.get(newIndex).getUser().getPinLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, maxZoomLevel));
            tvCurrentItems.setText(barangs.get(newIndex).getNama());
            currentIndex = newIndex;
            showActivityInformation(barangs.get(newIndex));
//            showDialogInformation(currentIndex);
//            showActivityInformation(currentIndex);
        }
    }

//    boolean show = false;
//    private void toggle() {
//        Transition transition = new Slide(Gravity.BOTTOM);
//        transition.setDuration(600);
//        transition.addTarget(R.id.clGroupItems);
//
//        TransitionManager.beginDelayedTransition(clParent, transition);
//        clGroupItems.setVisibility(show ? View.VISIBLE : View.GONE);
//    }

    public void showActivityInformation(Barang brg) {
//        clGroupItems.setVisibility(View.VISIBLE);
//        clGroupItems.setAlpha(0.0f);
//        clGroupItems.animate()
//                .translationX(clGroupItems.getWidth())
//                .alpha(1.0f)
//                .setListener(null);
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions().centerCrop()).load(brg.getBarangFotoUtama().getFotoUrl()).into(ivInformation);

    }

    public void showDialogInformation(int index) {
        Barang brg = barangs.get(index);
        MapsResultBarangBottomSheet.MapDetail dialog = MapsResultBarangBottomSheet.MapDetail.newInstance(brg);
        dialog.show(getSupportFragmentManager(), "add_photo_dialog_fragment");
    }


//    private void onImageRender(final Marker marker, Barang data, View mWindow) {
//        int position = Integer.valueOf(marker.getTag().toString());
//        Glide.with(mWindow).asBitmap().load(data.getBarangFotoUtama().getFotoUrl()).into(new SimpleTarget<Bitmap>(200, 200) {
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Drawable drawable = new BitmapDrawable(getResources(), resource);
//                drawable.setBounds(0, 0, 200, 200);
//                imgBarang.setImageDrawable(drawable);
//            }
//        });
//    }

//    public void changeFocusToIndex(int crement) {
//        int newIndex = currentIndex + crement;
//        showMessage(String.valueOf(newIndex));
//        if ((barangs.size() - 1 >= newIndex) && (newIndex >= 0)) {
//            for (Barang brg : barangs) {
//                barangs.get(barangs.indexOf(brg)).setIsSelected(0);
//            }
//            barangs.get(newIndex).setIsSelected(1);
//            LatLng latLng = new LatLng(barangs.get(newIndex).getUser().getPinLatitude(), barangs.get(newIndex).getUser().getPinLongitude());
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, maxZoomLevel));
//            tvCurrentItems.setText(barangs.get(newIndex).getNama());
//            currentIndex = newIndex;
//        }
//    }
}
