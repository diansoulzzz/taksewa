package com.taksewa.taksewa.ui.f_akun_vendor;

import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.MvpView;

public interface AkunVendorMvpView extends MvpView {
    void onPresenterReady(User user);

    void onUserIsVendor();

    void onUserNotVendor();
}
