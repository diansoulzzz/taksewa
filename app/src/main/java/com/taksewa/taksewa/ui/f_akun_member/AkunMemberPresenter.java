package com.taksewa.taksewa.ui.f_akun_member;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_akun.AkunMvpPresenter;
import com.taksewa.taksewa.ui.f_akun.AkunMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class AkunMemberPresenter<V extends AkunMemberMvpView> extends BasePresenter<V>
        implements AkunMemberMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public AkunMemberPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        DoGetProfileInfo();
    }

    public void DoGetProfileInfo() {
        getProfileInfo(response -> {
            getDataManager().setCurrentUserIsVendor((response.getData().getIsVendor() == 1));
            getDataManager().setCurrentUserIsVerified((response.getData().getVerified() != null));
            getMvpView().onPresenterReady(response.getData());
        }, throwable -> {
            Log.d(TAG, "fail");
            setUserAsLoggedOut();
            if (!isViewAttached()) {
                return;
            }
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        });
//        getCompositeDisposable().add(getDataManager()
//                .getProfileInfo()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<UserResponse.IData>() {
//                    @Override
//                    public void accept(@NonNull UserResponse.IData response) throws Exception {
//                        getDataManager().setCurrentUserIsVendor((response.getData().getIsVendor() == 1));
//                        getDataManager().setCurrentUserIsVerified((response.getData().getVerified() != null));
//                        getMvpView().onPresenterReady(response.getData());
////                        getCompositeDisposable().clear();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(@NonNull Throwable throwable)
//                            throws Exception {
//                        Log.d(TAG, "fail");
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
//                    }
//                }));
    }
}

