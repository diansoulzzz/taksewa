package com.taksewa.taksewa.ui.f_home;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.HomeRequest;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorMvpPresenter;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V>
        implements HomeMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public HomePresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        DoGetHomeData();
    }

    public void DoGetHomeData() {
        HomeRequest.NotFromID homeRequest = new HomeRequest.NotFromID();
        homeRequest.setId(0);
        if (getDataManager().getCurrentUserId() != null){
            homeRequest.setId(getDataManager().getCurrentUserId().intValue());
        }
        getCompositeDisposable().add(getDataManager()
                .getHomeData(homeRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HomeResponse.IData>() {
                    @Override
                    public void accept(@NonNull HomeResponse.IData response) throws Exception {
                        getMvpView().onPresenterReady(response.getData());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

