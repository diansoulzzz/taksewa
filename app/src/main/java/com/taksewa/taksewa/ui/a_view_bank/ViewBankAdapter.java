package com.taksewa.taksewa.ui.a_view_bank;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Bank;
import com.taksewa.taksewa.model.Kecamatan;
import com.taksewa.taksewa.ui._base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewBankAdapter {

    public interface Callback {
        void onClickView(Bank kecamatan, View itemView);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> implements Filterable {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<Bank> datas;
        private List<Bank> filteredDatas;
        private Callback mCallback;

        public AdapterList(Context context, List datas, Callback mCallback) {
            this.context = context;
            this.datas = datas;
            this.filteredDatas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view_barang_kategori, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.filteredDatas != null && this.filteredDatas.size() > 0) {
                return this.filteredDatas.size();
            }
            return 0;
        }

        @Override
        public int getItemViewType(int position) {
            if (filteredDatas != null && filteredDatas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String charString = constraint.toString();
                    if (charString.isEmpty()) {
                        filteredDatas = datas;
                    } else {
                        ArrayList<Bank> filteredTemp = new ArrayList<>();
                        for (Bank row : datas) {
                            if (row.getNama().toLowerCase().contains(charString.toLowerCase())) {
                                filteredTemp.add(row);
                            }
                        }
                        filteredDatas = filteredTemp;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filteredDatas;
//                    Log.d(TAG, "ONFILTER : " + charString);
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    Log.d(TAG, "ONPublis : " + constraint.toString());
                    filteredDatas = (ArrayList<Bank>) results.values;
                    notifyDataSetChanged();
                }
            };
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.btnKategori)
            Button btnKategori;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final Bank data = filteredDatas.get(i);
                assert data != null;
                if (data.getNama() != null) {
                    btnKategori.setText(String.format("%s", data.getNama()));
                }
                btnKategori.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onClickView(data, itemView);
                    }
                });
            }
        }
    }
}
