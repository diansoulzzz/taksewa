package com.taksewa.taksewa.ui.a_login;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.taksewa.taksewa.ui._base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void onServerAuth(String email, String password);

    void onFacebookAuth(AccessToken accessToken);

    void onGoogleAuth(GoogleSignInAccount googleSignInAccount);
}

