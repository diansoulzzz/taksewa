package com.taksewa.taksewa.ui.a_search_result;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_search.SearchActivity;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangFragment;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorFragment;
import com.taksewa.taksewa.ui.f_transaksi.TransaksiFragment;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberFragment;
import com.taksewa.taksewa.ui.f_transaksi_vendor.TransaksiVendorFragment;

import javax.inject.Inject;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchResultActivity extends BaseActivity implements SearchResultMvpView {

    @Inject
    SearchResultMvpPresenter<SearchResultMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    static String queryString;
    static BarangKategori kategori;
    static BarangSubKategori subKategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (getIntent().hasExtra("queryString")) {
            queryString = getIntent().getStringExtra("queryString");
        }
        if (getIntent().hasExtra("kategori")) {
            kategori = getIntent().getParcelableExtra("kategori");
            queryString = kategori.getNama();
        }
        if (getIntent().hasExtra("subKategori")) {
            subKategori = getIntent().getParcelableExtra("subKategori");
            queryString = subKategori.getNama();
        }
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(queryString);
        SearchPagerAdapter adapterViewPager = new SearchPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    @OnClick(R.id.toolBar)
    void onClickToolBar() {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra("queryString",queryString);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class SearchPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public SearchPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return SearchResultBarangFragment.newInstance(queryString,kategori,subKategori);
                case 1:
                    return SearchResultVendorFragment.newInstance(queryString);
                default:
                    return SearchResultBarangFragment.newInstance(queryString,kategori,subKategori);
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) return "Product";
            return "Toko";
        }
    }
}
