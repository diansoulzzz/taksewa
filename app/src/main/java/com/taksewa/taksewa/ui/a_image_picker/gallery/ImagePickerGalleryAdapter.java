package com.taksewa.taksewa.ui.a_image_picker.gallery;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseViewHolder;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagePickerGalleryAdapter {

    public interface CallbackGallery {
        void onClickView(String imageGalleryPath, AppCompatCheckBox checkBox);
    }

    public static class AdapterGallery extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<String> datas;
        private CallbackGallery mCallback;
        public AdapterGallery(Context context, List datas, CallbackGallery mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image_picker_gallery, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.ivPickerGallery)
            ImageView ivPickerGallery;
            @BindView(R.id.cbPickerGallery)
            AppCompatCheckBox cbPickerGallery;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final String data = datas.get(i);
                if (data != null) {
                    Glide.with(itemView).load(new File(data)).into(ivPickerGallery);
                }
                cbPickerGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onClickView(data,cbPickerGallery);
                    }
                });
//                cbPickerGallery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
////                        mCallback.onClickView(data,cbPickerGallery);
//                    }
//                });
//                itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        mCallback.onClickView(data,cbPickerGallery);
//                    }
//                });
            }
        }
    }
}
