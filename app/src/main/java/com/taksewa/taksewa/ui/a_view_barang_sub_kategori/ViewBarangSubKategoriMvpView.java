package com.taksewa.taksewa.ui.a_view_barang_sub_kategori;

import com.taksewa.taksewa.data.network.response.BarangSubKategoriResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ViewBarangSubKategoriMvpView extends MvpView {
    void onPresenterReady(BarangSubKategoriResponse.IList BarangSubKategoriIListResponse);
}
