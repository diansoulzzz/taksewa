package com.taksewa.taksewa.ui.a_view_kecamatan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Kecamatan;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriAdapter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpView;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewKecamatanActivity extends BaseActivity implements ViewKecamatanMvpView {

    private final String TAG = getClass().getSimpleName();

    @Inject
    ViewKecamatanMvpPresenter<ViewKecamatanMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvKecamatan)
    RecyclerView rvKecamatan;
    @BindView(R.id.etFilter)
    EditText etFilter;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    ViewKecamatanAdapter.AdapterList viewKecamatanAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<Kecamatan> kecamatanList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_kecamatan);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewKecamatanAdapter = new ViewKecamatanAdapter.AdapterList(this, kecamatanList, new ViewKecamatanAdapter.Callback() {
            @Override
            public void onClickView(Kecamatan kecamatan, View itemView) {
                Intent intent = new Intent();
                intent.putExtra("kecamatan", kecamatan);
//                intent.putExtra("barangKategoriId", barangKategori.getId().toString());
//                intent.putExtra("barangKategoriNama", barangKategori.getNama());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvKecamatan.setLayoutManager(gridLayoutManager);
        rvKecamatan.setAdapter(viewKecamatanAdapter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewKecamatanAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPresenter.onViewPrepared();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvKecamatan.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvKecamatan.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(AreaResponse.IKecamatanList kecamatanResponseList) {
        onRemoveWait();
        kecamatanList.clear();
        kecamatanList.addAll(kecamatanResponseList.getData());
        viewKecamatanAdapter.notifyDataSetChanged();
    }
}

