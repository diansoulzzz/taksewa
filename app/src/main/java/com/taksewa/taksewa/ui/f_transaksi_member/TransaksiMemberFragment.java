package com.taksewa.taksewa.ui.f_transaksi_member;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_search_result.SearchResultActivity;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailActivity;
import com.taksewa.taksewa.ui.f_transaksi.TransaksiFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TransaksiMemberFragment extends BaseFragment implements TransaksiMemberMvpView, SwipeRefreshLayout.OnRefreshListener {
    private final int RQ_TRANSAKSI_MEMBER_DETAIL = 1;
    private final String TAG = getClass().getSimpleName();


    @Inject
    TransaksiMemberPresenter<TransaksiMemberMvpView> mPresenter;

    @BindView(R.id.rvTransaksiMember)
    RecyclerView rvTransaksiMember;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    private TransaksiMemberAdapter.AdapterList adapterList;
    private ArrayList<HSewa> hSewas = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;

    public static TransaksiMemberFragment newInstance() {
        Bundle args = new Bundle();
        TransaksiMemberFragment fragment = new TransaksiMemberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaksi_member, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        srlLoader.setOnRefreshListener(this);
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        adapterList = new TransaksiMemberAdapter.AdapterList(getContext(), hSewas, new TransaksiMemberAdapter.Callback() {
            @Override
            public void onClickView(HSewa data) {
//                if (data.gethPiutangUtama().getKodenota() != null){
//                    showMessage(data.gethPiutangUtama().getKodenota());
//                }
//                showMessage(data.getStatusTransaksi().toString());
                Intent intent = new Intent(getContext(), TransaksiMemberDetailActivity.class);
                intent.putExtra("hsewa", data);
                startActivityForResult(intent, RQ_TRANSAKSI_MEMBER_DETAIL);
            }
        });
        rvTransaksiMember.setLayoutManager(gridLayoutManager);
        rvTransaksiMember.setAdapter(adapterList);
        setHasOptionsMenu(true);
        mPresenter.onViewPrepared();
    }

    @Override
    public void onPresenterReady(HSewaResponse.IList hsewaResponse) {
        hSewas.clear();
        hSewas.addAll(hsewaResponse.getData());
        adapterList.notifyDataSetChanged();
        srlLoader.setRefreshing(false);
        removeWait();
    }

    public void removeWait() {
        progressBar.hide();
        rvTransaksiMember.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        mPresenter.onViewPrepared();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RQ_TRANSAKSI_MEMBER_DETAIL) {
                onRefresh();
            }
        }
    }
}
