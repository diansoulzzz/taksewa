package com.taksewa.taksewa.ui.f_transaksi;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_kategori.KategoriMvpView;

public interface TransaksiMvpPresenter<V extends TransaksiMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

