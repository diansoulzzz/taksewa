package com.taksewa.taksewa.ui.a_splash;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangMvpView;

public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

    void onSplashReady();
}

