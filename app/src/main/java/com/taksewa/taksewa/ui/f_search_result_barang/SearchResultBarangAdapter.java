package com.taksewa.taksewa.ui.f_search_result_barang;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultBarangAdapter {

    public interface Callback {
        void onClickBarang(Barang barang);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        static final int VIEW_TYPE_EMPTY = 0;
        static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<Barang> datas;
        private Callback callback;
        private String queryString;

        public AdapterList(Context context, List datas, Callback callback, String queryString) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
            this.queryString = queryString;
        }

        @NotNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result_barang, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.title_oops_object_tidak_ditemukan, "product"), Html.fromHtml(parent.getResources().getString(R.string.subtitle_pencarian_untuk_object_tidak_ditemukan, queryString)).toString());
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (this.datas != null && this.datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            }
            return VIEW_TYPE_EMPTY;
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaBarang)
            TextView tvNamaBarang;
            @BindView(R.id.tvHargaBarang)
            TextView tvHargaBarang;
            @BindView(R.id.ivGambarBarang)
            ImageView ivGambarBarang;
            @BindView(R.id.rbStarReview)
            RatingBar rbStarReview;
            @BindView(R.id.tvRateReview)
            TextView tvRateReview;
            @BindView(R.id.clRating)
            ConstraintLayout clRating;
            String htmlFormat;
            String formatedHarga;
            String formatedLamaHari;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final Barang data = datas.get(i);
                if (data.getNama() != null) {
                    tvNamaBarang.setText(data.getNama());
                }
                if (data.getBarangFotoUtama().getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getBarangFotoUtama().getFotoUrl()).into(ivGambarBarang);
                }
                if (data.getBarangHargaSewaUtama() != null) {
                    formatedHarga = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getHarga().toString());
                    formatedLamaHari = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getLamaHari().toString());
                    tvHargaBarang.setText(Html.fromHtml(itemView.getResources().getString(R.string.view_produk_harga_lama_sewa_rp, formatedHarga, "")));
                }
                if (data.getReviewBarangs() != null) {
                    if (data.getReviewBarangs().size() > 0) {
                        clRating.setVisibility(View.VISIBLE);
                        String totalRating = String.format(CommonUtils.getDefaultLocale(), "(%.1f)", data.getTotalRating());
                        tvRateReview.setText(totalRating);
                        rbStarReview.setRating(data.getTotalRating());
                    }
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClickBarang(data);
                    }
                });
            }
        }
    }

}
