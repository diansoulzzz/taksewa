package com.taksewa.taksewa.ui.a_transaksi_vendor_detail;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.DSewa;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.CommonUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.NumberTextWatcher.getDecimalFormattedString;

public class TransaksiVendorDetailAdapter {

    public interface Callback {
        void onClickView(DSewa dSewa, View itemView);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<DSewa> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaksi_detail, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.empty_title_daftar_produk_anda_kosong), Html.fromHtml(parent.getResources().getString(R.string.empty_subtitle_kamu_belum_pernah_mendaftarkan_produk_sebelumnya)).toString());
            }
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (datas != null && datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaProduk)
            TextView tvNamaProduk;
            @BindView(R.id.ivImageProduk)
            ImageView ivImageProduk;
//            @BindView(R.id.tvQtySewa)
//            TextView tvQtySewa;
            @BindView(R.id.tvLamaSewaProduk)
            TextView tvLamaSewaProduk;
            @BindView(R.id.tvHargaSewaProduk)
            TextView tvHargaSewaProduk;
            String htmlFormat;
            String formatedHarga;
            String formatedLamaHari;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final DSewa data = datas.get(i);
                assert data != null;
                if (data.getBarangDetail().getBarang().getBarangFotoUtama() != null) {
                    Glide.with(itemView).load(data.getBarangDetail().getBarang().getBarangFotoUtama().getFotoUrl()).into(ivImageProduk);
                    tvNamaProduk.setText(data.getBarangDetail().getBarang().getNama());
//                    tvQtySewa.setText(Html.fromHtml(itemView.getResources().getString(R.string.qty_, String.valueOf(data.getBarangDetail().getdSewas().size()))));
                    tvLamaSewaProduk.setText(Html.fromHtml(itemView.getResources().getString(R.string.lama_hari_, String.valueOf(data.getLamaSewa().toString()))));
                    tvHargaSewaProduk.setText(Html.fromHtml(itemView.getResources().getString(R.string.harga_satuan_, getDecimalFormattedString(String.valueOf(data.getHargaSewa())))));
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClickView(data, view);
                    }
                });
            }
        }
    }
}
