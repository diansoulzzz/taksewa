package com.taksewa.taksewa.ui.f_home;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.synnapps.carouselview.ImageListener;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Carousel;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_image_preview.ImagePreviewActivity;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistActivity;
import com.taksewa.taksewa.ui.a_message_list.MessageListActivity;
import com.taksewa.taksewa.ui.a_saldo_history.SaldoHistoryActivity;
import com.taksewa.taksewa.ui.a_saldo_withdraw.SaldoWithdrawActivity;
import com.taksewa.taksewa.ui.a_search.SearchActivity;
import com.taksewa.taksewa.ui.a_search_result.SearchResultActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriActivity;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator2;

public class HomeFragment extends BaseFragment implements HomeMvpView, SwipeRefreshLayout.OnRefreshListener, ImageListener, NestedScrollView.OnScrollChangeListener, HomeFragmentAdapter.CallbackCarousel {

    private final int RQ_HOME_BARANG_KATEGORI_LIHAT_SEMUA = 1;
    private final int RQ_WITHDRAW_SALDO = 2;
    private final int RQ_HISTORY_SALDO = 3;
    private final String TAG = getClass().getSimpleName();

    @Inject
    HomeMvpPresenter<HomeMvpView> mPresenter;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    //    @BindView(R.id.cvPromo)
//    CarouselView cvPromo;
    @BindView(R.id.rvBarangRecommend)
    RecyclerView rvBarangRecommend;
    @BindView(R.id.rvBarangKategori)
    RecyclerView rvBarangKategori;
    @BindView(R.id.clLayout)
    ConstraintLayout clLayout;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.tvLihatSemuaKategori)
    TextView tvLihatSemuaKategori;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.clToolbar)
    ConstraintLayout clToolbar;
    @BindView(R.id.tvSaldo)
    TextView tvSaldo;
    @BindView(R.id.cvSaldo)
    CardView cvSaldo;
    @BindView(R.id.rvCarousel)
    RecyclerView rvCarousel;
    @BindView(R.id.ciCarousel)
    CircleIndicator2 ciCarousel;
    //    @BindView(R.id.ivToolbarBackground)
//    ImageView ivToolbarBackground;
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager linearLayoutManagerImage = new LinearLayoutManager(getViewContext(), LinearLayoutManager.HORIZONTAL, false);
    GridLayoutManager gridLayoutManager;
    HomeFragmentAdapter.AdapterBarang adapterBarang;
    HomeFragmentAdapter.AdapterBarangKategori adapterBarangKategori;
    HomeFragmentAdapter.AdapterCarousel adapterCarousel;

    List<Barang> barangList = new ArrayList<>();
    List<BarangKategori> barangKategoriList = new ArrayList<>();
    List<Carousel> carouselList = new ArrayList<>();
    private Menu menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.onViewPrepared();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
//        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            srlLoader.setProgressViewOffset(false, 0, 250);
        }
        setHasOptionsMenu(true);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolBar);
        nestedScrollView.setOnScrollChangeListener(this);
        srlLoader.setOnRefreshListener(this);

        gridLayoutManager = new GridLayoutManager(getContext(), 2);

        rvBarangRecommend.setLayoutManager(gridLayoutManager);
        gridLayoutManager = new GridLayoutManager(getContext(), 5);
        rvBarangKategori.setLayoutManager(gridLayoutManager);

        adapterCarousel = new HomeFragmentAdapter.AdapterCarousel(getViewContext(), carouselList, this);
        rvCarousel.setLayoutManager(linearLayoutManagerImage);
        rvCarousel.setAdapter(adapterCarousel);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rvCarousel);
        ciCarousel.attachToRecyclerView(rvCarousel, snapHelper);
        adapterCarousel.registerAdapterDataObserver(ciCarousel.getAdapterDataObserver());
        adapterBarang = new HomeFragmentAdapter.AdapterBarang(getContext(), barangList, new HomeFragmentAdapter.CallbackBarang() {
            @Override
            public void onClickBarang(Barang barang) {
                Intent intent = new Intent(getContext(), ViewBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivity(intent);
            }
        });
        rvBarangRecommend.setAdapter(adapterBarang);
        adapterBarangKategori = new HomeFragmentAdapter.AdapterBarangKategori(getContext(), barangKategoriList, new HomeFragmentAdapter.CallbackBarangKategori() {
            @Override
            public void onClickBarangKategori(BarangKategori barangKategori) {
                Intent intent = new Intent(getContext(), SearchResultActivity.class);
                intent.putExtra("kategori", barangKategori);
                startActivity(intent);
            }
        });
        rvBarangKategori.setAdapter(adapterBarangKategori);
    }

    public void setCarouselData(List<Carousel> carouselList) {
        this.carouselList.clear();
        this.carouselList.addAll(carouselList);
        adapterCarousel.notifyDataSetChanged();
//        cvPromo.setImageListener(this);
//        cvPromo.setPageCount(this.carouselList.size());
    }

    public void setAdapterData(List<Barang> barangList, List<BarangKategori> barangKategoriList) {
        this.barangList.clear();
        this.barangList.addAll(barangList);
        this.barangKategoriList.clear();
        this.barangKategoriList.addAll(barangKategoriList);
        adapterBarang.notifyDataSetChanged();
        adapterBarangKategori.notifyDataSetChanged();
        removeWait();
    }

    public void removeWait() {
        progressBar.hide();
        clLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.toolbar_home, menu);
        this.menu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefresh() {
        mPresenter.onViewPrepared();
    }

    @Override
    public void setImageForPosition(int position, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(20));

        Glide.with(getContext()).load(carouselList.get(position).getFotoUrl()).apply(requestOptions).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityImagePreview(carouselList.get(position).getFotoUrl(), carouselList.get(position).getKeterangan());
            }
        });
    }

    public void startActivityImagePreview(String filePath, String deskripsi) {
        Intent intent = new Intent(getContext(), ImagePreviewActivity.class);
        intent.putExtra("fotoUrl", filePath);
        intent.putExtra("deskripsi", deskripsi);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_wishlist:
                Intent intent = new Intent(getContext(), MemberWishlistActivity.class);
                startActivity(intent);
                return true;
            case R.id.navigation_notifikasi:
                Intent intent2 = new Intent(getContext(), MessageListActivity.class);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btnSearch)
    void onBtnSearchClick() {
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvLihatSemuaKategori)
    void onTvLihatSemuaKategoriClick() {
        Intent intent = new Intent(getContext(), ViewBarangKategoriActivity.class);
//        intent.putExtra("activity",this);
        startActivityForResult(intent, RQ_HOME_BARANG_KATEGORI_LIHAT_SEMUA);

    }

    @Override
    public void onPresenterReady(HomeResponse.GData homeResponse) {
        setAdapterData(homeResponse.getBarangList(), homeResponse.getBarangKategoriList());
        setCarouselData(homeResponse.getCarouselList());
        if (homeResponse.getUser() != null) {
            cvSaldo.setVisibility(View.VISIBLE);
            tvSaldo.setText(NumberTextWatcher.getDecimalFormattedString(homeResponse.getUser().getTotalSaldo().toString()));
        }
        srlLoader.setRefreshing(false);
    }

    @OnClick(R.id.cvSaldo)
    void onClickCvSaldo() {
        Intent intent = new Intent(getViewContext(), SaldoHistoryActivity.class);
        startActivityForResult(intent, RQ_HISTORY_SALDO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RQ_HOME_BARANG_KATEGORI_LIHAT_SEMUA) {
                assert data != null;
                BarangSubKategori barangSubKategori = data.getParcelableExtra("barangSubKategori");
                Intent intent = new Intent(getContext(), SearchResultActivity.class);
                intent.putExtra("subKategori", barangSubKategori);
                startActivity(intent);
            }
            if (requestCode == RQ_WITHDRAW_SALDO) {
                assert data != null;
                mPresenter.onViewPrepared();
            }
        }
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        int colorWhite = Color.WHITE; // ideally a global variable
        int colorPrimaryDark = getResources().getColor(R.color.colorPrimaryDark);// ideally a global variable
        int colorSilver = getResources().getColor(R.color.lightSilverColor);// ideally a global variable
        int minHeight = 500;
        if (menu != null) {
            if (scrollY < minHeight) {
                changeToolbarIconColor(colorWhite);
                clToolbar.setBackgroundColor(Color.TRANSPARENT);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
//                }
            } else if (scrollY > minHeight) {
                clToolbar.setBackgroundColor(getColorAlphaFromScroll(colorWhite, scrollY - minHeight));
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    getActivity().getWindow().setStatusBarColor(getColorAlphaFromScroll(colorPrimaryDark, scrollY - minHeight));
//                }
                changeToolbarIconColor(colorSilver);
            }
        }
        Log.d(TAG, "scroll :" + scrollY);
    }

    public static int getColorAlphaFromScroll(int color, int scrollY) {
        int newCol = color;
        if ((scrollY < 256) && (scrollY > 0)) {
            int alpha = (scrollY << 24) | (-1 >>> 8);
            newCol &= (alpha);
        }
        return newCol;
    }

    public void changeToolbarIconColor(int color) {
        for (int i = 0; i <= menu.size() - 1; i++) {
            menu.getItem(i).getIcon().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }

    public static int getColorWithAlpha(int color, float ratio) {
        int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);
        return newColor;
    }

    @OnClick(R.id.ivWithdraw)
    void onClickIvWithdraw() {
        Intent intent = new Intent(getViewContext(), SaldoWithdrawActivity.class);
        startActivityForResult(intent, RQ_WITHDRAW_SALDO);
    }

    @Override
    public void onClickCarousel(Carousel carousel) {
        startActivityImagePreview(carousel.getFotoUrl(), carousel.getKeterangan());
    }
}