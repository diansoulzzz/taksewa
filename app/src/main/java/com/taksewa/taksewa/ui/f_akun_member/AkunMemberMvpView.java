package com.taksewa.taksewa.ui.f_akun_member;

import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.MvpView;

public interface AkunMemberMvpView extends MvpView {
    void onPresenterReady(User user);
}
