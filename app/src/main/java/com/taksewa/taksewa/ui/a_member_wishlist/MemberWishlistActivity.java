package com.taksewa.taksewa.ui.a_member_wishlist;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriAdapter;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberWishlistActivity extends BaseActivity implements MemberWishlistMvpView {

    private final String TAG = getClass().getSimpleName();

    @Inject
    MemberWishlistMvpPresenter<MemberWishlistMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;

    @BindView(R.id.rvBarang)
    RecyclerView rvBarang;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    MemberWishlistAdapter.AdapterList memberWishlistBarangAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<Barang> barangs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_wishlist);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (!mPresenter.isUserLogin()) {
            startLoginActivity();
            finish();
            return;
        }
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        memberWishlistBarangAdapter = new MemberWishlistAdapter.AdapterList(this, barangs, new MemberWishlistAdapter.Callback() {
            @Override
            public void onClickView(Barang barang, View itemView) {
                Intent intent = new Intent(getBaseContext(), ViewBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivity(intent);
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarang.setLayoutManager(gridLayoutManager);
        rvBarang.setAdapter(memberWishlistBarangAdapter);
        mPresenter.onViewPrepared();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvBarang.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBarang.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(BarangResponse.IList barangResponse) {
        barangs.clear();
        barangs.addAll(barangResponse.getData());
        memberWishlistBarangAdapter.notifyDataSetChanged();
        onRemoveWait();
    }

}

