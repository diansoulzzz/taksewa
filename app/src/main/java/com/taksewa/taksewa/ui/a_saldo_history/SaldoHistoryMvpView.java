package com.taksewa.taksewa.ui.a_saldo_history;

import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface SaldoHistoryMvpView extends MvpView {

    void onPresenterReady(UserResponse.IData response);
}
