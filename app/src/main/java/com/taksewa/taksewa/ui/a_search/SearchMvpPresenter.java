package com.taksewa.taksewa.ui.a_search;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_register.RegisterMvpView;

public interface SearchMvpPresenter<V extends SearchMvpView> extends MvpPresenter<V> {

}

