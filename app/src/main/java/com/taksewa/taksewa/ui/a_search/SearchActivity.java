package com.taksewa.taksewa.ui.a_search;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.MenuItem;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_search_result.SearchResultActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends BaseActivity implements SearchMvpView, SearchView.OnQueryTextListener {

    @Inject
    SearchMvpPresenter<SearchMvpView> mPresenter;

    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.toolBar)
    Toolbar toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (getIntent().hasExtra("queryString")){
            searchView.setQuery(getIntent().getStringExtra("queryString"),true);
        }
        mPresenter.onAttach(this);
        searchView.setOnQueryTextListener(this);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon action bar is clicked; go to parent activity
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        if (!isValid()) {
            return false;
        }
        Intent intent = new Intent(getApplicationContext(), SearchResultActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("queryString", s);
        startActivity(intent);
        finish();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    boolean isValid() {
        boolean isvalid = true;
        if (TextUtils.isEmpty(searchView.getQuery())) {
            isvalid = false;
        }
        return isvalid;
    }
}
