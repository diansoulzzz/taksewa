package com.taksewa.taksewa.ui.a_maps_result_barang;


import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_main.MainMvpPresenter;
import com.taksewa.taksewa.ui.a_main.MainMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class MapsResultBarangPresenter<V extends MapsResultBarangMvpView> extends BasePresenter<V>
        implements MapsResultBarangMvpPresenter<V> {
    @Inject
    public MapsResultBarangPresenter(DataManager dataManager,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }
}
