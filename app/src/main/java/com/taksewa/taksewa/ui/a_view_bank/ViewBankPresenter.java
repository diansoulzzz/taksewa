package com.taksewa.taksewa.ui.a_view_bank;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.BankResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpPresenter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ViewBankPresenter<V extends ViewBankMvpView> extends BasePresenter<V>
        implements ViewBankMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public ViewBankPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        DoGetKecamatanList();
    }

    private void DoGetKecamatanList(){
        getCompositeDisposable().add(getDataManager()
                .getBankList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BankResponse.IList>() {
                    @Override
                    public void accept(@NonNull BankResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

}

