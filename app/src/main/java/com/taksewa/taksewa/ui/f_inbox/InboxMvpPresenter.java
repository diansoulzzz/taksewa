package com.taksewa.taksewa.ui.f_inbox;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_home.HomeMvpView;

public interface InboxMvpPresenter<V extends InboxMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

