package com.taksewa.taksewa.ui.a_saldo_withdraw;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.model.Bank;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankActivity;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.utils.CommonUtils.createAlertDialogBuilder;

public class SaldoWithdrawActivity extends BaseActivity implements SaldoWithdrawMvpView {

    public final String TAG = getClass().getSimpleName();
    private static final int VIEW_BANK_RQ = 1;
    @Inject
    SaldoWithdrawMvpPresenter<SaldoWithdrawMvpView> mPresenter;

    @BindView(R.id.tvSaldo)
    TextView tvSaldo;
    @BindView(R.id.tietNominal)
    TextInputEditText tietNominal;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.tietBank)
    TextInputEditText tietBank;
    @BindView(R.id.tietNomorRekening)
    TextInputEditText tietNomorRekening;
    User user;
    Bank bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo_withdraw);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NumberTextWatcher.CallbackWatcher callbackWatcher = new NumberTextWatcher.CallbackWatcher() {
            @Override
            public void onAfterTextFinish(String textAfter) {

            }
        };
        NumberTextWatcher numberTextWatcher = new NumberTextWatcher(tietNominal, callbackWatcher);
        tietNominal.addTextChangedListener(numberTextWatcher);
        mPresenter.onViewPrepared();
    }

    @Override
    public void onPresenterReady(UserResponse.IData response) {
        user = response.getData();
        if (user.getTotalSaldo() == null) return;
        tvSaldo.setText(String.format("Rp %s", NumberTextWatcher.getDecimalFormattedString(user.getTotalSaldo().toString())));
    }

    @Override
    public void onSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.tietBank)
    void onClickTietBank() {
        Intent intent = new Intent(this, ViewBankActivity.class);
        startActivityForResult(intent, VIEW_BANK_RQ);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == VIEW_BANK_RQ) {
                bank = data.getParcelableExtra("bank");
                tietBank.setText(String.format("%s", bank.getNama()));
            }
        }
    }

    DialogInterface.OnClickListener dialogListenerTarikSaldo = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    mPresenter.onWithdrawClick(new UserRequest.SaldoWithdraw(bank.getId(), tietNomorRekening.getText().toString(), Integer.valueOf(NumberTextWatcher.trimCommaOfString(tietNominal.getText().toString()))));
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @OnClick(R.id.btnWithdraw)
    void onClickBtnWithdraw() {
        createAlertDialogBuilder(getViewContext(), getString(R.string.tarik_saldo), getString(R.string.anda_yakin_tarik_saldo_anda), getString(R.string.yakin), getString(R.string.tidak), dialogListenerTarikSaldo).show();
    }
}
