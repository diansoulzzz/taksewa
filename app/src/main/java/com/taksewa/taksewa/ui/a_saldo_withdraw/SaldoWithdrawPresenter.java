package com.taksewa.taksewa.ui.a_saldo_withdraw;


import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class SaldoWithdrawPresenter<V extends SaldoWithdrawMvpView> extends BasePresenter<V>
        implements SaldoWithdrawMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public SaldoWithdrawPresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }

    @Override
    public void onViewPrepared() {
        DoGetProfileInfo();
    }

    @Override
    public void onWithdrawClick(UserRequest.SaldoWithdraw saldoWithdraw) {
        getCompositeDisposable().add(getDataManager()
                .postSaldoWithdraw(saldoWithdraw)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HomeResponse.GData>() {
                    @Override
                    public void accept(@NonNull HomeResponse.GData data) throws Exception {
                        getMvpView().onSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    public void DoGetProfileInfo() {
        getProfileInfo(response -> {
            getMvpView().onPresenterReady(response);
        }, throwable -> {
            Log.d(TAG, "fail");
            setUserAsLoggedOut();
            if (!isViewAttached()) {
                return;
            }
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        });
    }
}
