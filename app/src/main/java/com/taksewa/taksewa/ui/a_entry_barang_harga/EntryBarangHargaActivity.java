package com.taksewa.taksewa.ui.a_entry_barang_harga;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangAdapter;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EntryBarangHargaActivity extends BaseActivity implements EntryBarangHargaMvpView {

    private final String TAG = getClass().getSimpleName();

    @Inject
    EntryBarangHargaMvpPresenter<EntryBarangHargaMvpView> mPresenter;

    ArrayList<BarangHargaSewa> barangHargaSewas = new ArrayList<>();
    //    BarangHargaSewa defaultHargaSewa = new BarangHargaSewa();
    BarangHargaSewa barangHargaSewa;
    EntryBarangHargaAdapter.AdapterEntry entryBarangHargaAdapter;
    GridLayoutManager gridLayoutManager;
    @BindView(R.id.rvBarangHarga)
    RecyclerView rvBarangHarga;
    @BindView(R.id.toolBar)
    Toolbar toolbar;
    @BindView(R.id.tvErrorList)
    TextView tvErrorList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_barang_harga);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (getIntent().hasExtra("barangHargaSewas")) {
            barangHargaSewas.clear();
            barangHargaSewas = getIntent().getParcelableArrayListExtra("barangHargaSewas");
        }
        setSupportActionBar(toolbar);
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarangHarga.setLayoutManager(gridLayoutManager);
        entryBarangHargaAdapter = new EntryBarangHargaAdapter.AdapterEntry(getBaseContext(), barangHargaSewas, new EntryBarangHargaAdapter.CallbackImage() {
            @Override
            public void onAfterTextFinishLamaSewa(BarangHargaSewa barangHargaSewa, View itemView, String lamaHari, TextInputEditText tietLamaSewaBarang) {
//                barangHargaSewa.setLamaHari(Integer.valueOf(NumberTextWatcher.trimCommaOfString(lamaHari)));
            }

            @Override
            public void onAfterTextFinishHargaSewa(BarangHargaSewa barangHargaSewa, View itemView, String hargaSewa, TextInputEditText tietHargaSewaBarang) {
//                barangHargaSewa.setHarga(Integer.valueOf(NumberTextWatcher.trimCommaOfString(hargaSewa)));
            }

            @Override
            public void onClickBtnRemove(BarangHargaSewa barangHargaSewa, View itemView) {
//                if (barangHargaSewa.equals(defaultHargaSewa)){
//                    showMessage("Lama Hari 1 Wajib ada!");
//                    return;
//                }
                if (barangHargaSewas.size() > 1) {
                    barangHargaSewas.remove(barangHargaSewa);
                    entryBarangHargaAdapter.notifyDataSetChanged();
                } else {
                    showMessage(R.string.error_lama_harga_min);
                }
            }
        });
        entryBarangHargaAdapter.setHasStableIds(true);
        rvBarangHarga.setAdapter(entryBarangHargaAdapter);
        if (barangHargaSewas.size() < 1) {
            addHargaRow();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_entry_harga, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.navigation_next:
//                for (BarangHargaSewa barangHargaSewa : barangHargaSewas) {
//                    Log.d(TAG, barangHargaSewa.getId().toString());
//                    Log.d(TAG, barangHargaSewa.getBarangId().toString());
//                    Log.d(TAG, barangHargaSewa.getLamaHari().toString());
//                    Log.d(TAG, barangHargaSewa.getHarga().toString());
//                    Log.d(TAG, barangHargaSewa.getCreatedAt());
//                    Log.d(TAG, barangHargaSewa.getUpdatedAt());
//                    Log.d(TAG, barangHargaSewa.getDeletedAt());
//                }
                if (!isValid()) {
                    return false;
                }
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra("barangHargaSewas", barangHargaSewas);
                setResult(RESULT_OK, intent);
                finish();
//                int listLength = rvBarangHarga.getChildCount();
//                EditText editText;
//                EditText editText2;
//                for (int i = 0; i < listLength; i++)
//                {
//                    editText = rvBarangHarga.getChildAt(i).findViewById(R.id.tietLamaSewaBarang);
//                    editText2 = rvBarangHarga.getChildAt(i).findViewById(R.id.tietHargaSewaBarang);
//                    Log.d(TAG, editText.getText().toString());
//                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isValid() {
        boolean valid = true;
        tvErrorList.setVisibility(View.GONE);
        ArrayList<String> errorList = new ArrayList<>();
        if (barangHargaSewas.size() < 1) {
            errorList.add(getResources().getString(R.string.error_lama_harga_min));
            valid = false;
        }
        boolean hasOneDay = false;
        boolean zeroDay = false;
        boolean zeroPrice = false;
        for (BarangHargaSewa barangHargaSewa : barangHargaSewas) {
            if (barangHargaSewa.getLamaHari() == 1) {
                hasOneDay = true;
            }
            if (barangHargaSewa.getLamaHari() < 1) {
                zeroDay = true;
            }
            if (barangHargaSewa.getHarga() < 100) {
                zeroPrice = true;
            }
        }
        if (zeroDay) {
            errorList.add(getResources().getString(R.string.error_lama_hari_min));
            valid = false;
        }
        if (zeroPrice) {
            errorList.add(getResources().getString(R.string.error_harga_sewa_min));
            valid = false;
        }
        if (!hasOneDay) {
            errorList.add(getResources().getString(R.string.error_lama_hari_satu_empty));
            valid = false;
        }
        if (!valid) {
            StringBuilder groupedError = new StringBuilder();
            groupedError.append(getResources().getString(R.string.error_header));
            for (String error : errorList) {
                groupedError.append(error).append("\n");
            }
            tvErrorList.setText(groupedError);
            tvErrorList.setVisibility(View.VISIBLE);
        }
        return valid;
    }

    @OnClick(R.id.btnAddHarga)
    void onBtnAddHargaClick() {
        addHargaRow();
    }

    public void addHargaRow() {
        /*
        if (!barangHargaSewas.contains(defaultHargaSewa)) {
            defaultHargaSewa.setId(0);
            defaultHargaSewa.setBarangId(0);
            defaultHargaSewa.setHarga(100);
            defaultHargaSewa.setLamaHari(1);
            defaultHargaSewa.setCreatedAt("");
            defaultHargaSewa.setUpdatedAt("");
            defaultHargaSewa.setDeletedAt("");
            barangHargaSewas.add(defaultHargaSewa);
        } else {
            BarangHargaSewa barangHargaSewa = new BarangHargaSewa();
            barangHargaSewa.setId(barangHargaSewas.size());
            barangHargaSewa.setBarangId(0);
            barangHargaSewa.setHarga(0);
            barangHargaSewa.setLamaHari(0);
            barangHargaSewa.setCreatedAt("");
            barangHargaSewa.setUpdatedAt("");
            barangHargaSewa.setDeletedAt("");
            barangHargaSewas.add(barangHargaSewa);
        }
        */
        barangHargaSewa = new BarangHargaSewa();
//        barangHargaSewa.setId(null);
        if (barangHargaSewas.size()<1){
            barangHargaSewa.setLamaHari(1);
        }
        barangHargaSewa.setHarga(0);
        barangHargaSewa.setBarangId(0);
        barangHargaSewa.setCreatedAt("");
        barangHargaSewa.setUpdatedAt("");
        barangHargaSewa.setDeletedAt("");
        barangHargaSewas.add(barangHargaSewa);
        entryBarangHargaAdapter.notifyDataSetChanged();
    }

}
