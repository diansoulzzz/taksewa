package com.taksewa.taksewa.ui.f_search_result_barang;

import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface SearchResultBarangMvpView extends MvpView {

    void onPresenterReady(SearchResponse.IBarangData searchResponse);
}
