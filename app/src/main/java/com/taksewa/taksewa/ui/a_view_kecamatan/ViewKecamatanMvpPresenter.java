package com.taksewa.taksewa.ui.a_view_kecamatan;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpView;

public interface ViewKecamatanMvpPresenter<V extends ViewKecamatanMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

