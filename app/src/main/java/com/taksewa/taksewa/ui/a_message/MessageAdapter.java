package com.taksewa.taksewa.ui.a_message;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.ui._base.BaseViewHolder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.utils.CommonUtils.getCurrentDate;
import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getDiffDateFromToday;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class MessageAdapter {

    public interface Callback {
        void onClickView();
        void onClickViewBarang(Barang barang);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {
        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<Message> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_message, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new MessageAdapter.AdapterList.VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvMessageIn)
            TextView tvMessageIn;
            @BindView(R.id.tvMessageOut)
            TextView tvMessageOut;
            @BindView(R.id.tvMessageInTime)
            TextView tvMessageInTime;
            @BindView(R.id.tvMessageOutTime)
            TextView tvMessageOutTime;
            @BindView(R.id.tvMessageDate)
            TextView tvMessageDate;
            @BindView(R.id.vSpan)
            View vSpan;
            @BindView(R.id.clMessageCustomIn)
            ConstraintLayout clMessageCustomIn;
            @BindView(R.id.clMessageCustomOut)
            ConstraintLayout clMessageCustomOut;
            @BindView(R.id.ivImageCustomIn)
            ImageView ivImageCustomIn;
            @BindView(R.id.ivImageCustomOut)
            ImageView ivImageCustomOut;
            @BindView(R.id.tvTextCustomIn)
            TextView tvTextCustomIn;
            @BindView(R.id.tvTextCustomOut)
            TextView tvTextCustomOut;

            Message data;
            Barang barang;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            @OnClick(R.id.clMessageCustomIn)
            void onClMessageCustomInClick() {
                if (barang != null) {
                    callback.onClickViewBarang(barang);
                }
            }

            @OnClick(R.id.clMessageCustomOut)
            void onClMessageCustomOutClick() {
                if (barang != null) {
                    callback.onClickViewBarang(barang);
                }
            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                if (data.getIsSender() != 1) {
                    tvMessageOut.setVisibility(View.GONE);
                    tvMessageOutTime.setVisibility(View.GONE);

                    tvMessageIn.setVisibility(View.VISIBLE);
                    tvMessageInTime.setVisibility(View.VISIBLE);
                    if (data.getValue() != null) {
                        if (data.getType().equals("barang")) {
                            tvMessageIn.setVisibility(View.GONE);
                            Gson gson = new Gson();
                            barang = gson.fromJson(data.getValue(), Barang.class);
//                            Log.d(TAG, barang.getNama());
                            clMessageCustomIn.setVisibility(View.VISIBLE);
                            Glide.with(itemView).load(barang.getBarangFotoUtama().getFotoUrl()).into(ivImageCustomIn);
                            tvTextCustomIn.setText(barang.getNama());
                        } else {
                            tvMessageIn.setVisibility(View.VISIBLE);
                            clMessageCustomIn.setVisibility(View.GONE);
                            tvMessageIn.setText(data.getValue());
                        }
                        tvMessageInTime.setText(getStringDateFormat(getDateParsed(data.getCreatedAt()), "HH:mm"));
                    }
                } else {
                    tvMessageOut.setVisibility(View.VISIBLE);
                    tvMessageOutTime.setVisibility(View.VISIBLE);
                    tvMessageIn.setVisibility(View.GONE);
                    tvMessageInTime.setVisibility(View.GONE);
                    if (data.getValue() != null) {
                        if (data.getType().equals("barang")) {
                            tvMessageOut.setVisibility(View.GONE);
                            Gson gson = new Gson();
                            barang = gson.fromJson(data.getValue(), Barang.class);
//                            Log.d(TAG, barang.getNama());
                            clMessageCustomOut.setVisibility(View.VISIBLE);
                            Glide.with(itemView).load(barang.getBarangFotoUtama().getFotoUrl()).into(ivImageCustomOut);
                            tvTextCustomOut.setText(barang.getNama());
                        } else {
                            tvMessageOut.setVisibility(View.VISIBLE);
                            clMessageCustomOut.setVisibility(View.GONE);
                            tvMessageOut.setText(data.getValue());
                        }
                        tvMessageOutTime.setText(getStringDateFormat(getDateParsed(data.getCreatedAt()), "HH:mm"));
                    }
                }
                if (data.getIsShowDate() != null) {
                    if (data.getIsShowDate() == 1) {
                        Log.d(TAG, getStringDateFormat(getDateParsed(data.getCreatedAt()), "dd MMM y"));
                        Log.d(TAG, getStringDateFormat(getCurrentDate(), "dd MMM y"));
                        if (getStringDateFormat(getDateParsed(data.getCreatedAt()), "dd MMM y").equals(getStringDateFormat(getCurrentDate(), "dd MMM y"))) {
                            tvMessageDate.setText(itemView.getResources().getString(R.string.hari_ini));
                        } else {
                            if (getStringDateFormat(getDateParsed(data.getCreatedAt()), "dd MMM y").equals(getStringDateFormat(getDiffDateFromToday(-1), "dd MMM y"))) {
                                tvMessageDate.setText(itemView.getResources().getString(R.string.kemarin));
                            } else {
                                tvMessageDate.setText(getStringDateFormat(getDateParsed(data.getCreatedAt()), "dd MMM y"));
                            }
                        }
                        tvMessageDate.setVisibility(View.VISIBLE);
                        vSpan.setVisibility(View.VISIBLE);
                    } else {
                        tvMessageDate.setVisibility(View.GONE);
                        vSpan.setVisibility(View.GONE);
                    }
                }
            }

        }
    }
}
