package com.taksewa.taksewa.ui.a_member_wishlist;

import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface MemberWishlistMvpView extends MvpView {
    void onPresenterReady(BarangResponse.IList barangResponse);
}
