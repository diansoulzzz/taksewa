package com.taksewa.taksewa.ui.a_message_list;

import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface MessageListMvpView extends MvpView {
    void onPresenterReady(MessageResponse.IRoom messageResponseRoom);
}
