package com.taksewa.taksewa.ui.a_entry_barang;

import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.ui._base.MvpView;

public interface EntryBarangMvpView extends MvpView {
    void onFileUploadSuccess(BarangFoto barangFoto);

    void onFileUploadFailed(BarangFoto barangFoto);

    void onFileUploadProgress(BarangFoto barangFoto);

    void onFileDeleteSuccess(BarangFoto barangFoto);

    void onEntrySuccess(Barang barang);

    void onPresenterReady(BarangResponse.IData barangResponse);
}
