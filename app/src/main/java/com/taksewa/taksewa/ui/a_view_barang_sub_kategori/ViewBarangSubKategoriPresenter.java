package com.taksewa.taksewa.ui.a_view_barang_sub_kategori;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.BarangSubKategoriResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ViewBarangSubKategoriPresenter<V extends ViewBarangSubKategoriMvpView> extends BasePresenter<V>
        implements ViewBarangSubKategoriMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public ViewBarangSubKategoriPresenter(DataManager dataManager,
                                          SchedulerProvider schedulerProvider,
                                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
//        getMvpView().onPresenterReady();
        getBarangSubKategoriList();
    }

    public void getBarangSubKategoriList() {
        getCompositeDisposable().add(getDataManager()
                .getBarangSubKategoriList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangSubKategoriResponse.IList>() {
                    @Override
                    public void accept(@NonNull BarangSubKategoriResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

