package com.taksewa.taksewa.ui.a_view_barang_sub_kategori;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangSubKategoriResponse;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.ui._base.BaseActivity;
import java.util.ArrayList;

import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewBarangSubKategoriActivity extends BaseActivity implements ViewBarangSubKategoriMvpView {

    @Inject
    ViewBarangSubKategoriMvpPresenter<ViewBarangSubKategoriMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvBarangSubKategori)
    RecyclerView rvBarangSubKategori;
    @BindView(R.id.etFilter)
    EditText etFilter;

//    @BindView(R.id.progressBar)
//    ContentLoadingProgressBar progressBar;

    ViewBarangSubKategoriAdapter.AdapterEntry viewBarangSubKategoriAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<BarangSubKategori> barangSubKategoris = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_barang_sub_kategori);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
//        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("barangSubKategori")) {
            barangSubKategoris.clear();
            barangSubKategoris.addAll(getIntent().getParcelableArrayListExtra("barangSubKategori"));
        }
        viewBarangSubKategoriAdapter = new ViewBarangSubKategoriAdapter.AdapterEntry(this, barangSubKategoris, new ViewBarangSubKategoriAdapter.CallbackImage() {
            @Override
            public void onClickView(BarangSubKategori barangSubKategori, View itemView) {
                Intent intent = new Intent();
                intent.putExtra("barangSubKategori",barangSubKategori);
//                intent.putExtra("BarangSubKategoriId", BarangSubKategori.getId().toString());
//                intent.putExtra("BarangSubKategoriNama", BarangSubKategori.getNama());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarangSubKategori.setLayoutManager(gridLayoutManager);
        rvBarangSubKategori.setAdapter(viewBarangSubKategoriAdapter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewBarangSubKategoriAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        mPresenter.onViewPrepared();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
//        progressBar.hide();
        rvBarangSubKategori.setVisibility(View.VISIBLE);
//        clOptions.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBarangSubKategori.setVisibility(View.INVISIBLE);
//        clOptions.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(BarangSubKategoriResponse.IList BarangSubKategoriIListResponse) {
        onRemoveWait();
//        barangSubKategoris.clear();
//        barangSubKategoris.addAll(BarangSubKategoriIListResponse.getData());
//        viewBarangSubKategoriAdapter.notifyDataSetChanged();
    }

}

