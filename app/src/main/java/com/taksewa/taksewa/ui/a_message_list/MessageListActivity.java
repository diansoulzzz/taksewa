package com.taksewa.taksewa.ui.a_message_list;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.messaging.RemoteMessage;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.MessageRoom;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_message.MessageActivity;
import com.taksewa.taksewa.ui.a_message.MessageAdapter;
import com.taksewa.taksewa.ui.a_message.MessageMvpPresenter;
import com.taksewa.taksewa.ui.a_message.MessageMvpView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageListActivity extends BaseActivity implements MessageListMvpView {

    public final String TAG = getClass().getSimpleName();
    public final static int REQ_MESSAGE = 1;
    @Inject
    MessageListMvpPresenter<MessageListMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvMessageList)
    RecyclerView rvMessageList;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    MessageListAdapter.AdapterList adapterList;
    GridLayoutManager gridLayoutManager;
    ArrayList<MessageRoom> messageRooms = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (!mPresenter.isUserLogin()) {
            startLoginActivity();
            finish();
            return;
        }
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        onShowWait();
        gridLayoutManager = new GridLayoutManager(this, 1);
        adapterList = new MessageListAdapter.AdapterList(this, messageRooms, new MessageListAdapter.Callback() {
            @Override
            public void onClickView(MessageRoom messageRoom) {
                Intent intent = new Intent(getBaseContext(), MessageActivity.class);
                intent.putExtra("usersTo", messageRoom.getUserTo());
                startActivityForResult(intent, REQ_MESSAGE);
            }
        });
        rvMessageList.setLayoutManager(gridLayoutManager);
        rvMessageList.setAdapter(adapterList);
        mPresenter.onViewPrepared();
    }

    @Override
    public void onPresenterReady(MessageResponse.IRoom messageResponseRoom) {
        messageRooms.clear();
        messageRooms.addAll(messageResponseRoom.getData());
        adapterList.notifyDataSetChanged();
        onRemoveWait();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mPresenter.onViewPrepared();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();

    }

    public void onRemoveWait() {
        progressBar.hide();
        rvMessageList.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onShowWait() {
        rvMessageList.setVisibility(View.INVISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RemoteMessage remoteMessage) {
        mPresenter.onViewPrepared();
    }
}
