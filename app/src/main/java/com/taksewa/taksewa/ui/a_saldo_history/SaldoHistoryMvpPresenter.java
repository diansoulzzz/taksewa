package com.taksewa.taksewa.ui.a_saldo_history;


import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui._base.MvpPresenter;

@PerActivity
public interface SaldoHistoryMvpPresenter<V extends SaldoHistoryMvpView> extends MvpPresenter<V> {
    void onViewPrepared();
}
