package com.taksewa.taksewa.ui._base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.utils.CommonUtils;

import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements MvpView {

    private BaseActivity mActivity;
    private Unbinder mUnBinder;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

//    @Override
//    public void showLoading() {
//        hideLoading();
//        mProgressDialog = CommonUtils.showLoadingDialog(this.getContext());
//    }

//    @Override
//    public void hideLoading() {
//        if (mProgressDialog != null && mProgressDialog.isShowing()) {
//            mProgressDialog.cancel();
//        }
//    }

    @Override
    public void onError(String message) {
        if (mActivity != null) {
            mActivity.onError(message);
        }
    }

    @Override
    public void onError(@StringRes int resId) {
        if (mActivity != null) {
            mActivity.onError(resId);
        }
    }

    @Override
    public void showOverlayLoading() {
        hideOverlayLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(getContext());
    }

    @Override
    public void hideOverlayLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void showMessage(String message) {
        if (mActivity != null) {
            mActivity.showMessage(message);
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        if (mActivity != null) {
            mActivity.showMessage(resId);
        }
    }

    @Override
    public boolean isNetworkConnected() {
        if (mActivity != null) {
            return mActivity.isNetworkConnected();
        }
        return false;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void hideKeyboard() {
        if (mActivity != null) {
            mActivity.hideKeyboard();
        }
    }

//    @Override
//    public void openActivityOnTokenExpire() {
//        if (mActivity != null) {
//            mActivity.openActivityOnTokenExpire();
//        }
//    }

//    @Override
//    public void openAuthActivity(Integer tab) {
//        if (mActivity != null) {
//            mActivity.openAuthActivity(tab);
//        }
//    }


    @Override
    public void restartApp() {
        if (mActivity != null) {
            mActivity.restartApp();
        }
    }

    @Override
    public void onNeedToVerify() {
        if (mActivity != null) {
            mActivity.onNeedToVerify();
        }
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public GoogleSignInOptions getGoogleSignInOptions() {
        if (mActivity != null) {
            return mActivity.getGoogleSignInOptions();
        }
        return null;
    }

    @Override
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (mActivity != null) {
            mActivity.requestPermissionsSafely(permissions, requestCode);
        }
    }

    @Override
    public boolean hasPermission(String[] permissions) {
        if (mActivity != null) {
            return mActivity.hasPermission(permissions);
        }
        return false;
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    protected abstract void setUp(View view);

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void startVerifyActivity() {
        if (mActivity != null) {
            mActivity.startVerifyActivity();
        }
    }

    @Override
    public void startLoginActivity() {
        if (mActivity != null) {
            mActivity.startLoginActivity();
        }
    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }

    @Override
    public Context getViewContext() {
        if (mActivity != null) {
            return mActivity.getViewContext();
        }
        return null;
    }
}