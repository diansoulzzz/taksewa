package com.taksewa.taksewa.ui.a_view_barang_review;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangAdapter;
import com.taksewa.taksewa.utils.CommonUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class ViewBarangReviewAdapter {

    public interface Callback {
        void onClickView(ReviewBarang reviewBarang, View itemView);

        void onClickVendor(User user, View itemView);

        void onClickImage(FotoReview fotoReview);
    }
    public interface CallbackImage {
        void onClickImage(FotoReview fotoReview, View itemView);
    }

    public static class AdapterImage extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<FotoReview> datas;
        private ViewBarangAdapter.CallbackImage mCallback;
        private String selectedFile;


        public AdapterImage(Context context, List datas, ViewBarangAdapter.CallbackImage mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new AdapterImage.VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_entry_barang_image, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.ivPickerGallery)
            ImageView ivPickerGallery;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final FotoReview data = datas.get(i);
                assert data != null;
                View.OnClickListener listenerFoto = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onClickImage(data, itemView);
                    }
                };
                ivPickerGallery.setOnClickListener(listenerFoto);
                if ((data.getFotoUrl() != null)) {//STATE EDIT
                    Glide.with(itemView).load(data.getFotoUrl()).into(ivPickerGallery);
                }
            }
        }
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        static final int VIEW_TYPE_EMPTY = 0;
        static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<ReviewBarang> datas;
        private ViewBarangReviewAdapter.Callback callback;

        public AdapterList(Context context, List datas, ViewBarangReviewAdapter.Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NotNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review_from_member, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.empty_title_last_seen_produk), Html.fromHtml(parent.getResources().getString(R.string.empty_subtitle_last_seen_produk)).toString());
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (this.datas != null && this.datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            }
            return VIEW_TYPE_EMPTY;
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaUser)
            TextView tvNamaUser;
            @BindView(R.id.ivGambarUser)
            ImageView ivGambarUser;
            @BindView(R.id.tvTgl)
            TextView tvTgl;
            @BindView(R.id.tvUlasanAnda)
            TextView tvUlasanAnda;
            @BindView(R.id.rbStarReview)
            RatingBar rbStarReview;
            @BindView(R.id.clUlasanAnda)
            ConstraintLayout clUlasanAnda;
            @BindView(R.id.clReviewImage)
            ConstraintLayout clReviewImage;
            @BindView(R.id.rvReviewImage)
            RecyclerView rvReviewImage;
            ViewBarangReviewAdapter.AdapterImage adapterImage;
            GridLayoutManager gridLayoutManager;
            ArrayList<FotoReview> fotoReviews = new ArrayList<>();

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final ReviewBarang data = datas.get(i);
                assert data != null;
                if (data.getUser().getNama() != null) {
                    tvNamaUser.setText(data.getUser().getNama());
                }
                if (data.getUser().getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getUser().getFotoUrl()).into(ivGambarUser);
                }
                tvTgl.setText(getStringDateFormat(getDateParsed(data.getCreatedAt())));
                tvUlasanAnda.setText(data.getKomentar());
                rbStarReview.setRating(data.getRating());
                if (data.getFotoReviews() != null){
                    if (data.getFotoReviews().size()>0){
                        clReviewImage.setVisibility(View.VISIBLE);
                    }
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickView(data, itemView);
                    }
                });
                ivGambarUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickVendor(data.getUser(), itemView);
                    }
                });
                fotoReviews.clear();
                fotoReviews.addAll(data.getFotoReviews());
                gridLayoutManager = new GridLayoutManager(context, 5);
                adapterImage = new ViewBarangReviewAdapter.AdapterImage(context, fotoReviews, new ViewBarangAdapter.CallbackImage() {
                    @Override
                    public void onClickImage(FotoReview fotoReview, View itemView) {
                        callback.onClickImage(fotoReview);
                    }
                });
                rvReviewImage.setLayoutManager(gridLayoutManager);
                rvReviewImage.setAdapter(adapterImage);
                adapterImage.notifyDataSetChanged();
            }
        }
    }
}
