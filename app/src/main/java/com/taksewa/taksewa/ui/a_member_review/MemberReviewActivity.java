package com.taksewa.taksewa.ui.a_member_review;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangAdapter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpView;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberReviewActivity extends BaseActivity implements MemberReviewMvpView {

    private final String TAG = getClass().getSimpleName();
    private final int RQ_MEMBER_REVIEW_DETAIL = 1;
    @Inject
    MemberReviewMvpPresenter<MemberReviewMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;

    @BindView(R.id.rvBarang)
    RecyclerView rvBarang;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    MemberReviewAdapter.AdapterList memberReviewAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<HSewa> hSewas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_review);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        memberReviewAdapter = new MemberReviewAdapter.AdapterList(this, hSewas, new MemberReviewAdapter.Callback() {
            @Override
            public void onClickView(HSewa hSewa, View itemView) {
                Intent intent = new Intent(getBaseContext(), MemberReviewDetailActivity.class);
                intent.putExtra("hsewa", hSewa);
                startActivityForResult(intent, RQ_MEMBER_REVIEW_DETAIL);
            }

            @Override
            public void onClickVendor(User user, View itemView) {
//                Intent intent = new Intent(getBaseContext(), MemberReviewDetailActivity.class);
//                intent.putExtra("hsewa", hSewa);
//                startActivityForResult(intent, RQ_MEMBER_REVIEW_DETAIL);
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarang.setLayoutManager(gridLayoutManager);
        rvBarang.setAdapter(memberReviewAdapter);
        mPresenter.onViewPrepared();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvBarang.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBarang.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(HSewaResponse.IList hsewaResponse) {
        hSewas.clear();
        hSewas.addAll(hsewaResponse.getData());
        memberReviewAdapter.notifyDataSetChanged();
        onRemoveWait();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RQ_MEMBER_REVIEW_DETAIL) {
                assert data != null;
                hSewas.clear();
                hSewas.addAll(data.getParcelableArrayListExtra("hsewas"));
                memberReviewAdapter.notifyDataSetChanged();

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }


}

