package com.taksewa.taksewa.ui.f_transaksi_vendor;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class TransaksiVendorPresenter<V extends TransaksiVendorMvpView> extends BasePresenter<V>
        implements TransaksiVendorMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public TransaksiVendorPresenter(DataManager dataManager,
                                    SchedulerProvider schedulerProvider,
                                    CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        DoGetVendorTransactionList();
    }

    public void DoGetVendorTransactionList() {
        getCompositeDisposable().add(getDataManager()
                .getVendorTransactionList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IList>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

