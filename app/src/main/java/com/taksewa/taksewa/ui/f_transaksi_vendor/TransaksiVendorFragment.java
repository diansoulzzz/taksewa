package com.taksewa.taksewa.ui.f_transaksi_vendor;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailActivity;
import com.taksewa.taksewa.ui.a_transaksi_vendor_detail.TransaksiVendorDetailActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransaksiVendorFragment extends BaseFragment implements TransaksiVendorMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final int RQ_TRANSAKSI_VENDOR_DETAIL = 2;
    private final String TAG = getClass().getSimpleName();

    @Inject
    TransaksiVendorPresenter<TransaksiVendorMvpView> mPresenter;

    @BindView(R.id.rvTransaksiVendor)
    RecyclerView rvTransaksiVendor;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.btnFilter)
    Button btnFilter;

    PopupMenu popupMenu;
    //    String title;
    private TransaksiVendorAdapter.AdapterList adapterList;
    private ArrayList<HSewa> hSewas = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;

    public static TransaksiVendorFragment newInstance() {
        TransaksiVendorFragment fragmentFirst = new TransaksiVendorFragment();
//        Bundle args = new Bundle();
//        args.putString("someTitle", title);
//        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        title = getArguments().getString("someTitle");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaksi_vendor, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        srlLoader.setOnRefreshListener(this);
        popupMenu = new PopupMenu(getViewContext(), btnFilter);
        popupMenu.inflate(R.menu.popup_filter_transaksi_vendor);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                btnFilter.setText(item.getTitle());
                switch (item.getItemId()) {
                    case R.id.popup_semua:
                        return true;
                }
                return false;
            }
        });
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        adapterList = new TransaksiVendorAdapter.AdapterList(getContext(), hSewas, new TransaksiVendorAdapter.Callback() {
            @Override
            public void onClickView(HSewa data) {
//                showMessage(data.getStatusTransaksi().toString());
                Intent intent = new Intent(getContext(), TransaksiVendorDetailActivity.class);
                intent.putExtra("hsewa", data);
                startActivityForResult(intent, RQ_TRANSAKSI_VENDOR_DETAIL);
            }
        });
        rvTransaksiVendor.setLayoutManager(gridLayoutManager);
        rvTransaksiVendor.setAdapter(adapterList);
        setHasOptionsMenu(true);
        mPresenter.onViewPrepared();
    }

    @Override
    public void onPresenterReady(HSewaResponse.IList hsewaResponse) {
        hSewas.clear();
        hSewas.addAll(hsewaResponse.getData());
        adapterList.notifyDataSetChanged();
        srlLoader.setRefreshing(false);
        removeWait();
    }

    public void removeWait() {
        progressBar.hide();
        rvTransaksiVendor.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        mPresenter.onViewPrepared();
//        srlLoader.setRefreshing(false);
    }

    @OnClick(R.id.btnFilter)
    void onClickBtnFilter() {
        popupMenu.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RQ_TRANSAKSI_VENDOR_DETAIL) {
                onRefresh();
            }
        }
    }
}