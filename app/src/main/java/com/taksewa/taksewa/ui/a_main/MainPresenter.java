package com.taksewa.taksewa.ui.a_main;


import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {
    @Inject
    public MainPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }
}
