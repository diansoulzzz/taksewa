package com.taksewa.taksewa.ui.a_maps_result_barang;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsResultBarangBottomSheet {
    public interface CallbackList {
        void onClickView(Barang barang);// pass any parameter in your onCallBack which you want to return
    }

    public interface CallbackDetail {
        void onClickView(Barang barang);// pass any parameter in your onCallBack which you want to return
    }

    public static class MapDetail extends BottomSheetDialogFragment {
        public final String TAG = getClass().getSimpleName();
        @BindView(R.id.ivImageProduk)
        ImageView ivImageProduk;

        private Barang barang = new Barang();

        private CallbackDetail callbackDetail;

        public static MapDetail newInstance(Barang barang) {
            MapDetail dialog = new MapDetail();
            Bundle args = new Bundle();
            args.putParcelable("barang", barang);
            dialog.setArguments(args);
            return dialog;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.fragment_map_detail, container, false);
            ButterKnife.bind(this, view);
            setUp(view);
            return view;
        }

        public void setUp(View view) {
            Bundle mArgs = getArguments();
            assert mArgs != null;
            if (mArgs.getParcelable("barang") != null) {
                barang = getArguments().getParcelable("barang");
                Log.d(TAG, barang.getNama());
                Glide.with(this).load(barang.getBarangFotoUtama().getFotoUrl()).into(ivImageProduk);
            }
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (getActivity() instanceof CallbackDetail)
                callbackDetail = (CallbackDetail) getActivity();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return super.onCreateDialog(savedInstanceState);
        }

        public static MapDetail getInstance() {
            return new MapDetail();
        }

        @Override
        public int getTheme() {
            return R.style.BottomSheetDialogTheme;
        }
    }

    public static class MapList extends BottomSheetDialogFragment {
        @BindView(R.id.rvLocationList)
        RecyclerView rvLocationList;

        private ArrayList<Barang> barangs = new ArrayList<>();
        private MapsResultBarangAdapter.AdapterList adapterList;
        private GridLayoutManager gridLayoutManager;

        private CallbackList callbackList;

        public static MapList newInstance(ArrayList<Barang> barangs) {
            MapList dialog = new MapList();
            Bundle args = new Bundle();
            args.putParcelableArrayList("barangs", barangs);
            dialog.setArguments(args);
            return dialog;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.fragment_map_list, container, false);
            ButterKnife.bind(this, view);
            setUp(view);
            return view;
        }

        public void setUp(View view) {
            Bundle mArgs = getArguments();
            assert mArgs != null;
            if (mArgs.getParcelableArrayList("barangs") != null) {
                ArrayList<Barang> brgs = getArguments().getParcelableArrayList("barangs");
                barangs.addAll(brgs);
            }
            gridLayoutManager = new GridLayoutManager(getContext(), 1);
            adapterList = new MapsResultBarangAdapter.AdapterList(getContext(), barangs, new MapsResultBarangAdapter.Callback() {
                @Override
                public void onClickView(Barang data, View itemView) {
                    callbackList.onClickView(data);
                    dismiss();
                }
            });
            rvLocationList.setLayoutManager(gridLayoutManager);
            rvLocationList.setAdapter(adapterList);
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (getActivity() instanceof CallbackList)
                callbackList = (CallbackList) getActivity();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return super.onCreateDialog(savedInstanceState);
        }

        public static MapList getInstance() {
            return new MapList();
        }

        @Override
        public int getTheme() {
            return R.style.BottomSheetDialogTheme;
        }
    }

}
