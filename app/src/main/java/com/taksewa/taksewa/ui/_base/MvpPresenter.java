package com.taksewa.taksewa.ui._base;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.network.response.UserResponse;

import io.reactivex.functions.Consumer;


public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(ANError error);

    void setUserAsLoggedOut();

    boolean isUserLogin();

    boolean isUserVerified();

    void getProfileInfo(Consumer<UserResponse.IData> listenerSuccess, Consumer<Throwable> listenerFailed);

}
