package com.taksewa.taksewa.ui.a_entry_barang_harga;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EntryBarangHargaAdapter {

    public interface CallbackImage {
        void onAfterTextFinishLamaSewa(BarangHargaSewa barangHargaSewa, View itemView, String lamaHari, TextInputEditText tietLamaSewaBarang);

        void onAfterTextFinishHargaSewa(BarangHargaSewa barangHargaSewa, View itemView, String hargaSewa, TextInputEditText tietHargaSewaBarang);

        void onClickBtnRemove(BarangHargaSewa barangHargaSewa, View itemView);
    }

    public static class AdapterEntry extends RecyclerView.Adapter<BaseViewHolder> {
        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<BarangHargaSewa> datas;
        private CallbackImage mCallback;

        public AdapterEntry(Context context, List datas, CallbackImage mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_entry_barang_harga, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tietLamaSewaBarang)
            TextInputEditText tietLamaSewaBarang;
            @BindView(R.id.tietHargaSewaBarang)
            TextInputEditText tietHargaSewaBarang;
            @BindView(R.id.btnRemoveHarga)
            ImageView btnRemoveHarga;

            BarangHargaSewa data;
            String hargaSewa;
            String lamaSewa;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                NumberTextWatcher.CallbackWatcher callbackHarga = new NumberTextWatcher.CallbackWatcher() {
                    @Override
                    public void onAfterTextFinish(String textAfter) {
                        data.setHarga(Integer.valueOf(NumberTextWatcher.trimCommaOfString(textAfter)));
                        mCallback.onAfterTextFinishHargaSewa(data, itemView, textAfter, tietLamaSewaBarang);
                    }
                };
                NumberTextWatcher hargaSewaNumber = new NumberTextWatcher(tietHargaSewaBarang, callbackHarga);
                tietHargaSewaBarang.addTextChangedListener(hargaSewaNumber);

                NumberTextWatcher.CallbackWatcher callbackLamaSewa = new NumberTextWatcher.CallbackWatcher() {
                    @Override
                    public void onAfterTextFinish(String textAfter) {
                        data.setLamaHari(Integer.valueOf(NumberTextWatcher.trimCommaOfString(textAfter)));
                        mCallback.onAfterTextFinishLamaSewa(data, itemView, textAfter, tietLamaSewaBarang);
                    }
                };
                NumberTextWatcher lamaSewaNumber = new NumberTextWatcher(tietLamaSewaBarang, callbackLamaSewa);
                tietLamaSewaBarang.addTextChangedListener(lamaSewaNumber);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                if (data.getHarga() != null) {
                    hargaSewa = data.getHarga().toString();
                    tietHargaSewaBarang.setText(hargaSewa);
                }
                if (data.getLamaHari() != null) {
                    lamaSewa = data.getLamaHari().toString();
                    tietLamaSewaBarang.setText(lamaSewa);
                }
//                tietLamaSewaBarang.setTag(i);
                View.OnClickListener listenerBtnRemove = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        if (tietLamaSewaBarang.getTag().equals(i)){
//                            tietLamaSewaBarang.setText("0");
//                        }
//                        tietLamaSewaBarang.setText("0");
                        mCallback.onClickBtnRemove(data, itemView);
                    }
                };
                btnRemoveHarga.setOnClickListener(listenerBtnRemove);
            }
        }
    }
}
