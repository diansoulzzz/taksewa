package com.taksewa.taksewa.ui.a_saldo_history;


import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SaldoHistoryPresenter<V extends SaldoHistoryMvpView> extends BasePresenter<V>
        implements SaldoHistoryMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public SaldoHistoryPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }

    @Override
    public void onViewPrepared() {
        DoGetProfileInfo();
    }

    public void DoGetProfileInfo() {
        getProfileInfo(response -> {
            getMvpView().onPresenterReady(response);
        }, throwable -> {
            Log.d(TAG, "fail");
            setUserAsLoggedOut();
            if (!isViewAttached()) {
                return;
            }
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        });
    }
}
