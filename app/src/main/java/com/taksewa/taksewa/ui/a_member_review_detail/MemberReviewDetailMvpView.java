package com.taksewa.taksewa.ui.a_member_review_detail;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.ui._base.MvpView;

public interface MemberReviewDetailMvpView extends MvpView {
    void onPresenterReady(HSewaResponse.IData hsewaResponse);

    void onFileUploadSuccess(FotoReview fotoReview);

    void onFileUploadFailed(FotoReview fotoReview);

    void onFileUploadProgress(FotoReview fotoReview);

    void onFileDeleteSuccess(FotoReview fotoReview);

    void onEntrySuccess(HSewaResponse.IList response);

}
