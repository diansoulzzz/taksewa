package com.taksewa.taksewa.ui.a_image_picker;

import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ImagePickerPresenter<V extends ImagePickerMvpView> extends BasePresenter<V>
        implements ImagePickerMvpPresenter<V> {
    @Inject
    public ImagePickerPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
}

