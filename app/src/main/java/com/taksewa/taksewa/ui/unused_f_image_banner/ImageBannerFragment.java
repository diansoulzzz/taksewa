package com.taksewa.taksewa.ui.unused_f_image_banner;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;

import java.util.List;

public class ImageBannerFragment extends Fragment {
    public static ImageBannerFragment newInstance(int page, List<Barang> barangList) {
        ImageBannerFragment fragmentFirst = new ImageBannerFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_akun_member, container, false);
        return view;
    }
}
