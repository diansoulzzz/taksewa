package com.taksewa.taksewa.ui.a_maps_result_barang;


import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_main.MainMvpView;

@PerActivity
public interface MapsResultBarangMvpPresenter<V extends MapsResultBarangMvpView> extends MvpPresenter<V> {
//    boolean isUserLogin();
}
