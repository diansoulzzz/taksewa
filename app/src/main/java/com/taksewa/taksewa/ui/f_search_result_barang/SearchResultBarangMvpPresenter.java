package com.taksewa.taksewa.ui.f_search_result_barang;

import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.ui._base.MvpPresenter;

public interface SearchResultBarangMvpPresenter<V extends SearchResultBarangMvpView> extends MvpPresenter<V> {

    void onViewPrepared(String queryString, int sortBy, Filter filter);
}

