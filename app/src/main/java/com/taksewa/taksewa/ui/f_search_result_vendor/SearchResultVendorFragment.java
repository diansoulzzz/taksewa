package com.taksewa.taksewa.ui.f_search_result_vendor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_RELEVANCE;

public class SearchResultVendorFragment extends BaseFragment implements SearchResultVendorMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = getClass().getSimpleName();

    @Inject
    SearchResultVendorPresenter<SearchResultVendorMvpView> mPresenter;

    @BindView(R.id.rvResultVendor)
    RecyclerView rvResultVendor;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    private SearchResultVendorAdapter.AdapterList adapterList;
    private ArrayList<User> users = new ArrayList<>();
    private GridLayoutManager gridLayoutManager;
    private String queryString;
    private Filter filter = new Filter();
    private int sortBy = SORT_BY_RELEVANCE;

    public static SearchResultVendorFragment newInstance(String queryString) {
        SearchResultVendorFragment fragment = new SearchResultVendorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        args.putString("queryString", queryString);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queryString = getArguments().getString("queryString");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_result_vendor, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        srlLoader.setOnRefreshListener(this);
        gridLayoutManager = new GridLayoutManager(getContext(), 1);
        adapterList = new SearchResultVendorAdapter.AdapterList(getContext(), users, new SearchResultVendorAdapter.Callback() {
            @Override
            public void onClickView() {

            }
        }, queryString);
        rvResultVendor.setLayoutManager(gridLayoutManager);
        rvResultVendor.setAdapter(adapterList);
        setHasOptionsMenu(true);
        mPresenter.onViewPrepared(queryString, sortBy, filter);
    }

    public void onRemoveWait() {
        srlLoader.setRefreshing(false);
        progressBar.hide();
        rvResultVendor.setVisibility(View.VISIBLE);
//        clOptions.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvResultVendor.setVisibility(View.INVISIBLE);
//        clOptions.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRefresh() {
        onShowWait();
        mPresenter.onViewPrepared(queryString, sortBy, filter);
    }

    @Override
    public void onPresenterReady(SearchResponse.IVendorData searchResponse) {
        users.clear();
        users.addAll(searchResponse.getData().getUsers());
        if (users.size() > 0) {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 1);
        }
        rvResultVendor.setLayoutManager(gridLayoutManager);
        adapterList.notifyDataSetChanged();
        onRemoveWait();
    }
}
