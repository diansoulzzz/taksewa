package com.taksewa.taksewa.ui.a_image_preview;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;

import java.io.File;

import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagePreviewActivity extends BaseActivity {

    @BindView(R.id.ivImagePreview)
    ImageView ivImagePreview;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.etDeskripsi)
    TextView etDeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("deskripsi")) {
            String deskripsi = getIntent().getStringExtra("deskripsi");
            etDeskripsi.setVisibility(View.VISIBLE);
            etDeskripsi.setText(deskripsi);
        }
        if (getIntent().hasExtra("fotoUrl")) {
            String fotoUrl = getIntent().getStringExtra("fotoUrl");
            Glide.with(this).load(fotoUrl).into(ivImagePreview);
            return;
        }
        if (getIntent().hasExtra("filePath")) {
            String filePath = getIntent().getStringExtra("filePath");
            Glide.with(this).load((new File(filePath))).into(ivImagePreview);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
