package com.taksewa.taksewa.ui.a_image_picker.gallery;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_image_picker.camera.ImagePickerCameraFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.CommonUtils.getImagePaths;

public class ImagePickerGalleryFragment extends BaseFragment {

    private final String TAG = getClass().getSimpleName();

    String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;

    @BindView(R.id.rvImagePickerGallery)
    RecyclerView rvImagePickerGallery;
    @BindView(R.id.toolBar)
    Toolbar toolbar;

    final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED};

    ImagePickerGalleryAdapter.AdapterGallery adapterGallery;
    GridLayoutManager gridLayoutManager;
    ArrayList<String> selectedImg = new ArrayList<>();
    Integer totalParentImage = 0;

    private OnImagePickerGalleryListener mCallback;

    public interface OnImagePickerGalleryListener {
        void OnImagePickerGalleryPath(ArrayList<String> arrayFilePath);
    }

    public void setOnImagePickerGalleryListener(OnImagePickerGalleryListener mCallback) {
        this.mCallback = mCallback;
    }

    public static ImagePickerGalleryFragment newInstance(Integer totalParentImage) {
        ImagePickerGalleryFragment fragmentFirst = new ImagePickerGalleryFragment();
        Bundle args = new Bundle();
        args.putInt("totalParentImage", totalParentImage);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        totalParentImage = getArguments().getInt("totalParentImage", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_picker_gallery, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            setUnBinder(ButterKnife.bind(this, view));
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        if (!hasPermission(permissions)) {
            requestPermissionsSafely(permissions, REQUEST_PERMISSIONS_REQUEST_CODE);
            return;
        }
        setHasOptionsMenu(true);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        setAdapterGallery();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.toolbar_image_picker_gallery, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_next:
                if (selectedImg.size() > 0) {
                    ArrayList<String> arrayFilePath = new ArrayList<>(selectedImg);
                    mCallback.OnImagePickerGalleryPath(arrayFilePath);
//                    Log.d(TAG, imgUrl);
//                    if (filePath != null) {
//                        mCallback.onCameraPickerImagePath(filePath);
//                    }
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setAdapterGallery() {
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
        rvImagePickerGallery.setLayoutManager(gridLayoutManager);

        final Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, MediaStore.Images.Media.DATE_ADDED + " DESC");
        final List<String> imagePaths = getImagePaths(cursor);
        adapterGallery = new ImagePickerGalleryAdapter.AdapterGallery(getContext(), imagePaths, new ImagePickerGalleryAdapter.CallbackGallery() {
            @Override
            public void onClickView(String imageGalleryPath, AppCompatCheckBox checkBox) {
                if (!checkBox.isChecked()) {
                    selectedImg.remove(imageGalleryPath);
                    totalParentImage--;
                    return;
                }
                if (selectedImg.size() > 4 || totalParentImage > 4) {
                    checkBox.setChecked(false);
                    showMessage("Gambar Max 5!");
                    return;
                }
                selectedImg.add(imageGalleryPath);
                totalParentImage++;
            }
        });
        rvImagePickerGallery.setAdapter(adapterGallery);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
