package com.taksewa.taksewa.ui.f_akun_member;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_akun.AkunMvpView;

public interface AkunMemberMvpPresenter<V extends AkunMemberMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

