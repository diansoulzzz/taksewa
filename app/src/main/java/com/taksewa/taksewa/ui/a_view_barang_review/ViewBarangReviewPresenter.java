package com.taksewa.taksewa.ui.a_view_barang_review;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ViewBarangReviewPresenter<V extends ViewBarangReviewMvpView> extends BasePresenter<V>
        implements ViewBarangReviewMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    private BarangRequest.FromID fromID;

    @Inject
    public ViewBarangReviewPresenter(DataManager dataManager,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int brgId) {
        fromID = new BarangRequest.FromID();
        fromID.setId(brgId);
        getBarangData();
    }

    public void getBarangData() {
        getCompositeDisposable().add(getDataManager()
                .getBarangDataFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IData>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IData response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }
}

