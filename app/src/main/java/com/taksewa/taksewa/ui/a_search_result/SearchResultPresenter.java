package com.taksewa.taksewa.ui.a_search_result;

import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_search.SearchMvpPresenter;
import com.taksewa.taksewa.ui.a_search.SearchMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SearchResultPresenter<V extends SearchResultMvpView> extends BasePresenter<V>
        implements SearchResultMvpPresenter<V> {
    @Inject
    public SearchResultPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
}

