package com.taksewa.taksewa.ui.a_message_list;


import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.MessageRequest;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_message.MessageMvpPresenter;
import com.taksewa.taksewa.ui.a_message.MessageMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class MessageListPresenter<V extends MessageListMvpView> extends BasePresenter<V>
        implements MessageListMvpPresenter<V> {
    public final String TAG = getClass().getSimpleName();

    @Inject
    public MessageListPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }

    @Override
    public void onViewPrepared() {
        DoGetMessageRoom();
    }

    private void DoGetMessageRoom() {
        getCompositeDisposable().add(getDataManager()
                .getMessageRoom()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<MessageResponse.IRoom>() {
                    @Override
                    public void accept(@NonNull MessageResponse.IRoom response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }
}
