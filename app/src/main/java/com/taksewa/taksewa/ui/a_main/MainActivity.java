package com.taksewa.taksewa.ui.a_main;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Handler;
import android.view.MenuItem;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_login.LoginActivity;
import com.taksewa.taksewa.ui.f_akun.AkunFragment;
import com.taksewa.taksewa.ui.f_home.HomeFragment;
import com.taksewa.taksewa.ui.f_inbox.InboxFragment;
import com.taksewa.taksewa.ui.f_transaksi.TransaksiFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainMvpView, BottomNavigationView.OnNavigationItemSelectedListener {

    public final String TAG = getClass().getSimpleName();
    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @BindView(R.id.navigationView)
    BottomNavigationView navigationView;

    final Fragment homeFragment = new HomeFragment();
    final Fragment inboxFragment = new InboxFragment();
    final Fragment transaksiFragment = new TransaksiFragment();
    final Fragment akunFragment = new AkunFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = homeFragment;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        navigationView.setOnNavigationItemSelectedListener(this);
        fm.beginTransaction().add(R.id.fragment_container, akunFragment, "akunFragment").hide(akunFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, transaksiFragment, "transaksiFragment").hide(transaksiFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, inboxFragment, "inboxFragment").hide(inboxFragment).commit();
        fm.beginTransaction().add(R.id.fragment_container, homeFragment, "homeFragment").commit();
        if ((mPresenter.isUserLogin()) && (!mPresenter.isUserVerified())) { //FIRST TIME
            onNeedToVerify();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if ((menuItem.getItemId() != R.id.navigation_home) && (!mPresenter.isUserLogin())) {
            startLoginActivity();
            return false;
        }
//        if (mPresenter.isUserVerified()) showMessage("Verif"); else showMessage("Not Verif");
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                if (active != homeFragment) {
                    fm.beginTransaction().hide(active).show(homeFragment).commit();
                    active = homeFragment;
                }
                return true;
            case R.id.navigation_inbox:
                if (active != inboxFragment) {
                    fm.beginTransaction().hide(active).show(inboxFragment).commit();
                    active = inboxFragment;
                }
                return true;
            case R.id.navigation_transaksi:
                if (active != transaksiFragment) {
                    fm.beginTransaction().hide(active).show(transaksiFragment).commit();
                    active = transaksiFragment;
                }
                return true;
            case R.id.navigation_akun:
                if (active != akunFragment) {
                    fm.beginTransaction().hide(active).show(akunFragment).commit();
                    active = akunFragment;
                }
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        showMessage(getString(R.string.tekan_sekali_lagi_untuk_keluar));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
