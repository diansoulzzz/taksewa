package com.taksewa.taksewa.ui.a_member_wishlist;

import com.taksewa.taksewa.ui._base.MvpPresenter;

public interface MemberWishlistMvpPresenter<V extends MemberWishlistMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

