package com.taksewa.taksewa.ui.f_akun_vendor;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangActivity;
import com.taksewa.taksewa.ui.a_report_barang.ReportBarangActivity;
import com.taksewa.taksewa.ui.a_vendor_barang_list.VendorBarangListActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getDiffFromDate;

public class AkunVendorFragment extends BaseFragment implements AkunVendorMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = getClass().getSimpleName();

    @Inject
    AkunVendorMvpPresenter<AkunVendorMvpView> mPresenter;

    @BindView(R.id.clNotAktifVendor)
    ConstraintLayout clNotAktifVendor;

    @BindView(R.id.clAktifVendor)
    ConstraintLayout clAktifVendor;

    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.rbStarReputasiProduk)
    RatingBar rbStarReputasiProduk;
    @BindView(R.id.tvWaktuKonfirmasiValue)
    TextView tvWaktuKonfirmasiValue;
    @BindView(R.id.tvProdukSedangDipinjamValue)
    TextView tvProdukSedangDipinjamValue;
    @BindView(R.id.tvProdukSuksesDipinjamValue)
    TextView tvProdukSuksesDipinjamValue;
    String title;
    ArrayList<Barang> barangs = new ArrayList<>();
    ArrayList<HSewa> hSewaPenyewa = new ArrayList<>();
    int totalSedangDipinjam = 0;
    int totalSuksesDiPinjam = 0;

    public static AkunVendorFragment newInstance(int page, String title) {
        AkunVendorFragment fragmentFirst = new AkunVendorFragment();
        Bundle args = new Bundle();
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        title = getArguments().getString("someTitle");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_akun_vendor, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    //    otomatis d call
    @Override
    protected void setUp(View view) {
        Log.d(TAG, "LOS");
        setHasOptionsMenu(true);
        srlLoader.setOnRefreshListener(this);
        mPresenter.onViewPrepared();
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btnDaftarVendor)
    void onBtnDaftarVendorClick() {
        if (!mPresenter.isUserVerified()) {
            if (getActivity() != null) {
                startVerifyActivity();
//                getActivity().finish();
            }
            return;
        }
        mPresenter.DoPostDaftarVendor();
    }

    @Override
    public void onPresenterReady(User user) {
        srlLoader.setRefreshing(false);
        if (user.getIsVendor() == 0) {
            onUserNotVendor();
            return;
        }
        onUserIsVendor();
        if (user.getBarangs() != null) {
            barangs.addAll(user.getBarangs());
            float avg = 0;
            float total = 0;
            for (int i = 0; i < barangs.size(); i++) {
                if (barangs.get(i).getTotalRating() > 0) {
                    total = total + barangs.get(i).getTotalRating();
                    avg = total / barangs.size();
                }
            }
            rbStarReputasiProduk.setRating(avg);
        }
        if (user.gethSewasPenyewa() != null) {
            hSewaPenyewa.addAll(user.gethSewasPenyewa());
            if (hSewaPenyewa.size() <= 0) {
                tvWaktuKonfirmasiValue.setText(getResources().getString(R.string.belum_ada_transaksi));

            } else {
                int totalSedangDipinjam = 0;
                int totalSuksesDiPinjam = 0;
                float avg = 0;
                float total = 0;

                for (int i = 0; i < hSewaPenyewa.size(); i++) {
                    long diff = getDiffFromDate(getDateParsed(hSewaPenyewa.get(i).getTgl()), getDateParsed(hSewaPenyewa.get(i).getTglKonfirmasiPenyewa()));
                    total = total + diff;
                    avg = total / hSewaPenyewa.size();
                    if ((hSewaPenyewa.get(i).getTglDiterimaPenyewaKembali() == null) && (hSewaPenyewa.get(i).getdSewaUtama() != null) && (hSewaPenyewa.get(i).getdSewaUtama().getBarangDetail() != null) && (hSewaPenyewa.get(i).getdSewaUtama().getBarangDetail().getBarang() != null)) {
                        totalSedangDipinjam = totalSedangDipinjam + 1;
                    }
                    if ((hSewaPenyewa.get(i).getTglDiterimaPenyewaKembali() != null) && (hSewaPenyewa.get(i).getdSewaUtama() != null) && (hSewaPenyewa.get(i).getdSewaUtama().getBarangDetail() != null) && (hSewaPenyewa.get(i).getdSewaUtama().getBarangDetail().getBarang() != null)) {
                        totalSuksesDiPinjam = totalSuksesDiPinjam + 1;
                    }
                }
                tvProdukSedangDipinjamValue.setText(getResources().getString(R.string._buah, totalSedangDipinjam));
                tvProdukSuksesDipinjamValue.setText(getResources().getString(R.string._buah, totalSuksesDiPinjam));
                if (avg <= 0) {
                    tvWaktuKonfirmasiValue.setText(getResources().getString(R.string.hitungan_jam));
                } else {
                    tvWaktuKonfirmasiValue.setText(getResources().getString(R.string.hari, String.valueOf(avg)));
                }
            }
        }
    }

    @Override
    public void onUserIsVendor() {
        clNotAktifVendor.setVisibility(View.GONE);
        clAktifVendor.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUserNotVendor() {
        clNotAktifVendor.setVisibility(View.VISIBLE);
        clAktifVendor.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        srlLoader.setRefreshing(false);
//        mPresenter.onViewPrepared();
    }

    @OnClick(R.id.btnTambahBarang)
    void onClickBtnTambahBarang() {
        Intent intent = new Intent(getActivity(), EntryBarangActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnDaftarBarang)
    void onClickBtnDaftarBarang() {
        Intent intent = new Intent(getActivity(), VendorBarangListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvProdukSedangDipinjam)
    void onClickTvProdukSedangDipinjam() {
        Intent intent = new Intent(getViewContext(), ReportBarangActivity.class);
        intent.putExtra("tab", 0);
        startActivity(intent);
//        showMessage("pinjam");
    }

    @OnClick(R.id.tvProdukSuksesDipinjam)
    void onClickTvProdukSuksesDipinjam() {
        Intent intent = new Intent(getViewContext(), ReportBarangActivity.class);
        intent.putExtra("tab", 1);
        startActivity(intent);
//        showMessage("sukses");
    }
}
