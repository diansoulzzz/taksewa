package com.taksewa.taksewa.ui.f_search_result_barang.options;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Kotum;
import com.taksewa.taksewa.ui._base.BaseViewHolder;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterBarangAdapter {

    public interface CallbackKategori {
        void onClickView(BarangKategori barangKategori, boolean isChecked);
    }

    public interface CallbackSubKategori {
        void onClickView(BarangSubKategori barangKategori, boolean isChecked);
    }
    public interface CallbackKota {
        void onClickView(Kotum kota, boolean isChecked);
    }

    public static class AdapterListKategoriTag extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<BarangKategori> datas;
        private CallbackKategori mCallback;

        public AdapterListKategoriTag(Context context, List datas, CallbackKategori mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tag, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.btnTag)
            ToggleButton btnTag;

            private BarangKategori data;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                btnTag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            btnTag.setBackgroundResource(R.drawable.bg_rounded_all_corner_active);
                            btnTag.setTextColor(itemView.getResources().getColor(R.color.white));
                        } else {
                            btnTag.setBackgroundResource(R.drawable.bg_rounded_all_corner);
                            btnTag.setTextColor(itemView.getResources().getColor(R.color.black));
                        }
                        mCallback.onClickView(data, isChecked);
                    }
                });
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                btnTag.setText(data.getNama());
                btnTag.setTextOn(data.getNama());
                btnTag.setTextOff(data.getNama());
                btnTag.setChecked((data.getIsChecked() == 1));
            }
        }
    }

    public static class AdapterListSubKategoriTag extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<BarangSubKategori> datas;
        private CallbackSubKategori mCallback;

        public AdapterListSubKategoriTag(Context context, List datas, CallbackSubKategori mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tag, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.btnTag)
            ToggleButton btnTag;
            private BarangSubKategori data;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                btnTag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            btnTag.setBackgroundResource(R.drawable.bg_rounded_all_corner_active);
                            btnTag.setTextColor(itemView.getResources().getColor(R.color.white));
                        } else {
                            btnTag.setBackgroundResource(R.drawable.bg_rounded_all_corner);
                            btnTag.setTextColor(itemView.getResources().getColor(R.color.black));
                        }
                        mCallback.onClickView(data, isChecked);
                    }
                });
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                btnTag.setText(data.getNama());
                btnTag.setTextOn(data.getNama());
                btnTag.setTextOff(data.getNama());
                btnTag.setChecked((data.getIsChecked() == 1));
            }
        }
    }


    public static class AdapterListKotaTag extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<Kotum> datas;
        private CallbackKota mCallback;

        public AdapterListKotaTag(Context context, List datas, CallbackKota mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tag, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.btnTag)
            ToggleButton btnTag;
            private Kotum data;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                btnTag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            btnTag.setBackgroundResource(R.drawable.bg_rounded_all_corner_active);
                            btnTag.setTextColor(itemView.getResources().getColor(R.color.white));
                        } else {
                            btnTag.setBackgroundResource(R.drawable.bg_rounded_all_corner);
                            btnTag.setTextColor(itemView.getResources().getColor(R.color.black));
                        }
                        mCallback.onClickView(data, isChecked);
                    }
                });
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                btnTag.setText(data.getNama());
                btnTag.setTextOn(data.getNama());
                btnTag.setTextOff(data.getNama());
                btnTag.setChecked((data.getIsChecked() == 1));
            }
        }
    }
}
