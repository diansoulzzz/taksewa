package com.taksewa.taksewa.ui.a_view_bank;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.BankResponse;
import com.taksewa.taksewa.model.Bank;
import com.taksewa.taksewa.model.Kecamatan;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanAdapter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpPresenter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanMvpView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewBankActivity extends BaseActivity implements ViewBankMvpView {

    private final String TAG = getClass().getSimpleName();

    @Inject
    ViewBankMvpPresenter<ViewBankMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvBank)
    RecyclerView rvBank;
    @BindView(R.id.etFilter)
    EditText etFilter;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    ViewBankAdapter.AdapterList viewBankAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<Bank> bankList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bank);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBankAdapter = new ViewBankAdapter.AdapterList(this, bankList, new ViewBankAdapter.Callback() {
            @Override
            public void onClickView(Bank bank, View itemView) {
                Intent intent = new Intent();
                intent.putExtra("bank", bank);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBank.setLayoutManager(gridLayoutManager);
        rvBank.setAdapter(viewBankAdapter);
        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewBankAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPresenter.onViewPrepared();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvBank.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBank.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(BankResponse.IList bankResponse) {
        onRemoveWait();
        bankList.clear();
        bankList.addAll(bankResponse.getData());
        viewBankAdapter.notifyDataSetChanged();
    }
}

