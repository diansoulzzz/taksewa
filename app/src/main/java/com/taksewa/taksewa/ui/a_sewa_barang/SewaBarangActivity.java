package com.taksewa.taksewa.ui.a_sewa_barang;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BillInfoModel;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.model.MidtransCustomField;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_date_range_picker.DateRangePickerActivity;
import com.taksewa.taksewa.ui.a_main.MainActivity;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import com.midtrans.sdk.uikit.widgets.FancyButton;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.BuildConfig.BASE_URL;
import static com.taksewa.taksewa.BuildConfig.CLIENT_KEY;
import static com.taksewa.taksewa.utils.CommonUtils.getDefaultLocale;

public class SewaBarangActivity extends BaseActivity implements SewaBarangMvpView, TransactionFinishedCallback {

    private final String TAG = getClass().getSimpleName();

    private final int DATE_RANGE_PICKER_RQ = 1;
    @Inject
    SewaBarangMvpPresenter<SewaBarangMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.ivGambarBarang)
    ImageView ivGambarBarang;
    @BindView(R.id.tvNamaBarang)
    TextView tvNamaBarang;
    @BindView(R.id.tvHargaBarang)
    TextView tvHargaBarang;
    @BindView(R.id.btnPilihTglSewa)
    Button btnPilihTglSewa;
    @BindView(R.id.tietCatatanSewa)
    TextInputEditText tietCatatanSewa;
    @BindView(R.id.tvTotalBayar)
    TextView tvTotalBayar;
    //    @BindView(R.id.btnSewa)
//    Button btnSewa;
    @BindView(R.id.btnSewa)
    FancyButton btnSewa;
    @BindView(R.id.tvDepositBarang)
    TextView tvDepositBarang;
    @BindView(R.id.clSewaDetail)
    ConstraintLayout clSewaDetail;
    @BindView(R.id.btnSelectedDateRange)
    Button btnSelectedDateRange;
    @BindView(R.id.tvInputQty)
    EditText tvInputQty;
    @BindView(R.id.btnInputPlus)
    Button btnInputPlus;
    @BindView(R.id.btnInputMinus)
    Button btnInputMinus;
    @BindView(R.id.tvCalculateTotalHargaBarang)
    TextView tvCalculateTotalHargaBarang;
    @BindView(R.id.tvCalculateTotalDepositBarang)
    TextView tvCalculateTotalDepositBarang;
    @BindView(R.id.tvCalculateTotalBayarBarang)
    TextView tvCalculateTotalBayarBarang;
    @BindView(R.id.tvTotalOngkirBarang)
    TextView tvTotalOngkirBarang;
    @BindView(R.id.tvCalculateTotalOngkirBarang)
    TextView tvCalculateTotalOngkirBarang;
    @BindView(R.id.tvSelectedHargaBarang)
    TextView tvSelectedHargaBarang;
    @BindView(R.id.tvSelectedTotalHargaBarang)
    TextView tvSelectedTotalHargaBarang;
    Date dateStart = new Date();
    Date dateEnd = new Date();
    Barang barang = new Barang();
    int qtyBarang = 1;
    double depositBarang = 0;
    double hargaSelected = 0;
    int lamaHari = 0;
    double totalDeposit = 0;
    double totalHarga = 0;
    double totalBayar = 0;
    double totalOngkir = 0;
    BarangHargaSewa selectedHargaSewa = new BarangHargaSewa();
    MidtransCustomField.Custom1 custom1 = new MidtransCustomField.Custom1();
    MidtransCustomField.Custom2 custom2 = new MidtransCustomField.Custom2();
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sewa_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (!mPresenter.isUserLogin()) {
            startLoginActivity();
            finish();
            return;
        }
        if (!mPresenter.isUserVerified()) {
            startVerifyActivity();
            finish();
            return;
        }
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("barang")) {
            barang = getIntent().getParcelableExtra("barang");
            mPresenter.onViewPrepared(barang.getId());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon action bar is clicked; go to parent activity
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DATE_RANGE_PICKER_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                dateStart.setTime(data.getLongExtra("dateStart", 0));
                dateEnd.setTime(data.getLongExtra("dateEnd", 0));
                clSewaDetail.setVisibility(View.VISIBLE);
                btnPilihTglSewa.setText(R.string.ganti_tgl_sewa);
//                btnPilihTglSewa.setVisibility(View.GONE);
                btnSelectedDateRange.setText(getResources().getString(R.string.string_between, DateFormat.getDateInstance(DateFormat.SHORT).format(dateStart), DateFormat.getDateInstance(DateFormat.SHORT).format(dateEnd), String.valueOf(CommonUtils.daysBetween(dateStart, dateEnd) + 1)));
                calculateTotal();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btnSewa)
    public void onBtnSewaClick(View view) {
        showOverlayLoading();
        mPresenter.onViewTransactionPrepared();
        /*
        SdkUIFlowBuilder.init()
                .setClientKey(CLIENT_KEY) // client_key is mandatory
                .setContext(getBaseContext()) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(BASE_URL) //set merchant url (required)
                .enableLog(true) // enable sdk log (optional)
                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // set theme. it will replace theme on snap theme on MAP ( optional)
                .buildSDK();

        UserDetail userDetail = LocalDataHandler.readObject("user_details", UserDetail.class);
        if (userDetail == null) {
            userDetail = new UserDetail();
            userDetail.setUserFullName(userModel.getNama());
            userDetail.setEmail(userModel.getEmail());
            userDetail.setPhoneNumber(userModel.getNoTelp());

            ArrayList<UserAddress> userAddresses = new ArrayList<>();
            UserAddress userAddress = new UserAddress();
            userAddress.setAddress("Jl Slipi No 15");
            userAddress.setCity("Jakarta");
            userAddress.setAddressType(com.midtrans.sdk.corekit.core.Constants.ADDRESS_TYPE_BOTH);
            userAddress.setZipcode("40184");
            userAddress.setCountry("IDN");
            userAddresses.add(userAddress);
            userDetail.setUserAddresses(userAddresses);
            LocalDataHandler.saveObject("user_details", userDetail);
        }

        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();
        ItemDetails itemBarang = new ItemDetails();
        itemBarang.setId(barang.getId().toString());
        itemBarang.setName(barang.getNama() + " " + btnSelectedDateRange.getText());
        itemBarang.setPrice(totalHarga);
        itemBarang.setQuantity(qtyBarang);
        itemDetailsList.add(itemBarang);

        ItemDetails itemDeposit = new ItemDetails();
        itemDeposit.setId("0");
        itemDeposit.setName("Deposit : " + barang.getNama());
        itemDeposit.setPrice(totalDeposit);
        itemDeposit.setQuantity(qtyBarang);
        itemDetailsList.add(itemDeposit);

        BillInfoModel billInfoModel = new BillInfoModel("demo_label", "demo_value");

        TransactionRequest transactionRequest = new TransactionRequest(System.currentTimeMillis() + "", (int) totalBayar);
        transactionRequest.setItemDetails(itemDetailsList);
        transactionRequest.setBillInfoModel(billInfoModel);

        if (!TextUtils.isEmpty(tietCatatanSewa.getText())) {
            transactionRequest.setCustomField1(Objects.requireNonNull(tietCatatanSewa.getText()).toString());
        }
        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);
        MidtransSDK.getInstance().startPaymentUiFlow(SewaBarangActivity.this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        */
    }

    @OnClick(R.id.btnPilihTglSewa)
    void onBtnPilihTglSewaClick() {
        Intent intent = new Intent(getBaseContext(), DateRangePickerActivity.class);
        intent.putExtra("dateStart", dateStart.getTime());
        intent.putExtra("dateEnd", dateEnd.getTime());
        startActivityForResult(intent, DATE_RANGE_PICKER_RQ);
    }

    @Override
    public void onPresenterReady(BarangResponse.IData barangResponse) {
        barang = barangResponse.getData();
        selectedHargaSewa = barang.getBarangHargaSewaUtama();
        tvNamaBarang.setText(barang.getNama());
        Glide.with(this).load(barang.getBarangFotoUtama().getFotoUrl()).into(ivGambarBarang);
//        String hargaSewaBarang = getString(R.string.sewa_barang_harga_lama_sewa_rp, NumberTextWatcher.getDecimalFormattedString(barang.getBarangHargaSewaUtama().getHarga().toString()), barang.getBarangHargaSewaUtama().getLamaHari().toString());
//        String depositQtyBarang = getString(R.string.sewa_barang_deposit_qty_sewa_rp, NumberTextWatcher.getDecimalFormattedString(barang.getDepositMin().toString()));
//        tvHargaBarang.setText(Html.fromHtml(hargaSewaBarang));
//        tvDepositBarang.setText(Html.fromHtml(depositQtyBarang));
//        tvInputQty.setText(String.valueOf(qtyBarang));
        calculateTotal();
    }

    @Override
    public void onPresenterUserInfoReady(User user) {
        SdkUIFlowBuilder.init()
                .setClientKey(CLIENT_KEY) // client_key is mandatory
                .setContext(getBaseContext()) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(BASE_URL) //set merchant url (required)
                .enableLog(true) // enable sdk log (optional)
                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255")) // set theme. it will replace theme on snap theme on MAP ( optional)
                .buildSDK();

        UserDetail userDetail = LocalDataHandler.readObject("user_details", UserDetail.class);
        if (userDetail == null) {
            userDetail = new UserDetail();
            userDetail.setUserFullName(user.getNama());
            userDetail.setEmail(user.getEmail());
            userDetail.setUserId(user.getId().toString());
//            userDetail.setMerchantToken(user.getApiToken());
            userDetail.setPhoneNumber(user.getNoTelp());

            ArrayList<UserAddress> userAddresses = new ArrayList<>();
            UserAddress userAddress = new UserAddress();
            userAddress.setAddress("");
            userAddress.setCity("");
            userAddress.setAddressType(com.midtrans.sdk.corekit.core.Constants.ADDRESS_TYPE_BOTH);
            userAddress.setZipcode("");
            userAddress.setCountry("");
            userAddresses.add(userAddress);
            userDetail.setUserAddresses(userAddresses);
            LocalDataHandler.saveObject("user_details", userDetail);
        }

        ArrayList<ItemDetails> itemDetailsList = new ArrayList<>();

        ItemDetails itemBarang = new ItemDetails();
        itemBarang.setId(barang.getId().toString());
        String namaReplace = barang.getNama();
        if (barang.getNama().length() > 35) {
            namaReplace = barang.getNama().substring(0, 20)+"...";
        }
        namaReplace = String.format(getDefaultLocale(), "%s (%d Hari) x %d", namaReplace, lamaHari, qtyBarang);
//        + " (" + getResources().getString(R.string.string_between, DateFormat.getDateInstance(DateFormat.SHORT).format(dateStart), DateFormat.getDateInstance(DateFormat.SHORT).format(dateEnd) + ")", String.valueOf(CommonUtils.daysBetween(dateStart, dateEnd) + 1)
        itemBarang.setName(namaReplace);
        itemBarang.setPrice(totalHarga);
        itemBarang.setQuantity(1);
        itemDetailsList.add(itemBarang);

        ItemDetails itemDeposit = new ItemDetails();
        itemDeposit.setId("-1");
        itemDeposit.setName("Deposit");
        itemDeposit.setPrice(totalDeposit);
        itemDeposit.setQuantity(1);
        itemDetailsList.add(itemDeposit);

        ItemDetails itemOngkir = new ItemDetails();
        itemOngkir.setId("-2");
        itemOngkir.setName("Ongkos Kirim");
        itemOngkir.setPrice(totalOngkir);
        itemOngkir.setQuantity(1);
        itemDetailsList.add(itemOngkir);

        custom1.setTotalOngkir(totalOngkir);
        custom1.setTotalJmlItem(qtyBarang);
        custom1.setTotalLamaSewa(lamaHari);
        custom1.setTotalDeposit(totalDeposit);
        custom1.setTotalPembayaran(totalBayar);
        custom1.setTglInputPeminjaman(dateStart.getTime());
        custom1.setTglInputPengembalian(dateEnd.getTime());
        custom2.setBarangId(barang.getId());
        custom2.setHargaSewa(hargaSelected);
        custom2.setSubtotalSewa(totalHarga);

        BillInfoModel billInfoModel = new BillInfoModel("demo_label", "demo_value");

        TransactionRequest transactionRequest = new TransactionRequest(System.currentTimeMillis() + "", (int) totalBayar);
        transactionRequest.setItemDetails(itemDetailsList);
        transactionRequest.setBillInfoModel(billInfoModel);

        transactionRequest.setCustomField1(String.valueOf(gson.toJson(custom1)));
        transactionRequest.setCustomField2(String.valueOf(gson.toJson(custom2)));
        if (!TextUtils.isEmpty(tietCatatanSewa.getText())) {
            transactionRequest.setCustomField3(Objects.requireNonNull(tietCatatanSewa.getText()).toString());
        }

        hideOverlayLoading();

        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);
        MidtransSDK.getInstance().startPaymentUiFlow(SewaBarangActivity.this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @OnClick(R.id.btnInputPlus)
    void onBtnInputPlusClick() {
        if (qtyBarang < barang.getStokAvailable()) {
            qtyBarang++;
            tvInputQty.setText(String.valueOf(qtyBarang));
            calculateTotal();
        }
    }

    @OnClick(R.id.btnInputMinus)
    void onBtnInputMinusClick() {
        if (qtyBarang > 1) {
            qtyBarang--;
            tvInputQty.setText(String.valueOf(qtyBarang));
            calculateTotal();
        }
    }

    void calculateTotal() {
        lamaHari = (int) (CommonUtils.daysBetween(dateStart, dateEnd) + 1);
        for (BarangHargaSewa barangHargaSewa : barang.getBarangHargaSewas()) {
            if (barangHargaSewa.getLamaHari() <= lamaHari) {
                selectedHargaSewa = barangHargaSewa;
                hargaSelected = barangHargaSewa.getHarga();
            }
        }
        Log.d(TAG, "DATEDIFF : " + String.valueOf(lamaHari));
        String hargaSewaBarang = getString(R.string.sewa_produk_harga_lama_sewa_rp, NumberTextWatcher.getDecimalFormattedString(selectedHargaSewa.getHarga().toString()), selectedHargaSewa.getLamaHari().toString());
        String depositQtyBarang = getString(R.string.sewa_produk_deposit_qty_sewa_rp, NumberTextWatcher.getDecimalFormattedString(barang.getDepositMin().toString()));
        tvHargaBarang.setText(Html.fromHtml(hargaSewaBarang));
        tvDepositBarang.setText(Html.fromHtml(depositQtyBarang));
        tvInputQty.setText(String.valueOf(qtyBarang));
        totalHarga = lamaHari * qtyBarang * hargaSelected;
//        totalOngkir = (double) 10000;
        totalOngkir = (double) 0;
        depositBarang = barang.getDepositMin();
        totalDeposit = qtyBarang * depositBarang;
        totalBayar = totalHarga + totalDeposit + totalOngkir;
        tvSelectedTotalHargaBarang.setText(NumberTextWatcher.getDecimalFormattedString(String.valueOf((int) hargaSelected)));
        tvCalculateTotalOngkirBarang.setText(NumberTextWatcher.getDecimalFormattedString(String.valueOf((int) totalOngkir)));
        tvCalculateTotalHargaBarang.setText(NumberTextWatcher.getDecimalFormattedString(String.valueOf((int) totalHarga)));
        tvCalculateTotalDepositBarang.setText(NumberTextWatcher.getDecimalFormattedString(String.valueOf((int) totalDeposit)));
        tvTotalBayar.setText(Html.fromHtml(getResources().getString(R.string.total_bayar_param_rp, NumberTextWatcher.getDecimalFormattedString(String.valueOf((int) totalBayar)))));
        tvCalculateTotalBayarBarang.setText(NumberTextWatcher.getDecimalFormattedString(String.valueOf((int) totalBayar)));
    }

    @Override
    public void onTransactionFinished(TransactionResult transactionResult) {
        if (transactionResult.getResponse() != null) {
            switch (transactionResult.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:
//                    showMessage("Transaction Finished. ID: " + transactionResult.getResponse().getTransactionId());
                    break;
                case TransactionResult.STATUS_PENDING:
//                    showMessage("Transaction Pending. ID: " + transactionResult.getResponse().getTransactionId());
                    break;
                case TransactionResult.STATUS_FAILED:
//                    showMessage("Transaction Failed. ID: " + transactionResult.getResponse().getTransactionId());
                    break;
            }
            transactionResult.getResponse().getValidationMessages();
            onOrderComplete();
        } else if (transactionResult.isTransactionCanceled()) {
//            showMessage("Transaction Canceled");
        } else {
            if (transactionResult.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
//                showMessage("Transaction Invalid");
            } else {
//                showMessage("Finished with failure.");
            }
        }
    }

    void onOrderComplete() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
