package com.taksewa.taksewa.ui.a_member_last_seen_barang;

import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface MemberLastSeenBarangMvpView extends MvpView {
    void onPresenterReady(BarangResponse.IList barangResponse);
}
