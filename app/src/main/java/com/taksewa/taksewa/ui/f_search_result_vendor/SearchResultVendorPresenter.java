package com.taksewa.taksewa.ui.f_search_result_vendor;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.SearchRequest;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberMvpPresenter;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class SearchResultVendorPresenter<V extends SearchResultVendorMvpView> extends BasePresenter<V>
        implements SearchResultVendorMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public SearchResultVendorPresenter(DataManager dataManager,
                                       SchedulerProvider schedulerProvider,
                                       CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(String queryString, int sortBy, Filter filter) {
        SearchRequest.ISendVendor search = new SearchRequest.ISendVendor();
        search.setQuery(queryString);
        DoGetSearchVendor(search);
    }

    public void DoGetSearchVendor(SearchRequest.ISendVendor request) {
        getCompositeDisposable().add(getDataManager()
                .getSearchVendor(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<SearchResponse.IVendorData>() {
                    @Override
                    public void accept(@NonNull SearchResponse.IVendorData response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

