package com.taksewa.taksewa.ui.f_inbox;

import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_home.HomeMvpPresenter;
import com.taksewa.taksewa.ui.f_home.HomeMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class InboxPresenter<V extends InboxMvpView> extends BasePresenter<V>
        implements InboxMvpPresenter<V> {
    @Inject
    public InboxPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {

    }
}

