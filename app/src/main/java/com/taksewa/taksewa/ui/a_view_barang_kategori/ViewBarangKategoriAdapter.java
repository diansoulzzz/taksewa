package com.taksewa.taksewa.ui.a_view_barang_kategori;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewBarangKategoriAdapter {

    public interface CallbackImage {
        void onClickView(BarangKategori barangKategori, View itemView);
    }

    public static class AdapterEntry extends RecyclerView.Adapter<BaseViewHolder> implements Filterable {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<BarangKategori> datas;
        private List<BarangKategori> filteredDatas;
        private CallbackImage mCallback;

        public AdapterEntry(Context context, List datas, CallbackImage mCallback) {
            this.context = context;
            this.datas = datas;
            this.filteredDatas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view_barang_kategori, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.filteredDatas != null && this.filteredDatas.size() > 0) {
                return this.filteredDatas.size();
            }
            return 0;
        }

        @Override
        public int getItemViewType(int position) {
            if (filteredDatas != null && filteredDatas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String charString = constraint.toString();
                    if (charString.isEmpty()) {
                        filteredDatas = datas;
                    } else {
                        ArrayList<BarangKategori> filteredTemp = new ArrayList<>();
                        for (BarangKategori row : datas) {
                            if (row.getNama().toLowerCase().contains(charString.toLowerCase())) {
                                filteredTemp.add(row);
                            }
                        }
                        filteredDatas = filteredTemp;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filteredDatas;
                    Log.d(TAG, "ONFILTER : " + charString);
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    Log.d(TAG, "ONPublis : " + constraint.toString());
                    filteredDatas = (ArrayList<BarangKategori>) results.values;
                    notifyDataSetChanged();
                }
            };
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.btnKategori)
            Button btnKategori;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final BarangKategori data = filteredDatas.get(i);
                assert data != null;
                if (data.getNama() != null) {
                    btnKategori.setText(data.getNama());
                }
                if (data.getFotoUrl() != null) {
                    Glide.with(itemView).asBitmap().load(data.getFotoUrl()).into(new SimpleTarget<Bitmap>(50, 50) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            Drawable drawableLeft = new BitmapDrawable(context.getResources(), resource);
                            drawableLeft.setBounds(0, 0, 60, 60);
                            Drawable drawableRight = context.getResources().getDrawable(R.drawable.ic_keyboard_arrow_right_black_24dp);
                            drawableRight.setBounds(0, 0, 100, 60);
                            btnKategori.setCompoundDrawables(drawableLeft, null, drawableRight, null);
                        }
                    });
                }
                btnKategori.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onClickView(data, itemView);
                    }
                });
            }
        }
    }
}
