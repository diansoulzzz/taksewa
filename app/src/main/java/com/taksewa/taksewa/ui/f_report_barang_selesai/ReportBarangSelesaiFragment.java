package com.taksewa.taksewa.ui.f_report_barang_selesai;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_RELEVANCE;

public class ReportBarangSelesaiFragment extends BaseFragment implements ReportBarangSelesaiMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = getClass().getSimpleName();

    @Inject
    ReportBarangSelesaiPresenter<ReportBarangSelesaiMvpView> mPresenter;

    @BindView(R.id.rvResultBarang)
    RecyclerView rvResultBarang;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    private ReportBarangSelesaiAdapter.AdapterList adapterList;
    private ArrayList<HSewa> hSewas = new ArrayList<>();
    private GridLayoutManager gridLayoutManager = new GridLayoutManager(getViewContext(), 1);
    private String tipe;

    public static ReportBarangSelesaiFragment newInstance(String tipe) {
        ReportBarangSelesaiFragment fragment = new ReportBarangSelesaiFragment();
        Bundle args = new Bundle();
        args.putString("tipe", tipe);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipe = getArguments().getString("tipe");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_barang_selesai, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        srlLoader.setOnRefreshListener(this);
        adapterList = new ReportBarangSelesaiAdapter.AdapterList(getViewContext(), hSewas, data -> {

        });
        rvResultBarang.setLayoutManager(gridLayoutManager);
        rvResultBarang.setAdapter(adapterList);
        setHasOptionsMenu(true);
        mPresenter.onViewPrepared(tipe);
    }

    public void onRemoveWait() {
        srlLoader.setRefreshing(false);
        progressBar.hide();
        rvResultBarang.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvResultBarang.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRefresh() {
        onShowWait();
        mPresenter.onViewPrepared(tipe);
    }

    @Override
    public void onPresenterReady(HSewaResponse.IList iList) {
        hSewas.clear();
        hSewas.addAll(iList.getData());
        adapterList.notifyDataSetChanged();
        onRemoveWait();
    }
}
