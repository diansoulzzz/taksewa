package com.taksewa.taksewa.ui.a_view_barang_kategori;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ViewBarangKategoriPresenter<V extends ViewBarangKategoriMvpView> extends BasePresenter<V>
        implements ViewBarangKategoriMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public ViewBarangKategoriPresenter(DataManager dataManager,
                                       SchedulerProvider schedulerProvider,
                                       CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
//        getMvpView().onPresenterReady();
        getBarangKategoriList();
    }

    public void getBarangKategoriList() {
        getCompositeDisposable().add(getDataManager()
                .getBarangKategoriList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangKategoriResponse.IList>() {
                    @Override
                    public void accept(@NonNull BarangKategoriResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

