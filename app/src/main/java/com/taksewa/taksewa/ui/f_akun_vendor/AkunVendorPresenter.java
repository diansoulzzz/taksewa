package com.taksewa.taksewa.ui.f_akun_vendor;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberMvpPresenter;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class AkunVendorPresenter<V extends AkunVendorMvpView> extends BasePresenter<V>
        implements AkunVendorMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public AkunVendorPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        if (getDataManager().getCurrentUserisVendor()) {
            Log.d(TAG, "USER IS VENDOR");
            getMvpView().onUserIsVendor();
        }
        DoGetProfileInfo();
    }

    public void DoGetProfileInfo() {
        getProfileInfo(response -> {
            getDataManager().setCurrentUserIsVendor((response.getData().getIsVendor() == 1));
            getDataManager().setCurrentUserIsVerified((response.getData().getVerified() != null));
            getMvpView().onPresenterReady(response.getData());
        }, throwable -> {
            Log.d(TAG, "fail");
            setUserAsLoggedOut();
            if (!isViewAttached()) {
                return;
            }
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        });
//        getCompositeDisposable().add(getDataManager()
//                .getProfileInfo()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<UserResponse.IData>() {
//                    @Override
//                    public void accept(@NonNull UserResponse.IData response) throws Exception {
//                        getDataManager().setCurrentUserIsVendor((response.getData().getIsVendor() == 1));
//                        getDataManager().setCurrentUserIsVerified((response.getData().getVerified() != null));
//                        getMvpView().onPresenterReady(response.getData());
////                        getCompositeDisposable().clear();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(@NonNull Throwable throwable)
//                            throws Exception {
//                        Log.d(TAG, "fail");
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
//                    }
//                }));
    }

    @Override
    public void DoPostDaftarVendor() {
        getCompositeDisposable().add(getDataManager()
                .postDaftarVendor()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<UserResponse.IData>() {
                    @Override
                    public void accept(@NonNull UserResponse.IData response) throws Exception {
                        getDataManager().setCurrentUserIsVendor((response.getData().getIsVendor() == 1));
                        getMvpView().onPresenterReady(response.getData());
//                        getCompositeDisposable().clear();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

