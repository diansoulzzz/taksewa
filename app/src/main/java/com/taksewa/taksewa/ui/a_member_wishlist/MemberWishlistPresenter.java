package com.taksewa.taksewa.ui.a_member_wishlist;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class MemberWishlistPresenter<V extends MemberWishlistMvpView> extends BasePresenter<V>
        implements MemberWishlistMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public MemberWishlistPresenter(DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getMemberWishlist();
    }

    public void getMemberWishlist() {
        getCompositeDisposable().add(getDataManager()
                .getBarangWishlistList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IList>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }
}

