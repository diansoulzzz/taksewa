package com.taksewa.taksewa.ui.a_view_barang_review;

import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ViewBarangReviewMvpView extends MvpView {
    void onPresenterReady(BarangResponse.IData barangResponse);
}
