package com.taksewa.taksewa.ui.a_view_bank;

import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.BankResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ViewBankMvpView extends MvpView {
    void onPresenterReady(BankResponse.IList bankResponseList);
}
