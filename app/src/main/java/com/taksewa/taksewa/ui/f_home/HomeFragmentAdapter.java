package com.taksewa.taksewa.ui.f_home;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.Carousel;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragmentAdapter {

    public interface CallbackBarang {
        void onClickBarang(Barang barang);
    }

    public interface CallbackBarangKategori {
        void onClickBarangKategori(BarangKategori barangKategori);
    }

    public interface CallbackCarousel {
        void onClickCarousel(Carousel carousel);
    }

    public static class AdapterCarousel extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<Carousel> datas;
        private CallbackCarousel mCallback;

        public AdapterCarousel(Context context, List datas, CallbackCarousel mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_carousel_list, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.ivGambar)
            ImageView ivGambar;
            RequestOptions requestOptions = new RequestOptions();

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final Carousel data = datas.get(i);
                if (data.getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getFotoUrl()).apply(requestOptions.transforms(new CenterCrop(), new RoundedCorners(30))).into(ivGambar);
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mCallback.onClickCarousel(data);
                    }
                });
            }
        }
    }

    public static class AdapterBarang extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<Barang> datas;
        private CallbackBarang mCallback;

        public AdapterBarang(Context context, List datas, CallbackBarang mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_barang, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaBarang)
            TextView tvNamaBarang;
            @BindView(R.id.tvHargaBarang)
            TextView tvHargaBarang;
            @BindView(R.id.ivGambarBarang)
            ImageView ivGambarBarang;
            String htmlFormat;
            String formatedHarga;
            String formatedLamaHari;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final Barang data = datas.get(i);
                if (data.getNama() != null) {
                    tvNamaBarang.setText(data.getNama());
                }
                if (data.getBarangFotoUtama().getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getBarangFotoUtama().getFotoUrl()).into(ivGambarBarang);
                }
                if (data.getBarangHargaSewaUtama() != null) {
                    formatedHarga = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getHarga().toString());
                    formatedLamaHari = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getLamaHari().toString());
                    tvHargaBarang.setText(Html.fromHtml(itemView.getResources().getString(R.string.view_produk_harga_lama_sewa_rp, formatedHarga, "")));
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mCallback.onClickBarang(data);
                    }
                });
            }
        }
    }

    public static class AdapterBarangKategori extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<BarangKategori> datas;
        private CallbackBarangKategori mCallback;

        public AdapterBarangKategori(Context context, List datas, CallbackBarangKategori mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_barang_kategori, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaBarangKategori)
            TextView tvNamaBarangKategori;
            @BindView(R.id.ivGambarBarangKategori)
            ImageView ivGambarBarangKategori;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final BarangKategori data = datas.get(i);
                if (data.getNama() != null) {
                    tvNamaBarangKategori.setText(data.getNama());
                }
                if (data.getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getFotoUrl()).into(ivGambarBarangKategori);
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mCallback.onClickBarangKategori(data);
                    }
                });
            }
        }
    }
}
