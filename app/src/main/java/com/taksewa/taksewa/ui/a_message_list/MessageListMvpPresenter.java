package com.taksewa.taksewa.ui.a_message_list;


import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_message.MessageMvpView;

@PerActivity
public interface MessageListMvpPresenter<V extends MessageListMvpView> extends MvpPresenter<V> {
    void onViewPrepared();
}
