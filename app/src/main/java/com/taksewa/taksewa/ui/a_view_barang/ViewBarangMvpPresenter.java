package com.taksewa.taksewa.ui.a_view_barang;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpView;

public interface ViewBarangMvpPresenter<V extends ViewBarangMvpView> extends MvpPresenter<V> {

    void onViewPrepared(int brgId);

    void onFabWishlistClick(int brgId);
}

