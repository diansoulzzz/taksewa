package com.taksewa.taksewa.ui.f_transaksi_vendor;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberAdapter;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.IconStepStatus;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class TransaksiVendorAdapter {

    public interface Callback {
        void onClickView(HSewa data);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        static final int VIEW_TYPE_EMPTY = 0;
        static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<HSewa> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NotNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new TransaksiVendorAdapter.AdapterList.VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaksi_vendor, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), "Riwayat Penyewaan Kosong", "Kamu belum pernah melakukan transaksi penyewaan");
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (this.datas != null && this.datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            }
            return VIEW_TYPE_EMPTY;
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.ivMemberImage)
            ImageView ivMemberImage;
            @BindView(R.id.tvKodenota)
            TextView tvKodenota;
            @BindView(R.id.tvStatus)
            TextView tvStatus;
            @BindView(R.id.tvGrandTotal)
            TextView tvGrandTotal;
            @BindView(R.id.iconStepStatus)
            IconStepStatus iconStepStatus;
            @BindView(R.id.tvNamaMember)
            TextView tvNamaMember;
            @BindView(R.id.tvTglSewa)
            TextView tvTglSewa;
            @BindView(R.id.tvLamaHari)
            TextView tvLamaHari;

            HSewa data;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(v -> callback.onClickView(data));
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                if (data.getUserPeminjam() != null) {
                    Glide.with(itemView).load(data.getUserPeminjam().getFotoUrl()).into(ivMemberImage);
                }
                if (data.getKodenota() != null) {
                    tvKodenota.setText(data.getKodenota());
                }
                if (data.getGrandtotal() != null) {
                    tvGrandTotal.setText(Html.fromHtml(itemView.getResources().getString(R.string.rp, NumberTextWatcher.getDecimalFormattedString(data.getGrandtotal().toString()))));
                }
                if (data.getUserPeminjam().getNama() != null){
                    tvNamaMember.setText(data.getUserPeminjam().getNama());
                }
                if (data.getTgl() != null){
                    tvTglSewa.setText(getStringDateFormat(getDateParsed(data.getCreatedAt())));
                }
//                if (data.getdSewaUtama() != null) {
//                    tvLamaHari.setText(Html.fromHtml(itemView.getResources().getString(R.string.hari, data.getdSewaUtama().getLamaSewa().toString())));
//                }
                if (data.getStatusTransaksi() != null) {
                    tvStatus.setBackgroundColor(itemView.getResources().getColor(R.color.faint_yellow));
                    tvStatus.setText(data.getStatusTransaksiName());
                    if (data.getStatusTransaksi() == -1) {
                        tvStatus.setBackgroundColor(itemView.getResources().getColor(R.color.lightGrayColor));
                    } else if (data.getStatusTransaksi() == 8) {
                        tvStatus.setBackgroundColor(itemView.getResources().getColor(R.color.payment_status_success));
                    }
                    iconStepStatus.setStatusLevel(data.getStatusTransaksi());
                }
                /*
                if (data.getStatusTransaksi() != null) {
                    tvStatus.setBackgroundColor(itemView.getResources().getColor(R.color.faint_yellow));
                    if (data.getStatusTransaksi() == -1) {
                        tvStatus.setText("Transaksi Dibatalkan");
                        tvStatus.setBackgroundColor(itemView.getResources().getColor(R.color.dark_gray));
                    } else if (data.getStatusTransaksi() == 1) {
                        tvStatus.setText("Menunggu Pembayaran");
                    } else if (data.getStatusTransaksi() == 2) {
                        tvStatus.setText("Menunggu Konfirmasi");
                    } else if (data.getStatusTransaksi() == 3) {
                        tvStatus.setText("Pesanan sedang diproses");
                    } else if (data.getStatusTransaksi() == 4) {
                        tvStatus.setText("Pesanan sedang dikirim");
                    } else if (data.getStatusTransaksi() == 5) {
                        tvStatus.setText("Barang sedang dipinjam");
                    } else if (data.getStatusTransaksi() == 6) {
                        tvStatus.setText("Barang sudah dikembalikan");
                    } else if (data.getStatusTransaksi() == 7) {
                        tvStatus.setText("Transaksi selesai");
                        tvStatus.setBackgroundColor(itemView.getResources().getColor(R.color.payment_status_success));
                    }
                }
                */
            }
        }
//
//        public class EmptyHolder extends BaseViewHolder {
//            @BindView(R.id.ivEmptyImage)
//            ImageView ivEmptyImage;
//            @BindView(R.id.tvEmptyTitle)
//            TextView tvEmptyTitle;
//            @BindView(R.id.tvEmptySubtitle)
//            TextView tvEmptySubtitle;
//
//            public EmptyHolder(@NonNull View itemView) {
//                super(itemView);
//                ButterKnife.bind(this, itemView);
//            }
//
//            @Override
//            protected void clear() {
//
//            }
//
//            public void onBind(int i) {
//                tvEmptySubtitle.setText("Riwayat Penyewaan Kosong");
//                tvEmptySubtitle.setText("Kamu belum pernah melakukan transaksi penyewaan");
//            }
//        }
    }
}
