package com.taksewa.taksewa.ui.a_member_review_detail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.request.ReviewBarangRequest;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_image_picker.ImagePickerActivity;
import com.taksewa.taksewa.ui.a_image_preview.ImagePreviewActivity;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewAdapter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemberReviewDetailActivity extends BaseActivity implements MemberReviewDetailMvpView, RatingBar.OnRatingBarChangeListener {

    private final String TAG = getClass().getSimpleName();
    private final int RQ_PILIH_GAMBAR = 1;
    @Inject
    MemberReviewDetailMvpPresenter<MemberReviewDetailMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.ivGambarProduk)
    ImageView ivGambarProduk;
    @BindView(R.id.rvGambarList)
    RecyclerView rvGambarList;
    @BindView(R.id.rbStarReview)
    RatingBar rbStarReview;
    @BindView(R.id.tvRateReview)
    TextView tvRateReview;
    @BindView(R.id.tvNamaBarang)
    TextView tvNamaBarang;
    @BindView(R.id.tietUlasanProduk)
    TextInputEditText tietUlasanProduk;
    //    MemberReviewAdapter.AdapterList memberReviewAdapter;
    GridLayoutManager gridLayoutManager;
    HSewa hSewa = new HSewa();
    ArrayList<FotoReview> arraySelectedFile = new ArrayList<>();
    FotoReview barangTampung = new FotoReview();
    ReviewBarang reviewBarang = new ReviewBarang();
    MemberReviewDetailAdapter.AdapterEntry memberReviewDetailAdapter;
    ArrayList<FotoReview> fotoReviews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_review_detail);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("hsewa")) {
            hSewa = getIntent().getParcelableExtra("hsewa");
            mPresenter.onViewPrepared(hSewa.getId());
        }
        rbStarReview.setOnRatingBarChangeListener(this);
        barangTampung.setFotoLocalPath("");
        gridLayoutManager = new GridLayoutManager(this, 5);
        rvGambarList.setLayoutManager(gridLayoutManager);
        memberReviewDetailAdapter = new MemberReviewDetailAdapter.AdapterEntry(getBaseContext(), arraySelectedFile, new MemberReviewDetailAdapter.CallbackImage() {
            @Override
            public void onClickView(FotoReview imageGalleryPath, View itemView) {
                PopupMenu popupMenu = new PopupMenu(getViewContext(), itemView);
                popupMenu.inflate(R.menu.popup_entry_barang);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popup_delete:
                                removeImageAdapter(imageGalleryPath);
                                return true;
                            case R.id.popup_show:
                                startActivityImagePreview(imageGalleryPath.getFotoLocalPath());
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }

            @Override
            public void onClickAdd() {
                startActivityImagePickerActivity();
//                if (imageGalleryPath.getFotoLocalPath().equals("")) {
//                    startActivityImagePickerActivity();
//                    return;
//                }
            }
        });
        rvGambarList.setAdapter(memberReviewDetailAdapter);
        setAdapterSelectedImage();
    }

    public void startActivityImagePreview(String filePath) {
        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra("filePath", filePath);
        startActivity(intent);
    }

    public void startActivityImagePickerActivity() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra("totalParentImage", arraySelectedFile.size() - 1);
        startActivityForResult(intent, RQ_PILIH_GAMBAR);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
    }

    public void onShowWait() {

    }

    public void removeImageAdapter(FotoReview barangFoto) {
        if (barangFoto.getFotoUUID() != null) {
            mPresenter.onFileRemoved(barangFoto);
        }
        arraySelectedFile.remove(barangFoto);
        setAdapterSelectedImage();
    }

    public void setAdapterSelectedImage() {
        arraySelectedFile.remove(barangTampung);
        if (arraySelectedFile.size() < 5) {
            arraySelectedFile.add(barangTampung);
        }
        boolean isBoolAvailable = false;
        memberReviewDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFileUploadSuccess(FotoReview fotoReview) {
        Log.d(TAG, "Upload success " + fotoReview.getFotoUrl());
        setAdapterSelectedImage();
    }

    @Override
    public void onFileUploadFailed(FotoReview fotoReview) {
        Log.d(TAG, "Upload failed");
    }

    @Override
    public void onFileUploadProgress(FotoReview fotoReview) {
        Log.d(TAG, "Upload progress " + fotoReview.getProgressUpload());
        if (!arraySelectedFile.contains(fotoReview)) {
            arraySelectedFile.add(fotoReview);
        }
        setAdapterSelectedImage();
    }

    @Override
    public void onFileDeleteSuccess(FotoReview fotoReview) {

    }

    @Override
    public void onEntrySuccess(HSewaResponse.IList response) {
        hideOverlayLoading();
        showMessage(getString(R.string.success_review_, hSewa.getFirstBarang().getNama()));
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("hsewas", response.getData());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_PILIH_GAMBAR) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                ArrayList<String> arrayFilePath = data.getStringArrayListExtra("arrayFilePath");
                for (String filePath : arrayFilePath) {
                    FotoReview fotoReview = new FotoReview();
                    fotoReview.setFotoLocalPath(filePath);
                    mPresenter.onFileChosen(fotoReview);
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    @Override
    public void onPresenterReady(HSewaResponse.IData hsewaResponse) {
        hSewa = hsewaResponse.getData();
        if (hSewa.getFirstBarang() != null) {
            if (hSewa.getFirstBarang().getBarangFotoUtama() != null) {
                if (hSewa.getFirstBarang().getBarangFotoUtama().getFotoUrl() != null) {
                    Glide.with(this).load(hSewa.getFirstBarang().getBarangFotoUtama().getFotoUrl()).into(ivGambarProduk);
                    tvNamaBarang.setText(hSewa.getFirstBarang().getNama());
                }
            }
        }
        if (hSewa.getReviewBarangUtama() != null){
            reviewBarang = hsewaResponse.getData().getReviewBarangUtama();
            rbStarReview.setRating(reviewBarang.getRating());
            tietUlasanProduk.setText(reviewBarang.getKomentar());

            for (FotoReview fotoReview : reviewBarang.getFotoReviews()) {
                fotoReview.setProgressUpload(100);
                fotoReview.setFotoLocalPath(fotoReview.getFotoUrl());
                fotoReview.setFotoUrl(fotoReview.getFotoUrl());
                arraySelectedFile.add(fotoReview);
            }
        }
        setAdapterSelectedImage();
        onRemoveWait();
    }


    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        reviewBarang.setRating((int) rating);
        tvRateReview.setText(reviewBarang.getRatingName());
    }


    public boolean isValid() {
        boolean valid = true;
        if (TextUtils.isEmpty(tietUlasanProduk.getText())) {
            tietUlasanProduk.setError(getResources().getString(R.string.error_ulasan_empty));
            valid = false;
        }

        if (rbStarReview.getRating() <= 0) {
            valid = false;
        }
        boolean isAllImageUploaded = true;
        for (FotoReview fotoReview : arraySelectedFile) {
            if (!fotoReview.equals(barangTampung)) {
                Log.d(TAG, String.valueOf(fotoReview.getFotoUrl()));
                Log.d(TAG, String.valueOf(fotoReview.getProgressUpload()));
                Log.d(TAG, String.valueOf(fotoReview.getFotoUUID()));
                if (fotoReview.getFotoUrl() == null) {
                    isAllImageUploaded = false;
                }
            }
        }
        if (!isAllImageUploaded) {
            showMessage(R.string.error_picker_image_progress);
            valid = false;
        }
        return valid;
    }


    @OnClick(R.id.btnUlasanSubmit)
    void onClickBtnUlasanSubmit() {
        if (!isValid()) {
            return;
        }
        fotoReviews.clear();
        for (FotoReview fotoReview : arraySelectedFile) {
            if (!fotoReview.equals(barangTampung)) {
                fotoReviews.add(fotoReview);
            }
        }

        showOverlayLoading();
        reviewBarang.setHSewaId(hSewa.getId());
        reviewBarang.setKomentar(tietUlasanProduk.getText().toString());
        reviewBarang.setFotoReviews(fotoReviews);
        reviewBarang.setBarangId(hSewa.getFirstBarang().getId());
        reviewBarang.setHSewaId(hSewa.getId());
        ReviewBarangRequest.EntryData entryData = new ReviewBarangRequest.EntryData();
        entryData.setReviewBarang(reviewBarang);
        mPresenter.onViewSubmit(entryData);
    }
}

