package com.taksewa.taksewa.ui.a_transaksi_vendor_detail;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.DSewa;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_message.MessageActivity;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailAdapter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpPresenter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpView;
import com.taksewa.taksewa.ui.a_verify.VerifyActivity;
import com.taksewa.taksewa.utils.IconStepStatus;
import com.taksewa.taksewa.utils.LayoutPanel;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.model.HSewa.MENUNGGU_KONFIRMASI;
import static com.taksewa.taksewa.model.HSewa.PESANAN_DITOLAK_VENDOR;
import static com.taksewa.taksewa.model.HSewa.PESANAN_SEDANG_DIKIRIM;
import static com.taksewa.taksewa.model.HSewa.PESANAN_SEDANG_DIPROSES;
import static com.taksewa.taksewa.model.HSewa.PRODUK_SUDAH_DIKEMBALIKAN;
import static com.taksewa.taksewa.model.HSewa.TRANSAKSI_DIBATALKAN;
import static com.taksewa.taksewa.model.HSewa.TRANSAKSI_SELESAI;
import static com.taksewa.taksewa.utils.CommonUtils.capitalizerStr;
import static com.taksewa.taksewa.utils.CommonUtils.createAlertDialogBuilder;
import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;
import static com.taksewa.taksewa.utils.NumberTextWatcher.getDecimalFormattedString;

public class TransaksiVendorDetailActivity extends BaseActivity implements TransaksiVendorDetailMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = getClass().getSimpleName();

    @Inject
    TransaksiVendorDetailMvpPresenter<TransaksiVendorDetailMvpView> mPresenter;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.clExpandableLayoutInformasiTagihan)
    ConstraintLayout clExpandableLayoutInformasiTagihan;
    @BindView(R.id.btnHeaderPanelInformasiTagihan)
    LayoutPanel btnHeaderPanelStatus;
    @BindView(R.id.tvKodenotaPiutang)
    TextView tvKodenotaPiutang;
    @BindView(R.id.tvTotalTagihan)
    TextView tvTotalTagihan;
    @BindView(R.id.tvStatusTagihan)
    TextView tvStatusTagihan;
    @BindView(R.id.tvMetodePembayaran)
    TextView tvMetodePembayaran;
    @BindView(R.id.tvKodenotaSewa)
    TextView tvKodenotaSewa;
    @BindView(R.id.tvNamaMemberSewa)
    TextView tvNamaMemberSewa;
    @BindView(R.id.tvStatusSewa)
    TextView tvStatusSewa;
    @BindView(R.id.tvKeteranganSewa)
    TextView tvKeteranganSewa;
    @BindView(R.id.clLayout)
    ConstraintLayout clLayout;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.iconStepStatus)
    IconStepStatus iconStepStatus;
    @BindView(R.id.tvTglSewa)
    TextView tvTglSewa;
    @BindView(R.id.btnProsesPesanan)
    Button btnProsesPesanan;
    @BindView(R.id.btnTolakPesanan)
    Button btnTolakPesanan;
    @BindView(R.id.btnKirimPesanan)
    Button btnKirimPesanan;
    @BindView(R.id.btnTerimaKembaliProduk)
    Button btnTerimaKembaliProduk;
    @BindView(R.id.tvNamaProduk)
    TextView tvNamaProduk;
    @BindView(R.id.ivImageProduk)
    ImageView ivImageProduk;
    @BindView(R.id.tvQtySewa)
    TextView tvQtySewa;
    @BindView(R.id.tvLamaSewaProduk)
    TextView tvLamaSewaProduk;
    @BindView(R.id.tvHargaSewaProduk)
    TextView tvHargaSewaProduk;
    @BindView(R.id.rvProductDetail)
    RecyclerView rvProductDetail;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.ivChat)
    Button ivChat;
    @BindView(R.id.tvTglInputPeminjaman)
    TextView tvTglInputPeminjaman;
    @BindView(R.id.tvTglInputPengembalian)
    TextView tvTglInputPengembalian;
    @BindView(R.id.tvTglKonfirmasiPenyewa)
    TextView tvTglKonfirmasiPenyewa;
    @BindView(R.id.tvTglDikirimPenyewa)
    TextView tvTglDikirimPenyewa;
    @BindView(R.id.tvTglDiterimaPeminjam)
    TextView tvTglDiterimaPeminjam;
    @BindView(R.id.tvTglDiterimaPenyewaKembali)
    TextView tvTglDiterimaPenyewaKembali;
    @BindView(R.id.btnBatalPesanan)
    Button btnBatalPesanan;

    HSewa hSewa;
    private GridLayoutManager gridLayoutManager;
    private TransaksiMemberDetailAdapter.AdapterList adapterList;
    ArrayList<DSewa> dSewas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaksi_vendor_detail);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        hSewa = getIntent().getParcelableExtra("hsewa");
        mPresenter.onViewPrepared(hSewa.getId());
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        srlLoader.setOnRefreshListener(this);
        gridLayoutManager = new GridLayoutManager(this, 1);
        adapterList = new TransaksiMemberDetailAdapter.AdapterList(this, dSewas, new TransaksiMemberDetailAdapter.Callback() {
            @Override
            public void onClickView(DSewa dSewa, View itemView) {

            }
        });
        rvProductDetail.setLayoutManager(gridLayoutManager);
        rvProductDetail.setAdapter(adapterList);
    }

    @OnClick(R.id.btnHeaderPanelInformasiTagihan)
    void onBtnHeaderPanelClick(View v) {
        if (clExpandableLayoutInformasiTagihan.getVisibility() == View.VISIBLE) {
            btnHeaderPanelStatus.collapse();
            clExpandableLayoutInformasiTagihan.setVisibility(View.GONE);
        } else {
            btnHeaderPanelStatus.expand();
            clExpandableLayoutInformasiTagihan.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPresenterReady(HSewaResponse.IData response) {
        srlLoader.setRefreshing(false);
        this.hSewa = response.getData();
        if (hSewa.gethPiutangUtama().getKodenota() != null) {
            tvKodenotaPiutang.setText(hSewa.gethPiutangUtama().getKodenota());
        }
        tvStatusTagihan.setText(Html.fromHtml(getResources().getString(R.string.status_tagihan, hSewa.getStatusTransaksiName().toString())));
        tvTotalTagihan.setText(Html.fromHtml(getResources().getString(R.string.total_tagihan, "Rp. " + NumberTextWatcher.getDecimalFormattedString(hSewa.getGrandtotal().toString()))));
        if (hSewa.gethPiutangUtama().getPaymentMethod() != null) {
            tvMetodePembayaran.setText(Html.fromHtml(getResources().getString(R.string.metode_pembayaran, capitalizerStr(hSewa.gethPiutangUtama().getFullPaymentMethod()))));
        }
        tvKodenotaSewa.setText(Html.fromHtml(getResources().getString(R.string.no_transaksi, hSewa.getKodenota())));
        tvNamaMemberSewa.setText(Html.fromHtml(getResources().getString(R.string.nama_member, hSewa.getUserPenyewa().getNama())));
        tvStatusSewa.setText(Html.fromHtml(getResources().getString(R.string.status, capitalizerStr(hSewa.gethPiutangUtama().getMidtransTransactionStatus()))));
        tvKeteranganSewa.setText(Html.fromHtml(getResources().getString(R.string.keterangan, "")));
        iconStepStatus.setStatusLevel(hSewa.getStatusTransaksi());
        tvTglSewa.setText(Html.fromHtml(getResources().getString(R.string.tgl, getStringDateFormat(getDateParsed(hSewa.getCreatedAt())))));
        if (hSewa.getStatusTransaksi() == MENUNGGU_KONFIRMASI) {
            btnProsesPesanan.setVisibility(View.VISIBLE);
            btnTolakPesanan.setVisibility(View.VISIBLE);
        }
        if (hSewa.getStatusTransaksi() == PESANAN_SEDANG_DIPROSES) {
            btnBatalPesanan.setVisibility(View.VISIBLE);
            btnKirimPesanan.setVisibility(View.VISIBLE);
        }
        if (hSewa.getStatusTransaksi() == PRODUK_SUDAH_DIKEMBALIKAN) {
            btnTerimaKembaliProduk.setVisibility(View.VISIBLE);
        }
        if (hSewa.getFirstBarang() != null) {
            if (hSewa.getFirstBarang().getBarangFotoUtama() != null) {
                Glide.with(this).load(hSewa.getFirstBarang().getBarangFotoUtama().getFotoUrl()).into(ivImageProduk);
                tvNamaProduk.setText(hSewa.getFirstBarang().getNama());
            }
            if (hSewa.getdSewaUtama() != null) {
                tvQtySewa.setText(Html.fromHtml(getResources().getString(R.string.qty_, String.valueOf(hSewa.getdSewas().size()))));
                tvLamaSewaProduk.setText(Html.fromHtml(getResources().getString(R.string.lama_hari_, String.valueOf(hSewa.getdSewaUtama().getLamaSewa().toString()))));
                tvHargaSewaProduk.setText(Html.fromHtml(getResources().getString(R.string.harga_satuan_, getDecimalFormattedString(String.valueOf(hSewa.getdSewaUtama().getHargaSewa())))));
            }
        }
        if (hSewa.getTglDikirimPenyewa() != null) {
            tvTglInputPeminjaman.setVisibility(View.VISIBLE);
            tvTglInputPeminjaman.setText(Html.fromHtml(getResources().getString(R.string.tgl_input_peminjaman, getStringDateFormat(getDateParsed(hSewa.getTglInputPeminjaman())))));
        }
        if (hSewa.getTglInputPengembalian() != null) {
            tvTglInputPengembalian.setVisibility(View.VISIBLE);
            tvTglInputPengembalian.setText(Html.fromHtml(getResources().getString(R.string.tgl_input_pengembalian, getStringDateFormat(getDateParsed(hSewa.getTglInputPengembalian())))));
        }
        if (hSewa.getTglKonfirmasiPenyewa() != null) {
            tvTglKonfirmasiPenyewa.setVisibility(View.VISIBLE);
            tvTglKonfirmasiPenyewa.setText(Html.fromHtml(getResources().getString(R.string.tgl_konfirmasi_penyewa, getStringDateFormat(getDateParsed(hSewa.getTglKonfirmasiPenyewa())))));
        }
        if (hSewa.getTglDikirimPenyewa() != null) {
            tvTglDikirimPenyewa.setVisibility(View.VISIBLE);
            tvTglDikirimPenyewa.setText(Html.fromHtml(getResources().getString(R.string.tgl_dikirim_penyewa, getStringDateFormat(getDateParsed(hSewa.getTglDikirimPenyewa())))));
        }
        if (hSewa.getTglDiterimaPeminjam() != null) {
            tvTglDiterimaPeminjam.setVisibility(View.VISIBLE);
            tvTglDiterimaPeminjam.setText(Html.fromHtml(getResources().getString(R.string.tgl_diterima_peminjam, getStringDateFormat(getDateParsed(hSewa.getTglDiterimaPeminjam())))));
        }
        if (hSewa.getTglDiterimaPenyewaKembali() != null) {
            tvTglDiterimaPenyewaKembali.setVisibility(View.VISIBLE);
            tvTglDiterimaPenyewaKembali.setText(Html.fromHtml(getResources().getString(R.string.tgl_diterima_penyewa_kembali, getStringDateFormat(getDateParsed(hSewa.getTglDiterimaPenyewaKembali())))));
        }
        dSewas.clear();
        dSewas.addAll(hSewa.getdSewas());
        adapterList.notifyDataSetChanged();
        constraintLayout.setVisibility(View.VISIBLE);
        removeWait();
    }

    @Override
    public void onSuccessUpdate(HSewaResponse.IData response) {
        setResult(RESULT_OK);
        finish();
    }

    public void removeWait() {
        progressBar.hide();
        clLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        mPresenter.onViewPrepared(hSewa.getId());
    }

    DialogInterface.OnClickListener dialogListenerTolakPesanan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    hSewa.setStatusTransaksi(PESANAN_DITOLAK_VENDOR);
                    mPresenter.onChangeData(hSewa);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };
    DialogInterface.OnClickListener dialogListenerProsesPesanan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    hSewa.setStatusTransaksi(PESANAN_SEDANG_DIPROSES);
                    mPresenter.onChangeData(hSewa);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogListenerKirimPesanan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    hSewa.setStatusTransaksi(PESANAN_SEDANG_DIKIRIM);
                    mPresenter.onChangeData(hSewa);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogListenerTerimaKembaliProduk = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    hSewa.setStatusTransaksi(TRANSAKSI_SELESAI);
                    mPresenter.onChangeData(hSewa);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogListenerBatalPesanan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    hSewa.setStatusTransaksi(TRANSAKSI_DIBATALKAN);
                    mPresenter.onChangeData(hSewa);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @OnClick(R.id.btnTolakPesanan)
    void onClickBtnTolakPesanan() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.tolak_pesanan))
                .setMessage(getString(R.string.anda_yakin_tolak_pesanan))
                .setPositiveButton(getString(R.string.yakin), dialogListenerTolakPesanan)
                .setNegativeButton(getString(R.string.tidak), dialogListenerTolakPesanan)
                .show();
    }

    @OnClick(R.id.btnProsesPesanan)
    void onClickBtnProsesPesanan() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.proses_pesanan))
                .setMessage(getString(R.string.anda_yakin_proses_pesanan))
                .setPositiveButton(getString(R.string.yakin), dialogListenerProsesPesanan)
                .setNegativeButton(getString(R.string.tidak), dialogListenerProsesPesanan)
                .show();
    }

    @OnClick(R.id.btnKirimPesanan)
    void onClickBtnKirimPesanan() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.kirim_pesanan))
                .setMessage(getString(R.string.anda_yakin_kirim_pesanan))
                .setPositiveButton(getString(R.string.yakin), dialogListenerKirimPesanan)
                .setNegativeButton(getString(R.string.tidak), dialogListenerKirimPesanan)
                .show();
    }

    @OnClick(R.id.btnTerimaKembaliProduk)
    void onClickBtnTerimaKembaliProduk() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.terima_kembali_produk))
                .setMessage(getString(R.string.anda_yakin_terima_kembali_produk))
                .setPositiveButton(getString(R.string.yakin), dialogListenerTerimaKembaliProduk)
                .setNegativeButton(getString(R.string.tidak), dialogListenerTerimaKembaliProduk)
                .show();
    }

    @OnClick(R.id.btnBatalPesanan)
    void onBtnBatalPesanan() {
        createAlertDialogBuilder(getViewContext(), getString(R.string.batal_pesanan), getString(R.string.anda_yakin_batalkan_pesanan), getString(R.string.yakin), getString(R.string.tidak), dialogListenerBatalPesanan).show();
    }

    @OnClick(R.id.ivChat)
    void onClickIvChat() {
        Intent intent = new Intent(getBaseContext(), MessageActivity.class);
//        intent.putExtra("barang", barang);
        intent.putExtra("usersTo", hSewa.getUserPeminjam());
        startActivity(intent);
    }
}

