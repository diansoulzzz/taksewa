package com.taksewa.taksewa.ui.a_report_barang;

import com.taksewa.taksewa.ui._base.MvpPresenter;

public interface ReportBarangMvpPresenter<V extends ReportBarangMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

