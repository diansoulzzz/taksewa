package com.taksewa.taksewa.ui.a_entry_barang;

import android.net.Uri;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class EntryBarangPresenter<V extends EntryBarangMvpView> extends BasePresenter<V>
        implements EntryBarangMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    private StorageReference storageReferenceBarang = getFirebaseStorage().getReference().child("images/barang");

    @Inject
    public EntryBarangPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void onViewPrepared() {

    }

    @Override
    public void DoGetBarangData(int brgId) {
        BarangRequest.FromID fromID = new BarangRequest.FromID();
        fromID.setId(brgId);
        getCompositeDisposable().add(getDataManager()
                .getBarangDataFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IData>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull BarangResponse.IData response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void onFileChosen(BarangFoto barangFoto) {
        Uri file = Uri.fromFile(new File(barangFoto.getFotoLocalPath()));
        String fotoUUID = UUID.randomUUID().toString();
        StorageReference imageRef = storageReferenceBarang.child(fotoUUID);
//        InputStream stream = null;
//        stream = new FileInputStream(new File(barangFoto.getFotoUrl()));
        UploadTask uploadTask = imageRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                getMvpView().onFileUploadFailed(barangFoto);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                barangFoto.setFotoUrl(taskSnapshot.);
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        barangFoto.setFotoUrl(uri.toString());
                        barangFoto.setFotoUUID(fotoUUID);
                        getMvpView().onFileUploadSuccess(barangFoto);
                    }
                });
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                // ...
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                System.out.println("Upload is " + progress + "% done");
                barangFoto.setProgressUpload(progress);
                getMvpView().onFileUploadProgress(barangFoto);
            }
        });
    }

    @Override
    public void onFileRemoved(BarangFoto barangFoto) {
        StorageReference imageRef = storageReferenceBarang.child(barangFoto.getFotoUUID());
        imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                getMvpView().onFileDeleteSuccess(barangFoto);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        });
    }

    @Override
    public void onViewSubmitObject(Barang barang) {
        BarangRequest.EntryDataObject barangRequest = new BarangRequest.EntryDataObject();
        barangRequest.setBarang(barang);
        getCompositeDisposable().add(getDataManager()
                .postVendorBarangEntryObject(barangRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IData>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull BarangResponse.IData response) throws Exception {
                        getMvpView().onEntrySuccess(response.getData());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideOverlayLoading();
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void onViewSubmit(BarangRequest.EntryData requestEntry) {
        getCompositeDisposable().add(getDataManager()
                .postVendorBarangEntry(requestEntry)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IData>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull BarangResponse.IData response) throws Exception {
                        getMvpView().onEntrySuccess(response.getData());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideOverlayLoading();
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

}

