package com.taksewa.taksewa.ui.a_verify.camera;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.otaliastudios.cameraview.BitmapCallback;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.utils.CommonUtils.saveBitmapGetPath;

public class VerifyCardTakeCameraActivity extends BaseActivity {
    private final String TAG = getClass().getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

    @BindView(R.id.cameraView)
    CameraView cameraView;
    @BindView(R.id.ivAmbilPhoto)
    ImageView ivAmbilPhoto;
    @BindView(R.id.ivCameraPreview)
    ImageView ivCameraPreview;
    @BindView(R.id.ivUlangiPhoto)
    ImageView ivUlangiPhoto;
    @BindView(R.id.ivPakaiPhoto)
    ImageView ivPakaiPhoto;

    private Bitmap imgBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_card_take_camera);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void setUp() {
        if (!hasPermission(permissions)) {
            requestPermissionsSafely(permissions, REQUEST_PERMISSIONS_REQUEST_CODE);
            return;
        }
        cameraView.setLifecycleOwner(this);
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                result.toBitmap(new BitmapCallback() {
                    @Override
                    public void onBitmapReady(@Nullable Bitmap bitmap) {
                        onPhotoTaken(bitmap);
                    }
                });
                super.onPictureTaken(result);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        cameraView.destroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        cameraView.open();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.close();
    }

    @OnClick(R.id.ivAmbilPhoto)
    void onIvAmbilPhotoClick() {
        cameraView.takePictureSnapshot();
    }

    @OnClick(R.id.ivUlangiPhoto)
    void onIvUlangiPhotoClick() {
        onPhotoRestart();
    }

    @OnClick(R.id.ivPakaiPhoto)
    void onIvPakaiPhotoClick() {
        String filePath = saveBitmapGetPath(imgBitmap);
        if (filePath != null) {
            Intent intent = new Intent();
            intent.putExtra("filePath", filePath);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void onPhotoRestart() {
        ivPakaiPhoto.setVisibility(View.INVISIBLE);
        ivUlangiPhoto.setVisibility(View.INVISIBLE);
        ivCameraPreview.setVisibility(View.INVISIBLE);
        ivAmbilPhoto.setVisibility(View.VISIBLE);
        cameraView.setVisibility(View.VISIBLE);
    }

    public void onPhotoTaken(Bitmap bitmap) {
        imgBitmap = bitmap;
        Glide.with(this).load(bitmap).into(ivCameraPreview);
        ivPakaiPhoto.setVisibility(View.VISIBLE);
        ivUlangiPhoto.setVisibility(View.VISIBLE);
        ivCameraPreview.setVisibility(View.VISIBLE);
        ivAmbilPhoto.setVisibility(View.INVISIBLE);
        cameraView.setVisibility(View.INVISIBLE);
    }
}
