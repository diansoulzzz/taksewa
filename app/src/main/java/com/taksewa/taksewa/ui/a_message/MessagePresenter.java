package com.taksewa.taksewa.ui.a_message;


import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.MessageRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class MessagePresenter<V extends MessageMvpView> extends BasePresenter<V>
        implements MessageMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public MessagePresenter(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }

    @Override
    public void onMessageSending(Message message) {
        MessageRequest.EntryData entryData = new MessageRequest.EntryData();
        entryData.setMessage(message);
        DoPostNewMessage(entryData);
    }

    @Override
    public void onViewPrepared(int usersId) {
        MessageRequest.Take10 messageRequest = new MessageRequest.Take10();
        messageRequest.setUserIdTo(usersId);
        DoGetLast10Message(messageRequest);
    }

    private void DoPostNewMessage(MessageRequest.EntryData request){
        getCompositeDisposable().add(getDataManager()
                .postNewMessage(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<MessageResponse.IData>() {
                    @Override
                    public void accept(@NonNull MessageResponse.IData response) throws Exception {
                        getMvpView().onMessageSended(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    private void DoGetLast10Message(MessageRequest.Take10 request){
        getCompositeDisposable().add(getDataManager()
                .get10Message(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<MessageResponse.IList>() {
                    @Override
                    public void accept(@NonNull MessageResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }

}
