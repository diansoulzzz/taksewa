package com.taksewa.taksewa.ui.f_akun;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_verify.VerifyActivity;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberFragment;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunFragment extends BaseFragment implements AkunMvpView {
    public final String TAG = getClass().getSimpleName();
    public final int RQ_VERIFIKASI_USER = 1;
    @Inject
    AkunMvpPresenter<AkunMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvVerifikasiUser)
    TextView tvVerifikasiUser;
    @BindView(R.id.tvNamaUser)
    TextView tvNamaUser;
    @BindView(R.id.ivGambarUser)
    ImageView tvGambarUser;
    private AkunPagerAdapter adapterViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_akun, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            setHasOptionsMenu(true);
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    protected void setUp(View view) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        adapterViewPager = new AkunPagerAdapter(getChildFragmentManager());
        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(adapterViewPager);
        tabLayout.setupWithViewPager(viewPager);
        activity.setSupportActionBar(toolbar);
        mPresenter.onViewPrepared();
    }

    public static AkunFragment newInstance() {
        Bundle args = new Bundle();
        AkunFragment fragment = new AkunFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.toolbar_akun, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_setting:
                mPresenter.attemptLogout();
                return true;
            case R.id.navigation_notifikasi:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setUserProfile(String namaUser, boolean isVerified, String gambarUser) {
        tvNamaUser.setText(namaUser);
        if (isVerified) {
            tvVerifikasiUser.setTypeface(null, Typeface.ITALIC);
            tvVerifikasiUser.setText("Verified Account");
        } else {
            tvVerifikasiUser.setText("Not Verified");
        }
        Glide.with(getBaseActivity()).load(gambarUser).into(tvGambarUser);
    }

    public static class AkunPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public AkunPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return AkunMemberFragment.newInstance(0, "Page # 1");
                case 1:
                    return AkunVendorFragment.newInstance(1, "Page # 2");
                default:
                    return AkunMemberFragment.newInstance(0, "Page # 1");
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) return "Member";
            return "Vendor";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @OnClick(R.id.tvVerifikasiUser)
    void onClickTvVerifikasiUser() {
        if ((mPresenter.isUserLogin()) && (!mPresenter.isUserVerified())) { //FIRST TIME
            Intent intent = new Intent(getContext(), VerifyActivity.class);
            startActivityForResult(intent, RQ_VERIFIKASI_USER);
        }
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        if (!hidden) {
//            if (adapterViewPager != null) {
//                adapterViewPager.notifyDataSetChanged();
//            }
//        }
//    }
}
