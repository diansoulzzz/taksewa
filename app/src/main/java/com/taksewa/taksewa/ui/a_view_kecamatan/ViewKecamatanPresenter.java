package com.taksewa.taksewa.ui.a_view_kecamatan;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ViewKecamatanPresenter<V extends ViewKecamatanMvpView> extends BasePresenter<V>
        implements ViewKecamatanMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public ViewKecamatanPresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        DoGetKecamatanList();
    }

    private void DoGetKecamatanList(){
        getCompositeDisposable().add(getDataManager()
                .getKecamatanList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<AreaResponse.IKecamatanList>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull AreaResponse.IKecamatanList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

}

