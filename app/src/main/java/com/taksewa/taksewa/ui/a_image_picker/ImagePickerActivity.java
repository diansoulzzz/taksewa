package com.taksewa.taksewa.ui.a_image_picker;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Toast;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_image_picker.camera.ImagePickerCameraFragment;
import com.taksewa.taksewa.ui.a_image_picker.gallery.ImagePickerGalleryFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagePickerActivity extends BaseActivity implements ImagePickerMvpView, ImagePickerCameraFragment.OnImagePickerCameraListener, ImagePickerGalleryFragment.OnImagePickerGalleryListener {

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;

    @Inject
    ImagePickerMvpPresenter<ImagePickerMvpView> mPresenter;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    Integer totalParentImage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_picker);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        ImagePickerPagerAdapter adapterViewPager = new ImagePickerPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        tabLayout.setupWithViewPager(viewPager);
        totalParentImage = getIntent().getIntExtra("totalParentImage", 0);
//        arraySelectedFile.addAll(data.getStringArrayListExtra("arrayFilePath"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int granted : grantResults) {
                        if (granted != PackageManager.PERMISSION_GRANTED) {
                            showMessage("Permission denied");
                            finish();
                        }
                    }
                    setUp();
                }
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onCameraPickerImagePath(String filePath) {
        ArrayList<String> arrayFilePath = new ArrayList<>();
        arrayFilePath.add(filePath);
        onImagePickerFinish(arrayFilePath);
    }

    @Override
    public void OnImagePickerGalleryPath(ArrayList<String> arrayFilePath) {
        onImagePickerFinish(arrayFilePath);
    }

    public void onImagePickerFinish(ArrayList<String> arrayFilePath) {
        Intent intent = new Intent();
        intent.putStringArrayListExtra("arrayFilePath", arrayFilePath);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof ImagePickerCameraFragment) {
            ImagePickerCameraFragment headlinesFragment = (ImagePickerCameraFragment) fragment;
            headlinesFragment.setOnImagePickerCameraListener(this);
        }
        if (fragment instanceof ImagePickerGalleryFragment) {
            ImagePickerGalleryFragment headlinesFragment = (ImagePickerGalleryFragment) fragment;
            headlinesFragment.setOnImagePickerGalleryListener(this);
        }
    }

    public class ImagePickerPagerAdapter extends FragmentPagerAdapter {

        public ImagePickerPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ImagePickerGalleryFragment.newInstance(totalParentImage);
                case 1:
                    return ImagePickerCameraFragment.newInstance();
            }
            return ImagePickerGalleryFragment.newInstance(totalParentImage);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "GALLERY";
                case 1:
                    return "CAMERA";
            }
            return null;
        }
    }
}
