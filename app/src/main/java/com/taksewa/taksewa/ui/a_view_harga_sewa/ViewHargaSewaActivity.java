package com.taksewa.taksewa.ui.a_view_harga_sewa;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.ui._base.BaseActivity;

import java.io.File;
import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewHargaSewaActivity extends BaseActivity {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvBarangHargaSewa)
    RecyclerView rvBarangHargaSewa;
    ViewHargaSewaAdapter.AdapterList adapterHargaList;
    GridLayoutManager gridLayoutManager;
    ArrayList<BarangHargaSewa> barangHargaSewas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_harga_sewa);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gridLayoutManager = new GridLayoutManager(getBaseContext(), 1);
        if (getIntent().hasExtra("barangHargaSewas")) {
//            barangHargaSewas = getIntent().getParcelableArrayListExtra("barangHargaSewas");
            barangHargaSewas.clear();
            barangHargaSewas.addAll(getIntent().getParcelableArrayListExtra("barangHargaSewas"));
        }
//        Log.d(TAG,barangHargaSewas.get(0).getLamaHari().toString());
        adapterHargaList = new ViewHargaSewaAdapter.AdapterList(getBaseContext(), barangHargaSewas);
        rvBarangHargaSewa.setLayoutManager(gridLayoutManager);
        rvBarangHargaSewa.setAdapter(adapterHargaList);
//        adapterHargaList.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
