package com.taksewa.taksewa.ui._base;
import android.content.Context;

import androidx.annotation.StringRes;

public interface MvpView {

    void showOverlayLoading();

    void hideOverlayLoading();

    void restartApp();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

    void requestPermissionsSafely(String[] permissions, int requestCode);

    boolean hasPermission(String[] permissions);

    void onNeedToVerify();

    void startVerifyActivity();

    void startLoginActivity();

    Context getViewContext();
}
