package com.taksewa.taksewa.ui.f_akun_member;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangActivity;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistActivity;

import javax.inject.Inject;

import androidx.appcompat.app.AlertDialog;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunMemberFragment extends BaseFragment implements AkunMemberMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    AkunMemberMvpPresenter<AkunMemberMvpView> mPresenter;

    @BindView(R.id.btnWishlist)
    Button btnWishlist;
    @BindView(R.id.btnLogout)
    Button btnLogout;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
//    String title;

    public static AkunMemberFragment newInstance(int page, String title) {
        AkunMemberFragment fragmentFirst = new AkunMemberFragment();
        Bundle args = new Bundle();
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        title = getArguments().getString("someTitle");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_akun_member, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.onViewPrepared();
        setHasOptionsMenu(true);
        srlLoader.setOnRefreshListener(this);
        mPresenter.onViewPrepared();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btnTerakhirdilihat)
    void onClickBtnTerakhirdilihat() {
        Intent intent = new Intent(getContext(), MemberLastSeenBarangActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnWishlist)
    void onClickBtnWishlist() {
        Intent intent = new Intent(getContext(), MemberWishlistActivity.class);
        startActivity(intent);
    }

    DialogInterface.OnClickListener dialogClickListenerLogout = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    mPresenter.setUserAsLoggedOut();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @OnClick(R.id.btnLogout)
    void onClickBtnLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity());
        builder.setMessage(getString(R.string.anda_yakin_keluar))
                .setPositiveButton(getString(R.string.keluar), dialogClickListenerLogout)
                .setNegativeButton(getString(R.string.batal), dialogClickListenerLogout)
                .show();
    }

    @Override
    public void onRefresh() {
        mPresenter.onViewPrepared();
    }

    @Override
    public void onPresenterReady(User user) {
        srlLoader.setRefreshing(false);
    }
}
