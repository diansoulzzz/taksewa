package com.taksewa.taksewa.ui.a_message;


import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.ui._base.MvpPresenter;

@PerActivity
public interface MessageMvpPresenter<V extends MessageMvpView> extends MvpPresenter<V> {

    void onMessageSending(Message messageList);

    void onViewPrepared(int usersId);
}
