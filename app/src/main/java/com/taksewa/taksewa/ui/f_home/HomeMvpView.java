package com.taksewa.taksewa.ui.f_home;

import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface HomeMvpView extends MvpView {
    void onPresenterReady(HomeResponse.GData homeResponse);

}
