package com.taksewa.taksewa.ui.a_view_barang_review;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.ReviewBarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewAdapter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewBarangReviewActivity extends BaseActivity implements ViewBarangReviewMvpView {

    private final String TAG = getClass().getSimpleName();
    private final int RQ_MEMBER_REVIEW_DETAIL = 1;
    @Inject
    ViewBarangReviewMvpPresenter<ViewBarangReviewMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;

    @BindView(R.id.rvBarang)
    RecyclerView rvBarang;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    ViewBarangReviewAdapter.AdapterList viewBarangReviewAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<ReviewBarang> reviewBarangs = new ArrayList<>();

    private int brgId;
    Barang barang = new Barang();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_review);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (getIntent().hasExtra("brgId")) {
            brgId = getIntent().getIntExtra("brgId", 0);
        }
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewBarangReviewAdapter = new ViewBarangReviewAdapter.AdapterList(this, reviewBarangs, new ViewBarangReviewAdapter.Callback() {

            @Override
            public void onClickView(ReviewBarang reviewBarang, View itemView) {

            }

            @Override
            public void onClickVendor(User user, View itemView) {

            }

            @Override
            public void onClickImage(FotoReview fotoReview) {

            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarang.setLayoutManager(gridLayoutManager);
        rvBarang.setAdapter(viewBarangReviewAdapter);
        mPresenter.onViewPrepared(brgId);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvBarang.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBarang.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(BarangResponse.IData barangResponse) {
        barang = barangResponse.getData();
        reviewBarangs.clear();
        reviewBarangs.addAll(barang.getReviewBarangs());
        viewBarangReviewAdapter.notifyDataSetChanged();
        onRemoveWait();
    }

}

