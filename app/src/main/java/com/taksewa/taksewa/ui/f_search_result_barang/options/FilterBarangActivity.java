package com.taksewa.taksewa.ui.f_search_result_barang.options;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.model.Kotum;
import com.taksewa.taksewa.model.TagID;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.io.File;
import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class FilterBarangActivity extends BaseActivity {
    private final String TAG = getClass().getSimpleName();
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.btnAktifFilter)
    Button btnAktifFilter;
    @BindView(R.id.tietHargaMin)
    TextInputEditText tietHargaMin;
    @BindView(R.id.tietHargaMax)
    TextInputEditText tietHargaMax;
    @BindView(R.id.rvBarangKategori)
    RecyclerView rvBarangKategori;
    @BindView(R.id.rvBarangSubKategori)
    RecyclerView rvBarangSubKategori;
    @BindView(R.id.rvLokasi)
    RecyclerView rvLokasi;
    @BindView(R.id.cbRating4)
    AppCompatCheckBox cbRating4;
    Filter filter;
    FilterBarangAdapter.AdapterListKategoriTag adapterListKategoriTag;
    FilterBarangAdapter.AdapterListSubKategoriTag adapterListSubKategoriTag;
    FilterBarangAdapter.AdapterListKotaTag adapterListKotaTag;
    ArrayList<BarangKategori> barangKategoris = new ArrayList<>();
    ArrayList<BarangSubKategori> barangSubKategoris = new ArrayList<>();
    ArrayList<Kotum> kotas = new ArrayList<>();
//    ArrayList<BarangKategori> filterBarangKategoris = new ArrayList<>();
//    ArrayList<BarangSubKategori> filterBarangSubKategoris = new ArrayList<>();

    private ArrayList<TagID> filterBarangKategoriIds = new ArrayList<>();
    private ArrayList<TagID> filterBarangSubKategoriIds = new ArrayList<>();
    private ArrayList<TagID> filterKotaIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("filter")) {
            filter = getIntent().getParcelableExtra("filter");
//            showMessage(String.valueOf(filter.getFilterBarangKategoriIds().size()));
//            showMessage(filter.getBarangKategoriList().get(0).getNama());
//            hargaMin = filter.getHargaMin();
//            hargaMax = filter.getHargaMax();
        }
        NumberTextWatcher.CallbackWatcher callbackHargaMin = new NumberTextWatcher.CallbackWatcher() {
            @Override
            public void onAfterTextFinish(String textAfter) {
                filter.setHargaMin(Integer.valueOf(NumberTextWatcher.trimCommaOfString(textAfter)));
//                hargaMin = Double.valueOf(NumberTextWatcher.trimCommaOfString(textAfter));
            }
        };
        NumberTextWatcher hargaMinNumber = new NumberTextWatcher(tietHargaMin, callbackHargaMin);
        tietHargaMin.addTextChangedListener(hargaMinNumber);

        NumberTextWatcher.CallbackWatcher callbackHargaMax = new NumberTextWatcher.CallbackWatcher() {
            @Override
            public void onAfterTextFinish(String textAfter) {
                filter.setHargaMax(Integer.valueOf(NumberTextWatcher.trimCommaOfString(textAfter)));
//                hargaMax = Double.valueOf(NumberTextWatcher.trimCommaOfString(textAfter));
            }
        };
        NumberTextWatcher hargaMaxNumber = new NumberTextWatcher(tietHargaMax, callbackHargaMax);
        tietHargaMax.addTextChangedListener(hargaMaxNumber);

        tietHargaMin.setText(String.valueOf(filter.getHargaMin()));
        tietHargaMax.setText(String.valueOf(filter.getHargaMax()));
        if (filter.getRating() > 0) {
            cbRating4.setChecked(true);
        }

        if (filter.getFilterBarangKategoriIds() != null) {
            filterBarangKategoriIds.addAll(filter.getFilterBarangKategoriIds());
        }
        if (filter.getFilterBarangSubKategoriIds() != null) {
            filterBarangSubKategoriIds.addAll(filter.getFilterBarangSubKategoriIds());
        }
        if (filter.getFilterKotaIds() != null) {
            filterKotaIds.addAll(filter.getFilterKotaIds());
        }


        GridLayoutManager gridLayoutManager;
        if (filter.getBarangKategoriList() != null) {
            barangKategoris.addAll(filter.getBarangKategoriList());
            for (BarangKategori brgKategori : barangKategoris) {
                boolean isEqual = false;
                for (TagID filterKategori : filterBarangKategoriIds) {
                    if (brgKategori.getId().equals(filterKategori.getId())) {
                        isEqual = true;
                    }
                }
                if (isEqual) {
                    barangKategoris.get(barangKategoris.indexOf(brgKategori)).setIsChecked(1);
                } else {
                    barangKategoris.get(barangKategoris.indexOf(brgKategori)).setIsChecked(0);
                }
            }
            adapterListKategoriTag = new FilterBarangAdapter.AdapterListKategoriTag(this, barangKategoris, new FilterBarangAdapter.CallbackKategori() {
                @Override
                public void onClickView(BarangKategori barangKategori, boolean isChecked) {
                    if (isChecked) {
                        filterBarangKategoriIds.add(new TagID(barangKategori.getId()));
//                        for (TagID tagID : filterBarangKategoriIds) {
//                            if (!tagID.getId().equals(barangKategori.getId())) {
//                                filterBarangKategoriIds.add(new TagID(barangKategori.getId()));
//                                break;
//                            }
//                        }
                    } else {
                        ArrayList<TagID> removeList = new ArrayList<>();
                        for (TagID tagID : filterBarangKategoriIds) {
                            if (tagID.getId().equals(barangKategori.getId())) {
                                removeList.add(tagID);
                            }
                        }
                        filterBarangKategoriIds.removeAll(removeList);
                    }
                }
            });
            gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
//            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2,GridLayoutManager.HORIZONTAL,false);
            rvBarangKategori.setLayoutManager(gridLayoutManager);
            rvBarangKategori.setAdapter(adapterListKategoriTag);
            adapterListKategoriTag.notifyDataSetChanged();
        }

        if (filter.getBarangSubKategoriList() != null) {
            barangSubKategoris.addAll(filter.getBarangSubKategoriList());
            for (BarangSubKategori brgSubKategori : barangSubKategoris) {
                boolean isEqual = false;
                for (TagID filterSubKategori : filterBarangSubKategoriIds) {
                    if (brgSubKategori.getId().equals(filterSubKategori.getId())) {
                        isEqual = true;
                    }
                }
                if (isEqual) {
                    barangSubKategoris.get(barangSubKategoris.indexOf(brgSubKategori)).setIsChecked(1);
                } else {
                    barangSubKategoris.get(barangSubKategoris.indexOf(brgSubKategori)).setIsChecked(0);
                }
            }
            adapterListSubKategoriTag = new FilterBarangAdapter.AdapterListSubKategoriTag(this, barangSubKategoris, new FilterBarangAdapter.CallbackSubKategori() {
                @Override
                public void onClickView(BarangSubKategori barangSubKategori, boolean isChecked) {
                    if (isChecked) {
                        filterBarangSubKategoriIds.add(new TagID(barangSubKategori.getId()));
//                        for (TagID tagID : filterBarangSubKategoriIds) {
//                            if (!tagID.getId().equals(barangSubKategori.getId())) {
//                                filterBarangSubKategoriIds.add(new TagID(barangSubKategori.getId()));
//                                break;
//                            }
//                        }
                    } else {
                        ArrayList<TagID> removeList = new ArrayList<>();
                        for (TagID tagID : filterBarangSubKategoriIds) {
                            if (tagID.getId().equals(barangSubKategori.getId())) {
                                removeList.add(tagID);
                            }
                        }
                        filterBarangSubKategoriIds.removeAll(removeList);
                    }
                }
            });
            gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
//            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2,GridLayoutManager.HORIZONTAL,false);
            rvBarangSubKategori.setLayoutManager(gridLayoutManager);
            rvBarangSubKategori.setAdapter(adapterListSubKategoriTag);
            adapterListSubKategoriTag.notifyDataSetChanged();
        }


        if (filter.getKotaList() != null) {
            kotas.addAll(filter.getKotaList());
            for (Kotum kotum : kotas) {
                boolean isEqual = false;
                for (TagID filterKota : filterKotaIds) {
                    if (kotum.getId().equals(filterKota.getId())) {
                        isEqual = true;
                    }
                }
                if (isEqual) {
                    kotas.get(kotas.indexOf(kotum)).setIsChecked(1);
                } else {
                    kotas.get(kotas.indexOf(kotum)).setIsChecked(0);
                }
            }
            adapterListKotaTag = new FilterBarangAdapter.AdapterListKotaTag(this, kotas, new FilterBarangAdapter.CallbackKota() {
                @Override
                public void onClickView(Kotum kotum, boolean isChecked) {
                    if (isChecked) {
                        filterKotaIds.add(new TagID(kotum.getId()));
                    } else {
                        ArrayList<TagID> removeList = new ArrayList<>();
                        for (TagID tagID : filterKotaIds) {
                            if (tagID.getId().equals(kotum.getId())) {
                                removeList.add(tagID);
                            }
                        }
                        filterKotaIds.removeAll(removeList);
                    }
                }
            });
            gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);

            rvLokasi.setLayoutManager(gridLayoutManager);
            rvLokasi.setAdapter(adapterListKotaTag);
            adapterListKotaTag.notifyDataSetChanged();
        }
//        barangSubKategoris.addAll(filter.getBarangSubKategoriList());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.navigation_reset:
                this.onResetFilter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onResetFilter() {
        tietHargaMin.setText("0");
        tietHargaMax.setText("0");
        filter = new Filter();
        filter.setIsOn(0);
    }

    boolean isValid() {
        boolean isvalid = true;
        if (filter.getHargaMin() > filter.getHargaMax()) {
            isvalid = false;
        }
        return isvalid;
    }

    @OnClick(R.id.btnAktifFilter)
    void onBtnAktifFilterClick() {
        if (!isValid()) {
            showMessage(getString(R.string.harga_min_lebih_kecil_dari_pada_harga_max));
            return;
        }
        filter.setFilterBarangKategoriIds(filterBarangKategoriIds);
        filter.setFilterBarangSubKategoriIds(filterBarangSubKategoriIds);
        filter.setFilterKotaIds(filterKotaIds);
        if (cbRating4.isChecked()) {
            filter.setRating(4);
        } else {
            filter.setRating(0);
        }
        Intent intent = new Intent();
        intent.putExtra("filter", filter);
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_filter_barang, menu);
        return true;
    }
}
