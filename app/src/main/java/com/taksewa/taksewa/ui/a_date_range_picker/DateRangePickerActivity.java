package com.taksewa.taksewa.ui.a_date_range_picker;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.andexert.calendarlistview.library.DatePickerController;
import com.andexert.calendarlistview.library.DayPickerView;
import com.andexert.calendarlistview.library.SimpleMonthAdapter;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.text.format.DateFormat.getDateFormat;

public class DateRangePickerActivity extends BaseActivity implements DatePickerController {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.dayPickerListView)
    DayPickerView dayPickerListView;
    @BindView(R.id.tvSelectedPengambilan)
    TextView tvSelectedPengambilan;
    @BindView(R.id.tvSelectedPengembalian)
    TextView tvSelectedPengembalian;

    Date dateStart = new Date();
    Date dateEnd = new Date();
//    SimpleMonthAdapter.CalendarDay calendarDayStart = new SimpleMonthAdapter.CalendarDay();
//    SimpleMonthAdapter.CalendarDay calendarDayEnd = new SimpleMonthAdapter.CalendarDay();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_range_picker);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dayPickerListView.setController(this);
        if (getIntent().hasExtra("dateStart") || getIntent().hasExtra("dateEnd")) {
            dateStart = new Date(getIntent().getLongExtra("dateStart", 0));
            dateEnd = new Date(getIntent().getLongExtra("dateEnd", 0));
            Log.d(TAG,"START DATE CURRENT : " +String.valueOf(dateStart.getTime()));
            Log.d(TAG,"END DATE CURRENT : " +String.valueOf(dateEnd.getTime()));
            dayPickerListView.getSelectedDays().setFirst(new SimpleMonthAdapter.CalendarDay(dateStart.getTime()));
            dayPickerListView.getSelectedDays().setLast(new SimpleMonthAdapter.CalendarDay(dateEnd.getTime()));
            tvSelectedPengambilan.setText(DateFormat.getDateInstance(DateFormat.LONG).format(dateStart));
            tvSelectedPengembalian.setText(DateFormat.getDateInstance(DateFormat.LONG).format(dateEnd));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.navigation_next:
                if ((dateStart.getTime() != 0) && (dateEnd.getTime() != 0)) {
                    onDateRangePickerResult();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDateRangePickerResult() {
        Intent intent = new Intent();
        intent.putExtra("dateStart", dateStart.getTime());
        intent.putExtra("dateEnd", dateEnd.getTime());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_date_range_picker, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public int getMaxYear() {
        return 2020;
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day) {
        dateStart = new GregorianCalendar(year, month, day).getTime();
        dateEnd = new GregorianCalendar(year, month, day).getTime();
        tvSelectedPengambilan.setText(DateFormat.getDateInstance(DateFormat.LONG).format(dateStart));
        tvSelectedPengembalian.setText(DateFormat.getDateInstance(DateFormat.LONG).format(dateEnd));
        Log.d(TAG, "Day Selected : " + day + " / " + month + " / " + year);
    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {
        Log.d(TAG, "Date range selected : " + selectedDays.getFirst().toString() + " --> " + selectedDays.getLast().toString());
        dateStart = new Date(selectedDays.getFirst().getDate().getTime());
        dateEnd = new Date(selectedDays.getLast().getDate().getTime());
        if (dateEnd.getTime()<dateStart.getTime()){
            Date flipDateStart = dateStart;
            dateStart = dateEnd;
            dateEnd = flipDateStart;
        }
        tvSelectedPengambilan.setText(DateFormat.getDateInstance(DateFormat.LONG).format(dateStart));
        tvSelectedPengembalian.setText(DateFormat.getDateInstance(DateFormat.LONG).format(dateEnd));
    }

}
