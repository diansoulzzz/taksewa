package com.taksewa.taksewa.ui.a_verify;

import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface VerifyMvpView extends MvpView {

    void onVerificationComplete(boolean isSuccess);
}
