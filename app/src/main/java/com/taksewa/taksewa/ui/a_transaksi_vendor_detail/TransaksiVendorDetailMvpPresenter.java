package com.taksewa.taksewa.ui.a_transaksi_vendor_detail;

import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpView;

public interface TransaksiVendorDetailMvpPresenter<V extends TransaksiVendorDetailMvpView> extends MvpPresenter<V> {
    void onViewPrepared(int hSewaId);

    void onChangeData(HSewa hSewa);
}

