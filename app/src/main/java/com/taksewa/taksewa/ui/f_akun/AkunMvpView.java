package com.taksewa.taksewa.ui.f_akun;

import com.taksewa.taksewa.ui._base.MvpView;

public interface AkunMvpView extends MvpView {

    void setUserProfile(String namaUser, boolean isVerified, String gambarUser) ;

}
