package com.taksewa.taksewa.ui.a_register;

import com.taksewa.taksewa.ui._base.MvpView;

public interface RegisterMvpView extends MvpView {

    boolean isFormValid();
}
