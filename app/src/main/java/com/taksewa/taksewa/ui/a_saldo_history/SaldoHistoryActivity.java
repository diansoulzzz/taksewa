package com.taksewa.taksewa.ui.a_saldo_history;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.model.HPiutang;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_saldo_withdraw.SaldoWithdrawActivity;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SaldoHistoryActivity extends BaseActivity implements SaldoHistoryMvpView {

    public final String TAG = getClass().getSimpleName();
    public final int RQ_SALDO_WITHDRAW = 1;
    @Inject
    SaldoHistoryMvpPresenter<SaldoHistoryMvpView> mPresenter;

    //    @BindView(R.id.tvSaldo)
//    TextView tvSaldo;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.rvSaldoHistory)
    RecyclerView rvSaldoHistory;
    @BindView(R.id.btnWithdraw)
    Button btnWithdraw;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.clGroup)
    ConstraintLayout clGroup;
    @BindView(R.id.fabWithdraw)
    FloatingActionButton fabWithdraw;
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getViewContext());
    SaldoHistoryAdapter.AdapterList adapterList;
    User user;
    ArrayList<HPiutang> hPiutang = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo_history);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapterList = new SaldoHistoryAdapter.AdapterList(getViewContext(), hPiutang, new SaldoHistoryAdapter.Callback() {
            @Override
            public void onClickView(HPiutang hPiutang, View itemView) {

            }
        });
        rvSaldoHistory.setLayoutManager(linearLayoutManager);
        rvSaldoHistory.setAdapter(adapterList);
        mPresenter.onViewPrepared();
    }

    public void removeWait() {
        progressBar.hide();
        clGroup.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btnWithdraw)
    void onClickBtnWithdraw() {
        startWithdrawActivity();
    }

    void startWithdrawActivity() {
        Intent intent = new Intent(getViewContext(), SaldoWithdrawActivity.class);
        startActivityForResult(intent, RQ_SALDO_WITHDRAW);
    }

    @OnClick(R.id.fabWithdraw)
    void onClickFabWithdraw() {
        startWithdrawActivity();
    }

    @Override
    public void onPresenterReady(UserResponse.IData response) {
        user = response.getData();
        if (user.getTotalSaldo() == null) return;
        hPiutang.clear();
        hPiutang.addAll(user.gethPiutangsComplete());
        adapterList.notifyDataSetChanged();
        removeWait();
//        tvSaldo.setText(String.format("Rp %s", NumberTextWatcher.getDecimalFormattedString(user.getTotalSaldo().toString())));
    }
}
