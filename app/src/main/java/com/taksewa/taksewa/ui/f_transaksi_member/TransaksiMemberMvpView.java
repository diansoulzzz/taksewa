package com.taksewa.taksewa.ui.f_transaksi_member;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface TransaksiMemberMvpView extends MvpView {

    void onPresenterReady(HSewaResponse.IList transactionResponse);
}
