package com.taksewa.taksewa.ui.a_sewa_barang;

import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_search_result.SearchResultMvpView;

public interface SewaBarangMvpPresenter<V extends SewaBarangMvpView> extends MvpPresenter<V> {

    void onViewPrepared(int brgId);

    void onViewTransactionPrepared();
}

