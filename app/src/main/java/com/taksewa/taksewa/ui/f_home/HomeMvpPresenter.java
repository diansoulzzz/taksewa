package com.taksewa.taksewa.ui.f_home;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_akun_vendor.AkunVendorMvpView;

public interface HomeMvpPresenter<V extends HomeMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

