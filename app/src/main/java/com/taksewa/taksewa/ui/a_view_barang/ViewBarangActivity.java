package com.taksewa.taksewa.ui.a_view_barang;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;

import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_image_preview.ImagePreviewActivity;
import com.taksewa.taksewa.ui.a_message.MessageActivity;
import com.taksewa.taksewa.ui.a_view_barang_review.ViewBarangReviewActivity;
import com.taksewa.taksewa.ui.a_view_harga_sewa.ViewHargaSewaActivity;
import com.taksewa.taksewa.utils.AppBarStateChangeListener;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangActivity;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import javax.inject.Inject;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewBarangActivity extends BaseActivity implements ViewBarangMvpView, ImageListener, ImageClickListener {

    private final String TAG = getClass().getSimpleName();

    @Inject
    ViewBarangMvpPresenter<ViewBarangMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.btnSewa)
    Button btnSewa;
    @BindView(R.id.carouselView)
    CarouselView carouselView;
    @BindView(R.id.appBar)
    AppBarLayout appBarLayout;
    @BindView(R.id.tvNamaBarang)
    TextView tvNamaBarang;
    @BindView(R.id.rbStarReview)
    RatingBar rbStarReview;
    @BindView(R.id.tvHargaBarang)
    TextView tvHargaBarang;
    @BindView(R.id.ivGambarVendor)
    ImageView ivGambarVendor;
    @BindView(R.id.tvNamaVendor)
    TextView tvNamaVendor;
    @BindView(R.id.tvTempatVendor)
    TextView tvTempatVendor;
    @BindView(R.id.btnChat)
    Button btnChat;
    @BindView(R.id.fabWishlist)
    FloatingActionButton fabWishlist;
    @BindView(R.id.tvDeskripsi)
    TextView tvDeskripsi;
    @BindView(R.id.tvCarouselHargaBarang)
    TextView tvCarouselHargaBarang;
    @BindView(R.id.tvNamaKategoriBarang)
    TextView tvNamaKategoriBarang;
    @BindView(R.id.tvDepositBarang)
    TextView tvDepositBarang;
    @BindView(R.id.tvStarReview)
    TextView tvStarReview;
    @BindView(R.id.clBarangVendor)
    ConstraintLayout clBarangVendor;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.ivChat)
    ImageView ivChat;
    @BindView(R.id.rvReviewBarang)
    RecyclerView rvReviewBarang;
    @BindView(R.id.clUlasanTerbaru)
    ConstraintLayout clUlasanTerbaru;
    @BindView(R.id.btnLihatSemuaUlasan)
    Button btnLihatSemuaUlasan;
    @BindView(R.id.tvStokBarang)
    TextView tvStokBarang;
    ViewBarangAdapter.AdapterList viewBarangAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<ReviewBarang> reviewBarangs = new ArrayList<>();
    ViewBarangAdapter.AdapterImage adapterImage;

    private Menu menu;
    private int brgId;
    Barang barang = new Barang();

//    Intent shareIntent = new Intent(Intent.ACTION_SEND);

    AppBarStateChangeListener addOnOffsetChangedListener = new AppBarStateChangeListener() {
        @Override
        public void onStateChanged(AppBarLayout appBarLayout, AppBarStateChangeListener.State
                state) {
            Log.d("STATE", state.name());
            if (state == AppBarStateChangeListener.State.COLLAPSED) {
                if (barang.getNama() != null) {
                    changeToolbarTitleAndColor(barang.getNama(), Color.BLACK);
                }
            } else {
                changeToolbarTitleAndColor("", Color.WHITE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void setUp() {
        if (getIntent().hasExtra("brgId")) {
            brgId = getIntent().getIntExtra("brgId", 0);
        }
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout.addOnOffsetChangedListener(addOnOffsetChangedListener);
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvReviewBarang.setLayoutManager(gridLayoutManager);
        viewBarangAdapter = new ViewBarangAdapter.AdapterList(this, reviewBarangs, new ViewBarangAdapter.Callback() {
            @Override
            public void onClickView(ReviewBarang reviewBarang, View itemView) {

            }

            @Override
            public void onClickVendor(User user, View itemView) {

            }

            @Override
            public void onClickImage(FotoReview fotoReview) {
//                showMessage(fotoReview.getFotoUrl());
                startActivityImagePreview(fotoReview.getFotoUrl());
            }
        });
        rvReviewBarang.setAdapter(viewBarangAdapter);
        mPresenter.onViewPrepared(brgId);
    }

    public void changeToolbarTitleAndColor(String title, int color) {
        toolBar.setTitle(title);
        toolBar.setTitleTextColor(color);
        toolBar.setSubtitleTextColor(color);
        if (getSupportActionBar() != null) {
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
            upArrow.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
        if (menu != null) {
            for (int i = 0; i <= menu.size() - 1; i++) {
                menu.getItem(i).getIcon().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.navigation_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
//                barang.getBarangFotoUtama().getFotoUrl(),
                String shared = String.format("Sewa %s dari vendor %s, mulai Rp %s per hari hanya di Taksewa Sekarang!", barang.getNama(), barang.getUser().getNama(), NumberTextWatcher.getDecimalFormattedString(barang.getBarangHargaSewaUtama().getHarga().toString()));
//                String shared = "(Taksewa) " + barang.getNama() + " Harga mulai " + NumberTextWatcher.getDecimalFormattedString(barang.getBarangHargaSewaUtama().getHarga().toString()) + " per hari!! Sewa Sekarang \n Hanya di aplikasi.";
                sendIntent.putExtra(Intent.EXTRA_TEXT, shared);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share Produk Taksewa"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    private ShareActionProvider shareActionProvider;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_view_barang, menu);
//        MenuItem shareItem = menu.findItem(R.id.navigation_share);
//        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
//        shareActionProvider.setShareIntent(shareIntent);
        this.menu = menu;
        return true;
    }

    @Override
    public void setImageForPosition(int position, ImageView imageView) {
        Glide.with(getApplicationContext()).load(barang.getBarangFotos().get(position).getFotoUrl()).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityImagePreview(barang.getBarangFotos().get(position).getFotoUrl());
            }
        });
    }

    @OnClick(R.id.btnSewa)
    public void onBtnSewaClick(View view) {
        Intent intent = new Intent(getBaseContext(), SewaBarangActivity.class);
        intent.putExtra("barang", barang);
        startActivity(intent);
    }

    @Override
    public void onClick(int position) {
        Log.d(TAG, String.valueOf(position));
        startActivityImagePreview(barang.getBarangFotos().get(position).getFotoUrl());
    }

    @Override
    public void onPresenterReady(BarangResponse.IData barangResponse) {
        barang = barangResponse.getData();
        tvNamaBarang.setText(barang.getNama());
        tvDeskripsi.setText(barang.getDeskripsi());
        String hargaSewaBarang = getString(R.string.view_produk_harga_lama_sewa_rp, NumberTextWatcher.getDecimalFormattedString(barang.getBarangHargaSewaUtama().getHarga().toString()), barang.getBarangHargaSewaUtama().getLamaHari().toString());
        tvHargaBarang.setText(Html.fromHtml(hargaSewaBarang));
        hargaSewaBarang = getString(R.string.view_produk_harga_lama_sewa_rp, NumberTextWatcher.getDecimalFormattedString(barang.getBarangHargaSewaUtama().getHarga().toString()), "");
        tvCarouselHargaBarang.setText(Html.fromHtml(hargaSewaBarang));
        tvNamaVendor.setText(barang.getUser().getNama());
        tvTempatVendor.setText(barang.getUser().getNoTelp());
        tvNamaKategoriBarang.setText(barang.getBarangSubKategori().getNama());
        tvDepositBarang.setText(NumberTextWatcher.getDecimalFormattedString(barang.getDepositMin().toString()));
        Glide.with(this).load(barang.getUser().getFotoUrl()).into(ivGambarVendor);
        carouselView.setImageListener(this);
        carouselView.setPageCount(barang.getBarangFotos().size());
        rbStarReview.setRating(barang.getTotalRating());
        String totalRating = "0";
        if (barang.getTotalRating() > 0) {
            totalRating = String.format(CommonUtils.getDefaultLocale(), "%.1f", barang.getTotalRating());
        }
        tvStarReview.setText(getString(R.string.rate_per_max, totalRating, "5"));
        if (barang.getReviewBarangs() != null) {
            if (barang.getReviewBarangs().size() > 0) {
                clUlasanTerbaru.setVisibility(View.VISIBLE);
                reviewBarangs.addAll(barang.getReviewBarangs());
                viewBarangAdapter.notifyDataSetChanged();
                btnLihatSemuaUlasan.setText(Html.fromHtml(getResources().getString(R.string.lihat_semua_ulasan_, String.valueOf(barang.getReviewBarangs().size()))));
            }
        }
        tvStokBarang.setText(String.valueOf(barang.getStokAvailable()));
        if (barang.getStokAvailable() <= 0) {
            btnSewa.setEnabled(false);
            btnSewa.setText(getResources().getString(R.string.stok_kosong));
            btnSewa.setBackgroundColor(getResources().getColor(R.color.gray_secondary));
        }
//        sharedIntent
//        shareIntent.setType("text/plain");
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.putExtra(Intent.EXTRA_TEXT, barang.getNama());

//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
//        sendIntent.setType("text/plain");
//        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    @Override
    public void onPresenterWishlisted(BarangResponse.IWishListed wishListedResponse) {
        if (wishListedResponse.getData() != null) {
            fabWishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
        } else
            fabWishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
        hideOverlayLoading();
    }

    @Override
    public void onPresenterWishlisted(BarangResponse.IWishListed wishListedResponse, boolean isShowToast) {
        onPresenterWishlisted(wishListedResponse);
        if (isShowToast) {
            if (wishListedResponse.getData() != null) {
                showMessage(barang.getNama() + " ditambahkan ke wishlist");
            } else showMessage(barang.getNama() + " telah dihapus dari wishlist");
        }
    }

    @Override
    public void onProductRegisteredByThisUserLogin() {
        fabWishlist.setEnabled(false);
        btnSewa.setEnabled(false);
        btnSewa.setVisibility(View.GONE);
        clBarangVendor.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.GONE);
        fabWishlist.setVisibility(View.GONE);
    }

    @OnClick(R.id.ivChat)
    void onClickIvChat() {
        onClickBtnChat();
    }

    @OnClick(R.id.tvHargaBarangList)
    void onClickTvHargaBarangList() {
        Intent intent = new Intent(getBaseContext(), ViewHargaSewaActivity.class);
        intent.putParcelableArrayListExtra("barangHargaSewas", barang.getBarangHargaSewas());
        startActivity(intent);
    }

    @OnClick(R.id.btnChat)
    void onClickBtnChat() {
        Intent intent = new Intent(getBaseContext(), MessageActivity.class);
        intent.putExtra("barang", barang);
        intent.putExtra("usersTo", barang.getUser());
        startActivity(intent);
    }

    public void startActivityImagePreview(String filePath) {
        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra("fotoUrl", filePath);
        startActivity(intent);
    }

    @OnClick(R.id.fabWishlist)
    void onClickFabWishlist() {
        if (!mPresenter.isUserLogin()) {
            startLoginActivity();
        }
        showOverlayLoading();
        mPresenter.onFabWishlistClick(brgId);
    }

    @OnClick(R.id.btnLihatSemuaUlasan)
    void onClickBtnLihatSemuaUlasan() {
        Intent intent = new Intent(this, ViewBarangReviewActivity.class);
        intent.putExtra("brgId", brgId);
        startActivity(intent);
    }
}

