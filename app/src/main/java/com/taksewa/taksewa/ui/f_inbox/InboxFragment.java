package com.taksewa.taksewa.ui.f_inbox;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewActivity;
import com.taksewa.taksewa.ui.a_message.MessageActivity;
import com.taksewa.taksewa.ui.a_message_list.MessageListActivity;

import javax.inject.Inject;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InboxFragment extends BaseFragment implements InboxMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    InboxPresenter<InboxMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;

    public static InboxFragment newInstance() {
        Bundle args = new Bundle();
        InboxFragment fragment = new InboxFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        srlLoader.setOnRefreshListener(this);
        mPresenter.onViewPrepared();
        setHasOptionsMenu(true);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolBar);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btnChat)
    void onClickBtnChat() {
        Intent intent = new Intent(getContext(), MessageListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnUlasan)
    void onClickBtnUlasan() {
        Intent intent = new Intent(getContext(), MemberReviewActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        srlLoader.setRefreshing(false);
    }

}