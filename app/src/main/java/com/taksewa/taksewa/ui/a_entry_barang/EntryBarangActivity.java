package com.taksewa.taksewa.ui.a_entry_barang;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.Nullable;

import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_entry_barang_harga.EntryBarangHargaActivity;
import com.taksewa.taksewa.ui.a_image_picker.ImagePickerActivity;
import com.taksewa.taksewa.ui.a_image_preview.ImagePreviewActivity;
import com.taksewa.taksewa.ui.a_view_barang_kategori.ViewBarangKategoriActivity;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EntryBarangActivity extends BaseActivity implements EntryBarangMvpView {

    private final String TAG = getClass().getSimpleName();
    private final int ENTRY_BARANG_RQ = 1;
    private final int VIEW_BARANG_KATEGORI_RQ = 2;
    private final int VIEW_BARANG_HARGA_RQ = 3;
    @Inject
    EntryBarangMvpPresenter<EntryBarangMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.tietMinDepositBarang)
    TextInputEditText tietMinDepositBarang;
    @BindView(R.id.tietStokBarang)
    TextInputEditText tietStokBarang;
    @BindView(R.id.rvGambarList)
    RecyclerView rvGambarList;
    @BindView(R.id.btnPilihGambar)
    Button btnPilihGambar;
    @BindView(R.id.btnPilihKategori)
    Button btnPilihKategori;
    @BindView(R.id.btnInputHargaBarang)
    Button btnInputHargaBarang;
    @BindView(R.id.tietNamaBarang)
    TextInputEditText tietNamaBarang;
    @BindView(R.id.tietDeskripsiBarang)
    TextInputEditText tietDeskripsiBarang;
    @BindView(R.id.clGroupImage)
    ConstraintLayout clGroupImage;

    EntryBarangAdapter.AdapterEntry entryBarangAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<BarangFoto> arraySelectedFile = new ArrayList<>();
    BarangFoto barangTampung = new BarangFoto();
    ArrayList<BarangHargaSewa> barangHargaSewas = new ArrayList<>();
    ArrayList<BarangFoto> barangFotos = new ArrayList<>();
    //    BarangRequest.EntryData requestEntry = new BarangRequest.EntryData();
    BarangSubKategori barangSubKategori = new BarangSubKategori();
    BarangHargaSewa firstBarangHargaSewa = new BarangHargaSewa();

    Barang barang = new Barang();
    boolean isEditing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (getIntent().hasExtra("brgId")) {
            mPresenter.DoGetBarangData(getIntent().getIntExtra("brgId", 0));
            toolBar.setTitle(getResources().getString(R.string.edit_produk));
            isEditing = true;
        }
        barangTampung.setFotoLocalPath("");
        barangTampung.setUtama(0);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tietMinDepositBarang.addTextChangedListener(new NumberTextWatcher(tietMinDepositBarang));
        tietStokBarang.addTextChangedListener(new NumberTextWatcher(tietStokBarang));
        gridLayoutManager = new GridLayoutManager(this, 5);
        rvGambarList.setLayoutManager(gridLayoutManager);
        entryBarangAdapter = new EntryBarangAdapter.AdapterEntry(getBaseContext(), arraySelectedFile, new EntryBarangAdapter.CallbackImage() {
            @Override
            public void onClickView(BarangFoto imageGalleryPath, View itemView) {
//                if (imageGalleryPath.getFotoLocalPath().equals("")) {
//                    startActivityImagePickerActivity();
//                    return;
//                }
                PopupMenu popupMenu = new PopupMenu(getBaseContext(), itemView);
                popupMenu.inflate(R.menu.popup_entry_barang);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popup_delete:
                                removeImageAdapter(imageGalleryPath);
                                return true;
                            case R.id.popup_set_primary:
                                if (arraySelectedFile.contains(imageGalleryPath)) {
                                    for (BarangFoto arrayBarangFoto : arraySelectedFile) {
                                        arrayBarangFoto.setUtama(0);
                                    }
                                    arraySelectedFile.get(arraySelectedFile.indexOf(imageGalleryPath)).setUtama(1);
                                    entryBarangAdapter.notifyDataSetChanged();
                                }
//                                itemView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                return true;
                            case R.id.popup_show:
                                startActivityImagePreview(imageGalleryPath.getFotoLocalPath());
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }

            @Override
            public void onClickAdd() {
                startActivityImagePickerActivity();
//                if (imageGalleryPath.getFotoLocalPath().equals("")) {
//                    startActivityImagePickerActivity();
//                    return;
//                }
            }
        });
        rvGambarList.setAdapter(entryBarangAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                this.finish();
                onCancel();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startActivityImagePickerActivity() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra("totalParentImage", arraySelectedFile.size() - 1);
        startActivityForResult(intent, ENTRY_BARANG_RQ);
    }


    public void startActivityImagePreview(String filePath) {
        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra("filePath", filePath);
        startActivity(intent);
    }

    @OnClick(R.id.btnPilihGambar)
    void onBtnPilihGambarClick() {
        startActivityImagePickerActivity();
    }

    @OnClick(R.id.btnSimpan)
    void onBtnSimpanClick() {
        if (!isValid()) {
            return;
        }
        showOverlayLoading();
        int depositMin = Integer.valueOf(NumberTextWatcher.trimCommaOfString(tietMinDepositBarang.getText().toString()));
        int stok = Integer.valueOf(NumberTextWatcher.trimCommaOfString(tietStokBarang.getText().toString()));
        barangFotos.clear();
        for (BarangFoto barangFoto : arraySelectedFile) {
            if (!barangFoto.equals(barangTampung)) {
                barangFotos.add(barangFoto);
            }
        }

//        changed
        /*
        requestEntry.setDeskripsi(tietDeskripsiBarang.getText().toString());
        requestEntry.setNama(tietNamaBarang.getText().toString());
        requestEntry.setDepositMin(depositMin);
        requestEntry.setStok(stok);
        requestEntry.setBarangSubKategoriId(Integer.valueOf(btnPilihKategori.getTag().toString()));
        requestEntry.setBarangFotos(barangFotos);
        requestEntry.setBarangHargaSewas(barangHargaSewas);
        mPresenter.onViewSubmit(requestEntry);
        */
        barang.setDeskripsi(tietDeskripsiBarang.getText().toString());
        barang.setNama(tietNamaBarang.getText().toString());
        barang.setDepositMin(depositMin);
        barang.setStok(stok);
        barang.setBarangSubKategoriId(Integer.valueOf(btnPilihKategori.getTag().toString()));
        barang.setBarangFotos(barangFotos);
        barang.setBarangHargaSewas(barangHargaSewas);
        mPresenter.onViewSubmitObject(barang);

    }

    public boolean isValid() {
        boolean valid = true;
        if (TextUtils.isEmpty(tietNamaBarang.getText())) {
            tietNamaBarang.setError(getResources().getString(R.string.error_nama_empty));
            valid = false;
        }
        if (TextUtils.isEmpty(tietStokBarang.getText())) {
            tietStokBarang.setError(getResources().getString(R.string.error_stok_empty));
            valid = false;
        }
        if (TextUtils.isEmpty(tietMinDepositBarang.getText())) {
            tietMinDepositBarang.setError(getResources().getString(R.string.error_stok_empty));
            valid = false;
        }
        if (TextUtils.isEmpty(tietDeskripsiBarang.getText())) {
            tietDeskripsiBarang.setError(getResources().getString(R.string.error_deskripsi_empty));
            valid = false;
        }
        int depositMin = Integer.valueOf(NumberTextWatcher.trimCommaOfString(tietMinDepositBarang.getText().toString()));
//        int stok = Integer.valueOf(NumberTextWatcher.trimCommaOfString(tietStokBarang.getText().toString()));
        if (depositMin < 1) {
            tietMinDepositBarang.setError(getResources().getString(R.string.error_min_deposit_min));
            valid = false;
        }
        if (barangHargaSewas.size() < 1) {
            btnInputHargaBarang.setError(getResources().getString(R.string.error_input_harga_empty));
            valid = false;
        } else {
            btnInputHargaBarang.setError(null);
        }
        if (btnPilihKategori.getTag() == null) {
            btnPilihKategori.setError(getResources().getString(R.string.error_picker_kategori_empty));
            valid = false;
        } else {
            btnPilihKategori.setError(null);
        }
        if (arraySelectedFile.size() < 1) {
            btnPilihGambar.setError(getResources().getString(R.string.error_picker_image_empty));
            valid = false;
        }
        boolean isAllImageUploaded = true;
        for (BarangFoto barangFoto : arraySelectedFile) {
            if (!barangFoto.equals(barangTampung)) {
                Log.d(TAG, String.valueOf(barangFoto.getFotoUrl()));
                Log.d(TAG, String.valueOf(barangFoto.getProgressUpload()));
                Log.d(TAG, String.valueOf(barangFoto.getFotoUUID()));
                if (barangFoto.getFotoUrl() == null) {
                    isAllImageUploaded = false;
                }
            }
        }
        if (!isAllImageUploaded) {
            showMessage(R.string.error_picker_image_progress);
            valid = false;
        }
        return valid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ENTRY_BARANG_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
//                arraySelectedFile.addAll(data.getStringArrayListExtra("arrayFilePath"));
                ArrayList<String> arrayFilePath = data.getStringArrayListExtra("arrayFilePath");
                for (String filePath : arrayFilePath) {
                    BarangFoto barangFoto = new BarangFoto();
                    barangFoto.setFotoLocalPath(filePath);
                    barangFoto.setUtama(0);
                    mPresenter.onFileChosen(barangFoto);
                }
//                setAdapterSelectedImage();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        if (requestCode == VIEW_BARANG_KATEGORI_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                barangSubKategori = data.getParcelableExtra("barangSubKategori");
                applySubAndKategoriBarang();
                /*
//                String barangKategoriId = data.getStringExtra("barangKategoriId");
                String barangKategoriNama = getString(R.string.view_produk_kategori_picker, barangSubKategori.getBarangKategori().getNama() + " / " + barangSubKategori.getNama());
//                btnPilihKategori.setText(Html.fromHtml(barangKategoriNama));
//                barangKategoriNama = "This is <font color='red'>simple</font>.";
                btnPilihKategori.setText(Html.fromHtml(barangKategoriNama), TextView.BufferType.SPANNABLE);
//                Log.d(TAG,barangKategoriId);
                btnPilihKategori.setTag(barangSubKategori.getId());
//                barang.setBarangSubKategoriId(Integer.valueOf(barangKategoriId));
                btnPilihKategori.setError(null);
                */
            }
        }
        if (requestCode == VIEW_BARANG_HARGA_RQ) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                barangHargaSewas = data.getParcelableArrayListExtra("barangHargaSewas");
                applyHargaBarang();
                /*
                firstBarangHargaSewa = new BarangHargaSewa();
                for (BarangHargaSewa barangHargaSewa : barangHargaSewas) {
                    if (barangHargaSewa.getLamaHari() == 1) {
                        firstBarangHargaSewa = barangHargaSewa;
                        break;
                    }
                }
                String hargaMulai = NumberTextWatcher.getDecimalFormattedString(firstBarangHargaSewa.getHarga().toString());
//                String hargaSampai = NumberTextWatcher.getDecimalFormattedString(barangHargaSewas.get(barangHargaSewas.size() - 1).getHarga().toString());
                String hargaSewaBarang = getString(R.string.input_produk_harga_sewa_picker_rp, hargaMulai, firstBarangHargaSewa.getLamaHari().toString());
                btnInputHargaBarang.setText(Html.fromHtml(hargaSewaBarang), TextView.BufferType.SPANNABLE);
                if (barangHargaSewas.size() < 1) {
                    btnInputHargaBarang.setError(getResources().getString(R.string.error_input_harga_empty));
                } else {
                    btnInputHargaBarang.setError(null);
                }
//                barang.setBarangHargaSewas(barangHargaSewas);
                */
            }
        }
    }

    public void applyHargaBarang() {
        firstBarangHargaSewa = new BarangHargaSewa();
        for (BarangHargaSewa barangHargaSewa : barangHargaSewas) {
            if (barangHargaSewa.getLamaHari() == 1) {
                firstBarangHargaSewa = barangHargaSewa;
                break;
            }
        }
        String hargaMulai = NumberTextWatcher.getDecimalFormattedString(firstBarangHargaSewa.getHarga().toString());
        String hargaSewaBarang = getString(R.string.input_produk_harga_sewa_picker_rp, hargaMulai, firstBarangHargaSewa.getLamaHari().toString());
        btnInputHargaBarang.setText(Html.fromHtml(hargaSewaBarang), TextView.BufferType.SPANNABLE);
        if (barangHargaSewas.size() < 1) {
            btnInputHargaBarang.setError(getResources().getString(R.string.error_input_harga_empty));
        } else {
            btnInputHargaBarang.setError(null);
        }
    }

    public void applySubAndKategoriBarang() {
        String barangKategoriNama = getString(R.string.view_produk_kategori_picker, barangSubKategori.getBarangKategori().getNama() + " / " + barangSubKategori.getNama());
        btnPilihKategori.setText(Html.fromHtml(barangKategoriNama), TextView.BufferType.SPANNABLE);
        btnPilihKategori.setTag(barangSubKategori.getId());
        btnPilihKategori.setError(null);
    }

    public void removeImageAdapter(BarangFoto barangFoto) {
        if (barangFoto.getUtama() == 1) {
            showMessage("Foto utama tidak dapat dihapus!");
            return;
        }
        if (barangFoto.getFotoUUID() != null) {
            mPresenter.onFileRemoved(barangFoto);
        }
        arraySelectedFile.remove(barangFoto);
        setAdapterSelectedImage();
    }

    public void setAdapterSelectedImage() {
        arraySelectedFile.remove(barangTampung);
        if (arraySelectedFile.size() < 5) {
            arraySelectedFile.add(barangTampung);
        }
        boolean isBoolAvailable = false;
        for (BarangFoto isAvailable : arraySelectedFile) {
            if (isAvailable.getUtama() == 1) {
                isBoolAvailable = true;
            }
        }
        if (!isBoolAvailable) {
            arraySelectedFile.get(0).setUtama(1);
        }

        btnPilihGambar.setVisibility(View.GONE);
        rvGambarList.setVisibility(View.VISIBLE);
        entryBarangAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btnPilihKategori)
    void onBtnPilihKategoriClick() {
        Intent intent = new Intent(this, ViewBarangKategoriActivity.class);
        startActivityForResult(intent, VIEW_BARANG_KATEGORI_RQ);
    }

    @OnClick(R.id.btnInputHargaBarang)
    void onBtnInputHargaBarang() {
        Intent intent = new Intent(this, EntryBarangHargaActivity.class);
        intent.putParcelableArrayListExtra("barangHargaSewas", barangHargaSewas);
        startActivityForResult(intent, VIEW_BARANG_HARGA_RQ);
    }

    @Override
    public void onFileUploadSuccess(BarangFoto barangFoto) {
        Log.d(TAG, "Upload success " + barangFoto.getFotoUrl());
        setAdapterSelectedImage();
    }

    @Override
    public void onFileUploadFailed(BarangFoto barangFoto) {
        Log.d(TAG, "Upload failed");
    }

    @Override
    public void onFileUploadProgress(BarangFoto barangFoto) {
        Log.d(TAG, "Upload progress " + barangFoto.getProgressUpload());
        if (!arraySelectedFile.contains(barangFoto)) {
            arraySelectedFile.add(barangFoto);
        }
        setAdapterSelectedImage();
    }

    @Override
    public void onFileDeleteSuccess(BarangFoto barangFoto) {
//        arraySelectedFile.remove(barangFoto);
//        setAdapterSelectedImage();
    }

    @Override
    public void onEntrySuccess(Barang barang) {
        hideOverlayLoading();
        showMessage(getResources().getString(R.string.success_entry_produk, barang.getNama()));

        Intent intent = new Intent();
        intent.putExtra("barang", barang);
        setResult(RESULT_OK, intent);

        finish();
    }

    @Override
    public void onPresenterReady(BarangResponse.IData barangResponse) {
        barang = barangResponse.getData();
        barangSubKategori = barang.getBarangSubKategori();
        applySubAndKategoriBarang();
        barangHargaSewas = barang.getBarangHargaSewas();
        applyHargaBarang();
        for (BarangFoto barangFoto : barang.getBarangFotos()) {
            barangFoto.setProgressUpload(100);
            barangFoto.setFotoLocalPath(barangFoto.getFotoUrl());
            barangFoto.setFotoUrl(barangFoto.getFotoUrl());
            arraySelectedFile.add(barangFoto);
        }
//        arraySelectedFile.addAll(barang.getBarangFotos());
        entryBarangAdapter.notifyDataSetChanged();
        setAdapterSelectedImage();
        tietDeskripsiBarang.setText(barang.getDeskripsi());
        tietNamaBarang.setText(barang.getNama());
        tietMinDepositBarang.setText(NumberTextWatcher.getDecimalFormattedString(barang.getDepositMin().toString()));
        tietStokBarang.setText(NumberTextWatcher.getDecimalFormattedString(barang.getStok().toString()));
    }

    DialogInterface.OnClickListener dialogListenerBatalProduk = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    finish();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    public void onCancel() {
        Spanned title = Html.fromHtml(getString(R.string.batal_tambah_produk));
        Spanned message = Html.fromHtml(getString(R.string.anda_yakin_batal_tambah_produk));
        if (isEditing) {
            title = Html.fromHtml(getString(R.string.batal_lakukan_perubahan));
            message = Html.fromHtml(getString(R.string.anda_yakin_batal_edit_, barang.getNama()));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString(R.string.yakin), dialogListenerBatalProduk)
                .setNegativeButton(getString(R.string.tidak), dialogListenerBatalProduk)
                .show();
    }

    @Override
    public void onBackPressed() {
        onCancel();
    }

}
