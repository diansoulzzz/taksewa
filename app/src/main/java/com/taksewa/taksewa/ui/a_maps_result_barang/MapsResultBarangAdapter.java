package com.taksewa.taksewa.ui.a_maps_result_barang;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsResultBarangAdapter {
    public interface Callback {
        void onClickView(Barang data, View itemView);
    }

    public interface onMapAdapterEventListener {
        void onMapInfoWindowClick(Barang barang);

        void onMarkerClicked(Marker marker, Barang barang, View mWindow, ImageView imageView, Integer position);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<Barang> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang_list, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.empty_title_wishlist), Html.fromHtml(parent.getResources().getString(R.string.empty_subtitle_wishlist)).toString());
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (datas != null && datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaBarang)
            TextView tvNamaBarang;
            @BindView(R.id.tvHargaBarang)
            TextView tvHargaBarang;
            @BindView(R.id.ivGambarBarang)
            ImageView ivGambarBarang;
            @BindView(R.id.ivSelected)
            ImageView ivSelected;
            String formatedHarga;
            String formatedLamaHari;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final Barang data = datas.get(i);
                assert data != null;
                if (data.getNama() != null) {
                    tvNamaBarang.setText(data.getNama());
                }
                if (data.getBarangFotoUtama().getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getBarangFotoUtama().getFotoUrl()).into(ivGambarBarang);
                }
                if (data.getBarangHargaSewaUtama() != null) {
                    formatedHarga = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getHarga().toString());
                    formatedLamaHari = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getLamaHari().toString());
                    tvHargaBarang.setText(Html.fromHtml(itemView.getResources().getString(R.string.view_produk_harga_lama_sewa_rp, formatedHarga, "")));
                }
                if (data.getIsSelected() != null) {
                    if (data.getIsSelected() == 1) {
                        ivSelected.setVisibility(View.VISIBLE);
                    }
                } else ivSelected.setVisibility(View.GONE);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClickView(data, view);
                    }
                });
            }
        }
    }

    public static class AdapterMaps implements GoogleMap.InfoWindowAdapter,
            GoogleMap.OnMarkerClickListener,
            GoogleMap.OnInfoWindowClickListener {

        @BindView(R.id.imgBarang)
        ImageView imgBarang;
        @BindView(R.id.tvNamaBarang)
        TextView tvNamaBarang;
        @BindView(R.id.tvLokasiVendor)
        TextView tvLokasiVendor;

        private final View mWindow;
        private List<Barang> data;
        private Context context;
        public final String TAG = getClass().getSimpleName();
        private final onMapAdapterEventListener listener;

        public AdapterMaps(Context context, List<Barang> data, onMapAdapterEventListener listener) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mWindow = inflater.inflate(R.layout.item_map_window, null);
            ButterKnife.bind(this, mWindow);
            this.context = context;
            this.data = data;
            this.listener = listener;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            Log.d(TAG, TAG + "window" + marker.isInfoWindowShown());
            return mWindow;
        }

        @Override
        public View getInfoContents(final Marker marker) {
            Log.d(TAG, "context");
            return mWindow;
        }

        @Override
        public boolean onMarkerClick(final Marker marker) {
            int position = Integer.valueOf(marker.getTag().toString());
            listener.onMarkerClicked(marker, data.get(position), mWindow, imgBarang, position);
            return false;
        }

//        private void onImageRender(final Marker marker) {
//            int position = Integer.valueOf(marker.getTag().toString());
//            Glide.with(mWindow).asBitmap().load(data.get(position).getBarangFotoUtama().getFotoUrl()).into(new SimpleTarget<Bitmap>(200, 200) {
//                @Override
//                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                    Drawable drawable = new BitmapDrawable(context.getResources(), resource);
//                    drawable.setBounds(0, 0, 200, 200);
//                    imgBarang.setImageDrawable(drawable);
//                }
//            });
//        }

        @Override
        public void onInfoWindowClick(Marker marker) {
            int position = Integer.valueOf(marker.getTag().toString());
            onMapInfoWindowClickListener(data.get(position), listener);
        }

        public void onMapInfoWindowClickListener(Barang item, final onMapAdapterEventListener listener) {
            listener.onMapInfoWindowClick(item);
        }
    }
}
