package com.taksewa.taksewa.ui.a_vendor_barang_list;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.StatusResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class VendorBarangListPresenter<V extends VendorBarangListMvpView> extends BasePresenter<V>
        implements VendorBarangListMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public VendorBarangListPresenter(DataManager dataManager,
                                     SchedulerProvider schedulerProvider,
                                     CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void onViewPrepared() {
        getVendorBarangList();
    }

    public void getVendorBarangList() {
        getCompositeDisposable().add(getDataManager()
                .getVendorBarangList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IList>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }

    @Override
    public void postVendorBarangDelete(Barang barang) {
        BarangRequest.FromID fromID = new BarangRequest.FromID();
        fromID.setId(barang.getId());
        getCompositeDisposable().add(getDataManager()
                .postVendorBarangDelete(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IList>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IList response) throws Exception {
                        getMvpView().showMessage(String.format("%s berhasil dihapus.", barang.getNama()));
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }
}

