package com.taksewa.taksewa.ui.a_message;

import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface MessageMvpView extends MvpView {

    void onPresenterReady(MessageResponse.IList messageResponseList);

    void onMessageSended(MessageResponse.IData messageResponseData);
}
