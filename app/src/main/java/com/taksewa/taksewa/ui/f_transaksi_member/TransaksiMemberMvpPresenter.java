package com.taksewa.taksewa.ui.f_transaksi_member;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_transaksi.TransaksiMvpView;

public interface TransaksiMemberMvpPresenter<V extends TransaksiMemberMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

