package com.taksewa.taksewa.ui.a_main;


import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui._base.MvpPresenter;

@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {
//    boolean isUserLogin();
}
