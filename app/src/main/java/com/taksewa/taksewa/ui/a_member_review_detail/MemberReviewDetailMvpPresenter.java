package com.taksewa.taksewa.ui.a_member_review_detail;

import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.request.ReviewBarangRequest;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.ReviewBarang;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;

public interface MemberReviewDetailMvpPresenter<V extends MemberReviewDetailMvpView> extends MvpPresenter<V> {

    void onViewPrepared(int hsewaId);

    void onFileChosen(FotoReview fotoReview);

    void onFileRemoved(FotoReview fotoReview);

    void onViewSubmit(ReviewBarangRequest.EntryData requestEntry);

}

