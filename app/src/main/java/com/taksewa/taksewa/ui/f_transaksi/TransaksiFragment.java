package com.taksewa.taksewa.ui.f_transaksi;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberFragment;
import com.taksewa.taksewa.ui.f_transaksi_vendor.TransaksiVendorFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransaksiFragment extends BaseFragment implements TransaksiMvpView {

    @Inject
    TransaksiPresenter<TransaksiMvpView> mPresenter;
    @BindView(R.id.toolBar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private TransaksiPagerAdapter adapterViewPager;

    public static TransaksiFragment newInstance() {
        Bundle args = new Bundle();
        TransaksiFragment fragment = new TransaksiFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaksi, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;

    }

    @Override
    protected void setUp(View view) {
        mPresenter.onViewPrepared();
        setHasOptionsMenu(true);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        adapterViewPager = new TransaksiPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        tabLayout.setupWithViewPager(viewPager);
        activity.setSupportActionBar(toolbar);
    }

    public static class TransaksiPagerAdapter extends FragmentStatePagerAdapter {
        private static int NUM_ITEMS = 2;

        public TransaksiPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return TransaksiMemberFragment.newInstance();
                case 1:
                    return TransaksiVendorFragment.newInstance();
                default:
                    return TransaksiMemberFragment.newInstance();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) return "Member";
            return "Vendor";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            if (adapterViewPager != null) {
                adapterViewPager.notifyDataSetChanged();
            }
        }
    }

}