package com.taksewa.taksewa.ui.a_message;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.messaging.RemoteMessage;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.MessageResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.RemoteMessageWrapper;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class MessageActivity extends BaseActivity implements MessageMvpView {

    public final String TAG = getClass().getSimpleName();
    @Inject
    MessageMvpPresenter<MessageMvpView> mPresenter;

    @BindView(R.id.rvMessages)
    RecyclerView rvMessages;
    @BindView(R.id.btnSend)
    ImageView btnSend;
    @BindView(R.id.etTextMessage)
    EditText etTextMessage;
    @BindView(R.id.ivGambarTo)
    ImageView ivGambarTo;
    @BindView(R.id.tvTo)
    TextView tvTo;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.clMessageCustom)
    ConstraintLayout clMessageCustom;
    @BindView(R.id.ivImageCustom)
    ImageView ivImageCustom;
    @BindView(R.id.btnSendCustom)
    Button btnSendCustom;
    @BindView(R.id.btnCancelCustom)
    Button btnCancelCustom;
    @BindView(R.id.tvTextCustom)
    TextView tvTextCustom;
    MessageAdapter.AdapterList adapterList;
    GridLayoutManager gridLayoutManager;

    ArrayList<Message> messageItems = new ArrayList<>();
    User userTo = new User();
    Date c = Calendar.getInstance().getTime();
    Barang barang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void setUp() {
        if (!mPresenter.isUserLogin()) {
            startLoginActivity();
            finish();
            return;
        }
        toolBar.setTitle("");
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getIntent().hasExtra("usersTo")) {
            userTo = getIntent().getParcelableExtra("usersTo");
        }
        if (getIntent().hasExtra("notification_data")) {
            RemoteMessageWrapper remoteMessageWrapper = getIntent().getParcelableExtra("notification_data");
            Map<String, String> data = remoteMessageWrapper.getRemoteMessage().getData();
            int userIdTo = Integer.valueOf(data.get("users_id_to"));
            int userIdFrom = Integer.valueOf(data.get("users_id_from"));
            int isRead = Integer.valueOf(data.get("is_read"));
            String uuid = data.get("uuid");
            String type = data.get("type");
            String value = data.get("value");
            String userNamaFrom = data.get("users_nama_from");
            String usersFotoUrlFrom = data.get("users_foto_url_from");
            userTo.setId(userIdFrom);
            userTo.setNama(userNamaFrom);
            userTo.setFotoUrl(usersFotoUrlFrom);
        }
        if (userTo.getNama() != null) {
            tvTo.setText(userTo.getNama());
        }
        if (userTo.getFotoUrl() != null) {
            Glide.with(this).load(userTo.getFotoUrl()).apply(new RequestOptions().centerCrop()).into(ivGambarTo);
        }
        if (getIntent().hasExtra("barang")) {
            barang = getIntent().getParcelableExtra("barang");
            Glide.with(this).load(barang.getBarangFotoUtama().getFotoUrl()).into(ivImageCustom);
            tvTextCustom.setText(barang.getNama());
            clMessageCustom.setVisibility(View.VISIBLE);
        } else {
            clMessageCustom.setVisibility(View.GONE);
        }
        gridLayoutManager = new GridLayoutManager(this, 1);
        gridLayoutManager.setReverseLayout(true);
        adapterList = new MessageAdapter.AdapterList(this, messageItems, new MessageAdapter.Callback() {
            @Override
            public void onClickView() {

            }

            @Override
            public void onClickViewBarang(Barang barang) {
                Intent intent = new Intent(getBaseContext(), ViewBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivity(intent);
            }
        });
        rvMessages.setLayoutManager(gridLayoutManager);
        rvMessages.setAdapter(adapterList);
        mPresenter.onViewPrepared(userTo.getId());
    }

    boolean isValid() {
        boolean valid = true;
        if (TextUtils.isEmpty(etTextMessage.getText())) {
            valid = false;
        }
        return valid;
    }

    @Override
    public void onPresenterReady(MessageResponse.IList messageResponseList) {
//        showMessage("S");
        messageItems.addAll(messageResponseList.getData());
        adapterList.notifyDataSetChanged();
    }

    @Override
    public void onMessageSended(MessageResponse.IData messageResponseData) {
        messageItems.indexOf(messageResponseData.getData());
        adapterList.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        int userIdTo = Integer.valueOf(data.get("users_id_to"));
        int userIdFrom = Integer.valueOf(data.get("users_id_from"));
        int isRead = Integer.valueOf(data.get("is_read"));
        String uuid = data.get("uuid");
        String type = data.get("type");
        String value = data.get("value");
        if (userIdFrom != userTo.getId()) {
            return;
        }
        Message message = new Message();
        message.setUuid(uuid);
        message.setIsRead(isRead);
        message.setUsersIdTo(userIdTo);
        message.setUsersIdTo(userIdFrom);
        message.setType(type);
        message.setCreatedAt(getStringDateFormat(c, "yyyy-mm-dd hh:mm:ss"));
        message.setValue(value);
        message.setIsSender(0);
        message.setUserFrom(userTo);
        messageItems.add(0, message);
        adapterList.notifyDataSetChanged();
    }


    @OnClick(R.id.btnSend)
    void onBtnSendClick() {
        if (!isValid()) {
            return;
        }
        sendMessage(etTextMessage.getText().toString(), "text");
    }

    void sendMessage(String value, String type) {
        String uuid = UUID.randomUUID().toString();
        Message message = new Message();
        message.setUuid(uuid);
        message.setIsRead(0);
        message.setUsersIdTo(userTo.getId());
//        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
//        String formattedDate = df.format(c);
//        message.setType("text");
        message.setType(type);
        message.setValue(value);
        message.setCreatedAt(getStringDateFormat(c, "yyyy-mm-dd hh:mm:ss"));
        message.setIsSender(1);
//        showMessage(message.toString());
        messageItems.add(0, message);
        etTextMessage.setText("");
        mPresenter.onMessageSending(message);
        adapterList.notifyDataSetChanged();
    }

    @OnClick(R.id.btnSendCustom)
    void onBtnSendCustomClick() {
        clMessageCustom.setVisibility(View.GONE);
        if (barang != null) {
            sendMessage(barang.toString(), "barang");
        }
    }

    @OnClick(R.id.btnCancelCustom)
    void onBtnCancelCustomClick() {
        clMessageCustom.setVisibility(View.GONE);
    }

}
