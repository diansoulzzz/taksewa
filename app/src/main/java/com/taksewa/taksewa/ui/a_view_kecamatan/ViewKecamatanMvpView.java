package com.taksewa.taksewa.ui.a_view_kecamatan;

import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ViewKecamatanMvpView extends MvpView {
    void onPresenterReady(AreaResponse.IKecamatanList kecamatanResponseList);
}
