package com.taksewa.taksewa.ui.a_vendor_barang_list;

import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.MvpPresenter;

public interface VendorBarangListMvpPresenter<V extends VendorBarangListMvpView> extends MvpPresenter<V> {
    void onViewPrepared();

    void postVendorBarangDelete(Barang barang);
}

