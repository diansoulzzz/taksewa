package com.taksewa.taksewa.ui.a_member_review_detail;

import android.net.Uri;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.HSewaRequest;
import com.taksewa.taksewa.data.network.request.ReviewBarangRequest;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class MemberReviewDetailPresenter<V extends MemberReviewDetailMvpView> extends BasePresenter<V>
        implements MemberReviewDetailMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    private StorageReference storageReferenceBarang = getFirebaseStorage().getReference().child("images/review");

    @Inject
    public MemberReviewDetailPresenter(DataManager dataManager,
                                       SchedulerProvider schedulerProvider,
                                       CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int hsewaId) {
        getMemberReviewList(hsewaId);
    }

    public void getMemberReviewList(int hsewaId) {
        HSewaRequest.FromID fromID = new HSewaRequest.FromID();
        fromID.setId(hsewaId);
        getCompositeDisposable().add(getDataManager()
                .getMemberReviewDataFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IData>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IData response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }

    @Override
    public void onFileRemoved(FotoReview fotoReview) {
        StorageReference imageRef = storageReferenceBarang.child(fotoReview.getFotoUUID());
        imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                getMvpView().onFileDeleteSuccess(fotoReview);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        });
    }

    @Override
    public void onViewSubmit(ReviewBarangRequest.EntryData requestEntry) {
        getCompositeDisposable().add(getDataManager()
                .postMemberReviewEntry(requestEntry)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IList>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IList response) throws Exception {
                        getMvpView().onEntrySuccess(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().hideOverlayLoading();
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }

    @Override
    public void onFileChosen(FotoReview fotoReview) {
        Uri file = Uri.fromFile(new File(fotoReview.getFotoLocalPath()));
        String fotoUUID = UUID.randomUUID().toString();
        StorageReference imageRef = storageReferenceBarang.child(fotoUUID);
        UploadTask uploadTask = imageRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@androidx.annotation.NonNull Exception exception) {
                getMvpView().onFileUploadFailed(fotoReview);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        fotoReview.setFotoUrl(uri.toString());
                        fotoReview.setFotoUUID(fotoUUID);
                        getMvpView().onFileUploadSuccess(fotoReview);
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                fotoReview.setProgressUpload(progress);
                getMvpView().onFileUploadProgress(fotoReview);
            }
        });
    }
}

