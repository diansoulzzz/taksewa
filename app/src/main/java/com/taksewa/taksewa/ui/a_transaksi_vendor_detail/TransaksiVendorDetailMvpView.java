package com.taksewa.taksewa.ui.a_transaksi_vendor_detail;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface TransaksiVendorDetailMvpView extends MvpView {
    void onPresenterReady(HSewaResponse.IData response);

    void onSuccessUpdate(HSewaResponse.IData response);
}
