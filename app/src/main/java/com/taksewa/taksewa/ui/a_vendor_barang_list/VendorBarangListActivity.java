package com.taksewa.taksewa.ui.a_vendor_barang_list;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;
import com.taksewa.taksewa.utils.CommonUtils;

import java.text.DateFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class VendorBarangListActivity extends BaseActivity implements VendorBarangListMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = getClass().getSimpleName();
    private final int RQ_ENTRY_BARANG_ACTIVITY = 1;
    @Inject
    VendorBarangListMvpPresenter<VendorBarangListMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;

    @BindView(R.id.rvBarang)
    RecyclerView rvBarang;

    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    VendorBarangListAdapter.AdapterList vendorBarangListAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<Barang> barangs = new ArrayList<>();
    Barang barangSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_barang_list);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        onShowWait();
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vendorBarangListAdapter = new VendorBarangListAdapter.AdapterList(this, barangs, new VendorBarangListAdapter.Callback() {
            @Override
            public void onClickView(Barang barang, View itemView) {
                Intent intent = new Intent(getBaseContext(), ViewBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivity(intent);
            }

            @Override
            public void onClickEdit(Barang barang) {
                Intent intent = new Intent(getBaseContext(), EntryBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivityForResult(intent, RQ_ENTRY_BARANG_ACTIVITY);
            }

            @Override
            public void onClickDelete(Barang barang) {
                barangSelected = barang;
                AlertDialog.Builder builder = new AlertDialog.Builder(VendorBarangListActivity.this);
                builder.setMessage(Html.fromHtml(getString(R.string.anda_yakin_hapus_, barang.getNama())))
                        .setPositiveButton(getString(R.string.yakin), dialogClickListenerDelete)
                        .setNegativeButton(getString(R.string.batal), dialogClickListenerDelete)
                        .show();
            }
        });
        gridLayoutManager = new GridLayoutManager(this, 1);
        rvBarang.setLayoutManager(gridLayoutManager);
        rvBarang.setAdapter(vendorBarangListAdapter);
        mPresenter.onViewPrepared();
    }

    DialogInterface.OnClickListener dialogClickListenerDelete = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    mPresenter.postVendorBarangDelete(barangSelected);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:

                    break;
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRemoveWait() {
        progressBar.hide();
        rvBarang.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvBarang.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPresenterReady(BarangResponse.IList barangResponse) {
        barangs.clear();
        barangs.addAll(barangResponse.getData());
        vendorBarangListAdapter.notifyDataSetChanged();
        onRemoveWait();
    }

    @Override
    public void onRefresh() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_ENTRY_BARANG_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Barang barang = data.getParcelableExtra("barang");
                for (int i = 0; i <= barangs.size()-1; i++) {
                    if (barangs.get(i).getId().equals(barang.getId())){
                        barangs.set(i, barang);
                    }
                }
                vendorBarangListAdapter.notifyDataSetChanged();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

