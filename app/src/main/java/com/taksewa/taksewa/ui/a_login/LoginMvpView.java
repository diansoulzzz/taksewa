package com.taksewa.taksewa.ui.a_login;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.taksewa.taksewa.ui._base.MvpView;

public interface LoginMvpView extends MvpView {

    boolean isFormValid();
}
