package com.taksewa.taksewa.ui.a_entry_barang;

import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_sewa_barang.SewaBarangMvpView;

public interface EntryBarangMvpPresenter<V extends EntryBarangMvpView> extends MvpPresenter<V> {

    void onViewPrepared();

    void onFileChosen(BarangFoto barangFoto);

    void onFileRemoved(BarangFoto barangFoto);

    void onViewSubmit(BarangRequest.EntryData requestEntry);

    void DoGetBarangData(int brgId);

    void onViewSubmitObject(Barang barang);
}

