package com.taksewa.taksewa.ui.a_entry_barang;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.ui._base.BaseViewHolder;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EntryBarangAdapter {

    public interface CallbackImage {
        void onClickView(BarangFoto imageGalleryPath, View itemView);

        void onClickAdd();
    }

    public static class AdapterEntry extends RecyclerView.Adapter<BaseViewHolder> {
        private Context context;
        private List<BarangFoto> datas;
        private CallbackImage mCallback;
        private String selectedFile;

        public AdapterEntry(Context context, List datas, CallbackImage mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_entry_barang_image, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.ivPickerGallery)
            ImageView ivPickerGallery;
            @BindView(R.id.pbImageUploadStatus)
            ProgressBar pbImageUploadStatus;
            @BindView(R.id.vOverlay)
            View vOverlay;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final BarangFoto data = datas.get(i);
                assert data != null;
                View.OnClickListener listenerFoto = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onClickView(data, itemView);
                    }
                };
                View.OnClickListener listenerAddFoto = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCallback.onClickAdd();
                    }
                };
                if ((data.getFotoLocalPath() != null) && (data.getId() == null)) {//STATE NEW
                    Glide.with(itemView).load(new File(data.getFotoLocalPath())).into(ivPickerGallery);
                }
                if ((data.getFotoUrl() != null) && (data.getId() != null)) {//STATE EDIT
                    Glide.with(itemView).load(data.getFotoUrl()).into(ivPickerGallery);
                }
                if (data.getUtama() == 1) {
                    vOverlay.setBackgroundColor(itemView.getResources().getColor(R.color.colorAccent));
                } else {
                    vOverlay.setBackgroundColor(itemView.getResources().getColor(R.color.ultraLightGrayColor));
                }
                pbImageUploadStatus.setVisibility(View.VISIBLE);
                pbImageUploadStatus.setProgress((int) data.getProgressUpload());
                if (data.getProgressUpload() < 100) {
                    itemView.setOnClickListener(null);
                    pbImageUploadStatus.getProgressDrawable().setColorFilter(itemView.getResources().getColor(R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN);
//                    pbImageUploadStatus.setProgress((int) data.getProgressUpload());
                } else if (data.getProgressUpload() == 100) {
                    itemView.setOnClickListener(listenerFoto);
                    pbImageUploadStatus.getProgressDrawable().setColorFilter(Color.GREEN, android.graphics.PorterDuff.Mode.SRC_IN);
                }
                if (data.getFotoLocalPath().equals("")) {
                    itemView.setOnClickListener(listenerAddFoto);
                    pbImageUploadStatus.setVisibility(View.GONE);
                    Glide.with(itemView).load(R.drawable.ic_add_black_24dp).into(ivPickerGallery);
                }
            }
        }
    }
}
