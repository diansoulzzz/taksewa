package com.taksewa.taksewa.ui.a_vendor_barang_list;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VendorBarangListAdapter {

    public interface Callback {
        void onClickView(Barang barang, View itemView);
        void onClickEdit(Barang barang);
        void onClickDelete(Barang barang);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<Barang> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barang_list, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.empty_title_daftar_produk_anda_kosong), Html.fromHtml(parent.getResources().getString(R.string.empty_subtitle_kamu_belum_pernah_mendaftarkan_produk_sebelumnya)).toString());
            }
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (datas != null && datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaBarang)
            TextView tvNamaBarang;
            @BindView(R.id.tvHargaBarang)
            TextView tvHargaBarang;
            @BindView(R.id.ivGambarBarang)
            ImageView ivGambarBarang;
            @BindView(R.id.ivEdit)
            ImageView ivEdit;
            @BindView(R.id.ivDelete)
            ImageView ivDelete;
            String htmlFormat;
            String formatedHarga;
            String formatedLamaHari;
            @BindView(R.id.clOptions)
            ConstraintLayout clOptions;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                clOptions.setVisibility(View.VISIBLE);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final Barang data = datas.get(i);
                assert data != null;
                if (data.getNama() != null) {
                    tvNamaBarang.setText(data.getNama());
                }
                if (data.getBarangFotoUtama().getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getBarangFotoUtama().getFotoUrl()).into(ivGambarBarang);
                }
                if (data.getBarangHargaSewaUtama() != null) {
                    formatedHarga = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getHarga().toString());
                    formatedLamaHari = NumberTextWatcher.getDecimalFormattedString(data.getBarangHargaSewaUtama().getLamaHari().toString());
                    tvHargaBarang.setText(Html.fromHtml(itemView.getResources().getString(R.string.view_produk_harga_lama_sewa_rp, formatedHarga, "")));
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClickView(data, view);
                    }
                });
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClickEdit(data);
                    }
                });
                ivDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClickDelete(data);
                    }
                });
            }
        }
    }
}
