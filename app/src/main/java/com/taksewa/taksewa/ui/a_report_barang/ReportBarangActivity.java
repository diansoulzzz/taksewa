package com.taksewa.taksewa.ui.a_report_barang;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_member_review_detail.MemberReviewDetailActivity;
import com.taksewa.taksewa.ui.a_search_result.SearchResultActivity;
import com.taksewa.taksewa.ui.f_report_barang_selesai.ReportBarangSelesaiFragment;
import com.taksewa.taksewa.ui.f_search_result_barang.SearchResultBarangFragment;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportBarangActivity extends BaseActivity implements ReportBarangMvpView {

    private final String TAG = getClass().getSimpleName();

    @Inject
    ReportBarangMvpPresenter<ReportBarangMvpView> mPresenter;

    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    static Integer tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_barang);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ReportBarangAdapter adapterViewPager = new ReportBarangAdapter(getSupportFragmentManager());

        if (getIntent().hasExtra("tab")) {
            tab = getIntent().getIntExtra("tab", 0);
        }
        viewPager.setAdapter(adapterViewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(tab, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public static class ReportBarangAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public ReportBarangAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ReportBarangSelesaiFragment.newInstance("dipinjam");
                case 1:
                    return ReportBarangSelesaiFragment.newInstance("selesai");
                default:
                    return ReportBarangSelesaiFragment.newInstance("dipinjam");
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) return "Dipinjam";
            return "Selesai";
        }
    }
}

