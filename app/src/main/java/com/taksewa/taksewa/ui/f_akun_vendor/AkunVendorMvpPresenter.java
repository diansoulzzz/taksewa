package com.taksewa.taksewa.ui.f_akun_vendor;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_akun_member.AkunMemberMvpView;

public interface AkunVendorMvpPresenter<V extends AkunVendorMvpView> extends MvpPresenter<V> {

    void onViewPrepared();

    void DoPostDaftarVendor();
}

