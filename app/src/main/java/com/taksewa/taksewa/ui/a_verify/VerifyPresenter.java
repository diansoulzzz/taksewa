package com.taksewa.taksewa.ui.a_verify;

import android.net.Uri;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpPresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class VerifyPresenter<V extends VerifyMvpView> extends BasePresenter<V>
        implements VerifyMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();
    private StorageReference storageReferenceBarang = getFirebaseStorage().getReference().child("images/secure/ktp");
    private User user;
    private UserRequest.EntryData entryData;

    @Inject
    public VerifyPresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onViewPrepared() {

    }

    @Override
    public void onSubmitVerify(User user) {
        this.user = user;
        onFileUpload();
    }

    @Override
    public void onFileUpload() {
        Uri file = Uri.fromFile(new File(user.getFotoKtpLocalPath()));
        String fotoUUID = UUID.randomUUID().toString();
        StorageReference imageRef = storageReferenceBarang.child(fotoUUID);
        UploadTask uploadTask = imageRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                getMvpView().onVerificationComplete(false);
//                getMvpView().onFileUploadFailed(barangFoto);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        user.setFotoKtpUrl(uri.toString());
                        entryData = new UserRequest.EntryData();
                        entryData.setUser(user);
                        DoPostUserVerification();
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
//                barangFoto.setProgressUpload(progress);
//                getMvpView().onFileUploadProgress(barangFoto);
            }
        });
    }

    private void DoPostUserVerification() {
        getCompositeDisposable().add(getDataManager()
                .postUserVerification(entryData)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<UserResponse.IData>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull UserResponse.IData response) throws Exception {
                        getDataManager().setCurrentUserIsVerified((response.getData().getVerified() != null));
                        getDataManager().setCurrentUserName(response.getData().getNama());
                        getMvpView().onVerificationComplete(true);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        getMvpView().onVerificationComplete(false);
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }
}

