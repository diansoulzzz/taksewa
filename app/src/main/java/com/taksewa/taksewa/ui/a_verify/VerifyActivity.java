package com.taksewa.taksewa.ui.a_verify;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.schibstedspain.leku.LocationPickerActivity;
import com.schibstedspain.leku.locale.SearchZoneRect;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.AreaResponse;
import com.taksewa.taksewa.model.Bank;
import com.taksewa.taksewa.model.Kecamatan;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.model.UsersBank;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.google.android.gms.maps.model.LatLng;
import com.taksewa.taksewa.ui.a_verify.camera.VerifyCardTakeCameraActivity;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankActivity;
import com.taksewa.taksewa.ui.a_view_bank.ViewBankAdapter;
import com.taksewa.taksewa.ui.a_view_kecamatan.ViewKecamatanActivity;

import java.io.File;
import java.util.Calendar;

import javax.inject.Inject;

import androidx.annotation.Nullable;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class VerifyActivity extends BaseActivity implements VerifyMvpView {

    private static final int PLACE_PICKER_RQ = 1;
    private static final int CARD_TAKE_CAMERA_RQ = 2;
    private static final int VIEW_KECAMATAN_RQ = 3;
    private static final int VIEW_BANK_RQ = 4;
    @Inject
    VerifyMvpPresenter<VerifyMvpView> mPresenter;
    @BindView(R.id.btnVerifikasi)
    Button btnVerifikasi;
    @BindView(R.id.tietNamaLengkap)
    TextInputEditText tietNamaLengkap;
    @BindView(R.id.tietNoTelp)
    TextInputEditText tietNoTelp;
    @BindView(R.id.tietAlamat)
    TextInputEditText tietAlamat;
    @BindView(R.id.tietNoKtp)
    TextInputEditText tietNoKtp;
    @BindView(R.id.tietPinAlamat)
    TextInputEditText tietPinAlamat;
    @BindView(R.id.ivResultFotoKtp)
    ImageView ivResultFotoKtp;
    @BindView(R.id.tietKecamatan)
    TextInputEditText tietKecamatan;
    @BindView(R.id.tietKodePos)
    TextInputEditText tietKodePos;
    @BindView(R.id.tietTglLahir)
    TextInputEditText tietTglLahir;
    @BindView(R.id.tietNoRekBank)
    TextInputEditText tietNoRekBank;
    @BindView(R.id.tietAtasNamaBank)
    TextInputEditText tietAtasNamaBank;
    @BindView(R.id.tietBank)
    TextInputEditText tietBank;
    Kecamatan kecamatan;
    Bank bank;

    double lat = 0, lng = 0;
    String filePathKTP;

    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            String myFormat = "MM/dd/yy"; //In which you need put here
//            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            tietTglLahir.setText(getStringDateFormat(myCalendar.getTime(), "dd MMM yyyy"));
//            tietTglLahir.setText(getStringDateFormat(myCalendar.getTime(), "y-MM-dd"));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {
        if (!mPresenter.isUserLogin()) {
            startLoginActivity();
            finish();
            return;
        }
        myCalendar.set(Calendar.YEAR, 1990);
        myCalendar.set(Calendar.MONTH, 0);
        myCalendar.set(Calendar.DAY_OF_MONTH, 1);
    }

    @OnClick(R.id.ivAmbilFotoKtp)
    void OnClickIvAmbilFotoKtp() {
        Intent intent = new Intent(this, VerifyCardTakeCameraActivity.class);
        startActivityForResult(intent, CARD_TAKE_CAMERA_RQ);
    }

    @OnClick(R.id.btnVerifikasi)
    void OnClickBtnVerifikasi() {
        if (!isValid()) {
            return;
        }
        User user = new User();
        user.setNama(tietNamaLengkap.getText().toString());
        user.setAlamat(tietAlamat.getText().toString());
        user.setNoTelp(tietNoTelp.getText().toString());
        user.setFotoKtpLocalPath(filePathKTP);
        user.setPinAlamat(tietPinAlamat.getText().toString());
        user.setKodePos(tietKodePos.getText().toString());
        user.setPinLatitude(lat);
        user.setPinLongitude(lng);
        user.setKecamatanId(kecamatan.getId());
        user.setTglLahir(getStringDateFormat(myCalendar.getTime(), "yyyy-MM-dd"));
        UsersBank usersBank = new UsersBank();
        usersBank.setBankId(bank.getId());
        usersBank.setNama(tietAtasNamaBank.getText().toString());
        usersBank.setNomor(tietNoRekBank.getText().toString());
        usersBank.setUsersId(1);
        user.setUsersBankUtama(usersBank);
        showOverlayLoading();
        mPresenter.onSubmitVerify(user);
    }

    boolean isValid() {
        boolean valid = true;
        if (TextUtils.isEmpty(tietNamaLengkap.getText())) {
            tietNamaLengkap.setError("Nama Lengkap wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietNoTelp.getText())) {
            tietNoTelp.setError("No Telp wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietAlamat.getText())) {
            tietAlamat.setError("Alamat wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietKodePos.getText())) {
            tietKodePos.setError("No KTP wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietKecamatan.getText())) {
            tietKecamatan.setError("Kecamatan wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietBank.getText())) {
            tietBank.setError("Bank wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietAtasNamaBank.getText())) {
            tietAtasNamaBank.setError("Bank Atas Nama wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietNoRekBank.getText())) {
            tietNoRekBank.setError("Nomor Rekening wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietNoKtp.getText())) {
            tietNoKtp.setError("No KTP wajib di isi");
            valid = false;
        }
        if (TextUtils.isEmpty(tietPinAlamat.getText()) || (lng == 0) || lat == 0) {
            tietPinAlamat.setError("Pin Alamat wajib dipilih");
            valid = false;
        }
        if (TextUtils.isEmpty(filePathKTP)) {
            tietNoKtp.setError("No KTP wajib di foto");
            valid = false;
        }
        if (TextUtils.isEmpty(tietTglLahir.getText())) {
            tietTglLahir.setError("Tgl Lahir wajib di foto");
            valid = false;
        }
        return valid;
    }

    @OnClick(R.id.btnPinAlamat)
    void onClickPinAlamat() {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .withSearchZone(new SearchZoneRect(new LatLng(-7.257472, 112.752090), new LatLng(-7.257472, 112.752090)))
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withUnnamedRoadHidden()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .build(getApplicationContext());
//                .withStreetHidden()
//                .withCityHidden()
//                .withZipCodeHidden()
//                .withUnnamedRoadHidden()
//                .withDefaultLocaleSearchZone()
//                .shouldReturnOkOnBackPressed()

        startActivityForResult(locationPickerIntent, PLACE_PICKER_RQ);
    }

    @OnClick(R.id.tietKecamatan)
    void onClickTietKecamatan() {
        Intent intent = new Intent(this, ViewKecamatanActivity.class);
        startActivityForResult(intent, VIEW_KECAMATAN_RQ);
    }

    @OnClick(R.id.tietBank)
    void onClickTietBank() {
        Intent intent = new Intent(this, ViewBankActivity.class);
        startActivityForResult(intent, VIEW_BANK_RQ);
    }


    @OnClick(R.id.tietTglLahir)
    void onClickTietTglLahir() {
        new DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == PLACE_PICKER_RQ) {
                tietPinAlamat.setText(data.getStringExtra(LOCATION_ADDRESS));
                lat = data.getDoubleExtra(LATITUDE, 0.0);
                lng = data.getDoubleExtra(LONGITUDE, 0.0);
            }
            if (requestCode == CARD_TAKE_CAMERA_RQ) {
                filePathKTP = data.getStringExtra("filePath");
                Glide.with(this).load(new File(filePathKTP)).into(ivResultFotoKtp);
                ivResultFotoKtp.setVisibility(View.VISIBLE);
            }
            if (requestCode == VIEW_KECAMATAN_RQ) {
                kecamatan = data.getParcelableExtra("kecamatan");
                tietKecamatan.setText(String.format("%s, %s %s, %s", kecamatan.getKotum().getProvinsi().getNama(), kecamatan.getKotum().getTipe(), kecamatan.getKotum().getNama(), kecamatan.getNama()));
            }
            if (requestCode == VIEW_BANK_RQ) {
                bank = data.getParcelableExtra("bank");
                tietBank.setText(String.format("%s", bank.getNama()));
            }
        }
    }

    @Override
    public void onVerificationComplete(boolean isSuccess) {
        hideOverlayLoading();
        if (isSuccess) {
            finish();
        }
    }
}
