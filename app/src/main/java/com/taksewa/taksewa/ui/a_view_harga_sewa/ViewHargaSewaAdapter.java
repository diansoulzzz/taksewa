package com.taksewa.taksewa.ui.a_view_harga_sewa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.BarangHargaSewa;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewHargaSewaAdapter {

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {
        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<BarangHargaSewa> datas;

        public AdapterList(Context context, List datas) {
            this.context = context;
            this.datas = datas;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view_harga_sewa, viewGroup, false));
//            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
//            return new VHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvHargaSewaBarang)
            TextView tvHargaSewaBarang;
            @BindView(R.id.tvLamaSewaBarang)
            TextView tvLamaSewaBarang;

            BarangHargaSewa data;
            String hargaSewa;
            String lamaSewa;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                if (data.getHarga() != null) {
                    hargaSewa = data.getHarga().toString();
                    tvHargaSewaBarang.setText(NumberTextWatcher.getDecimalFormattedString(hargaSewa));
                }
                if (data.getLamaHari() != null) {
                    lamaSewa = data.getLamaHari().toString();
                    tvLamaSewaBarang.setText(lamaSewa);
                }
            }
        }
    }
}
