package com.taksewa.taksewa.ui.f_transaksi_vendor;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface TransaksiVendorMvpView extends MvpView {

    void onPresenterReady(HSewaResponse.IList transactionResponse);
}
