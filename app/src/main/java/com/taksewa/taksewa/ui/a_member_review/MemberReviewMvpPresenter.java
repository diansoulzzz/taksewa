package com.taksewa.taksewa.ui.a_member_review;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpView;

public interface MemberReviewMvpPresenter<V extends MemberReviewMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

