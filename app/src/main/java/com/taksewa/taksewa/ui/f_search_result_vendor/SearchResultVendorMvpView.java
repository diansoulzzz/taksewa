package com.taksewa.taksewa.ui.f_search_result_vendor;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface SearchResultVendorMvpView extends MvpView {

    void onPresenterReady(SearchResponse.IVendorData searchResponse);
}
