package com.taksewa.taksewa.ui._base;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.AuthRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.ErrorResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.utils.AppConstants;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private static final String TAG = "BasePresenter";

    private final DataManager mDataManager;
    private final SchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mCompositeDisposable;
    private FirebaseAuth firebaseAuth;
    private FirebaseStorage firebaseStorage;

    private V mMvpView;

    @Inject
    public BasePresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        this.mDataManager = dataManager;
        this.mSchedulerProvider = schedulerProvider;
        this.mCompositeDisposable = compositeDisposable;
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.firebaseStorage = FirebaseStorage.getInstance();
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.dispose();
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }

    public FirebaseStorage getFirebaseStorage() {
        return firebaseStorage;
    }

    public void isAuthServCheck() {
        getProfileInfo(response -> sendFirebaseInstaceIdToken(), throwable -> {
            Log.d(TAG, "fail");
            setUserAsLoggedOut();
            if (!isViewAttached()) {
                return;
            }
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        });
//        getCompositeDisposable().add(getDataManager()
//                .getProfileInfo()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(response -> {
//                    Log.d(TAG, "success");
//                    sendFirebaseInstaceIdToken();
//                }, throwable -> {
//                    Log.d(TAG, "fail");
//                    setUserAsLoggedOut();
//                    if (!isViewAttached()) {
//                        return;
//                    }
//                    if (throwable instanceof ANError) {
//                        ANError anError = (ANError) throwable;
//                        handleApiError(anError);
//                    }
//                }));
    }

    @Override
    public void handleApiError(ANError error) {
        if (error == null || error.getErrorBody() == null) {
            getMvpView().onError(R.string.api_default_error);
            return;
        }

        if (error.getErrorCode() == AppConstants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.CONNECTION_ERROR)) {
            getMvpView().onError(R.string.connection_error);
            return;
        }

        if (error.getErrorCode() == AppConstants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.REQUEST_CANCELLED_ERROR)) {
            getMvpView().onError(R.string.api_retry_error);
            return;
        }
//
        final GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();
        try {
            ErrorResponse apiError = gson.fromJson(error.getErrorBody(), ErrorResponse.class);

            if (apiError == null || apiError.getMessage() == null) {
                getMvpView().onError(R.string.api_default_error);
                return;
            }

            switch (error.getErrorCode()) {
                case HttpsURLConnection.HTTP_UNAUTHORIZED:
                    getMvpView().onError(R.string.api_http_unauthorize);
                    return;
                case HttpsURLConnection.HTTP_FORBIDDEN:
                    setUserAsLoggedOut();
//                    getMvpView().openActivityOnTokenExpire();
                    getMvpView().onError(R.string.api_http_forbiden);
                    return;
                case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                    getMvpView().onError(R.string.api_internal_error);
                    return;
                case HttpsURLConnection.HTTP_NOT_FOUND:
                    getMvpView().onError(R.string.api_http_not_found);
                    return;
//                    getMvpView().onError(apiError.getMessage());
                default:
//                    getMvpView().onError(apiError.getMessage());
                    getMvpView().onError(R.string.api_default_error);
                    return;
            }
        } catch (JsonSyntaxException | NullPointerException e) {
            Log.e(TAG, "handleApiError", e);
//            getMvpView().showMessage(e.getMessage());
            getMvpView().onError(R.string.api_default_error);
        }
    }

    @Override
    public void setUserAsLoggedOut() {
        getFirebaseAuth().signOut();
        GoogleSignIn.getClient(getMvpView().getViewContext(), new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()).signOut();
        getDataManager().setAccessToken(null);
        getDataManager().setUserAsLoggedOut();
        getMvpView().restartApp();
    }

    @Override
    public boolean isUserLogin() {
        return (getFirebaseAuth().getCurrentUser() != null);
    }

    @Override
    public boolean isUserVerified() {
        return getDataManager().getCurrentUserisVerified();
    }

    @Override
    public void getProfileInfo(Consumer<UserResponse.IData> listenerSuccess, Consumer<Throwable> listenerFailed) {
        getCompositeDisposable().add(getDataManager()
                .getProfileInfo()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(listenerSuccess, listenerFailed));
    }


    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    public void sendFirebaseInstaceIdToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();
                        getCompositeDisposable().add(getDataManager()
                                .postFirebaseFcmToken(new AuthRequest.FirebaseFcmToken(token))
                                .subscribeOn(getSchedulerProvider().io())
                                .observeOn(getSchedulerProvider().ui())
                                .subscribe(response -> {
                                    Log.d(TAG, "success");
                                }, throwable -> {
                                    Log.d(TAG, "fail");
                                    if (!isViewAttached()) {
                                        return;
                                    }
                                    if (throwable instanceof ANError) {
                                        ANError anError = (ANError) throwable;
                                        handleApiError(anError);
                                    }
                                }));
                        Log.d(TAG, "TOKENE :" + token);
                    }
                });
    }
}