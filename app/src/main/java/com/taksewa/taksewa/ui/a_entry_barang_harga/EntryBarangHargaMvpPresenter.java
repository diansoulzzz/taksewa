package com.taksewa.taksewa.ui.a_entry_barang_harga;

import com.taksewa.taksewa.model.BarangFoto;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_entry_barang.EntryBarangMvpView;

public interface EntryBarangHargaMvpPresenter<V extends EntryBarangHargaMvpView> extends MvpPresenter<V> {

    void onViewPrepared();

}

