package com.taksewa.taksewa.ui.a_vendor_barang_list;

import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface VendorBarangListMvpView extends MvpView {
    void onPresenterReady(BarangResponse.IList barangResponse);
}
