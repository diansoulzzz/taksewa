package com.taksewa.taksewa.ui.a_transaksi_vendor_detail;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.HSewaRequest;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpPresenter;
import com.taksewa.taksewa.ui.a_transaksi_member_detail.TransaksiMemberDetailMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class TransaksiVendorDetailPresenter<V extends TransaksiVendorDetailMvpView> extends BasePresenter<V>
        implements TransaksiVendorDetailMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    private HSewaRequest.FromID fromID;

    @Inject
    public TransaksiVendorDetailPresenter(DataManager dataManager,
                                          SchedulerProvider schedulerProvider,
                                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int hSewaId) {
        fromID = new HSewaRequest.FromID();
        fromID.setId(hSewaId);
        DoGetHSewaData();
    }

    @Override
    public void onChangeData(HSewa hSewa) {
        HSewaRequest.ChangeData changeData = new HSewaRequest.ChangeData();
        changeData.setData(hSewa);
        getCompositeDisposable().add(getDataManager()
                .postVendorTransactionDataChange(changeData)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IData>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IData response) throws Exception {
                        getMvpView().onSuccessUpdate( response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    public void DoGetHSewaData() {
        getCompositeDisposable().add(getDataManager()
                .getVendorTransactionDataFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IData>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IData response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

