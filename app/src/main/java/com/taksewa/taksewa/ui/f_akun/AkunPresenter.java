package com.taksewa.taksewa.ui.f_akun;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class AkunPresenter<V extends AkunMvpView> extends BasePresenter<V>
        implements AkunMvpPresenter<V> {
    private final String TAG = getClass().getSimpleName();

    @Inject
    public AkunPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void attemptLogout() {
        setUserAsLoggedOut();
    }

    @Override
    public void onViewPrepared() {
        setUp();
    }

    public void setUp() {
        getMvpView().setUserProfile(getDataManager().getCurrentUserName(), getDataManager().getCurrentUserisVerified(), getDataManager().getCurrentUserProfilePicUrl());
    }

}

