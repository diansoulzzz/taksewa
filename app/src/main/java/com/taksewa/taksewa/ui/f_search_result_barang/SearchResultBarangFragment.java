package com.taksewa.taksewa.ui.f_search_result_barang;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.di.component.ActivityComponent;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.model.BarangSubKategori;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.model.TagID;
import com.taksewa.taksewa.ui._base.BaseFragment;
import com.taksewa.taksewa.ui.a_maps_result_barang.MapsResultBarangActivity;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangActivity;
import com.taksewa.taksewa.ui.f_search_result_barang.options.FilterBarangActivity;
import com.taksewa.taksewa.ui.f_search_result_barang.options.SortBarangActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.taksewa.taksewa.data.network.request.SearchRequest.ISendBarang.SORT_BY_RELEVANCE;

public class SearchResultBarangFragment extends BaseFragment implements SearchResultBarangMvpView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = getClass().getSimpleName();
    private final int RQ_FILTER_BARANG = 1;
    private final int RQ_SORT_BARANG = 2;
    private final int RQ_VIEW_MAPS_BARANG = 3;
    @Inject
    SearchResultBarangPresenter<SearchResultBarangMvpView> mPresenter;
    @BindView(R.id.rvResultBarang)
    RecyclerView rvResultBarang;
    @BindView(R.id.srlLoader)
    SwipeRefreshLayout srlLoader;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.btnFilter)
    Button btnFilter;
    @BindView(R.id.btnSort)
    Button btnSort;
    @BindView(R.id.btnShowMap)
    Button btnShowMap;
    @BindView(R.id.clOptions)
    ConstraintLayout clOptions;

    private SearchResultBarangAdapter.AdapterList adapterList;
    private ArrayList<Barang> barangs = new ArrayList<>();
    private ArrayList<TagID> barangKategoriIds = new ArrayList<>();
    private ArrayList<TagID> barangSubKategoriIds = new ArrayList<>();

    private GridLayoutManager gridLayoutManager;
    private String queryString;
    private Filter filter = new Filter();
    private int sortBy = SORT_BY_RELEVANCE;
    private BarangKategori kategori;
    private BarangSubKategori subKategori;

    public static SearchResultBarangFragment newInstance(String queryString, BarangKategori kategori, BarangSubKategori subKategori) {
        SearchResultBarangFragment fragment = new SearchResultBarangFragment();
        Bundle args = new Bundle();
        args.putString("queryString", queryString);
        args.putParcelable("kategori", kategori);
        args.putParcelable("subKategori", subKategori);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        queryString = getArguments().getString("queryString");
        kategori = getArguments().getParcelable("kategori");
        subKategori = getArguments().getParcelable("subKategori");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_result_barang, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        srlLoader.setOnRefreshListener(this);
        adapterList = new SearchResultBarangAdapter.AdapterList(getContext(), barangs, new SearchResultBarangAdapter.Callback() {
            @Override
            public void onClickBarang(Barang barang) {
                Intent intent = new Intent(getContext(), ViewBarangActivity.class);
                intent.putExtra("brgId", barang.getId());
                startActivity(intent);
            }
        }, queryString);
//        gridLayoutManager = new GridLayoutManager(getContext(), 2);
//        rvResultBarang.setLayoutManager(gridLayoutManager);
        rvResultBarang.setAdapter(adapterList);
        setHasOptionsMenu(true);
//        filter = new Filter();
//        sortBy = SORT_BY_RELEVANCE;
        mPresenter.onViewPrepared(queryString, sortBy, filter);
    }

    @Override
    public void onPresenterReady(SearchResponse.IBarangData searchResponse) {
        barangs.clear();
        filter.setBarangKategoriList(searchResponse.getData().getBarangKategori());
        filter.setBarangSubKategoriList(searchResponse.getData().getBarangSubKategori());
        filter.setKotaList(searchResponse.getData().getKota());
//        showMessage(filter.getBarangKategoriList().get(0).getNama());
        barangs.addAll(searchResponse.getData().getBarangList());
        if (barangs.size() > 0) {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 1);
        }
        rvResultBarang.setLayoutManager(gridLayoutManager);
        adapterList.notifyDataSetChanged();
        onRemoveWait();
    }

    public void onRemoveWait() {
        srlLoader.setRefreshing(false);
        progressBar.hide();
        rvResultBarang.setVisibility(View.VISIBLE);
        clOptions.setVisibility(View.VISIBLE);
    }

    public void onShowWait() {
        rvResultBarang.setVisibility(View.INVISIBLE);
        clOptions.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRefresh() {
        onShowWait();
        mPresenter.onViewPrepared(queryString, sortBy, filter);
    }

    @OnClick(R.id.btnFilter)
    void onBtnFilterClick() {
        Intent intent = new Intent(getContext(), FilterBarangActivity.class);
        intent.putExtra("filter", filter);
        startActivityForResult(intent, RQ_FILTER_BARANG);
//        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    @OnClick(R.id.btnSort)
    void onBtnSortClick() {
        Intent intent = new Intent(getContext(), SortBarangActivity.class);
        intent.putExtra("sortBy", sortBy);
        startActivityForResult(intent, RQ_SORT_BARANG);
    }

    @OnClick(R.id.btnShowMap)
    void onBtnShowMapClick() {
        Intent intent = new Intent(getContext(), MapsResultBarangActivity.class);
        intent.putParcelableArrayListExtra("barangs", barangs);
        startActivityForResult(intent, RQ_VIEW_MAPS_BARANG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RQ_FILTER_BARANG) {
                assert data != null;
                filter = data.getParcelableExtra("filter");

//                barangKategoriIds.clear();
//                for (BarangKategori k : filter.getBarangKategoriList()) {
//                    barangKategoriIds.add(k.getId());
//                }
//                barangSubKategoriIds.clear();
//                for (BarangSubKategori sk : filter.getBarangSubKategoriList()) {
//                    barangSubKategoriIds.add(sk.getId());
//                }
                barangKategoriIds.clear();
                barangKategoriIds.addAll(filter.getFilterBarangKategoriIds());

                barangSubKategoriIds.clear();
                barangSubKategoriIds.addAll(filter.getFilterBarangSubKategoriIds());

                filter.setBarangKategori(null);
                filter.setBarangKategoriList(null);
//                filter.setFilterBarangKategoriList(null);

                filter.setBarangSubKategori(null);
                filter.setBarangSubKategoriList(null);
                filter.setKotaList(null);
//                filter.setFilterBarangSubKategoriList(null);

//                showMessage(String.valueOf(filter.getHargaMax()));
                onRefresh();
//                mPresenter.onViewPrepared(queryString, sortBy, filter);
            }
            if (requestCode == RQ_SORT_BARANG) {
                assert data != null;
                sortBy = data.getIntExtra("sortBy", SORT_BY_RELEVANCE);
//                showMessage(String.valueOf(sortBy));
                onRefresh();
//                mPresenter.onViewPrepared(queryString, sortBy, filter);
            }
        }
    }
}
