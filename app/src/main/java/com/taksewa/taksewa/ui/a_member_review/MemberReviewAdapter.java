package com.taksewa.taksewa.ui.a_member_review;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangAdapter;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorAdapter;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.utils.CommonUtils.getDateParsed;
import static com.taksewa.taksewa.utils.CommonUtils.getStringDateFormat;

public class MemberReviewAdapter {

    public interface Callback {
        void onClickView(HSewa hSewas, View itemView);

        void onClickVendor(User user, View itemView);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {

        static final int VIEW_TYPE_EMPTY = 0;
        static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<HSewa> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NotNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), parent.getResources().getString(R.string.empty_title_review_member), Html.fromHtml(parent.getResources().getString(R.string.empty_subtitle_review_member)).toString());
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (this.datas != null && this.datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            }
            return VIEW_TYPE_EMPTY;
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvNamaVendor)
            TextView tvNamaVendor;
            @BindView(R.id.ivGambarVendor)
            ImageView ivGambarVendor;
            @BindView(R.id.tvTgl)
            TextView tvTgl;
            @BindView(R.id.tvKodenota)
            TextView tvKodenota;
            @BindView(R.id.btnUlasan)
            Button btnUlasan;
            @BindView(R.id.tvUlasanAnda)
            TextView tvUlasanAnda;
            @BindView(R.id.rbStarReview)
            RatingBar rbStarReview;
            @BindView(R.id.clUlasanAnda)
            ConstraintLayout clUlasanAnda;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final HSewa data = datas.get(i);
                assert data != null;
                if (data.getUserPenyewa().getNama() != null) {
                    tvNamaVendor.setText(data.getUserPenyewa().getNama());
                }
                if (data.getUserPenyewa().getFotoUrl() != null) {
                    Glide.with(itemView).load(data.getUserPenyewa().getFotoUrl()).into(ivGambarVendor);
                }
                tvTgl.setText(getStringDateFormat(getDateParsed(data.getCreatedAt())));
                tvKodenota.setText(data.getKodenota());
                if (data.getReviewBarangUtama() != null) {
                    clUlasanAnda.setVisibility(View.VISIBLE);
                    rbStarReview.setRating(data.getReviewBarangUtama().getRating());
                    btnUlasan.setText(itemView.getResources().getString(R.string.ubah_ulasan));
                } else {
                    clUlasanAnda.setVisibility(View.GONE);
                    btnUlasan.setText(itemView.getResources().getString(R.string.beri_ulasan));
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickView(data, itemView);
                    }
                });
                btnUlasan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickView(data, itemView);
                    }
                });
                ivGambarVendor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickVendor(data.getUserPenyewa(), itemView);
                    }
                });
            }
        }
    }
}
