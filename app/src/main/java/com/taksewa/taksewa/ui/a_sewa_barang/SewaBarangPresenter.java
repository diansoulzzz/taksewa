package com.taksewa.taksewa.ui.a_sewa_barang;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_search_result.SearchResultMvpPresenter;
import com.taksewa.taksewa.ui.a_search_result.SearchResultMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class SewaBarangPresenter<V extends SewaBarangMvpView> extends BasePresenter<V>
        implements SewaBarangMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    private BarangRequest.FromID fromID;

    @Inject
    public SewaBarangPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int brgId) {
        fromID = new BarangRequest.FromID();
        fromID.setId(brgId);
        DoGetBarangData();
    }

    @Override
    public void onViewTransactionPrepared() {
        DoGetProfileInfo();
    }

    public void DoGetProfileInfo() {
        getProfileInfo(response -> {
            getMvpView().onPresenterUserInfoReady(response.getData());
        }, throwable -> {
            Log.d(TAG, "fail");
            setUserAsLoggedOut();
            if (!isViewAttached()) {
                return;
            }
            if (throwable instanceof ANError) {
                ANError anError = (ANError) throwable;
                handleApiError(anError);
            }
        });
//        getCompositeDisposable().add(getDataManager()
//                .getProfileInfo()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<UserResponse.IData>() {
//                    @Override
//                    public void accept(@NonNull UserResponse.IData response) throws Exception {
//                        getMvpView().onPresenterUserInfoReady(response.getData());
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(@NonNull Throwable throwable)
//                            throws Exception {
//                        Log.d(TAG, "fail");
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
//                    }
//                }));
    }

    public void DoGetBarangData() {
        getCompositeDisposable().add(getDataManager()
                .getBarangDataFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IData>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IData response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

