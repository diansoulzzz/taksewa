package com.taksewa.taksewa.ui.f_report_barang_selesai;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.HSewaRequest;
import com.taksewa.taksewa.data.network.request.SearchRequest;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.model.Filter;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ReportBarangSelesaiPresenter<V extends ReportBarangSelesaiMvpView> extends BasePresenter<V>
        implements ReportBarangSelesaiMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public ReportBarangSelesaiPresenter(DataManager dataManager,
                                        SchedulerProvider schedulerProvider,
                                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(String tipe) {
        DoGetSearchVendor(new HSewaRequest.FromFilter(tipe));
    }

    public void DoGetSearchVendor(HSewaRequest.FromFilter fromFilter) {
        getCompositeDisposable().add(getDataManager()
                .getVendorReportTransactionListFromFilter(fromFilter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IList>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IList iList) throws Exception {
                        getMvpView().onPresenterReady(iList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}

