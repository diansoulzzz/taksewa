package com.taksewa.taksewa.ui.a_saldo_withdraw;


import com.taksewa.taksewa.data.network.request.UserRequest;
import com.taksewa.taksewa.di.PerActivity;
import com.taksewa.taksewa.ui._base.MvpPresenter;

@PerActivity
public interface SaldoWithdrawMvpPresenter<V extends SaldoWithdrawMvpView> extends MvpPresenter<V> {
    void onViewPrepared();

    void onWithdrawClick(UserRequest.SaldoWithdraw saldoWithdraw);
}
