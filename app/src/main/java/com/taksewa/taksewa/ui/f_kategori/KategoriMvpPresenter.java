package com.taksewa.taksewa.ui.f_kategori;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_inbox.InboxMvpView;

public interface KategoriMvpPresenter<V extends KategoriMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

