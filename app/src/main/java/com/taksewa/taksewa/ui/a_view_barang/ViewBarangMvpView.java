package com.taksewa.taksewa.ui.a_view_barang;

import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ViewBarangMvpView extends MvpView {

    void onPresenterReady(BarangResponse.IData barangResponse);

    void onPresenterWishlisted(BarangResponse.IWishListed wishListedResponse);

    void onPresenterWishlisted(BarangResponse.IWishListed wishListedResponse, boolean isShowToast);

    void onProductRegisteredByThisUserLogin();
}
