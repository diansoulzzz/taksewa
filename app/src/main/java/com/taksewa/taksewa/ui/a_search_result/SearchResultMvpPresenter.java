package com.taksewa.taksewa.ui.a_search_result;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_search.SearchMvpView;

public interface SearchResultMvpPresenter<V extends SearchResultMvpView> extends MvpPresenter<V> {

}

