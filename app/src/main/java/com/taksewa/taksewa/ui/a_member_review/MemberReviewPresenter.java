package com.taksewa.taksewa.ui.a_member_review;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpPresenter;
import com.taksewa.taksewa.ui.a_member_last_seen_barang.MemberLastSeenBarangMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class MemberReviewPresenter<V extends MemberReviewMvpView> extends BasePresenter<V>
        implements MemberReviewMvpPresenter<V> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public MemberReviewPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getMemberReviewList();
    }

    public void getMemberReviewList() {
        getCompositeDisposable().add(getDataManager()
                .getMemberReviewList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<HSewaResponse.IList>() {
                    @Override
                    public void accept(@NonNull HSewaResponse.IList response) throws Exception {
                        getMvpView().onPresenterReady(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {

                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }

                    }
                }));
    }
}

