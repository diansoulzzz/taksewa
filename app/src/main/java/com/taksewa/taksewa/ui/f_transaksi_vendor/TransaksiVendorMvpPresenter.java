package com.taksewa.taksewa.ui.f_transaksi_vendor;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.f_transaksi_member.TransaksiMemberMvpView;

public interface TransaksiVendorMvpPresenter<V extends TransaksiVendorMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

