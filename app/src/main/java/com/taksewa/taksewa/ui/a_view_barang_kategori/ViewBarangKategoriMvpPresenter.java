package com.taksewa.taksewa.ui.a_view_barang_kategori;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang.ViewBarangMvpView;

public interface ViewBarangKategoriMvpPresenter<V extends ViewBarangKategoriMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

