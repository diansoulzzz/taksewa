package com.taksewa.taksewa.ui.a_saldo_withdraw;

import com.taksewa.taksewa.data.network.response.UserResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface SaldoWithdrawMvpView extends MvpView {

    void onPresenterReady(UserResponse.IData response);

    void onSuccess();
}
