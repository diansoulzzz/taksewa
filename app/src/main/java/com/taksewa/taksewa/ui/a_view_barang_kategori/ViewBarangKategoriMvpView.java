package com.taksewa.taksewa.ui.a_view_barang_kategori;

import com.taksewa.taksewa.data.network.response.BarangKategoriResponse;
import com.taksewa.taksewa.model.BarangKategori;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ViewBarangKategoriMvpView extends MvpView {
    void onPresenterReady(BarangKategoriResponse.IList barangKategoriIListResponse);
}
