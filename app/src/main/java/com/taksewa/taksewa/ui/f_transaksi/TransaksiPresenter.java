package com.taksewa.taksewa.ui.f_transaksi;

import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.f_kategori.KategoriMvpPresenter;
import com.taksewa.taksewa.ui.f_kategori.KategoriMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TransaksiPresenter<V extends TransaksiMvpView> extends BasePresenter<V>
        implements TransaksiMvpPresenter<V> {
    @Inject
    public TransaksiPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {

    }
}

