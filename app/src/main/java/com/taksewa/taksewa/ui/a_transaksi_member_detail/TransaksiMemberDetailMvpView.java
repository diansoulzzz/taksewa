package com.taksewa.taksewa.ui.a_transaksi_member_detail;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.MvpView;

public interface TransaksiMemberDetailMvpView extends MvpView {
    void onPresenterReady(HSewaResponse.IData response);

    void onSuccessUpdate(HSewaResponse.IData response);
}
