package com.taksewa.taksewa.ui.a_member_last_seen_barang;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_member_wishlist.MemberWishlistMvpView;

public interface MemberLastSeenBarangMvpPresenter<V extends MemberLastSeenBarangMvpView> extends MvpPresenter<V> {

    void onViewPrepared();
}

