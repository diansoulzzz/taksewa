package com.taksewa.taksewa.ui.a_register;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseActivity;
import com.taksewa.taksewa.ui.a_login.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterMvpView, FacebookCallback<LoginResult> {

    private final String TAG = getClass().getSimpleName();

    @Inject
    RegisterMvpPresenter<RegisterMvpView> mPresenter;

    @BindView(R.id.tvSudahPunyaAkun)
    TextView tvSudahPunyaAkun;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.tietEmail)
    TextInputEditText tietEmail;
    @BindView(R.id.tietPassword)
    TextInputEditText tietPassword;

    private GoogleSignInClient googleSignInClient;
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                mPresenter.onGoogleAuth(account);
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void setUp() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        googleSignInClient = GoogleSignIn.getClient(this, getGoogleSignInOptions());
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager, this);
    }

    @OnClick(R.id.tvSudahPunyaAkun)
    void onSudahPunyaAkunClick() {
        startLoginActivityAndFinish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.navigation_login:
                startLoginActivityAndFinish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startLoginActivityAndFinish() {
        startLoginActivity();
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_register, menu);
        return true;
    }

    @Override
    public boolean isFormValid() {
        boolean isValid = true;
        if (TextUtils.isEmpty(tietEmail.getText())) {
            tietEmail.setError("Email required");
            isValid = false;
        }
        if (TextUtils.isEmpty(tietPassword.getText())) {
            tietPassword.setError("Password required");
            isValid = false;
        }
        return isValid;
    }

    @OnClick(R.id.btnRegister)
    void onLoginClick() {
        if (isFormValid()) {
            mPresenter.onServerAuth(tietEmail.getText().toString(), tietPassword.getText().toString());
        }
    }

    @OnClick(R.id.btnLoginGoogle)
    void onLoginGoogleClick() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.btnLoginFacebook)
    void onFbLoginClick(View v) {
        loginManager.logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_birthday", "user_friends"));
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Log.d(TAG, "facebook:onSuccess:" + loginResult);
        mPresenter.onFacebookAuth(loginResult.getAccessToken());
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v(TAG, response.toString());
                        try {
                            String email = object.getString("email");
                            Log.d(TAG, email);
//                                    String birthday = object.getString("birthday"); // 01/31/1980 format
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void onCancel() {
        Log.d(TAG, "facebook:onCancel");
    }

    @Override
    public void onError(FacebookException error) {
        Log.d(TAG, "facebook:onError", error);
    }
}
