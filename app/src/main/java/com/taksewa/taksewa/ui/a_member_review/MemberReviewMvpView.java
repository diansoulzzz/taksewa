package com.taksewa.taksewa.ui.a_member_review;

import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface MemberReviewMvpView extends MvpView {
    void onPresenterReady(HSewaResponse.IList hsewaResponse);
}
