package com.taksewa.taksewa.ui.a_verify;

import com.taksewa.taksewa.model.User;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpView;

public interface VerifyMvpPresenter<V extends VerifyMvpView> extends MvpPresenter<V> {

    void onViewPrepared();

    void onSubmitVerify(User user);

    void onFileUpload();
}

