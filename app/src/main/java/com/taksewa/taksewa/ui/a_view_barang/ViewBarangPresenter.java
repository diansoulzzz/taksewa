package com.taksewa.taksewa.ui.a_view_barang;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.data.network.request.BarangRequest;
import com.taksewa.taksewa.data.network.response.BarangResponse;
import com.taksewa.taksewa.data.network.response.HomeResponse;
import com.taksewa.taksewa.ui._base.BasePresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpPresenter;
import com.taksewa.taksewa.ui.a_splash.SplashMvpView;
import com.taksewa.taksewa.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ViewBarangPresenter<V extends ViewBarangMvpView> extends BasePresenter<V>
        implements ViewBarangMvpPresenter<V> {
    private final String TAG = getClass().getSimpleName();

    private BarangRequest.FromID fromID;

    @Inject
    public ViewBarangPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int brgId) {
        fromID = new BarangRequest.FromID();
        fromID.setId(brgId);
        DoGetBarangData();
    }

    @Override
    public void onFabWishlistClick(int brgId) {
        fromID = new BarangRequest.FromID();
        fromID.setId(brgId);
        DoPostToogleWishlisted();
    }

    public void DoGetBarangData() {
        getCompositeDisposable().add(getDataManager()
                .getBarangDataFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IData>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IData response) throws Exception {
                        if (isUserLogin()) {
                            if (getDataManager().getCurrentUserId().intValue() == response.getData().getUsersId()) {
                                getMvpView().onProductRegisteredByThisUserLogin();
                            }
                        }
                        getMvpView().onPresenterReady(response);
                        DoGetWishlisted();
                        DoPostToogleLastSeenBarang();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    public void DoGetWishlisted() {
        getCompositeDisposable().add(getDataManager()
                .getBarangIsWishlistFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IWishListed>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IWishListed response) throws Exception {
                        getMvpView().onPresenterWishlisted(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
                    }
                }));
    }

    public void DoPostToogleLastSeenBarang() {
        getCompositeDisposable().add(getDataManager()
                .postToogleLastSeenBarangFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.ILastSeenList>() {
                    @Override
                    public void accept(@NonNull BarangResponse.ILastSeenList response) throws Exception {
//                        getMvpView().onPresenterWishlisted(response, true);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                    }
                }));
    }

    public void DoPostToogleWishlisted() {
        getCompositeDisposable().add(getDataManager()
                .postToogleBarangWishlistFromId(fromID)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<BarangResponse.IWishListed>() {
                    @Override
                    public void accept(@NonNull BarangResponse.IWishListed response) throws Exception {
                        getMvpView().onPresenterWishlisted(response, true);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        Log.d(TAG, "fail");
                        if (!isViewAttached()) {
                            return;
                        }
                    }
                }));
    }
}

