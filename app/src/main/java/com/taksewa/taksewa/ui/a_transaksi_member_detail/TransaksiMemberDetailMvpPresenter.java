package com.taksewa.taksewa.ui.a_transaksi_member_detail;

import com.taksewa.taksewa.model.HSewa;
import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_view_barang_sub_kategori.ViewBarangSubKategoriMvpView;

public interface TransaksiMemberDetailMvpPresenter<V extends TransaksiMemberDetailMvpView> extends MvpPresenter<V> {
    void onViewPrepared(int hSewaId);

    void onChangeData(HSewa hSewa);
}

