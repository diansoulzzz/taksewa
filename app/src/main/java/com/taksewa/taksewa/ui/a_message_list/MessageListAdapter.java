package com.taksewa.taksewa.ui.a_message_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.Message;
import com.taksewa.taksewa.model.MessageRoom;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.ui.f_search_result_vendor.SearchResultVendorAdapter;
import com.taksewa.taksewa.utils.CommonUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageListAdapter {

    public interface Callback {
        void onClickView(MessageRoom messageRoom);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {
        static final int VIEW_TYPE_EMPTY = 0;
        static final int VIEW_TYPE_NORMAL = 1;

        private final String TAG = getClass().getSimpleName();
        private Context context;
        private List<MessageRoom> datas;
        private Callback callback;

        public AdapterList(Context context, List datas, Callback callback) {
            this.context = context;
            this.datas = datas;
            this.callback = callback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new MessageListAdapter.AdapterList.VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_list, parent, false));
                case VIEW_TYPE_EMPTY:
                default:
                    return new CommonUtils.EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false), "Daftar Chat Kosong", "");
            }
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemViewType(int position) {
            if (this.datas != null && this.datas.size() > 0) {
                return VIEW_TYPE_NORMAL;
            }
            return VIEW_TYPE_EMPTY;
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvLastMessage)
            TextView tvLastMessage;
            @BindView(R.id.ivGambar)
            ImageView ivGambar;
            @BindView(R.id.tvNamaTo)
            TextView tvNamaTo;
            MessageRoom data;
            Barang barang;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickView(data);
                    }
                });
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                data = datas.get(i);
                assert data != null;
                if (data.getUserTo() != null) {
                    Glide.with(itemView).load(data.getUserTo().getFotoUrl()).into(ivGambar);
                    tvNamaTo.setText(data.getUserTo().getNama());
                }
                if (data.getValue() != null) {
                    if (data.getType().equals("barang")) {
                        Gson gson = new Gson();
                        barang = gson.fromJson(data.getValue(), Barang.class);
                        tvLastMessage.setText(barang.getNama());
                    } else {
                        String text = data.getValue();
                        if (data.getValue().length()>80){
                            text = data.getValue().substring(0, 77)+"...";
                        }
                        tvLastMessage.setText(text);
                    }
                }
            }
        }
    }
}
