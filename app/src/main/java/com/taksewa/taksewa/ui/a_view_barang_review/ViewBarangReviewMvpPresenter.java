package com.taksewa.taksewa.ui.a_view_barang_review;

import com.taksewa.taksewa.ui._base.MvpPresenter;
import com.taksewa.taksewa.ui.a_member_review.MemberReviewMvpView;

public interface ViewBarangReviewMvpPresenter<V extends ViewBarangReviewMvpView> extends MvpPresenter<V> {

    void onViewPrepared(int brgId);
}

