package com.taksewa.taksewa.ui.f_report_barang_selesai;

import com.taksewa.taksewa.data.network.response.HSewaResponse;
import com.taksewa.taksewa.data.network.response.SearchResponse;
import com.taksewa.taksewa.ui._base.MvpView;

public interface ReportBarangSelesaiMvpView extends MvpView {

    void onPresenterReady(HSewaResponse.IList iList);
}
