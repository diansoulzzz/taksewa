package com.taksewa.taksewa.ui.a_saldo_history;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.model.FotoReview;
import com.taksewa.taksewa.model.HPiutang;
import com.taksewa.taksewa.ui._base.BaseViewHolder;
import com.taksewa.taksewa.utils.CommonUtils;
import com.taksewa.taksewa.utils.NumberTextWatcher;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaldoHistoryAdapter {

    public interface Callback {
        void onClickView(HPiutang hPiutang, View itemView);
    }

    public interface StickyHeaderInterface {

        int getHeaderPositionForItem(int itemPosition);

        int getHeaderLayout(int headerPosition);

        void bindHeaderData(View header, int headerPosition);

        boolean isHeader(int itemPosition);
    }

    public static class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {
        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;

        private Context context;
        private List<HPiutang> datas;
        private Callback mCallback;

        public AdapterList(Context context, List datas, Callback mCallback) {
            this.context = context;
            this.datas = datas;
            this.mCallback = mCallback;
        }

        @NonNull
        @Override
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            switch (viewType) {
                case VIEW_TYPE_NORMAL:
                    return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_saldo_history, viewGroup, false));
            }
            return new VHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_saldo_history, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
            viewHolder.onBind(i);
        }

        @Override
        public int getItemCount() {
            if (this.datas != null && this.datas.size() > 0) {
                return this.datas.size();
            }
            return 0;
        }

        public class VHolder extends BaseViewHolder {
            @BindView(R.id.tvHeader)
            TextView tvHeader;
            @BindView(R.id.tvNominal)
            TextView tvNominal;
            @BindView(R.id.tvStatus)
            TextView tvStatus;
            @BindView(R.id.tvTgl)
            TextView tvTgl;

            public VHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @Override
            protected void clear() {

            }

            public void onBind(int i) {
                final HPiutang data = datas.get(i);
                assert data != null;
                tvHeader.setText(data.getStatusName());
                tvNominal.setText(NumberTextWatcher.getDecimalFormattedString(data.getTotalBayar().toString()));
                tvTgl.setText(CommonUtils.getDateParsedToFormat(data.getTgl(), "dd MMM yyyy"));
                if (data.getTotalBayar() > 0) {
                    tvStatus.setText(String.format("+%s", itemView.getResources().getString(R.string.transfer_masuk)));
                } else {
                    tvStatus.setText(String.format("-%s", itemView.getResources().getString(R.string.transfer_keluar)));
                }
            }
        }
    }
}
