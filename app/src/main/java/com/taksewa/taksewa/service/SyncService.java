package com.taksewa.taksewa.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;


import com.taksewa.taksewa.MyApp;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.di.component.DaggerServiceComponent;
import com.taksewa.taksewa.di.component.ServiceComponent;
import com.taksewa.taksewa.utils.AppLogger;

import javax.inject.Inject;

public class SyncService extends Service {

    private final String TAG = getClass().getSimpleName();

    @Inject
    DataManager mDataManager;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SyncService.class);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, SyncService.class);
        context.startService(starter);
    }

    public static void stop(Context context) {
        context.stopService(new Intent(context, SyncService.class));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service Started");
        ServiceComponent component = DaggerServiceComponent.builder()
                .applicationComponent(((MyApp) getApplication()).getComponent())
                .build();
        component.inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AppLogger.d(TAG, "SyncService started");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        AppLogger.d(TAG, "SyncService stopped");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
