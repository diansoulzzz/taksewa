package com.taksewa.taksewa.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.taksewa.taksewa.MyApp;
import com.taksewa.taksewa.R;
import com.taksewa.taksewa.data.DataManager;
import com.taksewa.taksewa.di.component.DaggerServiceComponent;
import com.taksewa.taksewa.di.component.ServiceComponent;
import com.taksewa.taksewa.model.Barang;
import com.taksewa.taksewa.model.RemoteMessageWrapper;
import com.taksewa.taksewa.ui.a_main.MainActivity;
import com.taksewa.taksewa.ui.a_splash.SplashActivity;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import javax.inject.Inject;

import static com.taksewa.taksewa.utils.CommonUtils.isJSONValid;

public class LiveMessagingService extends FirebaseMessagingService {

    @Inject
    DataManager mDataManager;

    private final String TAG = getClass().getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service Started");
        ServiceComponent component = DaggerServiceComponent.builder()
                .applicationComponent(((MyApp) getApplication()).getComponent())
                .build();
        component.inject(this);

    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();
//        Message msg = new Message();
//        msg.setValue("ADSASD");
        EventBus.getDefault().post(remoteMessage);
        sendNotification(notification, data, remoteMessage);
    }


    private void sendNotification(RemoteMessage.Notification notification, Map<String, String> data, RemoteMessage remoteMessage) {
        try {
            Log.d(TAG, "Click Action : " + notification.getClickAction());
            Log.d(TAG, "Data : " + data.toString());
        } catch (Exception e) {
            Log.e(TAG, "Error : " + e.getMessage());
        }

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);//send notification open to?
//        if (data.get("intent") != null) {
//            if (Objects.equals(data.get("intent"), "chatting")) {
//                User userTo = new User();
//                userTo.setId(Integer.valueOf(Objects.requireNonNull(data.get("users_id_to"))));
//                userTo.setNama(data.get("users_nama_to"));
//                userTo.setFotoUrl(data.get("users_foto_url_to"));
//                intent = new Intent(this, MessageActivity.class); //send notification open to?
//                intent.putExtra("usersTo", userTo);
//            }
//        }
        Intent intent = new Intent(this, SplashActivity.class);

//        Intent intent = new Intent();
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (data.containsKey("click_action")) {
//            intent = new Intent(data.get("click_action"));
            intent.putExtra("notification_data", new RemoteMessageWrapper(remoteMessage));
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String body;
        if (isJSONValid(notification.getBody())) {
            Gson gson = new Gson();
            Barang barang = gson.fromJson(notification.getBody(), Barang.class);
            body = barang.getNama();
        } else {
            body = notification.getBody();
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                .setContentTitle(notification.getTitle())
                .setStyle(new NotificationCompat.BigTextStyle())
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setContentInfo(notification.getTitle())
                .setLargeIcon(icon)
                .setColor(getResources().getColor(R.color.colorPrimaryDark))
                .setLights(getResources().getColor(R.color.colorPrimaryDark), 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.mipmap.ic_launcher);
        try {
            String picture_url = data.get("picture_url");
            if (picture_url != null && !"".equals(picture_url)) {
                URL url = new URL(picture_url);
                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                notificationBuilder.setStyle(
                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Notification Channel is required for Android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "channel_id", "channel_name", NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("channel description");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(getColor(R.color.colorPrimaryDark));
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        sendRegistrationToServer(token);
    }

    public void sendRegistrationToServer(String token) {

//        CompositeDisposable compositeDisposable = new CompositeDisposable();
//        compositeDisposable.add()
    }
}
