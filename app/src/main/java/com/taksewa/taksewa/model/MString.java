package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MString implements Parcelable {
    int id;
    String nama;
    boolean isOn;

    public MString() {
    }

    public MString(int id, String nama, boolean isOn) {
        this.id = id;
        this.nama = nama;
        this.isOn = isOn;
    }

    protected MString(Parcel in) {
        id = in.readInt();
        nama = in.readString();
        isOn = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nama);
        dest.writeByte((byte) (isOn ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MString> CREATOR = new Creator<MString>() {
        @Override
        public MString createFromParcel(Parcel in) {
            return new MString(in);
        }

        @Override
        public MString[] newArray(int size) {
            return new MString[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }
}
