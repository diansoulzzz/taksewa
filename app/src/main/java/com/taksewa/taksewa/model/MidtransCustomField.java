package com.taksewa.taksewa.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MidtransCustomField {
    public static class Custom1 {
        @SerializedName("total_deposit")
        @Expose
        private double totalDeposit;
        @SerializedName("total_ongkir")
        @Expose
        private double totalOngkir;
        @SerializedName("total_lama_sewa")
        @Expose
        private double totalLamaSewa;
        @SerializedName("total_jml_item")
        @Expose
        private double totalJmlItem;
        @SerializedName("total_pembayaran")
        @Expose
        private double totalPembayaran;
        @SerializedName("tgl_input_peminjaman")
        @Expose
        private long tglInputPeminjaman;
        @SerializedName("tgl_input_pengembalian")
        @Expose
        private long tglInputPengembalian;

        public double getTotalDeposit() {
            return totalDeposit;
        }

        public void setTotalDeposit(double totalDeposit) {
            this.totalDeposit = totalDeposit;
        }

        public double getTotalOngkir() {
            return totalOngkir;
        }

        public void setTotalOngkir(double totalOngkir) {
            this.totalOngkir = totalOngkir;
        }

        public double getTotalLamaSewa() {
            return totalLamaSewa;
        }

        public void setTotalLamaSewa(double totalLamaSewa) {
            this.totalLamaSewa = totalLamaSewa;
        }

        public double getTotalJmlItem() {
            return totalJmlItem;
        }

        public void setTotalJmlItem(double totalJmlItem) {
            this.totalJmlItem = totalJmlItem;
        }

        public double getTotalPembayaran() {
            return totalPembayaran;
        }

        public void setTotalPembayaran(double totalPembayaran) {
            this.totalPembayaran = totalPembayaran;
        }

        public long getTglInputPeminjaman() {
            return tglInputPeminjaman;
        }

        public void setTglInputPeminjaman(long tglInputPeminjaman) {
            this.tglInputPeminjaman = tglInputPeminjaman;
        }

        public long getTglInputPengembalian() {
            return tglInputPengembalian;
        }

        public void setTglInputPengembalian(long tglInputPengembalian) {
            this.tglInputPengembalian = tglInputPengembalian;
        }
    }

    public static class Custom2 {
        @SerializedName("barang_id")
        @Expose
        private int barangId;
        @SerializedName("harga_sewa")
        @Expose
        private double hargaSewa;
        @SerializedName("subtotal_sewa")
        @Expose
        private double subtotalSewa;

        public int getBarangId() {
            return barangId;
        }

        public void setBarangId(int barangId) {
            this.barangId = barangId;
        }

        public double getHargaSewa() {
            return hargaSewa;
        }

        public void setHargaSewa(double hargaSewa) {
            this.hargaSewa = hargaSewa;
        }

        public double getSubtotalSewa() {
            return subtotalSewa;
        }

        public void setSubtotalSewa(double subtotalSewa) {
            this.subtotalSewa = subtotalSewa;
        }

    }
}
