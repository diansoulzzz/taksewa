package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReviewBarang implements Parcelable {
    public ReviewBarang() {
    }

    public static final int RATING_SANGAT_BURUK = 1;
    public static final int RATING_BURUK = 2;
    public static final int RATING_CUKUP = 3;
    public static final int RATING_BAIK = 4;
    public static final int RATING_SANGAT_BAIK = 5;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("komentar")
    @Expose
    private String komentar;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("h_sewa_id")
    @Expose
    private Integer hSewaId;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("barang_id")
    @Expose
    private Integer barangId;
    @SerializedName("foto_reviews")
    @Expose
    private List<FotoReview> fotoReviews = null;
    @SerializedName("barang")
    @Expose
    private Barang barang;
    @SerializedName("user")
    @Expose
    private User user;

    protected ReviewBarang(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            rating = null;
        } else {
            rating = in.readInt();
        }
        komentar = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            hSewaId = null;
        } else {
            hSewaId = in.readInt();
        }
        if (in.readByte() == 0) {
            usersId = null;
        } else {
            usersId = in.readInt();
        }
        if (in.readByte() == 0) {
            barangId = null;
        } else {
            barangId = in.readInt();
        }
        fotoReviews = in.createTypedArrayList(FotoReview.CREATOR);
        barang = in.readParcelable(Barang.class.getClassLoader());
        user = in.readParcelable(User.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (rating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(rating);
        }
        dest.writeString(komentar);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        if (hSewaId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(hSewaId);
        }
        if (usersId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersId);
        }
        if (barangId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(barangId);
        }
        dest.writeTypedList(fotoReviews);
        dest.writeParcelable(barang, flags);
        dest.writeParcelable(user, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReviewBarang> CREATOR = new Creator<ReviewBarang>() {
        @Override
        public ReviewBarang createFromParcel(Parcel in) {
            return new ReviewBarang(in);
        }

        @Override
        public ReviewBarang[] newArray(int size) {
            return new ReviewBarang[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getHSewaId() {
        return hSewaId;
    }

    public void setHSewaId(Integer hSewaId) {
        this.hSewaId = hSewaId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Integer getBarangId() {
        return barangId;
    }

    public void setBarangId(Integer barangId) {
        this.barangId = barangId;
    }

    public List<FotoReview> getFotoReviews() {
        return fotoReviews;
    }

    public void setFotoReviews(List<FotoReview> fotoReviews) {
        this.fotoReviews = fotoReviews;
    }

    public String getRatingName() {
        if (rating == RATING_SANGAT_BURUK) {
            return "Sangat Buruk";
        } else if (rating == RATING_BURUK) {
            return "Buruk";
        } else if (rating == RATING_CUKUP) {
            return "Cukup";
        } else if (rating == RATING_BAIK) {
            return "Baik";
        } else if (rating == RATING_SANGAT_BAIK) {
            return "Sangat Baik";
        }
        return "";
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
