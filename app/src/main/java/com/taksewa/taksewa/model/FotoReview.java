package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FotoReview implements Parcelable {
    public FotoReview() {
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("foto_url")
    @Expose
    private String fotoUrl;
    @SerializedName("review_barang_id")
    @Expose
    private Integer reviewBarangId;
    private String fotoLocalPath;

    private String fotoUUID;

    private double progressUpload;

    protected FotoReview(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        fotoUrl = in.readString();
        if (in.readByte() == 0) {
            reviewBarangId = null;
        } else {
            reviewBarangId = in.readInt();
        }
        fotoLocalPath = in.readString();
        fotoUUID = in.readString();
        progressUpload = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(fotoUrl);
        if (reviewBarangId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(reviewBarangId);
        }
        dest.writeString(fotoLocalPath);
        dest.writeString(fotoUUID);
        dest.writeDouble(progressUpload);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FotoReview> CREATOR = new Creator<FotoReview>() {
        @Override
        public FotoReview createFromParcel(Parcel in) {
            return new FotoReview(in);
        }

        @Override
        public FotoReview[] newArray(int size) {
            return new FotoReview[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public Integer getReviewBarangId() {
        return reviewBarangId;
    }

    public void setReviewBarangId(Integer reviewBarangId) {
        this.reviewBarangId = reviewBarangId;
    }

    public String getFotoLocalPath() {
        return fotoLocalPath;
    }

    public void setFotoLocalPath(String fotoLocalPath) {
        this.fotoLocalPath = fotoLocalPath;
    }

    public String getFotoUUID() {
        return fotoUUID;
    }

    public void setFotoUUID(String fotoUUID) {
        this.fotoUUID = fotoUUID;
    }

    public double getProgressUpload() {
        return progressUpload;
    }

    public void setProgressUpload(double progressUpload) {
        this.progressUpload = progressUpload;
    }
}