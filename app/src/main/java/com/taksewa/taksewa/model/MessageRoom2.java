package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageRoom2 implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_read")
    @Expose
    private Integer isRead;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("users_id_from")
    @Expose
    private Integer usersIdFrom;
    @SerializedName("users_id_to")
    @Expose
    private Integer usersIdTo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("last_id")
    @Expose
    private Integer lastId;
    @SerializedName("is_sender")
    @Expose
    private Integer isSender;
    @SerializedName("user_from")
    private User userFrom;
    @SerializedName("user_to")
    private User userTo;

    protected MessageRoom2(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        value = in.readString();
        type = in.readString();
        if (in.readByte() == 0) {
            isRead = null;
        } else {
            isRead = in.readInt();
        }
        uuid = in.readString();
        if (in.readByte() == 0) {
            usersIdFrom = null;
        } else {
            usersIdFrom = in.readInt();
        }
        if (in.readByte() == 0) {
            usersIdTo = null;
        } else {
            usersIdTo = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            lastId = null;
        } else {
            lastId = in.readInt();
        }
        if (in.readByte() == 0) {
            isSender = null;
        } else {
            isSender = in.readInt();
        }
        userFrom = in.readParcelable(User.class.getClassLoader());
        userTo = in.readParcelable(User.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(value);
        dest.writeString(type);
        if (isRead == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isRead);
        }
        dest.writeString(uuid);
        if (usersIdFrom == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersIdFrom);
        }
        if (usersIdTo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersIdTo);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        if (lastId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lastId);
        }
        if (isSender == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isSender);
        }
        dest.writeParcelable(userFrom, flags);
        dest.writeParcelable(userTo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessageRoom2> CREATOR = new Creator<MessageRoom2>() {
        @Override
        public MessageRoom2 createFromParcel(Parcel in) {
            return new MessageRoom2(in);
        }

        @Override
        public MessageRoom2[] newArray(int size) {
            return new MessageRoom2[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getUsersIdFrom() {
        return usersIdFrom;
    }

    public void setUsersIdFrom(Integer usersIdFrom) {
        this.usersIdFrom = usersIdFrom;
    }

    public Integer getUsersIdTo() {
        return usersIdTo;
    }

    public void setUsersIdTo(Integer usersIdTo) {
        this.usersIdTo = usersIdTo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getLastId() {
        return lastId;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }

    public Integer getIsSender() {
        return isSender;
    }

    public void setIsSender(Integer isSender) {
        this.isSender = isSender;
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public User getUserTo() {
        return userTo;
    }

    public void setUserTo(User userTo) {
        this.userTo = userTo;
    }
}
