package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HPiutang implements Parcelable {
    public static final int STATUS_NORMAL = 1;
    public static final int STATUS_DEPOSIT_RETURN = 2;
    public static final int STATUS_INCOME_RENTAL = 3;
    public static final int STATUS_WITHDRAW_PENDING = 4;
    public static final int STATUS_WITHDRAW_APPROVED = 5;

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kodenota")
    @Expose
    private String kodenota;
    @SerializedName("tgl")
    @Expose
    private String tgl;
    @SerializedName("total_bayar")
    @Expose
    private Integer totalBayar;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("status_pembayaran")
    @Expose
    private Integer statusPembayaran;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("users_peminjam_id")
    @Expose
    private Integer usersPeminjamId;
    @SerializedName("h_sewa_id")
    @Expose
    private Integer hSewaId;
    @SerializedName("users_bank_id")
    @Expose
    private Integer usersBankId;
    @SerializedName("midtrans_signature_key")
    @Expose
    private String midtransSignatureKey;
    @SerializedName("midtrans_transaction_id")
    @Expose
    private String midtransTransactionId;
    @SerializedName("midtrans_approval_code")
    @Expose
    private String midtransApprovalCode;
    @SerializedName("midtrans_transaction_status")
    @Expose
    private String midtransTransactionStatus;
    @SerializedName("midtrans_payment_code")
    @Expose
    private String midtransPaymentCode;
    @SerializedName("midtrans_store")
    @Expose
    private String midtransStore;
    @SerializedName("midtrans_status_message")
    @Expose
    private String midtransStatusMessage;
    @SerializedName("midtrans_status_code")
    @Expose
    private String midtransStatusCode;

    public HPiutang() {
    }

    protected HPiutang(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        kodenota = in.readString();
        tgl = in.readString();
        if (in.readByte() == 0) {
            totalBayar = null;
        } else {
            totalBayar = in.readInt();
        }
        paymentMethod = in.readString();
        if (in.readByte() == 0) {
            statusPembayaran = null;
        } else {
            statusPembayaran = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        if (in.readByte() == 0) {
            usersPeminjamId = null;
        } else {
            usersPeminjamId = in.readInt();
        }
        if (in.readByte() == 0) {
            hSewaId = null;
        } else {
            hSewaId = in.readInt();
        }
        if (in.readByte() == 0) {
            usersBankId = null;
        } else {
            usersBankId = in.readInt();
        }
        midtransSignatureKey = in.readString();
        midtransTransactionId = in.readString();
        midtransApprovalCode = in.readString();
        midtransTransactionStatus = in.readString();
        midtransPaymentCode = in.readString();
        midtransStore = in.readString();
        midtransStatusMessage = in.readString();
        midtransStatusCode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(kodenota);
        dest.writeString(tgl);
        if (totalBayar == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalBayar);
        }
        dest.writeString(paymentMethod);
        if (statusPembayaran == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(statusPembayaran);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        if (usersPeminjamId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersPeminjamId);
        }
        if (hSewaId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(hSewaId);
        }
        if (usersBankId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersBankId);
        }
        dest.writeString(midtransSignatureKey);
        dest.writeString(midtransTransactionId);
        dest.writeString(midtransApprovalCode);
        dest.writeString(midtransTransactionStatus);
        dest.writeString(midtransPaymentCode);
        dest.writeString(midtransStore);
        dest.writeString(midtransStatusMessage);
        dest.writeString(midtransStatusCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HPiutang> CREATOR = new Creator<HPiutang>() {
        @Override
        public HPiutang createFromParcel(Parcel in) {
            return new HPiutang(in);
        }

        @Override
        public HPiutang[] newArray(int size) {
            return new HPiutang[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodenota() {
        return kodenota;
    }

    public void setKodenota(String kodenota) {
        this.kodenota = kodenota;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public Integer getTotalBayar() {
        return totalBayar;
    }

    public void setTotalBayar(Integer totalBayar) {
        this.totalBayar = totalBayar;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getStatusPembayaran() {
        return statusPembayaran;
    }

    public void setStatusPembayaran(Integer statusPembayaran) {
        this.statusPembayaran = statusPembayaran;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getUsersPeminjamId() {
        return usersPeminjamId;
    }

    public void setUsersPeminjamId(Integer usersPeminjamId) {
        this.usersPeminjamId = usersPeminjamId;
    }

    public Integer getHSewaId() {
        return hSewaId;
    }

    public void setHSewaId(Integer hSewaId) {
        this.hSewaId = hSewaId;
    }

    public Integer getUsersBankId() {
        return usersBankId;
    }

    public void setUsersBankId(Integer usersBankId) {
        this.usersBankId = usersBankId;
    }

    public String getMidtransSignatureKey() {
        return midtransSignatureKey;
    }

    public void setMidtransSignatureKey(String midtransSignatureKey) {
        this.midtransSignatureKey = midtransSignatureKey;
    }

    public String getMidtransTransactionId() {
        return midtransTransactionId;
    }

    public void setMidtransTransactionId(String midtransTransactionId) {
        this.midtransTransactionId = midtransTransactionId;
    }

    public String getMidtransApprovalCode() {
        return midtransApprovalCode;
    }

    public void setMidtransApprovalCode(String midtransApprovalCode) {
        this.midtransApprovalCode = midtransApprovalCode;
    }

    public String getMidtransTransactionStatus() {
        return midtransTransactionStatus;
    }

    public void setMidtransTransactionStatus(String midtransTransactionStatus) {
        this.midtransTransactionStatus = midtransTransactionStatus;
    }

    public String getMidtransPaymentCode() {
        return midtransPaymentCode;
    }

    public void setMidtransPaymentCode(String midtransPaymentCode) {
        this.midtransPaymentCode = midtransPaymentCode;
    }

    public String getMidtransStore() {
        return midtransStore;
    }

    public void setMidtransStore(String midtransStore) {
        this.midtransStore = midtransStore;
    }

    public String getMidtransStatusMessage() {
        return midtransStatusMessage;
    }

    public void setMidtransStatusMessage(String midtransStatusMessage) {
        this.midtransStatusMessage = midtransStatusMessage;
    }

    public String getMidtransStatusCode() {
        return midtransStatusCode;
    }

    public void setMidtransStatusCode(String midtransStatusCode) {
        this.midtransStatusCode = midtransStatusCode;
    }

    public String getFullPaymentMethod() {
        if (!getMidtransStore().isEmpty()) {
            return getPaymentMethod().replace("_", " ") + " - " + getMidtransStore().replace("_", " ");
        }
        return getPaymentMethod().replace("_", " ");
    }

    public String getStatusName() {
        switch (this.statusPembayaran) {
            case STATUS_NORMAL:
                return "NORMAL";
            case STATUS_DEPOSIT_RETURN:
                return "PENGEMBALIAN DEPOSIT";
            case STATUS_INCOME_RENTAL:
                return "INCOME PERSEWAAN";
            case STATUS_WITHDRAW_PENDING:
                return "TARIK DANA (PENDING)";
            case STATUS_WITHDRAW_APPROVED:
                return "TARIK DANA";
        }
        return "";
    }
}