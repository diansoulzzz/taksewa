package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.messaging.RemoteMessage;

public class RemoteMessageWrapper implements Parcelable {
    private RemoteMessage remoteMessage;

    public RemoteMessageWrapper() {
    }

    public RemoteMessageWrapper(RemoteMessage remoteMessage) {
        this.remoteMessage = remoteMessage;
    }

    protected RemoteMessageWrapper(Parcel in) {
        remoteMessage = in.readParcelable(RemoteMessage.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(remoteMessage, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RemoteMessageWrapper> CREATOR = new Creator<RemoteMessageWrapper>() {
        @Override
        public RemoteMessageWrapper createFromParcel(Parcel in) {
            return new RemoteMessageWrapper(in);
        }

        @Override
        public RemoteMessageWrapper[] newArray(int size) {
            return new RemoteMessageWrapper[size];
        }
    };

    public RemoteMessage getRemoteMessage() {
        return remoteMessage;
    }

    public void setRemoteMessage(RemoteMessage remoteMessage) {
        this.remoteMessage = remoteMessage;
    }
}
