package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsersBank implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nomor")
    @Expose
    private String nomor;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("bank_id")
    @Expose
    private Integer bankId;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("utama")
    @Expose
    private Integer utama;

    public UsersBank() {
    }

    protected UsersBank(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nomor = in.readString();
        nama = in.readString();
        if (in.readByte() == 0) {
            bankId = null;
        } else {
            bankId = in.readInt();
        }
        if (in.readByte() == 0) {
            usersId = null;
        } else {
            usersId = in.readInt();
        }
        if (in.readByte() == 0) {
            utama = null;
        } else {
            utama = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nomor);
        dest.writeString(nama);
        if (bankId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(bankId);
        }
        if (usersId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersId);
        }
        if (utama == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(utama);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsersBank> CREATOR = new Creator<UsersBank>() {
        @Override
        public UsersBank createFromParcel(Parcel in) {
            return new UsersBank(in);
        }

        @Override
        public UsersBank[] newArray(int size) {
            return new UsersBank[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Integer getUtama() {
        return utama;
    }

    public void setUtama(Integer utama) {
        this.utama = utama;
    }


}
