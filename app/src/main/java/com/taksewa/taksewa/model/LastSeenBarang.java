package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastSeenBarang implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("usersId")
    @Expose
    private String usersId;
    @SerializedName("barangId")
    @Expose
    private String barangId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public LastSeenBarang() {
    }

    protected LastSeenBarang(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        usersId = in.readString();
        barangId = in.readString();
        createdAt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(usersId);
        dest.writeString(barangId);
        dest.writeString(createdAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LastSeenBarang> CREATOR = new Creator<LastSeenBarang>() {
        @Override
        public LastSeenBarang createFromParcel(Parcel in) {
            return new LastSeenBarang(in);
        }

        @Override
        public LastSeenBarang[] newArray(int size) {
            return new LastSeenBarang[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public String getBarangId() {
        return barangId;
    }

    public void setBarangId(String barangId) {
        this.barangId = barangId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
