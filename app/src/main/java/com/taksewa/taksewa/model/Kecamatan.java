package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kecamatan implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("kota_id")
    @Expose
    private Integer kotaId;
    @SerializedName("kotum")
    @Expose
    private Kotum kotum;

    protected Kecamatan(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nama = in.readString();
        if (in.readByte() == 0) {
            kotaId = null;
        } else {
            kotaId = in.readInt();
        }
        kotum = in.readParcelable(Kotum.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nama);
        if (kotaId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(kotaId);
        }
        dest.writeParcelable(kotum, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Kecamatan> CREATOR = new Creator<Kecamatan>() {
        @Override
        public Kecamatan createFromParcel(Parcel in) {
            return new Kecamatan(in);
        }

        @Override
        public Kecamatan[] newArray(int size) {
            return new Kecamatan[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getKotaId() {
        return kotaId;
    }

    public void setKotaId(Integer kotaId) {
        this.kotaId = kotaId;
    }

    public Kotum getKotum() {
        return kotum;
    }

    public void setKotum(Kotum kotum) {
        this.kotum = kotum;
    }

}
