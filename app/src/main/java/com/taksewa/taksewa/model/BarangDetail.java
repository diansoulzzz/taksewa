package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangDetail implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("barang_id")
    @Expose
    private Integer barangId;
    @SerializedName("status_barang")
    @Expose
    private Integer statusBarang;
    @SerializedName("barang")
    @Expose
    private Barang barang;

    protected BarangDetail(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            barangId = null;
        } else {
            barangId = in.readInt();
        }
        if (in.readByte() == 0) {
            statusBarang = null;
        } else {
            statusBarang = in.readInt();
        }
        barang = in.readParcelable(Barang.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (barangId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(barangId);
        }
        if (statusBarang == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(statusBarang);
        }
        dest.writeParcelable(barang, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BarangDetail> CREATOR = new Creator<BarangDetail>() {
        @Override
        public BarangDetail createFromParcel(Parcel in) {
            return new BarangDetail(in);
        }

        @Override
        public BarangDetail[] newArray(int size) {
            return new BarangDetail[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBarangId() {
        return barangId;
    }

    public void setBarangId(Integer barangId) {
        this.barangId = barangId;
    }

    public Integer getStatusBarang() {
        return statusBarang;
    }

    public void setStatusBarang(Integer statusBarang) {
        this.statusBarang = statusBarang;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

}
