package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangHargaSewa implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("barang_id")
    @Expose
    private Integer barangId;
    @SerializedName("lama_hari")
    @Expose
    private Integer lamaHari;
    @SerializedName("harga")
    @Expose
    private Integer harga;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;

    public BarangHargaSewa() {
    }

    protected BarangHargaSewa(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            barangId = null;
        } else {
            barangId = in.readInt();
        }
        if (in.readByte() == 0) {
            lamaHari = null;
        } else {
            lamaHari = in.readInt();
        }
        if (in.readByte() == 0) {
            harga = null;
        } else {
            harga = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
    }

    public static final Creator<BarangHargaSewa> CREATOR = new Creator<BarangHargaSewa>() {
        @Override
        public BarangHargaSewa createFromParcel(Parcel in) {
            return new BarangHargaSewa(in);
        }

        @Override
        public BarangHargaSewa[] newArray(int size) {
            return new BarangHargaSewa[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBarangId() {
        return barangId;
    }

    public void setBarangId(Integer barangId) {
        this.barangId = barangId;
    }

    public Integer getLamaHari() {
        return lamaHari;
    }

    public void setLamaHari(Integer lamaHari) {
        this.lamaHari = lamaHari;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (barangId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(barangId);
        }
        if (lamaHari == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lamaHari);
        }
        if (harga == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(harga);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
    }
}