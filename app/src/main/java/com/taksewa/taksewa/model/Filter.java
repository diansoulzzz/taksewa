package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Filter implements Parcelable {
    @SerializedName("is_on")
    @Expose
    private int isOn;
    @SerializedName("harga_min")
    @Expose
    private int hargaMin;
    @SerializedName("harga_max")
    @Expose
    private int hargaMax;
    @SerializedName("rating")
    @Expose
    private double rating;
    @SerializedName("barang_kategori")
    @Expose
    private BarangKategori barangKategori;
    @SerializedName("barang_sub_kategori")
    @Expose
    private BarangSubKategori barangSubKategori;
    @SerializedName("barang_kategori_list")
    @Expose
    private ArrayList<BarangKategori> barangKategoriList;
    @SerializedName("barang_sub_kategori_list")
    @Expose
    private ArrayList<BarangSubKategori> barangSubKategoriList;
    @SerializedName("filter_barang_kategori_ids")
    @Expose
    private ArrayList<TagID> filterBarangKategoriIds = new ArrayList<>();
    @SerializedName("filter_barang_sub_kategori_ids")
    @Expose
    private ArrayList<TagID> filterBarangSubKategoriIds = new ArrayList<>();
    @SerializedName("kota_list")
    @Expose
    private ArrayList<Kotum> kotaList;
    @SerializedName("filter_kota_ids")
    @Expose
    private ArrayList<TagID> filterKotaIds = new ArrayList<>();

    public Filter() {
    }

    protected Filter(Parcel in) {
        isOn = in.readInt();
        hargaMin = in.readInt();
        hargaMax = in.readInt();
        rating = in.readDouble();
        barangKategori = in.readParcelable(BarangKategori.class.getClassLoader());
        barangSubKategori = in.readParcelable(BarangSubKategori.class.getClassLoader());
        barangKategoriList = in.createTypedArrayList(BarangKategori.CREATOR);
        barangSubKategoriList = in.createTypedArrayList(BarangSubKategori.CREATOR);
        filterBarangKategoriIds = in.createTypedArrayList(TagID.CREATOR);
        filterBarangSubKategoriIds = in.createTypedArrayList(TagID.CREATOR);
        kotaList = in.createTypedArrayList(Kotum.CREATOR);
        filterKotaIds = in.createTypedArrayList(TagID.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(isOn);
        dest.writeInt(hargaMin);
        dest.writeInt(hargaMax);
        dest.writeDouble(rating);
        dest.writeParcelable(barangKategori, flags);
        dest.writeParcelable(barangSubKategori, flags);
        dest.writeTypedList(barangKategoriList);
        dest.writeTypedList(barangSubKategoriList);
        dest.writeTypedList(filterBarangKategoriIds);
        dest.writeTypedList(filterBarangSubKategoriIds);
        dest.writeTypedList(kotaList);
        dest.writeTypedList(filterKotaIds);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Filter> CREATOR = new Creator<Filter>() {
        @Override
        public Filter createFromParcel(Parcel in) {
            return new Filter(in);
        }

        @Override
        public Filter[] newArray(int size) {
            return new Filter[size];
        }
    };

    public ArrayList<Kotum> getKotaList() {
        return kotaList;
    }

    public void setKotaList(ArrayList<Kotum> kotaList) {
        this.kotaList = kotaList;
    }

    public ArrayList<TagID> getFilterKotaIds() {
        return filterKotaIds;
    }

    public void setFilterKotaIds(ArrayList<TagID> filterKotaIds) {
        this.filterKotaIds = filterKotaIds;
    }

    public int getHargaMin() {
        return hargaMin;
    }

    public void setHargaMin(int hargaMin) {
        this.hargaMin = hargaMin;
    }

    public int getHargaMax() {
        return hargaMax;
    }

    public void setHargaMax(int hargaMax) {
        this.hargaMax = hargaMax;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public BarangKategori getBarangKategori() {
        return barangKategori;
    }

    public void setBarangKategori(BarangKategori barangKategori) {
        this.barangKategori = barangKategori;
    }

    public BarangSubKategori getBarangSubKategori() {
        return barangSubKategori;
    }

    public void setBarangSubKategori(BarangSubKategori barangSubKategori) {
        this.barangSubKategori = barangSubKategori;
    }

    public int getIsOn() {
        return isOn;
    }

    public void setIsOn(int isOn) {
        this.isOn = isOn;
    }

    public ArrayList<BarangKategori> getBarangKategoriList() {
        return barangKategoriList;
    }

    public void setBarangKategoriList(ArrayList<BarangKategori> barangKategoriList) {
        this.barangKategoriList = barangKategoriList;
    }

    public ArrayList<BarangSubKategori> getBarangSubKategoriList() {
        return barangSubKategoriList;
    }

    public void setBarangSubKategoriList(ArrayList<BarangSubKategori> barangSubKategoriList) {
        this.barangSubKategoriList = barangSubKategoriList;
    }

    public ArrayList<TagID> getFilterBarangKategoriIds() {
        return filterBarangKategoriIds;
    }

    public void setFilterBarangKategoriIds(ArrayList<TagID> filterBarangKategoriIds) {
        this.filterBarangKategoriIds = filterBarangKategoriIds;
    }

    public ArrayList<TagID> getFilterBarangSubKategoriIds() {
        return filterBarangSubKategoriIds;
    }

    public void setFilterBarangSubKategoriIds(ArrayList<TagID> filterBarangSubKategoriIds) {
        this.filterBarangSubKategoriIds = filterBarangSubKategoriIds;
    }
}
