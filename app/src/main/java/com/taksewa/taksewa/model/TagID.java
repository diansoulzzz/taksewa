package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TagID implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;

    public TagID() {
    }

    public TagID(Integer id) {
        this.id = id;
    }

    protected TagID(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TagID> CREATOR = new Creator<TagID>() {
        @Override
        public TagID createFromParcel(Parcel in) {
            return new TagID(in);
        }

        @Override
        public TagID[] newArray(int size) {
            return new TagID[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
