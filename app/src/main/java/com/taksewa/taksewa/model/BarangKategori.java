package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BarangKategori implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("foto_url")
    @Expose
    private String fotoUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("barang_sub_kategoris")
    @Expose
    private ArrayList<BarangSubKategori> barangSubKategoris = new ArrayList<>();

    private Integer isChecked;
    public BarangKategori() {
    }

    protected BarangKategori(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nama = in.readString();
        fotoUrl = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        if (in.readByte() == 0) {
            usersId = null;
        } else {
            usersId = in.readInt();
        }
        barangSubKategoris = in.createTypedArrayList(BarangSubKategori.CREATOR);
        if (in.readByte() == 0) {
            isChecked = null;
        } else {
            isChecked = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nama);
        dest.writeString(fotoUrl);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        if (usersId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersId);
        }
        dest.writeTypedList(barangSubKategoris);
        if (isChecked == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isChecked);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BarangKategori> CREATOR = new Creator<BarangKategori>() {
        @Override
        public BarangKategori createFromParcel(Parcel in) {
            return new BarangKategori(in);
        }

        @Override
        public BarangKategori[] newArray(int size) {
            return new BarangKategori[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }


    public ArrayList<BarangSubKategori> getBarangSubKategoris() {
        return barangSubKategoris;
    }

    public void setBarangSubKategoris(ArrayList<BarangSubKategori> barangSubKategoris) {
        this.barangSubKategoris = barangSubKategoris;
    }

    public Integer getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Integer isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public String toString() {
        return nama;
    }
}