package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DSewa implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("barang_detail_id")
    @Expose
    private Integer barangDetailId;
    @SerializedName("h_sewa_id")
    @Expose
    private Integer hSewaId;
    @SerializedName("tgl_input_sewa_awal")
    @Expose
    private String tglInputSewaAwal;
    @SerializedName("tgl_input_sewa_akhir")
    @Expose
    private String tglInputSewaAkhir;
    @SerializedName("lama_sewa")
    @Expose
    private Integer lamaSewa;
    @SerializedName("harga_sewa")
    @Expose
    private Integer hargaSewa;
    @SerializedName("subtotal")
    @Expose
    private Integer subtotal;
    @SerializedName("barang_detail")
    @Expose
    private BarangDetail barangDetail;

    protected DSewa(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            barangDetailId = null;
        } else {
            barangDetailId = in.readInt();
        }
        if (in.readByte() == 0) {
            hSewaId = null;
        } else {
            hSewaId = in.readInt();
        }
        tglInputSewaAwal = in.readString();
        tglInputSewaAkhir = in.readString();
        if (in.readByte() == 0) {
            lamaSewa = null;
        } else {
            lamaSewa = in.readInt();
        }
        if (in.readByte() == 0) {
            hargaSewa = null;
        } else {
            hargaSewa = in.readInt();
        }
        if (in.readByte() == 0) {
            subtotal = null;
        } else {
            subtotal = in.readInt();
        }
        barangDetail = in.readParcelable(BarangDetail.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (barangDetailId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(barangDetailId);
        }
        if (hSewaId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(hSewaId);
        }
        dest.writeString(tglInputSewaAwal);
        dest.writeString(tglInputSewaAkhir);
        if (lamaSewa == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lamaSewa);
        }
        if (hargaSewa == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(hargaSewa);
        }
        if (subtotal == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(subtotal);
        }
        dest.writeParcelable(barangDetail, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DSewa> CREATOR = new Creator<DSewa>() {
        @Override
        public DSewa createFromParcel(Parcel in) {
            return new DSewa(in);
        }

        @Override
        public DSewa[] newArray(int size) {
            return new DSewa[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBarangDetailId() {
        return barangDetailId;
    }

    public void setBarangDetailId(Integer barangDetailId) {
        this.barangDetailId = barangDetailId;
    }

    public Integer getHSewaId() {
        return hSewaId;
    }

    public void setHSewaId(Integer hSewaId) {
        this.hSewaId = hSewaId;
    }

    public String getTglInputSewaAwal() {
        return tglInputSewaAwal;
    }

    public void setTglInputSewaAwal(String tglInputSewaAwal) {
        this.tglInputSewaAwal = tglInputSewaAwal;
    }

    public String getTglInputSewaAkhir() {
        return tglInputSewaAkhir;
    }

    public void setTglInputSewaAkhir(String tglInputSewaAkhir) {
        this.tglInputSewaAkhir = tglInputSewaAkhir;
    }

    public Integer getLamaSewa() {
        return lamaSewa;
    }

    public void setLamaSewa(Integer lamaSewa) {
        this.lamaSewa = lamaSewa;
    }

    public Integer getHargaSewa() {
        return hargaSewa;
    }

    public void setHargaSewa(Integer hargaSewa) {
        this.hargaSewa = hargaSewa;
    }

    public Integer getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    public BarangDetail getBarangDetail() {
        return barangDetail;
    }

    public void setBarangDetail(BarangDetail barangDetail) {
        this.barangDetail = barangDetail;
    }
}