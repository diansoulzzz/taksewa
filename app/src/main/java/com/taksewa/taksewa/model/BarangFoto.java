package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarangFoto implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("foto_url")
    @Expose
    private String fotoUrl;
    @SerializedName("utama")
    @Expose
    private Integer utama;
    @SerializedName("barang_id")
    @Expose
    private Integer barangId;

    private String fotoLocalPath;

    private String fotoUUID;

    private double progressUpload;

    public BarangFoto() {
    }

    protected BarangFoto(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        fotoUrl = in.readString();
        if (in.readByte() == 0) {
            utama = null;
        } else {
            utama = in.readInt();
        }
        if (in.readByte() == 0) {
            barangId = null;
        } else {
            barangId = in.readInt();
        }
        fotoLocalPath = in.readString();
        fotoUUID = in.readString();
        progressUpload = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(fotoUrl);
        if (utama == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(utama);
        }
        if (barangId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(barangId);
        }
        dest.writeString(fotoLocalPath);
        dest.writeString(fotoUUID);
        dest.writeDouble(progressUpload);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BarangFoto> CREATOR = new Creator<BarangFoto>() {
        @Override
        public BarangFoto createFromParcel(Parcel in) {
            return new BarangFoto(in);
        }

        @Override
        public BarangFoto[] newArray(int size) {
            return new BarangFoto[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public Integer getUtama() {
        return utama;
    }

    public void setUtama(Integer utama) {
        this.utama = utama;
    }

    public Integer getBarangId() {
        return barangId;
    }

    public void setBarangId(Integer barangId) {
        this.barangId = barangId;
    }

    public String getFotoLocalPath() {
        return fotoLocalPath;
    }

    public void setFotoLocalPath(String fotoLocalPath) {
        this.fotoLocalPath = fotoLocalPath;
    }

    public double getProgressUpload() {
        return progressUpload;
    }

    public void setProgressUpload(double progressUpload) {
        this.progressUpload = progressUpload;
    }

    public String getFotoUUID() {
        return fotoUUID;
    }

    public void setFotoUUID(String fotoUUID) {
        this.fotoUUID = fotoUUID;
    }
}