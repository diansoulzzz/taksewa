package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class User implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("foto_url")
    @Expose
    private String fotoUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("aktif")
    @Expose
    private Integer aktif;
    @SerializedName("firebase_auth_provider_id")
    @Expose
    private String firebaseAuthProviderId;
    @SerializedName("api_token")
    @Expose
    private String apiToken;
    @SerializedName("verified")
    @Expose
    private String verified;
    @SerializedName("no_ktp")
    @Expose
    private String noKtp;
    @SerializedName("foto_ktp_url")
    @Expose
    private String fotoKtpUrl;
    @SerializedName("firebase_auth_token")
    @Expose
    private String firebaseAuthToken;
    @SerializedName("firebase_auth_uid")
    @Expose
    private String firebaseAuthUid;
    @SerializedName("firebase_instance_id")
    @Expose
    private String firebaseInstanceId;
    @SerializedName("firebase_fcm_token")
    @Expose
    private String firebaseFcmToken;
    @SerializedName("no_telp")
    @Expose
    private String noTelp;
    @SerializedName("firebase_auth_password_hash")
    @Expose
    private String firebaseAuthPasswordHash;
    @SerializedName("firebase_auth_token_valid_at")
    @Expose
    private String firebaseAuthTokenValidAt;
    @SerializedName("tgl_lahir")
    @Expose
    private String tglLahir;
    @SerializedName("firebase_auth_last_login_at")
    @Expose
    private String firebaseAuthLastLoginAt;
    @SerializedName("is_vendor")
    @Expose
    private Integer isVendor;
    @SerializedName("wishlists")
    @Expose
    private Wishlist wishlists;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("kode_pos")
    @Expose
    private String kodePos;
    @SerializedName("pin_alamat")
    @Expose
    private String pinAlamat;
    @SerializedName("pin_latitude")
    @Expose
    private double pinLatitude;
    @SerializedName("pin_longitude")
    @Expose
    private double pinLongitude;
    @SerializedName("kecamatan_id")
    @Expose
    private Integer kecamatanId;
    @SerializedName("kecamatan")
    @Expose
    private Kecamatan kecamatan;
    @SerializedName("users_bank_utama")
    @Expose
    private UsersBank usersBankUtama;
    @SerializedName("total_saldo")
    @Expose
    private Integer totalSaldo;
    @SerializedName("h_piutangs_complete")
    @Expose
    private List<HPiutang> hPiutangsComplete = null;
    private String fotoKtpLocalPath;
    @SerializedName("barangs")
    @Expose
    private ArrayList<Barang> barangs;
    @SerializedName("h_sewas_penyewa")
    @Expose
    private ArrayList<HSewa> hSewasPenyewa;


    public User() {
    }

    protected User(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nama = in.readString();
        email = in.readString();
        fotoUrl = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            aktif = null;
        } else {
            aktif = in.readInt();
        }
        firebaseAuthProviderId = in.readString();
        apiToken = in.readString();
        verified = in.readString();
        noKtp = in.readString();
        fotoKtpUrl = in.readString();
        firebaseAuthToken = in.readString();
        firebaseAuthUid = in.readString();
        firebaseInstanceId = in.readString();
        firebaseFcmToken = in.readString();
        noTelp = in.readString();
        firebaseAuthPasswordHash = in.readString();
        firebaseAuthTokenValidAt = in.readString();
        tglLahir = in.readString();
        firebaseAuthLastLoginAt = in.readString();
        if (in.readByte() == 0) {
            isVendor = null;
        } else {
            isVendor = in.readInt();
        }
        wishlists = in.readParcelable(Wishlist.class.getClassLoader());
        alamat = in.readString();
        kodePos = in.readString();
        pinAlamat = in.readString();
        pinLatitude = in.readDouble();
        pinLongitude = in.readDouble();
        if (in.readByte() == 0) {
            kecamatanId = null;
        } else {
            kecamatanId = in.readInt();
        }
        kecamatan = in.readParcelable(Kecamatan.class.getClassLoader());
        usersBankUtama = in.readParcelable(UsersBank.class.getClassLoader());
        if (in.readByte() == 0) {
            totalSaldo = null;
        } else {
            totalSaldo = in.readInt();
        }
        hPiutangsComplete = in.createTypedArrayList(HPiutang.CREATOR);
        fotoKtpLocalPath = in.readString();
        barangs = in.createTypedArrayList(Barang.CREATOR);
        hSewasPenyewa = in.createTypedArrayList(HSewa.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nama);
        dest.writeString(email);
        dest.writeString(fotoUrl);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        if (aktif == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(aktif);
        }
        dest.writeString(firebaseAuthProviderId);
        dest.writeString(apiToken);
        dest.writeString(verified);
        dest.writeString(noKtp);
        dest.writeString(fotoKtpUrl);
        dest.writeString(firebaseAuthToken);
        dest.writeString(firebaseAuthUid);
        dest.writeString(firebaseInstanceId);
        dest.writeString(firebaseFcmToken);
        dest.writeString(noTelp);
        dest.writeString(firebaseAuthPasswordHash);
        dest.writeString(firebaseAuthTokenValidAt);
        dest.writeString(tglLahir);
        dest.writeString(firebaseAuthLastLoginAt);
        if (isVendor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isVendor);
        }
        dest.writeParcelable(wishlists, flags);
        dest.writeString(alamat);
        dest.writeString(kodePos);
        dest.writeString(pinAlamat);
        dest.writeDouble(pinLatitude);
        dest.writeDouble(pinLongitude);
        if (kecamatanId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(kecamatanId);
        }
        dest.writeParcelable(kecamatan, flags);
        dest.writeParcelable(usersBankUtama, flags);
        if (totalSaldo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalSaldo);
        }
        dest.writeTypedList(hPiutangsComplete);
        dest.writeString(fotoKtpLocalPath);
        dest.writeTypedList(barangs);
        dest.writeTypedList(hSewasPenyewa);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getAktif() {
        return aktif;
    }

    public void setAktif(Integer aktif) {
        this.aktif = aktif;
    }

    public String getFirebaseAuthProviderId() {
        return firebaseAuthProviderId;
    }

    public void setFirebaseAuthProviderId(String firebaseAuthProviderId) {
        this.firebaseAuthProviderId = firebaseAuthProviderId;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getFotoKtpUrl() {
        return fotoKtpUrl;
    }

    public void setFotoKtpUrl(String fotoKtpUrl) {
        this.fotoKtpUrl = fotoKtpUrl;
    }

    public String getFirebaseAuthToken() {
        return firebaseAuthToken;
    }

    public void setFirebaseAuthToken(String firebaseAuthToken) {
        this.firebaseAuthToken = firebaseAuthToken;
    }

    public String getFirebaseAuthUid() {
        return firebaseAuthUid;
    }

    public void setFirebaseAuthUid(String firebaseAuthUid) {
        this.firebaseAuthUid = firebaseAuthUid;
    }

    public String getFirebaseInstanceId() {
        return firebaseInstanceId;
    }

    public void setFirebaseInstanceId(String firebaseInstanceId) {
        this.firebaseInstanceId = firebaseInstanceId;
    }

    public String getFirebaseFcmToken() {
        return firebaseFcmToken;
    }

    public void setFirebaseFcmToken(String firebaseFcmToken) {
        this.firebaseFcmToken = firebaseFcmToken;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getFirebaseAuthPasswordHash() {
        return firebaseAuthPasswordHash;
    }

    public void setFirebaseAuthPasswordHash(String firebaseAuthPasswordHash) {
        this.firebaseAuthPasswordHash = firebaseAuthPasswordHash;
    }

    public String getFirebaseAuthTokenValidAt() {
        return firebaseAuthTokenValidAt;
    }

    public void setFirebaseAuthTokenValidAt(String firebaseAuthTokenValidAt) {
        this.firebaseAuthTokenValidAt = firebaseAuthTokenValidAt;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getFirebaseAuthLastLoginAt() {
        return firebaseAuthLastLoginAt;
    }

    public void setFirebaseAuthLastLoginAt(String firebaseAuthLastLoginAt) {
        this.firebaseAuthLastLoginAt = firebaseAuthLastLoginAt;
    }

    public Integer getIsVendor() {
        return isVendor;
    }

    public void setIsVendor(Integer isVendor) {
        this.isVendor = isVendor;
    }

    public Wishlist getWishlists() {
        return wishlists;
    }

    public void setWishlists(Wishlist wishlists) {
        this.wishlists = wishlists;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getPinAlamat() {
        return pinAlamat;
    }

    public void setPinAlamat(String pinAlamat) {
        this.pinAlamat = pinAlamat;
    }

    public double getPinLatitude() {
        return pinLatitude;
    }

    public void setPinLatitude(double pinLatitude) {
        this.pinLatitude = pinLatitude;
    }

    public double getPinLongitude() {
        return pinLongitude;
    }

    public void setPinLongitude(double pinLongitude) {
        this.pinLongitude = pinLongitude;
    }

    public Integer getKecamatanId() {
        return kecamatanId;
    }

    public void setKecamatanId(Integer kecamatanId) {
        this.kecamatanId = kecamatanId;
    }

    public Kecamatan getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getFotoKtpLocalPath() {
        return fotoKtpLocalPath;
    }

    public void setFotoKtpLocalPath(String fotoKtpLocalPath) {
        this.fotoKtpLocalPath = fotoKtpLocalPath;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public UsersBank getUsersBankUtama() {
        return usersBankUtama;
    }

    public void setUsersBankUtama(UsersBank usersBankUtama) {
        this.usersBankUtama = usersBankUtama;
    }

    public Integer getTotalSaldo() {
        return totalSaldo;
    }

    public void setTotalSaldo(Integer totalSaldo) {
        this.totalSaldo = totalSaldo;
    }

    public List<HPiutang> gethPiutangsComplete() {
        return hPiutangsComplete;
    }

    public void sethPiutangsComplete(List<HPiutang> hPiutangsComplete) {
        this.hPiutangsComplete = hPiutangsComplete;
    }

    public ArrayList<Barang> getBarangs() {
        return barangs;
    }

    public void setBarangs(ArrayList<Barang> barangs) {
        this.barangs = barangs;
    }

    public ArrayList<HSewa> gethSewasPenyewa() {
        return hSewasPenyewa;
    }

    public void sethSewasPenyewa(ArrayList<HSewa> hSewasPenyewa) {
        this.hSewasPenyewa = hSewasPenyewa;
    }
}