package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HSewa implements Parcelable {
    public static final int PESANAN_DITOLAK_VENDOR = -2;
    public static final int TRANSAKSI_DIBATALKAN = -1;
    public static final int MENUNGGU_PEMBAYARAN = 1;
    public static final int MENUNGGU_KONFIRMASI = 2;
    public static final int PESANAN_SEDANG_DIPROSES = 3;
    public static final int PESANAN_SEDANG_DIKIRIM = 4;
    public static final int PRODUK_SEDANG_DIPINJAM = 5;
    public static final int PRODUK_SUDAH_DIKEMBALIKAN = 6;
    public static final int TRANSAKSI_SELESAI = 7;
    public static final int TRANSAKSI_CASHOUT = 8;

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kodenota")
    @Expose
    private String kodenota;
    @SerializedName("tgl")
    @Expose
    private String tgl;
    @SerializedName("deposit")
    @Expose
    private Integer deposit;
    @SerializedName("biaya_ongkir")
    @Expose
    private Integer biayaOngkir;
    @SerializedName("grandtotal")
    @Expose
    private Integer grandtotal;
    @SerializedName("sisabayar")
    @Expose
    private Integer sisabayar;
    @SerializedName("terbayar")
    @Expose
    private Integer terbayar;
    @SerializedName("tgl_input_peminjaman")
    @Expose
    private String tglInputPeminjaman;
    @SerializedName("tgl_input_pengembalian")
    @Expose
    private String tglInputPengembalian;
    @SerializedName("tgl_konfirmasi_penyewa")
    @Expose
    private String tglKonfirmasiPenyewa;
    @SerializedName("tgl_dikirim_penyewa")
    @Expose
    private String tglDikirimPenyewa;
    @SerializedName("tgl_diterima_peminjam")
    @Expose
    private String tglDiterimaPeminjam;
    @SerializedName("tgl_diterima_penyewa_kembali")
    @Expose
    private String tglDiterimaPenyewaKembali;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("user_peminjam")
    @Expose
    private User userPeminjam;
    @SerializedName("user_penyewa")
    @Expose
    private User userPenyewa;
    @SerializedName("users_penyewa_id")
    @Expose
    private Integer usersPenyewaId;
    @SerializedName("users_peminjam_id")
    @Expose
    private Integer usersPeminjamId;
    @SerializedName("status_transaksi")
    @Expose
    private Integer statusTransaksi;
    @SerializedName("midtrans_order_id")
    @Expose
    private String midtransOrderId;
    @SerializedName("d_sewas")
    @Expose
    private ArrayList<DSewa> dSewas = null;
    @SerializedName("h_piutangs")
    @Expose
    private ArrayList<HPiutang> hPiutangs = null;
    @SerializedName("h_piutang_utama")
    @Expose
    private HPiutang hPiutangUtama;
    @SerializedName("d_sewa_utama")
    @Expose
    private DSewa dSewaUtama;
    @SerializedName("review_barang_utama")
    @Expose
    private ReviewBarang reviewBarangUtama;
    @SerializedName("review_barangs")
    @Expose
    private ArrayList<ReviewBarang> reviewBarangs;

    public HSewa() {
    }

    protected HSewa(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        kodenota = in.readString();
        tgl = in.readString();
        if (in.readByte() == 0) {
            deposit = null;
        } else {
            deposit = in.readInt();
        }
        if (in.readByte() == 0) {
            biayaOngkir = null;
        } else {
            biayaOngkir = in.readInt();
        }
        if (in.readByte() == 0) {
            grandtotal = null;
        } else {
            grandtotal = in.readInt();
        }
        if (in.readByte() == 0) {
            sisabayar = null;
        } else {
            sisabayar = in.readInt();
        }
        if (in.readByte() == 0) {
            terbayar = null;
        } else {
            terbayar = in.readInt();
        }
        tglInputPeminjaman = in.readString();
        tglInputPengembalian = in.readString();
        tglKonfirmasiPenyewa = in.readString();
        tglDikirimPenyewa = in.readString();
        tglDiterimaPeminjam = in.readString();
        tglDiterimaPenyewaKembali = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        userPeminjam = in.readParcelable(User.class.getClassLoader());
        userPenyewa = in.readParcelable(User.class.getClassLoader());
        if (in.readByte() == 0) {
            usersPenyewaId = null;
        } else {
            usersPenyewaId = in.readInt();
        }
        if (in.readByte() == 0) {
            usersPeminjamId = null;
        } else {
            usersPeminjamId = in.readInt();
        }
        if (in.readByte() == 0) {
            statusTransaksi = null;
        } else {
            statusTransaksi = in.readInt();
        }
        midtransOrderId = in.readString();
        dSewas = in.createTypedArrayList(DSewa.CREATOR);
        hPiutangs = in.createTypedArrayList(HPiutang.CREATOR);
        hPiutangUtama = in.readParcelable(HPiutang.class.getClassLoader());
        dSewaUtama = in.readParcelable(DSewa.class.getClassLoader());
        reviewBarangUtama = in.readParcelable(ReviewBarang.class.getClassLoader());
        reviewBarangs = in.createTypedArrayList(ReviewBarang.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(kodenota);
        dest.writeString(tgl);
        if (deposit == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(deposit);
        }
        if (biayaOngkir == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(biayaOngkir);
        }
        if (grandtotal == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(grandtotal);
        }
        if (sisabayar == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(sisabayar);
        }
        if (terbayar == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(terbayar);
        }
        dest.writeString(tglInputPeminjaman);
        dest.writeString(tglInputPengembalian);
        dest.writeString(tglKonfirmasiPenyewa);
        dest.writeString(tglDikirimPenyewa);
        dest.writeString(tglDiterimaPeminjam);
        dest.writeString(tglDiterimaPenyewaKembali);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        dest.writeParcelable(userPeminjam, flags);
        dest.writeParcelable(userPenyewa, flags);
        if (usersPenyewaId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersPenyewaId);
        }
        if (usersPeminjamId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersPeminjamId);
        }
        if (statusTransaksi == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(statusTransaksi);
        }
        dest.writeString(midtransOrderId);
        dest.writeTypedList(dSewas);
        dest.writeTypedList(hPiutangs);
        dest.writeParcelable(hPiutangUtama, flags);
        dest.writeParcelable(dSewaUtama, flags);
        dest.writeParcelable(reviewBarangUtama, flags);
        dest.writeTypedList(reviewBarangs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HSewa> CREATOR = new Creator<HSewa>() {
        @Override
        public HSewa createFromParcel(Parcel in) {
            return new HSewa(in);
        }

        @Override
        public HSewa[] newArray(int size) {
            return new HSewa[size];
        }
    };

    public User getUserPeminjam() {
        return userPeminjam;
    }

    public void setUserPeminjam(User userPeminjam) {
        this.userPeminjam = userPeminjam;
    }

    public User getUserPenyewa() {
        return userPenyewa;
    }

    public void setUserPenyewa(User userPenyewa) {
        this.userPenyewa = userPenyewa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodenota() {
        return kodenota;
    }

    public void setKodenota(String kodenota) {
        this.kodenota = kodenota;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }

    public Integer getBiayaOngkir() {
        return biayaOngkir;
    }

    public void setBiayaOngkir(Integer biayaOngkir) {
        this.biayaOngkir = biayaOngkir;
    }

    public Integer getGrandtotal() {
        return grandtotal;
    }

    public void setGrandtotal(Integer grandtotal) {
        this.grandtotal = grandtotal;
    }

    public Integer getSisabayar() {
        return sisabayar;
    }

    public void setSisabayar(Integer sisabayar) {
        this.sisabayar = sisabayar;
    }

    public Integer getTerbayar() {
        return terbayar;
    }

    public void setTerbayar(Integer terbayar) {
        this.terbayar = terbayar;
    }

    public String getTglInputPeminjaman() {
        return tglInputPeminjaman;
    }

    public void setTglInputPeminjaman(String tglInputPeminjaman) {
        this.tglInputPeminjaman = tglInputPeminjaman;
    }

    public String getTglInputPengembalian() {
        return tglInputPengembalian;
    }

    public void setTglInputPengembalian(String tglInputPengembalian) {
        this.tglInputPengembalian = tglInputPengembalian;
    }

    public String getTglKonfirmasiPenyewa() {
        return tglKonfirmasiPenyewa;
    }

    public void setTglKonfirmasiPenyewa(String tglKonfirmasiPenyewa) {
        this.tglKonfirmasiPenyewa = tglKonfirmasiPenyewa;
    }

    public String getTglDikirimPenyewa() {
        return tglDikirimPenyewa;
    }

    public void setTglDikirimPenyewa(String tglDikirimPenyewa) {
        this.tglDikirimPenyewa = tglDikirimPenyewa;
    }

    public String getTglDiterimaPeminjam() {
        return tglDiterimaPeminjam;
    }

    public void setTglDiterimaPeminjam(String tglDiterimaPeminjam) {
        this.tglDiterimaPeminjam = tglDiterimaPeminjam;
    }

    public String getTglDiterimaPenyewaKembali() {
        return tglDiterimaPenyewaKembali;
    }

    public void setTglDiterimaPenyewaKembali(String tglDiterimaPenyewaKembali) {
        this.tglDiterimaPenyewaKembali = tglDiterimaPenyewaKembali;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getUsersPenyewaId() {
        return usersPenyewaId;
    }

    public void setUsersPenyewaId(Integer usersPenyewaId) {
        this.usersPenyewaId = usersPenyewaId;
    }

    public Integer getStatusTransaksi() {
        return statusTransaksi;
    }

    public void setStatusTransaksi(Integer statusTransaksi) {
        this.statusTransaksi = statusTransaksi;
    }

    public String getMidtransOrderId() {
        return midtransOrderId;
    }

    public void setMidtransOrderId(String midtransOrderId) {
        this.midtransOrderId = midtransOrderId;
    }

    public Integer getUsersPeminjamId() {
        return usersPeminjamId;
    }

    public void setUsersPeminjamId(Integer usersPeminjamId) {
        this.usersPeminjamId = usersPeminjamId;
    }

    public ArrayList<DSewa> getdSewas() {
        return dSewas;
    }

    public void setdSewas(ArrayList<DSewa> dSewas) {
        this.dSewas = dSewas;
    }

    public ArrayList<HPiutang> gethPiutangs() {
        return hPiutangs;
    }

    public void sethPiutangs(ArrayList<HPiutang> hPiutangs) {
        this.hPiutangs = hPiutangs;
    }

    public HPiutang gethPiutangUtama() {
        return hPiutangUtama;
    }

    public void sethPiutangUtama(HPiutang hPiutangUtama) {
        this.hPiutangUtama = hPiutangUtama;
    }

    // -1 BATAL, 0 ORDER, 1 DIKONFIRMASI, 2 PROSES, 3 DIRIM, 4 DITERIMA, 5 DIKEMBALIKAN, 6 DEPOSIT KEMBALI
    public String getStatusTransaksiName() {
        if (getStatusTransaksi() == PESANAN_DITOLAK_VENDOR) {
            return "Pesanan Ditolak Vendor";
        } else if (getStatusTransaksi() == TRANSAKSI_DIBATALKAN) {
            return "Transaksi Dibatalkan";
        } else if (getStatusTransaksi() == MENUNGGU_PEMBAYARAN) {
            return "Menunggu Pembayaran";
        } else if (getStatusTransaksi() == MENUNGGU_KONFIRMASI) {
            return "Menunggu Konfirmasi";
        } else if (getStatusTransaksi() == PESANAN_SEDANG_DIPROSES) {
            return "Pesanan sedang diproses";
        } else if (getStatusTransaksi() == PESANAN_SEDANG_DIKIRIM) {
            return "Pesanan sedang dikirim";
        } else if (getStatusTransaksi() == PRODUK_SEDANG_DIPINJAM) {
            return "Barang sedang dipinjam";
        } else if (getStatusTransaksi() == PRODUK_SUDAH_DIKEMBALIKAN) {
            return "Barang sudah dikembalikan";
        } else if (getStatusTransaksi() == TRANSAKSI_SELESAI) {
            return "Barang sudah diterima Vendor";
        } else if (getStatusTransaksi() == TRANSAKSI_CASHOUT) {
            return "Selesai (Uang & Deposit sudah ditransfer)";
        }
        return null;
    }

    public DSewa getdSewaUtama() {
        return dSewaUtama;
    }

    public void setdSewaUtama(DSewa dSewaUtama) {
        this.dSewaUtama = dSewaUtama;
    }

    public Barang getFirstBarang() {
        if (getdSewaUtama() != null) {
            if (getdSewaUtama().getBarangDetail() != null) {
                if (getdSewaUtama().getBarangDetail().getBarang() != null) {
                    return getdSewaUtama().getBarangDetail().getBarang();
                }
            }
        }
        return null;
    }

    public ReviewBarang getReviewBarangUtama() {
        return reviewBarangUtama;
    }

    public void setReviewBarangUtama(ReviewBarang reviewBarangUtama) {
        this.reviewBarangUtama = reviewBarangUtama;
    }

    public ArrayList<ReviewBarang> getReviewBarangs() {
        return reviewBarangs;
    }

    public void setReviewBarangs(ArrayList<ReviewBarang> reviewBarangs) {
        this.reviewBarangs = reviewBarangs;
    }
}