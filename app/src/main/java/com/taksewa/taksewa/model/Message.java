package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_read")
    @Expose
    private Integer isRead;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("users_id_from")
    @Expose
    private Integer usersIdFrom;
    @SerializedName("users_id_to")
    @Expose
    private Integer usersIdTo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("user_to")
    @Expose
    private User userTo;
    @SerializedName("user_from")
    @Expose
    private User userFrom;
    @SerializedName("last_id")
    @Expose
    private Integer lastId;
    @SerializedName("is_sender")
    @Expose
    private Integer isSender;
    @SerializedName("is_show_date")
    @Expose
    private Integer isShowDate;

    public Message() {
    }

    protected Message(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        value = in.readString();
        type = in.readString();
        if (in.readByte() == 0) {
            isRead = null;
        } else {
            isRead = in.readInt();
        }
        uuid = in.readString();
        if (in.readByte() == 0) {
            usersIdFrom = null;
        } else {
            usersIdFrom = in.readInt();
        }
        if (in.readByte() == 0) {
            usersIdTo = null;
        } else {
            usersIdTo = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        userTo = in.readParcelable(User.class.getClassLoader());
        userFrom = in.readParcelable(User.class.getClassLoader());
        if (in.readByte() == 0) {
            lastId = null;
        } else {
            lastId = in.readInt();
        }
        if (in.readByte() == 0) {
            isSender = null;
        } else {
            isSender = in.readInt();
        }
        if (in.readByte() == 0) {
            isShowDate = null;
        } else {
            isShowDate = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(value);
        dest.writeString(type);
        if (isRead == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isRead);
        }
        dest.writeString(uuid);
        if (usersIdFrom == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersIdFrom);
        }
        if (usersIdTo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersIdTo);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        dest.writeParcelable(userTo, flags);
        dest.writeParcelable(userFrom, flags);
        if (lastId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lastId);
        }
        if (isSender == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isSender);
        }
        if (isShowDate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isShowDate);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public Integer getIsShowDate() {
        return isShowDate;
    }

    public void setIsShowDate(Integer isShowDate) {
        this.isShowDate = isShowDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getUsersIdFrom() {
        return usersIdFrom;
    }

    public void setUsersIdFrom(Integer usersIdFrom) {
        this.usersIdFrom = usersIdFrom;
    }

    public Integer getUsersIdTo() {
        return usersIdTo;
    }

    public void setUsersIdTo(Integer usersIdTo) {
        this.usersIdTo = usersIdTo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public User getUserTo() {
        return userTo;
    }

    public void setUserTo(User userTo) {
        this.userTo = userTo;
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public Integer getLastId() {
        return lastId;
    }

    public void setLastId(Integer lastId) {
        this.lastId = lastId;
    }

    public Integer getIsSender() {
        return isSender;
    }

    public void setIsSender(Integer isSender) {
        this.isSender = isSender;
    }
}
