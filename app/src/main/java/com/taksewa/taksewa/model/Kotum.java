package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kotum implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("tipe")
    @Expose
    private String tipe;
    @SerializedName("kode_pos")
    @Expose
    private String kodePos;
    @SerializedName("provinsi_id")
    @Expose
    private Integer provinsiId;
    @SerializedName("provinsi")
    @Expose
    private Provinsi provinsi;
    private Integer isChecked;

    protected Kotum(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nama = in.readString();
        tipe = in.readString();
        kodePos = in.readString();
        if (in.readByte() == 0) {
            provinsiId = null;
        } else {
            provinsiId = in.readInt();
        }
        provinsi = in.readParcelable(Provinsi.class.getClassLoader());
        if (in.readByte() == 0) {
            isChecked = null;
        } else {
            isChecked = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nama);
        dest.writeString(tipe);
        dest.writeString(kodePos);
        if (provinsiId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(provinsiId);
        }
        dest.writeParcelable(provinsi, flags);
        if (isChecked == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isChecked);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Kotum> CREATOR = new Creator<Kotum>() {
        @Override
        public Kotum createFromParcel(Parcel in) {
            return new Kotum(in);
        }

        @Override
        public Kotum[] newArray(int size) {
            return new Kotum[size];
        }
    };

    public Integer getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Integer isChecked) {
        this.isChecked = isChecked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public Integer getProvinsiId() {
        return provinsiId;
    }

    public void setProvinsiId(Integer provinsiId) {
        this.provinsiId = provinsiId;
    }

    public Provinsi getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(Provinsi provinsi) {
        this.provinsi = provinsi;
    }

}