package com.taksewa.taksewa.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Barang implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("barang_sub_kategori_id")
    @Expose
    private Integer barangSubKategoriId;
    @SerializedName("users_id")
    @Expose
    private Integer usersId;
    @SerializedName("stok")
    @Expose
    private Integer stok;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("deposit_min")
    @Expose
    private Integer depositMin;
    @SerializedName("stok_available")
    @Expose
    private Integer stokAvailable;
    @SerializedName("stok_onbook")
    @Expose
    private Integer stokOnbook;
    @SerializedName("ongkir_dalam_jawa")
    @Expose
    private Integer ongkirDalamJawa;
    @SerializedName("ongkir_luar_jawa")
    @Expose
    private Integer ongkirLuarJawa;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("barang_foto_utama")
    @Expose
    private BarangFoto barangFotoUtama;
    @SerializedName("barang_fotos")
    @Expose
    private ArrayList<BarangFoto> barangFotos = null;
    @SerializedName("barang_sub_kategori")
    @Expose
    private BarangSubKategori barangSubKategori;
    @SerializedName("barang_harga_sewas")
    @Expose
    private ArrayList<BarangHargaSewa> barangHargaSewas = null;
    @SerializedName("barang_harga_sewa_utama")
    @Expose
    private BarangHargaSewa barangHargaSewaUtama;
    @SerializedName("wishlisted")
    @Expose
    private Wishlist wishlisted;
    @SerializedName("barang_review")
    @Expose
    private ArrayList<ReviewBarang> reviewBarangs;
    @SerializedName("total_rating")
    private float totalRating;
    private Integer isSelected;

    public Barang() {
    }

    protected Barang(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        nama = in.readString();
        deskripsi = in.readString();
        if (in.readByte() == 0) {
            barangSubKategoriId = null;
        } else {
            barangSubKategoriId = in.readInt();
        }
        if (in.readByte() == 0) {
            usersId = null;
        } else {
            usersId = in.readInt();
        }
        if (in.readByte() == 0) {
            stok = null;
        } else {
            stok = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        if (in.readByte() == 0) {
            depositMin = null;
        } else {
            depositMin = in.readInt();
        }
        if (in.readByte() == 0) {
            stokAvailable = null;
        } else {
            stokAvailable = in.readInt();
        }
        if (in.readByte() == 0) {
            stokOnbook = null;
        } else {
            stokOnbook = in.readInt();
        }
        if (in.readByte() == 0) {
            ongkirDalamJawa = null;
        } else {
            ongkirDalamJawa = in.readInt();
        }
        if (in.readByte() == 0) {
            ongkirLuarJawa = null;
        } else {
            ongkirLuarJawa = in.readInt();
        }
        user = in.readParcelable(User.class.getClassLoader());
        barangFotoUtama = in.readParcelable(BarangFoto.class.getClassLoader());
        barangFotos = in.createTypedArrayList(BarangFoto.CREATOR);
        barangSubKategori = in.readParcelable(BarangSubKategori.class.getClassLoader());
        barangHargaSewas = in.createTypedArrayList(BarangHargaSewa.CREATOR);
        barangHargaSewaUtama = in.readParcelable(BarangHargaSewa.class.getClassLoader());
        wishlisted = in.readParcelable(Wishlist.class.getClassLoader());
        reviewBarangs = in.createTypedArrayList(ReviewBarang.CREATOR);
        totalRating = in.readFloat();
        if (in.readByte() == 0) {
            isSelected = null;
        } else {
            isSelected = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(nama);
        dest.writeString(deskripsi);
        if (barangSubKategoriId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(barangSubKategoriId);
        }
        if (usersId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usersId);
        }
        if (stok == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(stok);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        if (depositMin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(depositMin);
        }
        if (stokAvailable == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(stokAvailable);
        }
        if (stokOnbook == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(stokOnbook);
        }
        if (ongkirDalamJawa == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ongkirDalamJawa);
        }
        if (ongkirLuarJawa == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ongkirLuarJawa);
        }
        dest.writeParcelable(user, flags);
        dest.writeParcelable(barangFotoUtama, flags);
        dest.writeTypedList(barangFotos);
        dest.writeParcelable(barangSubKategori, flags);
        dest.writeTypedList(barangHargaSewas);
        dest.writeParcelable(barangHargaSewaUtama, flags);
        dest.writeParcelable(wishlisted, flags);
        dest.writeTypedList(reviewBarangs);
        dest.writeFloat(totalRating);
        if (isSelected == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isSelected);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Barang> CREATOR = new Creator<Barang>() {
        @Override
        public Barang createFromParcel(Parcel in) {
            return new Barang(in);
        }

        @Override
        public Barang[] newArray(int size) {
            return new Barang[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getBarangSubKategoriId() {
        return barangSubKategoriId;
    }

    public void setBarangSubKategoriId(Integer barangSubKategoriId) {
        this.barangSubKategoriId = barangSubKategoriId;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getDepositMin() {
        return depositMin;
    }

    public void setDepositMin(Integer depositMin) {
        this.depositMin = depositMin;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BarangFoto getBarangFotoUtama() {
        return barangFotoUtama;
    }

    public void setBarangFotoUtama(BarangFoto barangFotoUtama) {
        this.barangFotoUtama = barangFotoUtama;
    }

    public ArrayList<BarangFoto> getBarangFotos() {
        return barangFotos;
    }

    public void setBarangFotos(ArrayList<BarangFoto> barangFotos) {
        this.barangFotos = barangFotos;
    }

    public BarangSubKategori getBarangSubKategori() {
        return barangSubKategori;
    }

    public void setBarangSubKategori(BarangSubKategori barangSubKategori) {
        this.barangSubKategori = barangSubKategori;
    }

    public ArrayList<BarangHargaSewa> getBarangHargaSewas() {
        return barangHargaSewas;
    }

    public void setBarangHargaSewas(ArrayList<BarangHargaSewa> barangHargaSewas) {
        this.barangHargaSewas = barangHargaSewas;
    }

    public BarangHargaSewa getBarangHargaSewaUtama() {
        return barangHargaSewaUtama;
    }

    public void setBarangHargaSewaUtama(BarangHargaSewa barangHargaSewaUtama) {
        this.barangHargaSewaUtama = barangHargaSewaUtama;
    }

    public Integer getStokAvailable() {
        return stokAvailable;
    }

    public void setStokAvailable(Integer stokAvailable) {
        this.stokAvailable = stokAvailable;
    }

    public Integer getStokOnbook() {
        return stokOnbook;
    }

    public void setStokOnbook(Integer stokOnbook) {
        this.stokOnbook = stokOnbook;
    }

    public Integer getOngkirDalamJawa() {
        return ongkirDalamJawa;
    }

    public void setOngkirDalamJawa(Integer ongkirDalamJawa) {
        this.ongkirDalamJawa = ongkirDalamJawa;
    }

    public Integer getOngkirLuarJawa() {
        return ongkirLuarJawa;
    }

    public void setOngkirLuarJawa(Integer ongkirLuarJawa) {
        this.ongkirLuarJawa = ongkirLuarJawa;
    }

    public Wishlist getWishlisted() {
        return wishlisted;
    }

    public void setWishlisted(Wishlist wishlisted) {
        this.wishlisted = wishlisted;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this, this.getClass());
    }

    public ArrayList<ReviewBarang> getReviewBarangs() {
        return reviewBarangs;
    }

    public void setReviewBarangs(ArrayList<ReviewBarang> reviewBarangs) {
        this.reviewBarangs = reviewBarangs;
    }

    public float getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(float totalRating) {
        this.totalRating = totalRating;
    }

    public Integer getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }
}