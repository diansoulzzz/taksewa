package com.taksewa.taksewa.utils;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.taksewa.taksewa.R;
import com.taksewa.taksewa.ui._base.BaseViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class CommonUtils {

    private static final String TAG = "CommonUtils";
    private static Locale defaultLocale = new Locale("in", "ID");
    private static Date currentDate;

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.z_common_progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(new Date());
    }

    public static List<String> getImagePaths(Cursor cursor) {
        List<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int image_path_col = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                result.add(cursor.getString(image_path_col));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public static String saveBitmapGetPath(Bitmap finalBitmap) {
        String filePath = null;
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/Taksewa");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String fname = UUID.randomUUID().toString();
//        fname = UUID.randomUUID().toString();
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            filePath = file.getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return filePath;
    }

    public static long daysBetween(Date startDate, Date endDate) {
        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    private static Calendar getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }

    public static class EmptyHolder extends BaseViewHolder {
        @BindView(R.id.ivEmptyImage)
        ImageView ivEmptyImage;
        @BindView(R.id.tvEmptyTitle)
        TextView tvEmptyTitle;
        @BindView(R.id.tvEmptySubtitle)
        TextView tvEmptySubtitle;
        String title;
        String subTitle;

        public EmptyHolder(@NonNull View itemView, String title, String subTitle) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.title = title;
            this.subTitle = subTitle;
        }

        @Override
        protected void clear() {

        }

        public void onBind(int i) {
            tvEmptyTitle.setText(title);
            tvEmptySubtitle.setText(subTitle);
        }
    }

    public static Date getDateParsed(String dateString) {
        String defaultFormatFrom = "yyyy-MM-dd hh:mm:ss";
//        2019-04-25 11:48:57
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(defaultFormatFrom, Locale.US);
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    public static Date getDateParsed(String dateString, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, defaultLocale);
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    public static String getStringDateFormat(Date date) {
        String defaultFormatTo = "dd MMM yyyy HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(defaultFormatTo, defaultLocale);
        return simpleDateFormat.format(date);
    }

    public static String getStringDateFormat(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, defaultLocale);
        return simpleDateFormat.format(date);
    }

    public static String capitalizerStr(String word) {
        String[] words = word.split(" ");
        StringBuilder sb = new StringBuilder();
        if (words[0].length() > 0) {
            sb.append(Character.toUpperCase(words[0].charAt(0))).append(words[0].subSequence(1, words[0].length()).toString().toLowerCase());
            for (int i = 1; i < words.length; i++) {
                sb.append(" ");
                sb.append(Character.toUpperCase(words[i].charAt(0))).append(words[i].subSequence(1, words[i].length()).toString().toLowerCase());
            }
        }
        return sb.toString();
    }

    public static Date getCurrentDate() {
        currentDate = Calendar.getInstance().getTime();
        return currentDate;
    }

    public static Date getDiffDateFromToday(int diff) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, diff);
        return cal.getTime();
    }

    public static String getDateParsedToFormat(String dateString, String format) {
        return getStringDateFormat(getDateParsed(dateString), format);
    }

    public static long getDiffFromDate(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        return days;
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static Locale getDefaultLocale() {
        return defaultLocale;
    }

    public static AlertDialog.Builder createAlertDialogBuilder(Context context, Spanned title, Spanned message, Spanned yes, Spanned no, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(yes, listener).setNegativeButton(no, listener);
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        return builder;
    }

    public static AlertDialog.Builder createAlertDialogBuilder(Context context, String title, String message, String yes, String no, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(yes, listener).setNegativeButton(no, listener);
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        return builder;
    }

    public static PopupMenu createPopupMenu(Context context, View targetView, int resMenu, PopupMenu.OnMenuItemClickListener listener) {
        PopupMenu popupMenu = new PopupMenu(context, targetView);
        popupMenu.inflate(resMenu);
        popupMenu.setOnMenuItemClickListener(listener);
        return popupMenu;
    }
}
