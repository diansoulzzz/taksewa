package com.taksewa.taksewa.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.taksewa.taksewa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VLayoutGroup extends FrameLayout {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSubtitle)
    TextView tvSubtitle;
    @BindView(R.id.tvSeeAll)
    TextView tvSeeAll;

    private String titleText, subtitleText, seeAllText;

    public VLayoutGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public VLayoutGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        View v = LayoutInflater.from(context).inflate(R.layout.z_v_layout_group, this, true);
        ButterKnife.bind(this, v);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.VLayoutGroup, 0, 0);
        try {
            titleText = a.getString(R.styleable.VLayoutGroup_titleText);
            subtitleText = a.getString(R.styleable.VLayoutGroup_subtitleText);
            seeAllText = a.getString(R.styleable.VLayoutGroup_seeAllText);
            applyStyle();
        } finally {
            a.recycle();
        }
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
        applyStyle();
    }

    public void setSubtitleText(String subtitleText) {
        this.subtitleText = subtitleText;
        applyStyle();
    }

    public void setSeeAllText(String seeAllText) {
        this.seeAllText = seeAllText;
        applyStyle();
    }

    public void applyStyle() {
        if (TextUtils.isEmpty(subtitleText)){
            tvSubtitle.setVisibility(GONE);
        }
        if (TextUtils.isEmpty(seeAllText)){
            tvSeeAll.setVisibility(GONE);
        }
        tvTitle.setText(titleText);
        tvSubtitle.setText(subtitleText);
        tvSeeAll.setText(seeAllText);
    }
}