package com.taksewa.taksewa.utils;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.taksewa.taksewa.R;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.taksewa.taksewa.model.HSewa.MENUNGGU_KONFIRMASI;
import static com.taksewa.taksewa.model.HSewa.MENUNGGU_PEMBAYARAN;
import static com.taksewa.taksewa.model.HSewa.PESANAN_DITOLAK_VENDOR;
import static com.taksewa.taksewa.model.HSewa.PESANAN_SEDANG_DIKIRIM;
import static com.taksewa.taksewa.model.HSewa.PESANAN_SEDANG_DIPROSES;
import static com.taksewa.taksewa.model.HSewa.PRODUK_SEDANG_DIPINJAM;
import static com.taksewa.taksewa.model.HSewa.PRODUK_SUDAH_DIKEMBALIKAN;
import static com.taksewa.taksewa.model.HSewa.TRANSAKSI_CASHOUT;
import static com.taksewa.taksewa.model.HSewa.TRANSAKSI_DIBATALKAN;
import static com.taksewa.taksewa.model.HSewa.TRANSAKSI_SELESAI;

//import static androidx.constraintlayout.widget.ConstraintProperties.WRAP_CONTENT;

public class IconStepStatus extends FrameLayout {
    @BindView(R.id.ivIconHourglass)
    ImageView ivIconHourglass;
    @BindView(R.id.ivIconPayment)
    ImageView ivIconPayment;
    @BindView(R.id.ivIconShipping)
    ImageView ivIconShipping;
    @BindView(R.id.ivIconPacking)
    ImageView ivIconPacking;
    @BindView(R.id.ivIconBorrow)
    ImageView ivIconBorrow;
    @BindView(R.id.ivIconReturn)
    ImageView ivIconReturn;
    @BindView(R.id.ivIconFinish)
    ImageView ivIconFinish;
    @BindView(R.id.ivIconCancel)
    ImageView ivIconCancel;
    @BindView(R.id.ivIconCashout)
    ImageView ivIconCashout;

    private Drawable iconDrawable;
    private Drawable iconDrawableCancel;
    private Integer statusLevel;
    private Boolean showLastIconOnly;
    private float iconPadding;
    private float iconSize;

    public IconStepStatus(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public IconStepStatus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_icon_step_status, this, true);
        ButterKnife.bind(this, v);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.IconStepStatus, 0, 0);
        try {
            statusLevel = a.getInteger(R.styleable.IconStepStatus_statusLevel, 1);
            showLastIconOnly = a.getBoolean(R.styleable.IconStepStatus_showLastIconOnly, false);
            iconDrawable = a.getDrawable(R.styleable.IconStepStatus_iconDrawable);
            if (iconDrawable == null) {
                iconDrawable = getResources().getDrawable(R.drawable.bg_circle_icon_primary);
            }
            if (iconDrawableCancel == null) {
                iconDrawableCancel = getResources().getDrawable(R.drawable.bg_circle_icon_cancel);
            }
            iconPadding = a.getDimension(R.styleable.IconStepStatus_iconPadding, getResources().getDimension(R.dimen.size_3));
            iconSize = a.getDimension(R.styleable.IconStepStatus_iconSize, getResources().getDimension(R.dimen.size_30));
            applyStyle();
        } finally {
            a.recycle();
        }
    }

    public void setIconSize(float iconSize) {
        this.iconSize = iconSize;
        applyStyle();
    }

    public void setIconPadding(float iconPadding) {
        this.iconPadding = iconPadding;
        applyStyle();
    }

    public void setIconDrawable(Drawable iconDrawable) {
        this.iconDrawable = iconDrawable;
        applyStyle();
    }

    public void setStatusLevel(Integer statusLevel) {
        this.statusLevel = statusLevel;
        applyStyle();
    }

    public void setShowLastIconOnly(Boolean showLastIconOnly) {
        this.showLastIconOnly = showLastIconOnly;
        applyStyle();
    }

    public void applyStyle() {
        ivIconCancel.setVisibility(GONE); //ALWAYS GONE

        ivIconHourglass.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconPayment.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconShipping.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconPacking.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconBorrow.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconReturn.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconFinish.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconCashout.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);
        ivIconCancel.setPadding((int) iconPadding, (int) iconPadding, (int) iconPadding, (int) iconPadding);

        ivIconHourglass.getLayoutParams().height = (int) iconSize;
        ivIconHourglass.getLayoutParams().width = (int) iconSize;

        ivIconPayment.getLayoutParams().height = (int) iconSize;
        ivIconPayment.getLayoutParams().width = (int) iconSize;

        ivIconShipping.getLayoutParams().height = (int) iconSize;
        ivIconShipping.getLayoutParams().width = (int) iconSize;

        ivIconPacking.getLayoutParams().height = (int) iconSize;
        ivIconPacking.getLayoutParams().width = (int) iconSize;

        ivIconBorrow.getLayoutParams().height = (int) iconSize;
        ivIconBorrow.getLayoutParams().width = (int) iconSize;

        ivIconReturn.getLayoutParams().height = (int) iconSize;
        ivIconReturn.getLayoutParams().width = (int) iconSize;

        ivIconFinish.getLayoutParams().height = (int) iconSize;
        ivIconFinish.getLayoutParams().width = (int) iconSize;

        ivIconCashout.getLayoutParams().height = (int) iconSize;
        ivIconCashout.getLayoutParams().width = (int) iconSize;

        ivIconCancel.getLayoutParams().height = (int) iconSize;
        ivIconCancel.getLayoutParams().width = (int) iconSize;

        if (statusLevel >= MENUNGGU_PEMBAYARAN) {
            ivIconHourglass.setBackground(iconDrawable);
        }
        if (statusLevel >= MENUNGGU_KONFIRMASI) {
            ivIconPayment.setBackground(iconDrawable);
        }
        if (statusLevel >= PESANAN_SEDANG_DIPROSES) {
            ivIconPacking.setBackground(iconDrawable);
        }
        if (statusLevel >= PESANAN_SEDANG_DIKIRIM) {
            ivIconShipping.setBackground(iconDrawable);
        }
        if (statusLevel >= PRODUK_SEDANG_DIPINJAM) {
            ivIconBorrow.setBackground(iconDrawable);
        }
        if (statusLevel >= PRODUK_SUDAH_DIKEMBALIKAN) {
            ivIconReturn.setBackground(iconDrawable);
        }
        if (statusLevel >= TRANSAKSI_SELESAI) {
            ivIconFinish.setBackground(iconDrawable);
        }
        if (statusLevel >= TRANSAKSI_CASHOUT) {
            ivIconCashout.setBackground(iconDrawable);
        }
        if ((statusLevel >= TRANSAKSI_DIBATALKAN) || (statusLevel >= PESANAN_DITOLAK_VENDOR)) {
            ivIconCancel.setBackground(iconDrawableCancel);
        }
        if ((showLastIconOnly) || (statusLevel == TRANSAKSI_DIBATALKAN) || (statusLevel == PESANAN_DITOLAK_VENDOR)) {
            ivIconHourglass.setVisibility(GONE);
            ivIconPayment.setVisibility(GONE);
            ivIconShipping.setVisibility(GONE);
            ivIconPacking.setVisibility(GONE);
            ivIconBorrow.setVisibility(GONE);
            ivIconReturn.setVisibility(GONE);
            ivIconFinish.setVisibility(GONE);
            ivIconCashout.setVisibility(GONE);

            if ((statusLevel == TRANSAKSI_DIBATALKAN) || (statusLevel == PESANAN_DITOLAK_VENDOR)) {
                ivIconCancel.setVisibility(VISIBLE);
            } else {
                if (statusLevel == MENUNGGU_PEMBAYARAN) {
                    ivIconHourglass.setVisibility(VISIBLE);
                }
                if (statusLevel == MENUNGGU_KONFIRMASI) {
                    ivIconPayment.setVisibility(VISIBLE);
                }
                if (statusLevel == PESANAN_SEDANG_DIPROSES) {
                    ivIconPacking.setVisibility(VISIBLE);
                }
                if (statusLevel == PESANAN_SEDANG_DIKIRIM) {
                    ivIconShipping.setVisibility(VISIBLE);
                }
                if (statusLevel == PRODUK_SEDANG_DIPINJAM) {
                    ivIconBorrow.setVisibility(VISIBLE);
                }
                if (statusLevel == PRODUK_SUDAH_DIKEMBALIKAN) {
                    ivIconReturn.setVisibility(VISIBLE);
                }
                if (statusLevel == TRANSAKSI_SELESAI) {
                    ivIconFinish.setVisibility(VISIBLE);
                }
                if (statusLevel == TRANSAKSI_CASHOUT) {
                    ivIconCashout.setVisibility(VISIBLE);
                }
            }
        }
    }
}