package com.taksewa.taksewa.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.taksewa.taksewa.model.BarangHargaSewa;

import java.text.DecimalFormat;
import java.util.StringTokenizer;

public class NumberTextWatcher implements TextWatcher {
    private final String TAG = getClass().getSimpleName();
    private EditText editText;
//    private String defaultValue;
    private int ifZeroSelection;
    private int lastSelectionStart;
    private int lastSelectionEnd;
    private static int lastSelectionCalc;
    private static int totalCommas;
    private static int oldTotalCommas;
    private static String currentChar;
    private double tempValue;
    private static DecimalFormat decimalFormat = new DecimalFormat("0.#");


    public interface CallbackWatcher {
        void onAfterTextFinish(String textAfter);
    }

    public CallbackWatcher callbackWatcher;

    public NumberTextWatcher(EditText editText) {
        this.editText = editText;
        this.editText.setText("0");
    }

    public NumberTextWatcher(EditText editText, CallbackWatcher callbackWatcher) {
        this.editText = editText;
        this.callbackWatcher = callbackWatcher;
        this.editText.setText("0");
    }
//
//    public NumberTextWatcher(EditText editText, CallbackWatcher callbackWatcher, String defaultValue) {
//        this.editText = editText;
//        this.callbackWatcher = callbackWatcher;
//        this.defaultValue = defaultValue;
//        this.editText.setText("0");
//    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Log.d(TAG, "beforeTextChanged = char : " + s + ", start : " + start + ", count : " + count + ", after : " + after);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d(TAG, "onTextChanged = char : " + s + ", start : " + start + ", count : " + count + ", before : " + before);

    }

    @Override
    public void afterTextChanged(Editable s) {
        Log.d(TAG, "afterTextChanged = Editable : " + s);
        editText.removeTextChangedListener(this);
        try {
            String value = editText.getText().toString();
            lastSelectionStart = editText.getSelectionStart();
            lastSelectionEnd = editText.getSelectionEnd();
            if (value.length() < 1) {
                editText.setText("0");
                editText.setSelection(1);
            }
            if (!value.equals("")) {
                if (value.startsWith(".")) {
                    editText.setText("0.");
                }
                tempValue = Double.valueOf(editText.getText().toString().replaceAll(",", ""));
                Log.d(TAG, "decimalValue : " + String.valueOf(decimalFormat.format(tempValue)));
                value = String.valueOf(decimalFormat.format(tempValue));
                if (value.startsWith("0") && !value.startsWith("0.")) {
                    editText.setText("0");
                } else {

                }
                String str = editText.getText().toString();
                if (!value.equals(""))
                    editText.setText(getDecimalFormattedString(str));
                lastSelectionCalc = lastSelectionStart + totalCommas - oldTotalCommas;
                Log.d(TAG, "totalLetter : " + String.valueOf(str.length()));
                Log.d(TAG, "oldTotalCommas : " + String.valueOf(oldTotalCommas));
                Log.d(TAG, "totalCommas : " + String.valueOf(totalCommas));
                Log.d(TAG, "lastSelect : " + String.valueOf(lastSelectionEnd));
                Log.d(TAG, "calculateSelect : " + String.valueOf(lastSelectionCalc));
//                editText.getText().charAt(lastSelectionStart);
                editText.setSelection(lastSelectionCalc);

            }
//            editText.addTextChangedListener(this);
        } catch (Exception ex) {
            ex.printStackTrace();
//            editText.addTextChangedListener(this);
        }
        if (editText.getSelectionEnd() < 1) {
            editText.setSelection(1);
        }
        if (callbackWatcher != null) {
            callbackWatcher.onAfterTextFinish(editText.getText().toString());
        }
        editText.addTextChangedListener(this);

    }

    public static String getDecimalFormattedString(String value) {
        totalCommas = 0;
        oldTotalCommas = 0;
        String str1 = value;
        for (int i = 0; i < str1.length(); i++) {
            currentChar = String.valueOf(str1.charAt(i));
            if (currentChar.contains(",")) {
                oldTotalCommas++;
            }
        }
        str1 = String.valueOf(decimalFormat.format(Double.valueOf(str1.replaceAll(",", ""))));
        StringTokenizer lst = new StringTokenizer(value, ".");
        String str2 = "";
        if (lst.countTokens() > 1) {
            str1 = lst.nextToken();
            str2 = lst.nextToken();
        }
        String str3 = "";
        int i = 0;
        int j = -1 + str1.length();
        if (str1.charAt(-1 + str1.length()) == '.') {
            j--;
            str3 = ".";
        }
        for (int k = j; ; k--) {
            if (k < 0) {
                if (str2.length() > 0)
                    str3 = str3 + "." + str2;
                return str3;
            }
            if (i == 3) {
                str3 = "," + str3;
                i = 0;
                totalCommas++;
            }
            str3 = str1.charAt(k) + str3;
            i++;
        }
    }

    public static String trimCommaOfString(String string) {
        if (string.contains(",")) {
            return string.replace(",", "");
        } else {
            return string;
        }
    }

}