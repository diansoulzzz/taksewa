package com.taksewa.taksewa.utils;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.taksewa.taksewa.R;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LayoutPanel extends FrameLayout {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;
    @BindView(R.id.ivHeaderRight)
    ImageView ivHeaderRight;
    @BindView(R.id.clHeader)
    ConstraintLayout clHeader;

    private ObjectAnimator anim;
    private int rotationAngle = 0;
    private int animDuration = 300;
    private float panelPadding;
    private float panelHeight;
    private CharSequence title;
    private Integer titleColor;

    public LayoutPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LayoutPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        View v = LayoutInflater.from(context).inflate(R.layout.view_layout_panel, this, true);
        ButterKnife.bind(this, v);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LayoutPanel, 0, 0);
        try {
            titleColor = a.getColor(R.styleable.LayoutPanel_titleColor, Color.BLACK);
            title = a.getString(R.styleable.LayoutPanel_title);
            animDuration = a.getInteger(R.styleable.LayoutPanel_animDuration, 300);
            panelHeight = a.getDimension(R.styleable.LayoutPanel_panelHeight, 60);
            panelPadding = a.getDimension(R.styleable.LayoutPanel_panelPadding, 10);
            applyStyle();
        } finally {
            a.recycle();
        }
    }

    public void applyStyle() {
        clHeader.setPadding((int) panelPadding, (int) panelPadding, (int) panelPadding, (int) panelPadding);
        clHeader.setMinHeight((int) panelHeight);
        tvHeaderTitle.setText(title);
        tvHeaderTitle.setTextColor(titleColor);
    }

    public void setPanelPadding(float panelPadding) {
        this.panelPadding = panelPadding;
//        clHeader.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
    }

    public void setPanelHeight(float panelHeight) {
        this.panelHeight = panelHeight;
//        clHeader.setMinHeight((int) height);
    }

    public void setTitle(CharSequence title) {
        this.title = title;
//        tvHeaderTitle.setText(title);
    }

    public void setTitleColor(Integer titleColor) {
        this.titleColor = titleColor;
//        tvHeaderTitle.setTextColor(titleColor);
    }

    public void setAnimDuration(int animDuration) {
        this.animDuration = animDuration;
    }

    public void setAnim(ObjectAnimator anim) {
        this.anim = anim;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    public void expand() {
        anim = ObjectAnimator.ofFloat(ivHeaderRight, "rotation", rotationAngle, rotationAngle - 180);
        anim.setDuration(animDuration);
        anim.start();
        rotationAngle -= 180;
        rotationAngle = rotationAngle % 360;
    }

    public void collapse() {
        anim = ObjectAnimator.ofFloat(ivHeaderRight, "rotation", rotationAngle, rotationAngle + 180);
        anim.setDuration(animDuration);
        anim.start();
        rotationAngle += 180;
        rotationAngle = rotationAngle % 360;
    }
}  